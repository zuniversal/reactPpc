


import ArtLine from '@@/ArtLine'
import Procedure from '@@/Procedure'
import ProductLine from '@@/ProductLine'
import StanderdDay from '@@/StanderdDay'
import Indexs from '@@/Indexs'
import PnNo from '@@/PnNo'
import ScheduleNo from '@@/ScheduleNo'
import Trees from '@@/Trees'
// import Nos from '@@/Nos'
// import PnNos from '@@/PnNos'
import CapcityScatter from '@@/CapcityScatter'
import PoArrange from '@@/PoArrange'
import ManualNum from '@@/ManualNum'
import ManualPn from '@@/ManualPn'
import ProductionSchedue from '@@/ProductionSchedue'

import {getMonth, monthFilter, } from 'util'


import moment from 'moment'

export const daySuffix = '01T12:33:36+08:00'

export const defaultNow = moment().format(DATE_FORMAT_BAR)
// export const oneMonth = moment(moment().add(1, 'months')).subtract(1, 'days').format(DATE_FORMAT_BAR)
export const oneMonth = moment(moment().add(1, 'months')).format(DATE_FORMAT_BAR)
// console.log('defaultNow,  ：', defaultNow, defaultNow.split('-')[1], oneMonth, moment(['2018', '02', '11']), moment());
export const filterNow = [defaultNow.split('-')[0], defaultNow.split('-')[1], daySuffix].join('-')
// export const filterNow = '2018-10-01T12:33:36+08:00'

// export const filterNow = [defaultNow.split('-')[0], `0${defaultNow.split('-')[1] - 1}`, daySuffix].join('-')
// export const delayOneMonth =  [oneMonth.split('-')[0], oneMonth.split('-')[1], daySuffix].join('-')
// console.log('filterNow,  ：', filterNow, );// // 
// const newDate = [defaultNow.split('-')[0], defaultNow.split('-')[1], `01${defaultNow.split('-')[2].substring(2, defaultNow.split('-')[2].length)}`].join('-')
// console.log('newDate ：', newDate);
// console.log('defaultNow ：',  defaultNow, delayOneMonth, delayOneMonth.split('-'), delayOneMonth.split('-')[2].substring(2, delayOneMonth.split('-')[2].length));
export const delayTwoMonth = moment(moment().add(1, 'months')).format(DATE_FORMAT_BAR)
// export const delayTwoMonth = '2018-12-01T12:33:36+08:00'
export const delayThreeMonth = moment(moment().add(2, 'months')).format(DATE_FORMAT_BAR)
// export const delayThreeMonth = '2018-12-01T12:33:36+08:00'
export const delayFourMonth = moment(moment().add(4, 'months')).format(DATE_FORMAT_BAR)



export const routeArr = [
  { path: 'procedure', components: Procedure, name: 'base', middle:"page/procedure/", seeAuth: [2,  ], actionAuth: [3,  ], }, 
  { path: 'productLine', components: ProductLine, name: 'base', middle:"page/productLine/", seeAuth: [2,  ], actionAuth: [3,  ], }, 
  { path: 'standerdDay', components: StanderdDay, name: 'base', middle:"page/standerdDay/", seeAuth: [2,  ], actionAuth: [3,  ], }, 
  { path: 'PoArrange', components: PoArrange, name: 'base', middle:"page/poArrange/", seeAuth: [16,  ], actionAuth: [17,  ], }, 
  { path: 'capcityScatter', components: CapcityScatter, name: 'report', middle:"page/capcityScatter/", seeAuth: [15,  ], actionAuth: [3,  ], }, 
  
  { path: 'artLine', components: ArtLine, name: 'schedue', middle:"page/artLine/", seeAuth: [4,  ], actionAuth: [5,  ], }, 
  { path: 'indexs', components: Indexs, name: 'schedue', middle:"page/indexs/", seeAuth: [2,  ], actionAuth: [18,  ], }, 
  { path: 'schedue', components: ScheduleNo, name: 'schedue', middle:"page/schedue/", seeAuth: [6, ], actionAuth: {1: 7, 3: 8, 6: 9,  },  }, 
  { path: 'pnNo', components: PnNo, name: 'schedue', middle:"page/pnNo/", seeAuth: [14, ], actionAuth: [], }, 
  // { path: 'pnNos', components: PnNos, name: 'schedue', middle:"page/pnNos/", seeAuth: [2,  ], actionAuth: [3,  ], }, 
  // { path: 'Nos', components: Nos, name: 'schedue', middle:"page/Nos/", seeAuth: [6, 7, 8, 9, ], }, 
  { path: 'Trees', components: Trees, name: 'schedue', middle:"page/Trees/", seeAuth: [2,  ], actionAuth: [3,  ], }, 
  { path: 'manualNum', components: ManualNum, name: 'schedue', middle:"page/manualNum/", seeAuth: [10,  ], actionAuth: [11,  ], }, 
  // seeAuth没有的默认1
  { path: 'manualPn', components: ManualPn, name: 'schedue', middle:"page/manualPn/", seeAuth: [1,  ], actionAuth: [], }, 
  { path: 'productionSchedue', components: ProductionSchedue, name: 'productionSchedue', middle:"page/productionSchedue/", seeAuth: [2,  ], actionAuth: [3,  ], }, 
]


export const rules = [{ required: true, message: '字段不能爲空!' }]

export const grid = 30

export const items = [
  {"transform":"translate(-33.5%,-34%) scale(0.32)"},
  {"transform":"translate(-33.5%,0%) scale(0.32)"},
  {"transform":"translate(-33.5%,34%) scale(0.32)"},
  {"transform":"translate(33%, 0) scale(1)"},
]
export const DATE_FORMAT = 'YYYY/MM/DD';
export const DATE_FORMAT_BAR = 'YYYY-MM-DD';
export const MONTH_FORMAT = 'YYYY-MM';
export const MONTH_FORMAT_TXT = 'YYYY年MM月批';
// export const MONTH_FORMAT = 'YYYY年MM月';
export const FORMAT_DAY = 'DD';
export const FORMAT_MONTH_ONE = 'M';
export const DATE_FORMAT_BARS = 'YYYYMMDD';
export const DATE_FORMAT_BARSG = 'YYYYMM-DD';

export const bacodeInfo = [
  { label: '批號:', k: 'pn_no' },
  { label: '款號:', k: 'style_no' },
  { label: '訂單號:', k: 'sc_no' },
  { label: '營業針種:', k: 'sc_gauge' },
  { label: '電機針種:', k: 'mc_gauge' },
  { label: '縫盤針種:', k: 'sew_gauge' },
  { label: '產地公司:', k: 'company_name' },
  { label: '貨期:', k: 'shipment_date' },
  { label: '客戶:', k: 'cust_name' },
  { label: '數量:', k: 'p_qty' },
  { label: '款式:', k: 'cloth_style' },
  { label: '毛期:', k: 'yarn_date' },
  { label: '輔料期:', k: 'acc_date' },
  
]

export const bacodeInfoWithDate = [
  { label: '批號:', k: 'pn_no' },
  { label: '款號:', k: 'style_no' },
  { label: '營業針種:', k: 'sc_gauge' },
  { label: '縫盤針種:', k: 'sew_gauge' },
  { label: '客戶:', k: 'cust_name' },
  { label: '款式:', k: 'cloth_style' },
  { label: '織機開始日期:', k: [ 'mc_s_date', 'mc_e_date', ] },
  { label: '縫盤開始日期:', k: [ 'sew_s_date', 'sew_e_date', ] },
  { label: '後整開始日期:', k: [ 'lastwork_s_date', 'lastwork_e_date', ] },
  // { label: '織機結束日期:', k: 'mc_e_date' },
  // { label: '縫盤結束日期:', k: 'sew_s_date' },
  // { label: '後整結束日期:', k: 'lastwork_e_date' },
  { label: '生産完成期:', k: 'shipment_date' },
]


export const bacodeCopyInfo = [ 'pn_no', 'style_no', 'sc_no', 'sc_gauge', 'mc_gauge', 'cust_name', 'cloth_style', ]

export const produceForm = [
  { label: "工序編碼", key: "work_no", formType: 'Input', rules, },
  { label: "工序代號", key: "work_code", formType: 'Input', rules, },
  { label: "工序名稱", key: "work_name", formType: 'Input', rules, },
]
export const airlineForm = [
  { label: "工序", key: "work_no", formType: 'Input', rules, },
  { label: "SAM(秒)", key: "work_sam", formType: 'Input', rules, },
  { label: "日産(人/台)", key: "day_qty", formType: 'Input', rules, },
  { label: "標准天數", key: "work_days", formType: 'Input', rules, },
  { label: "總計人/台數", key: "t_workers", formType: 'Input', rules, },
  { label: "偏移天数", key: "dev_days", formType: 'Input', rules, },
  { label: "標准日産", key: "t_day_qty", formType: 'Input', rules, },
  { label: "每天需人數(台數)", key: "workers", formType: 'Input', rules, },
  { label: "人數(尾期)", key: "last_workers", formType: 'Input', rules, },
  { label: "件數(尾期)", key: "last_qty", formType: 'Input', rules, },
]
export const eventTitle = [
  { title: "産線産能情況", class_id: 1,  },
  { title: "自動排單先決條件", class_id: 2,  },
  { title: "計劃與進度提醒", class_id: 3,  },
  { title: "物料與關鍵事件提醒", class_id: 4,  },
]
export const dragPage = ['/page/pnNo', '/page/schedue', '/page/Nos', '/page/pnNos', ]

// 生产线
export const processDisableArr = [
  'work_no', 
  'gauge', 
]
export const disableFactory = { label: "禁用産線", key: "void", formType: 'Checkbox', rules, }
export const productLineForm = [
  { label: "工厂代號", key: "factory_code", formType: 'Input', rules, },
  { label: "工厂名稱", key: "factory_name", formType: 'Input', rules, },
  // { label: "部門", key: "dep_no", formType: 'Input', rules, },
  // { label: "科別", key: "fac_no", formType: 'Input', rules, },
  { label: "部門", key: "dep_no", formType: 'Select', datakey: 'depData', optionkey: 'dep_no', optionTxt: 'dep_name', plSpecial: true, optMore: true, rules, },
  { label: "科別", key: "fac_no", formType: 'Select', datakey: 'facData', optionkey: 'fac_no', optionTxt: 'fac_name', plSpecial: true, optMore: true, rules, },
  // { label: "工序名稱", key: "work_no", formType: 'Select', datakey: 'procedureData', selectCb: 'selectCb', optionkey: 'work_no', optionTxt: 'work_name', rules, },
  // { label: "針種", key: "gauge", formType: 'Select', datakey: 'gaugeData', selectCb: 'selectCb', optionkey: 'gauge_no', optionTxt: 'gauge_txt', selectDisable: true, disableKey: 'work_no', disableFuc: 'gaugeFilter', rules, },
  { label: "工序名稱", key: "work_no", formType: 'Select', datakey: 'procedureData', optionkey: 'work_no', optionTxt: 'work_name', rules, },
  { label: "針種", key: "gauge", formType: 'Select', datakey: 'gaugeData', optionkey: 'gauge_no', optionTxt: 'gauge_txt', selectDisable: true, disableKey: 'work_no', disableFuc: 'gaugeFilter', rules, },
  // { label: "效率", key: "prod_rate", formType: 'Input', rules, },
  { label: "效率", key: "prod_rate", formType: 'InputNum', rules, },
]

export const onSelfCompany = [
  { label: "可做工序", key: "work_no", formType: 'Select', datakey: 'procedureData', selectCb: 'selectCb', optionkey: 'work_no', optionTxt: 'work_name', optionDisable: true, disableKey: 'work_no', disableFuc: 'workFilter', rules, },
]


export const productLineOutForm = [
  { label: "工厂代號", key: "factory_code", formType: 'Input', inputCb: 'inputCb', rules, },
  { label: "工厂名稱", key: "factory_name", formType: 'Input', rules, },
  { label: "工序名稱", key: "work_no", formType: 'Select', datakey: 'procedureData',
  //  selectCb: 'selectCb', 
   optionkey: 'work_no', optionTxt: 'work_name', rules, },
  { label: "針種", key: "gauge", formType: 'Select', datakey: 'gaugeData',
  //  selectCb: 'selectCb', 
   optionkey: 'gauge_no', optionTxt: 'gauge_txt', selectDisable: true, disableKey: 'work_no', disableFuc: 'gaugeFilter', rules, },
  { label: "效率", key: "prod_rate", formType: 'InputNum', rules, },
]


// export const processForm = [
//   { label: "工序名稱", key: "work_no", formType: 'Select', datakey: 'procedureData', selectCb: 'selectCb', optionkey: 'work_no', optionTxt: 'work_name', rules, },
//   // { label: "針種", key: "gauge", formType: 'Select', datakey: 'customerData', selectCb: 'selectCb', optionkey: 'cust_name', optionTxt: 'cust_name', rules, },
//   { label: "針種", key: "gauge", formType: 'Select', datakey: 'gaugeData', selectCb: 'selectCb', optionkey: 'gauge_no', optionTxt: 'gauge_txt', selectDisable: true, disableKey: 'work_no', disableFuc: 'gaugeFilter', rules, },
//   // { label: "針種", key: "gauge", formType: 'Select', datakey: 'gaugeData', selectCb: 'selectCb', optionkey: 'gauge_no', optionTxt: 'gauge_txt', disableKey: 'work_no', rules, },
//   { label: "機台數/人數", key: "workers", formType: 'Input', rules, },
//   { label: "工時", key: "pts", formType: 'Input', rules, haveInit: true, initVal: 8, },
//   { label: "開始日期", key: "s_date", formType: 'SingleDate', haveInit: true, initVal: defaultNow, rules, },
// ]
export const processForm = [
  { label: "工序名稱", key: "work_no", formType: 'Select', datakey: 'procedureData', optionkey: 'work_no', optionTxt: 'work_name', rules, },
  // { label: "針種", key: "gauge", formType: 'Select', datakey: 'customerData', optionkey: 'cust_name', optionTxt: 'cust_name', rules, },
  { label: "針種", key: "gauge", formType: 'Select', datakey: 'gaugeData', optionkey: 'gauge_no', optionTxt: 'gauge_txt', selectDisable: true, disableKey: 'work_no', disableFuc: 'gaugeFilter', rules, },
  // { label: "針種", key: "gauge", formType: 'Select', datakey: 'gaugeData', optionkey: 'gauge_no', optionTxt: 'gauge_txt', disableKey: 'work_no', rules, },
  { label: "機台數/人數", key: "workers", formType: 'Input', rules, },
  { label: "工時", key: "pts", formType: 'Input', rules, haveInit: true, initVal: 8, },
  { label: "開始日期", key: "s_date", formType: 'SingleDate', haveInit: true, initVal: defaultNow, rules, },
]



export const filterOptionForm = [
  // // { label: "生産完成期", key: "shipment_datefr", formType: 'MonthDate', haveInit: true, initVal: filterNow, format: MONTH_FORMAT_TXT,  },
  // // { label: "", key: "shipment_dateto", formType: 'MonthDate', haveInit: true, initVal: delayOneMonth, format: MONTH_FORMAT_TXT, disabled: true, isRecive: true, noLabel: true, hide: true, },
  // { label: "生産完成期", key: "shipment_datefr", formType: 'SingleDate', haveInit: true, initVal: filterNow, },
  // // { label: "批", key: "shipment_dateto", formType: 'SingleDate', haveInit: true, initVal: delayOneMonth, noLabel: true, },
  // // { label: "批", key: "shipment_dateto", formType: 'SingleDate', haveInit: true, initVal: delayThreeMonth, 
  // // { label: "批", key: "shipment_dateto", formType: 'SingleDate', haveInit: true, initVal: delayThreeMonth, 
  // { label: "批", key: "shipment_dateto", formType: 'SingleDate', haveInit: true, initVal: delayTwoMonth, 
  //   // disabled: true, isRecive: true, noLabel: true, 
  // },
  { label: "客户", key: "customer", formType: 'Select', datakey: 'customerData', optionkey: 'cust_name', optionTxt: 'cust_name', },
  { label: "款號", key: "style_no", formType: 'Select', datakey: 'styleNoData', optionkey: 'style_no', optionTxt: 'style_no', },
  { label: "批號", key: "pn_no", formType: 'Input', },
]

// export const filterNows = moment().format(DATE_FORMAT_BARS)
// export const delayTwoMonths = moment(moment().add(2, 'months')).format(DATE_FORMAT_BARS)
export const filterNows = moment().format(DATE_FORMAT_BARSG)
export const delayTwoMonths = moment(moment().add(2, 'months')).format(DATE_FORMAT_BARSG)



export const initParams = {
  // "shipment_dateto": oneMonth,
  // "shipment_dateto": delayThreeMonth,
  "shipment_dateto": delayTwoMonth,
  "shipment_datefr": filterNow,
  // "shipment_dateto": delayTwoMonths,
  // "shipment_datefr": filterNows,
  // "shipment_datefr": '2018-09-01',
  // "shipment_dateto": '2018-12-01', 
  "pn_no": "",
  "style_no": "",
  "customer": '',
  "p_datefr": '',
  "p_dateto": '',
  isSearch: false,
}

// 图标
export const legend = ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月', ]
export const monthArr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', ]
// 产能
export const capcityConfig = {
  title: '',
  data: [
    { legendTxt: "需要産能", seriesData: [], },
    { legendTxt: "基准産能", seriesData: [], },
  ],
  legend: [],
}

export const workNoArr = [1, 3, 6]

export const unitArr = { 
  1: ['W', 'P'], 
  3: ['P'],
  6: ['P'], 
}
export const unitFilters = [
  {text: '机台/人', value: 'W',},
  {text: '件', value: 'P',}
]

// 标准天数
export const standardFilterTxt = {W: '机台/人', P: '件'}

export const standerDayObj = { 
  type_id: "A",
  unit: 'null',
  work_days: 1,
  work_name: "",
  work_no: 0,
}

export const tagColorArr = [
  // {work_no: 1, color: '#f50',},
  // {work_no: 3, color: 'cyan',},
  {work_no: 1, color: 'cyan',},
  {work_no: 3, color: 'purple',},
  {work_no: 6, color: 'red',},
]

export const workConfig = [
  {work_no: 1, work_name: "織機", color: 'cyan'},
  {work_no: 3, work_name: '縫挑', color: 'purple'},
  {work_no: 6, work_name: '后整', color: 'red'},
  
]

export const gaugeColor = {
  // '*': 'pink', 
  C: 'red',
  F: 'blue',
}

export const workOption = {key: 'work_no', txt: 'work_name',}

export const workAll = {work_no: 0, work_name: '全部',}

export const factoryTypeConfig = [
  {k: 'I', t: "內廠",},
  {k: 'O', t: '外廠',},
  {k: 'A', t: '全部',}
]
export const factoryTypeOption = {key: 'k', txt: 't',}

// export const tagColorArr = [
//   '#f50', 'cyan', 'red', '#108ee9', '#13CE66',  '#7265e6', 'rgb(255, 213, 114)', 
// ]

// export const poInfoConfig = {color: '顔色', size: '尺碼', }

export const poInfoConfig = [
  {unq: 'color', title: '顔色', rowKey: 'combo_id', seq: 'combo_seq', color: '#f50', 
    tag: [ { k: 'combo_id', link: '--', }, { k: 'size_no', link: '--', }, { k: 'qty', link: ';', }], 
    process: ['pink', '#108ee9', 'red', ]
  },
  {unq: 'size', title: '尺碼', rowKey: 'size_no', seq: 'size_seq', color: '#108ee9', 
    tag: [ { k: 'work_name', link: '--', }, { k: 'day_qty', link: ';', }], 
    process: ['#f50', 'purple', ]
  },
  {unq: 'colorDetail', title: '色碼表', },
  {unq: 'processDetail', title: '工序表', },
  {unq: 'O', color: '#ccc', },
  {unq: 'N', color: '#13CE66', },
  {unq: 'U', color: 'purple', },
]

export const poTableConfig = [
  // {unq: 'colorSize', tagItem: ['pink', '#108ee9', 'red', ], },
  {unq: 'colorSize', tagItem: ['#f50', 'cyan',  ], },
  {unq: 'process', tagItem: ['#f50', 'purple', ],  },
]


export const poInfoForm = [
  { label: "批號", key: "pn_no", formType: 'Input', rules, },
  { label: "針種", key: "gauge_no", formType: 'Select', datakey: 'gaugeData', optionkey: 'gauge_no', optionTxt: 'gauge_no', },
  { label: "縫盤針種", key: "sew_gauge", formType: 'Select', datakey: 'sewData', optionkey: 'sew_no', optionTxt: 'sew', },
  { label: "毛期", key: "yarn_date", formType: 'SingleDate', haveInit: true, initVal: defaultNow, rules, },
  { label: "輔料期", key: "acs_date", formType: 'SingleDate', haveInit: true, initVal: defaultNow, rules, },
  { label: "預計開機期", key: "yj_mc_date", formType: 'SingleDate', haveInit: true, initVal: defaultNow, rules, },
  { label: "生産完成期", key: "prod_date", formType: 'SingleDate', haveInit: true, initVal: defaultNow, rules, },
  { label: "客戶名", key: "cust_name", formType: 'Input', rules, },
]
export const sewKey = ['C', 'F',]
export const sewData = [
  { sew: '粗針', sew_no: 'C', },
  { sew: '幼針', sew_no: 'F', },
]

// export const width90 = ['colorDetail', 'processDetail', ]
export const width90 = ['colorDetail', ]


const commonConfig = {
  enableRowGroup: true, enablePivot: true, editable: true, 
}
export const event34 = [
  { headerName: '後整開始期', field: 's_date', ...commonConfig, width: 100, },
  { headerName: '洗水嘜送貨期', field: 'yarn_date', ...commonConfig, width: 100, },
]

export const event12 = [
  { headerName: '開機日期', field: 's_date', ...commonConfig, width: 100,},
  { headerName: '預計毛期', field: 'yarn_date', ...commonConfig, width: 100, },
]

export const event45 = [
  { headerName: '開機日期', field: 's_date', ...commonConfig, width: 100,},
]

export const event5 = [
  { headerName: '客批辦', field: 'kpb_status', ...commonConfig, width: 90,},
]


export const event412 = [
  { headerName: '支數成分', field: 'pn_yarn', ...commonConfig, width: 900,},
]

export const event43 = [
  { headerName: '輔料狀態', field: 'acs_type', ...commonConfig, width: 80,},
]

export const event32 = [
  { headerName: '電機清期', field: 'mc_e_date', ...commonConfig, width: 150, isDanger: 'mc_e_date',},
  { headerName: '縫挑清期', field: 'sew_e_date', ...commonConfig, width: 150, isDanger: 'sew_e_date',},
  { headerName: '後整清期', field: 'lastwork_e_date', ...commonConfig, width: 150, isDanger: 'lastwork_e_date',},
]
export const event35 = [
  { headerName: '織機', field: 'mc_e_date', ...commonConfig, width: 150, isLock: 'mc_e_date',},
  { headerName: '縫挑', field: 'sew_e_date', ...commonConfig, width: 150, isLock: 'sew_e_date',},
  { headerName: '後整', field: 'lastwork_e_date', ...commonConfig, width: 150, isLock: 'lastwork_e_date',},
]
export const class34 = [
  { headerName: '款號', field: 'style_no', ...commonConfig, width: 200, },
  { headerName: '客戶名', field: 'customer', ...commonConfig, width: 200, },
  { headerName: '生産完成期', field: 'prod_date', ...commonConfig, width: 200,},
  // { headerName: '生産狀態', field: 'prod_status', ...commonConfig, width: 200, },
  // { headerName: '預計毛期', field: 'old_qty', ...commonConfig, width: 200, },
  { headerName: '數量', field: 'qty', ...commonConfig, width: 200, },
  // { headerName: '數量狀態', field: 'qty_status', ...commonConfig, width: 200, },
  { headerName: '更新日期', field: 'update_date', ...commonConfig, width: 200, },
]

export const class2 = [
  { headerName: '開單日期', field: 'input_date', ...commonConfig, width: 120,},
]
export const event24 = [
  { headerName: '工廠針種', field: 'mc_gauge', ...commonConfig, width: 120,},
]

export const dangerDateConfig = [
  'mc_e_date', 'sew_e_date', 'lastwork_e_date'
]


// export const eventType = [1, 2]
// const eventFilter = (e) => e !== 5 ? eventType.some(v => v === e) ? event12 : event34 : event5
export const eventType = [2]
export const eventType12 = [1, 2]
// const eventFilter = (e) => e !== 2 ? e !== 5 ? eventType.some(v => v === e) ? event12 : event34 : event5 : event5
// const eventFilter = (c, e) => c === 4 && eventType.some(v => v === e) ? eventType.some(v => v === e) ? event12 : event34 : event12
const eventFilter = (c, e) => {
  console.log(' c, e ： ', c, e, c === 4 && eventType12.some(v => v === e), c === 2 ? (c === 4 && eventType12.some(v => v === e) ? event12 : event34) : event5  )
  // if (c === 2) {
  //   console.log(' event5 ： ',  )
  //   return event5 
  // }
  // if (c === 4 && eventType12.some(v => v === e)) {
  //   console.log(' eventType12 ： ',  )
  //   return event12 
  // }
  //   console.log(' event34 ： ',  )
  // return event34 
  console.log(' event45event45event45   11  ： ', c, e,  )
  if (c === 4 && e === 5) {
    console.log(' event45event45event45  22  ： ',  )
    return [...event45, ]
  }
  if ((c === 4 && e === 5) || (c === 3 && e === 1)) {
    return [...event12, ...event5]
  }
  if (c === 4 && e === 3) {
    return [...event34, ...event43, ]
  }
  if (c === 3 && e === 2) {
    // console.log(' event32event32 ： ',  )
    return [...event32, ]
  }
  if (c === 3 && e === 4) {
    console.log(' event32event34event34event32 ： ',  )
    return [...class34, ]
  }
  // if (c === 4 && (e === 1 || e === 2)) {
  //   // console.log(' event32event32 ： ',  )
  //   return [...event12, ...event412, ]
  // }
  if (c === 3 && e === 5) {
    console.log(' event5event5 ： ',  )
    // return event5
    return [...event5, ...event35, ]
  }
  if (c === 2 && e === 4) {
    console.log(' event24 ： ',  )
    return [...event24, ...event5, ...class2, ]
  }
  const res = c === 2 ? [...event5, ...class2, ] :  (c === 4 && eventType12.some(v => v === e) ? event12 : c === 3 ? event12 : event34)
  console.log(' resresres ： ', res,  )// 
  return res
}

export const eventFixColmun = {
  wrapperConfig: {
    headerName: '批號', 
    enableRowGroup: true, enablePivot: true, editable: true, 
  },
  column: [
    { headerName: '批號', field: 'pn_no', width: 200, pinned: true, ...commonConfig, },
  ]
}

export const event46FixColmun = {
  ...eventFixColmun,
  column: [
    { headerName: '報價單號', field: 'pn_no', width: 200, pinned: true, ...commonConfig, },
  ]
}

const column46 = [
  { headerName: '款號', field: 'style_no', ...commonConfig, width: 200, },
  { headerName: '找不到款號', field: 'no_style_no', ...commonConfig, width: 200, },
  { headerName: '款號被取消', field: 'style_void',  ...commonConfig, width: 200, },
  { headerName: '針種', field: 'sc_gauge', ...commonConfig, width: 230, },
  { headerName: '數量', field: 'qty', ...commonConfig, width: 200, },
  { headerName: '款式', field: 'cloth_style', ...commonConfig, width: 200, },
]

export const eventColmun = (c, e, isHideDate, ) => ({
  wrapperConfig: {
    headerName: '基本信息',
    enableRowGroup: true, enablePivot: true, editable: true, 
  },
  column: c === 3 && e === 4 ? class34 : e === 6 ? column46 : [
    { headerName: '訂單號', field: 'sc_no', ...commonConfig, width: 150, },
    { headerName: '款號', field: 'style_no', ...commonConfig, width: 200, },
    { headerName: '營業針種', field: 'sc_gauge', ...commonConfig, width: 150, },
    ...eventFilter(c, e),
    ...c === 4 && (e === 1 || e === 2) ? event412 : [],
    { headerName: '生産完成期', field: 'prod_date', ...commonConfig, width: 150, },
    { headerName: '數量', field: 'qty', ...commonConfig, width: 150, },
    { headerName: '客户名称', field: 'cust_name', ...commonConfig, width: 200, },
    { headerName: '款式', field: 'cloth_style', ...commonConfig, width: 280, },
  ]
})


export const eventConfig = (c, e) => {
  console.log(' eventConfig c, e ： ', c, e,  )
  return [
    e === 6 ? event46FixColmun : eventFixColmun,
    eventColmun(c, e),
  ]
}


export const capcityFixColmun = {
  wrapperConfig: {
    headerName: '針種', 
    enableRowGroup: true, enablePivot: true, editable: true, 
  },
  column: [
    { headerName: '針種', field: 'gauge', width: 150, pinned: true, ...commonConfig, },
    { headerName: '名稱', field: 'title', width: 220, pinned: true, ...commonConfig, },
  ]
}
export const capcityColmun = (e) => ({
  wrapperConfig: {
    headerName: '産能信息 列對應： 基礎産能 / 實際産能',
    enableRowGroup: true, enablePivot: true, editable: true, 
  },
  column: [
    { headerName: `${monthFilter(getMonth())}月`, field: 'basic_capacity1', idx: 1, pinned: true, ...commonConfig, width: 80, },
    { headerName: `${monthFilter(getMonth())}月`, field: 'done_capacity1', idx: 1, pinned: true, ...commonConfig, width: 80, },
    { headerName: `${monthFilter(getMonth()+1)}月`, field: 'basic_capacity2', idx: 2, pinned: true, ...commonConfig, width: 80, },
    { headerName: `${monthFilter(getMonth()+1)}月`, field: 'done_capacity2', idx: 2, pinned: true, ...commonConfig, width: 80, },
    { headerName: `${monthFilter(getMonth()+2)}月`, field: 'basic_capacity3', idx: 3, pinned: true, ...commonConfig, width: 80, },
    { headerName: `${monthFilter(getMonth()+2)}月`, field: 'done_capacity3', idx: 3, pinned: true, ...commonConfig, width: 80, },
    { headerName: `${monthFilter(getMonth()+3)}月`, field: 'basic_capacity3', idx: 3, pinned: true, ...commonConfig, width: 80, },
    { headerName: `${monthFilter(getMonth()+3)}月`, field: 'done_capacity3', idx: 3, pinned: true, ...commonConfig, width: 80, },
    // { headerName: `${getMonth()}月 基准産能`, field: 'basic_capacity1', idx: 1, pinned: true, ...commonConfig, width: 150, },
    // { headerName: `${getMonth()}月 實際産能`, field: 'done_capacity1', idx: 1, pinned: true, ...commonConfig, width: 150, },
    // { headerName: `${getMonth()+1}月 基准産能`, field: 'basic_capacity2', idx: 2, pinned: true, ...commonConfig, width: 150, },
    // { headerName: `${getMonth()+1}月 實際産能`, field: 'done_capacity2', idx: 2, pinned: true, ...commonConfig, width: 150, },
    // { headerName: `${getMonth()+2}月 基准産能`, field: 'basic_capacity3', idx: 3, pinned: true, ...commonConfig, width: 150, },
    // { headerName: `${getMonth()+2}月 實際産能`, field: 'done_capacity3', idx: 3, pinned: true, ...commonConfig, width: 150, },
  ]
})

export const capcityTableConfig = (e) => [
  capcityFixColmun,
  capcityColmun(e),
]




export const productionSchedueColmun = (e) => ({
  wrapperConfig: {
    headerName: '生産進度報表',
    enableRowGroup: true, enablePivot: true, editable: true, 
  },
  column: [
    { headerName: '廠款號', field: 'style_no', width: 200, ...commonConfig, },
    { headerName: '客戶簡稱', field: 'customer_id', width: 160, ...commonConfig, },
    { headerName: '客戶名', field: 'customer_name', width: 160, ...commonConfig, },
    { headerName: '針種', field: 'gauge', width: 140, ...commonConfig, },
    { headerName: '後整完成期', field: 'prod_date', width: 140, ...commonConfig, },
    { headerName: '縫盤針種', field: 'sew_gague', width: 140, ...commonConfig, },
    { headerName: '營業批辦日産', field: 'sample_day_qty', width: 140, ...commonConfig, },
    { headerName: '辦房復辦日產', field: 'factory_day_qty', width: 140, ...commonConfig, },
    { headerName: '工廠日產', field: 'repaly_day_qty', width: 140, ...commonConfig, },
    { headerName: '預計毛期', field: 'yarn_date', width: 140, ...commonConfig, },
    { headerName: '實際毛期', field: 'real_yarn_date', width: 140, ...commonConfig, },
    { headerName: '已批辦', field: 'sample_ok_stauts', width: 140, ...commonConfig, },
    { headerName: '已批辦日期', field: 'sample_ok_date', width: 140, ...commonConfig, },
    { headerName: '輔料日期', field: 'acs_date', width: 140, ...commonConfig, },
    { headerName: '生產單數量', field: 'pn_qty', width: 140, ...commonConfig, },
    { headerName: '電機工廠', field: 'mc_factory_name', width: 200, ...commonConfig, },
    { headerName: '已織數', field: 'mc_real_qty', width: 140, ...commonConfig, },
    { headerName: '已清片', field: 'mc_status', width: 140, ...commonConfig, },
    { headerName: '預計開機期', field: 'yj_mc_s_date', width: 140, ...commonConfig, },
    { headerName: '實際開機期', field: 'real_mc_s_date', width: 140, ...commonConfig, },
    { headerName: '預計清片期', field: 'yj_mc_e_date', width: 140, ...commonConfig, },
    { headerName: '實際清片期', field: 'real_mc_e_date', width: 140, ...commonConfig, },
    { headerName: '縫盤工廠', field: 'sew_factory_name', width: 140, ...commonConfig, },
    { headerName: '交片日期', field: 'send_piece_date', width: 140, ...commonConfig, },
    { headerName: '交片數', field: 'send_piece_qty', width: 140, ...commonConfig, },
    { headerName: '縫盤數', field: 'real_sew_qty', width: 140, ...commonConfig, },
    { headerName: '已清縫', field: 'sew_status', width: 140, ...commonConfig, },
    { headerName: '預計開縫期', field: 'yj_sew_s_date', width: 140, ...commonConfig, },
    { headerName: '實際開縫期', field: 'real_sew_s_date', width: 140, ...commonConfig, },
    { headerName: '預計清縫期', field: 'yj_sew_e_date', width: 140, ...commonConfig, },
    { headerName: '實際清縫期', field: 'real_sew_e_date', width: 140, ...commonConfig, },
    { headerName: '後整工廠', field: 'lastwork_factory_name', width: 140, ...commonConfig, },
    { headerName: '預計後整開始期', field: 'yj_lastwork_s_date', width: 140, ...commonConfig, },
    { headerName: '後整磅片數', field: 'lastwork_weight_qty', width: 140, ...commonConfig, },
    { headerName: '實際後整開始期', field: 'real_lastwork_s_date', width: 140, ...commonConfig, },
    { headerName: '後整已打包數', field: 'lastwork_pack_qty', width: 140, ...commonConfig, },
    { headerName: '預計後整清期', field: 'yj_lastwork_e_date', width: 140, ...commonConfig, },
    { headerName: '實際後整清期', field: 'real_lastwork_e_date', width: 140, ...commonConfig, },
    { headerName: '後整完成數', field: 'lastwork_qty', width: 140, ...commonConfig, },
    { headerName: '餘數', field: 'lastwork_spare_qty', width: 140, ...commonConfig, },
    { headerName: '後整已清', field: 'lastwork_status', width: 140, ...commonConfig, },
    { headerName: '附加工序', field: 'add_workname', width: 200, ...commonConfig, },
    { headerName: '備註', field: 'remark', width: 140, ...commonConfig, },
    // { headerName: 'sample_size', field: 'sample_size', width: 140, ...commonConfig, },
  ]
})


export const productionSchedueConfig = (e) => [
  eventFixColmun,
  productionSchedueColmun(e),
]









export const realExist = ['real_qty', 'open_date', 'finish_date', 'rate']
export const equalAttr = ['factory_id', 's_date', 'e_date']


export const scrollBtnInfo = [
  { dire: 'down', txt: '下' }, 
  { dire: 'up', txt: '上' },  
  { dire: 'left', txt: '左' },  
  { dire: 'right', txt: '右' }, 
]


export const statusInfo = [
  // { k: 'A', t: '齊倉', color: '#13CE66', }, 
  { k: 'A', t: '齊倉', color: 'green', }, 
  { k: 'B', t: '未齊', color: 'purple', },    
  { k: 'C', t: '未到', color: '#f04134', },  
  { k: 'D', t: '未訂', color: '#f50', },
]


export const sizeConfig = [
  { s: 'S', t: '', label: "S", key: "S", formType: 'Checkbox', rules, },
  { s: 'M', t: '', label: "M", key: "M", formType: 'Checkbox', rules, },
  { s: 'L', t: '', label: "L", key: "L", formType: 'Checkbox', rules, },
  { s: 'X', t: '', label: "X", key: "X", formType: 'Checkbox', rules, },
  { s: 'XL', t: '', label: "XL", key: "XL", formType: 'Checkbox', rules, },
]



export const returnSelectConfig = [
  { k: 'pn_no', ph: `批號`, },
  { k: 'style_no', ph: `款號`, },
]



export const actionType = {
  delete: '刪除',
  add: '添加',
  edit: '編輯',
}



export const pnDateArr = ['s_date', 'e_date', ]
// export const pnDateArr = ['new_s_date', 'new_e_date', ]
// export const pnDateArr = [
//   { key: "s_date1", com: 'Year' },
//   { key: "s_date2", com: 'Date' },
//   { key: "e_date1", com: 'Year' },
//   { key: "e_date2", com: 'Date' },
// ]

export const manualNumFormExtra = [
  { label: "數量", key: "qty", formType: 'Input', },
  { label: "日期", key: "r_date", formType: 'SingleDate', haveInit: true, initVal: defaultNow, },
  { label: "備注", key: "remark", formType: 'Text', },
]


// export const dateNow = moment().format(DATE_FORMAT_BAR)
// export const sub7 = moment().subtract(7, 'days').format(DATE_FORMAT_BAR)
export const dateNow = moment()
export const sub7 = moment().subtract(7, 'days')
export const subHalfYear = moment().subtract(180, 'days').format(DATE_FORMAT_BAR)
// console.log(' defaultNow22222222 ： ', sub7, DATE_FORMAT_BAR, subHalfYear,    )// 

// export const manualNumInit = {
//   date_s: sub7, 
//   date_e: dateNow, 
// }

export const manualDate =  [sub7, dateNow,  ]
export const manualNumInit = {
  // date: ['2019-01-01', dateNow,  ], 
  date: [sub7, dateNow,  ], 
  barcode_no: undefined,
  pn_no: undefined,
  work_no: undefined,
  factory_id: undefined,
}

export const manualNumSearchFormExtra = [
  { label: "日期", key: "date", formType: 'RangeDate', 
  // dateCb: 'dateCb', 
  // , haveInit: true, initVal: [sub7, dateNow,  ], isCbParam: true, 
  span: 6, disableRange: subHalfYear, },
]

export const manualNumForm = [
  { label: "批號", key: "pn_no", formType: 'Select', datakey: 'pn_no_data', selectCb: 'pn_noSelectCb', isCbConfig: true, optionkey: 'pn_no', optionTxt: 'pn_no', disabledData: 'formWorkData', resetKey: ['work_no', 'factory_id',], span: 6, },
  { label: "工序", key: "work_no", formType: 'Select', datakey: 'work_no_data', selectCb: 'workSelectCb', isCbConfig: true, optionkey: 'work_no', optionTxt: 'work_name', disabledData: 'formFactoryData', resetKey: 'factory_id', span: 6, },
  { label: "工區", key: "factory_id", formType: 'Select', datakey: 'factory_id_data', optionkey: 'factory_id', optionTxt: 'factory_name', disableKey: 'work_no', span: 6, },
]





// export const filterOptionDate = [
//   // "shipment_datefr",  "shipment_dateto", 
//   { key: "shipment_datefr", initVal: filterNows, },
//   { key: "shipment_dateto", initVal: delayTwoMonths, },
// ]
console.log(' filterNows ： ', filterNows, delayTwoMonths, filterNows.slice(0, 4), filterNows.slice(5, 10),  filterNow.slice(5, 9).replace('-', '') )// 
// export const filterOptionDate = [
//   { key: "shipment_datefr1", initVal: filterNow.slice(0, 4), com: 'Year' },
//   // { key: "shipment_datefr2", initVal: filterNow.slice(5, 10), com: 'Date' },
//   { key: "shipment_datefr2", initVal: filterNow.slice(5, 10).replace('-', ''), com: 'Date' },
//   { key: "shipment_dateto1", initVal: delayTwoMonth.slice(0, 4), com: 'Year' },
//   // { key: "shipment_dateto2", initVal: delayTwoMonth.slice(5, 10), com: 'Date' },
//   { key: "shipment_dateto2", initVal: delayTwoMonth.slice(5, 10).replace('-', ''), com: 'Date' },
// ]
// export const filterOptionDate = [
//   { key: "shipment_datefr1", initVal: '2018', com: 'Year' },
//   { key: "shipment_datefr2", initVal: '10-10', com: 'Date' },
//   { key: "shipment_dateto1", initVal: '2018', com: 'Year' },
//   { key: "shipment_dateto2", initVal: '11-10', com: 'Date' },
// ]
export const filterOptionDate = [
  { key: "shipment_datefr", initVal: moment(filterNow).format(DATE_FORMAT_BAR), },
  { key: "shipment_dateto", initVal: moment(delayTwoMonth).format(DATE_FORMAT_BAR), },
]

// console.log(' moment().format(DATE_FORMAT_BAR) ： ', moment().format(DATE_FORMAT_BAR), filterNow, moment('20180217').format(DATE_FORMAT_BAR),  )// 


export const splitMap = {
  'split': '拆分',
  'edit': '修改起止日期或産線',
}



export const manaulPnForm = {
  // "shipment_dateto": oneMonth,
  // "shipment_dateto": delayThreeMonth,
  "shipment_dateto": moment(delayTwoMonth).format(DATE_FORMAT_BAR), 
  "shipment_datefr": moment(filterNow).format(DATE_FORMAT_BAR), 
  // "shipment_dateto": delayTwoMonths,
  // "shipment_datefr": filterNows,
  "pn_no": "",
  "style_no": "",
  "customer": '',
}

export const productionSchedueForm = {
  ...manaulPnForm,
  "pn_nofr": "",
  "pn_noto": "",
}




export const manulPnDateArr = {
  1: ['mc_s_date', 'mc_e_date', ],
  3: ['sew_s_date', 'sew_e_date', ],
  6: ['lastwork_s_date', 'lastwork_e_date', ],
}



export const eventSub7 = moment().subtract(7, 'days').format(DATE_FORMAT_BAR)
export const dateFrNow = moment().format(DATE_FORMAT_BAR)

export const eventDate = [
  { key: "datefr", initVal: eventSub7, },
  { key: "dateto", initVal: dateFrNow, },
]

export const unLockDate = [
  { key: "input_date", initVal: eventSub7, },
]



export const boardArr = [
  { company_id: "1", name: '敏興首頁', },
  { company_id: "2", name: '聯嶺首頁', },
  { company_id: "3", name: '緯興首頁', },
]


export const authArr = [
  { path: 'index', name: 'base', middle:"page/index/", seeAuth: [1,  ], actionAuth: [18,  ], }, 

]



