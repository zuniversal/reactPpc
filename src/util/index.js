import React from 'react';
import ReactDOM from 'react-dom';
import { INPUT_TXT, IMAGE_PREFIX, IMG_URL,  } from 'constants'
import { Icon, notification, Button, Input, message, } from 'antd'
export const openNotification = (msg, className, icon) => {
    const icons = icon != undefined ? "frown-o" : "smile-o"
    const classNames = className != undefined ? className : ""
    notification.open({
        message: msg,
        icon: <Icon type={icons} style={{ color: '#108ee9' }} />,
        // duration: 143,
        // bottom: 200,
        // className: classNames,
    });
};


export const confirms = (c = 1, m, t = 3, cb, ) => {
    // console.log('confirms ：', c, t, cb, )
    let type = 'success'
    if (c === 0) {
        type = 'error'
    } else if (c === 2) {
        type = 'warn'
    }
    message.config({
        duration: 2,
    })
    message[type](m, t, cb)
};

// export const isNoTips = res => JSON.parse(res.config.data).noTips
// export const tipsConfirm = res => {
//     const {code, mes, } = res.data 
//     const codeExist = code !== 1 && code != undefined
//     if (codeExist || (codeExist && isNoTips(res))) {
//         console.log('confirmsconfirmsconfirms ：', res.data, code !== 1, code != undefined, !isNoTips(res), (code !== 1 && code != undefined), (code !== 1 && isNoTips(res)) )
//         // confirms(code, mes,  )
//     }
// }
export const isNoTips = res => {
    // console.log(' codeExistcodeExist ： ', res.config, res.config.datas,  )
    return res.config.datas.noTips
}
export const tipsConfirm = res => {
    const {code, mes, } = res.data  
    const codeExist = code === 1 || code == undefined
    console.log(' %c codeExist 组件 this.statethis.propss ： ', `color: #333; font-weight: bold`, code == undefined, code !== 1, !!isNoTips(res), isNoTips(res), res.config.datas, code, res, res.config.url  )
    // if (!(!code !== 1 && !!isNoTips(res))) {// 
    if (!codeExist || !isNoTips(res)) {
        // console.log(' codeExist confirmsconfirmsconfirms= ：', res.datas, code == 1, code != undefined, !isNoTips(res), (code !== 1 && code != undefined), (code !== 1 && isNoTips(res)) )
        confirms(code, mes,  )
    }
}




export const showTotal = (total) => `總共 ${total} 條`

export const backupFn = (o) => JSON.parse(JSON.stringify(o))
let t;

export const setItem = (k, v) => localStorage.setItem(k, JSON.stringify(v))
export const getItem = (k) => JSON.parse(localStorage.getItem(k))
export const removeItem = (k) => localStorage.removeItem(k)

export const setItems = (k, v) => sessionStorage.setItem(k, JSON.stringify(v))
export const getItems = k => JSON.parse(sessionStorage.getItem(k))
export const removeItems = k => sessionStorage.removeItem(k)

export const render = (Container, Component) => ReactDOM.render(<Container>{Component}</Container>,document.getElementById('root'))

export const logins = (nextState, replace, cb) => {
//   console.log('getItem( user_info ：', getItem('user_info'),);
//   const token = (getItem('company_id') != undefined ? getItem('company_id').token : '')
  const token = getItems('company_id')
  console.log('#######nextState, replace, cb============= ：', nextState, token, token != undefined);
  // console.log('token ：', getItem('user_info') != undefined ? getItem('user_info').token : '',
  // '==', getItem('user_info').token, 'tokensssssss ：', token, token == '');
  if (token == undefined) {
    //   console.log('false')// 
    replace('/login');
  }
  cb();
}
// v：当需要
export const ajax = (cb, v, time) => {
  if (t) clearTimeout(t);
  t = setTimeout(() => cb(v), time != undefined ? time : 300);
}
export const getAll = p => Promise.all(p).then(res => Promise.all(res))
export const OPTIONS = p => ({headers: {"Authorization": p }})

export const randomNumber = (n1, n2) => {
    if (arguments.length === 2) {
        return Math.round(n1 + Math.random() * (n2 - n1));
    }
    else if (arguments.length === 1) {
        return Math.round(Math.random() * n1)
    }
    else {
        return Math.round(Math.random() * 255)
    }
}
export const rc = () => '#' + Math.random().toString(16).substring(2).substr(0, 6)
export const disabledDate = c => c && c.valueOf() < Date.now();
export const ts = t => Date.parse(new Date(t))
export const color = (n = 10) => {
    // console.log('n ：', n);
    const color = []
    for (let i = 0; i < n; i++) { color.push(rc())}
    return color
}
export const animate = n => `animated ${n}`
export const createProperty = (arr, f) => {
    const origin = {}
    arr.forEach((v, i) => origin[v] = f(v))
    return origin
}











export const getToken = (k = 'user_info') => getItems(k) != undefined ? getItems(k).token : ''
export const getUserId = (k = 'user_info') => getItems(k) != undefined ? getItems(k).user_id : ''

export const wipe = (s, t = 'px') => s.substring(0, s.lastIndexOf(t))
export const dateForm = (d, j = '-', s = '-') => d.split('T')[0].split(s).join(j)
export const dateSplit = (d, s = '-') => d.split('T')[0].split(s)
export const newDate = (a,) => new Date(a[0], a[1] - 1, a[2])
export const daysLen = (s, e) => (e - s) / 86400000 + 1
export const dateArrToLon = (d, i) => new Date(d + i * 86400000)
export const toLocale = (d) => d.toLocaleDateString()
// 2018-03-10T00:00:00.000Z => 2018/3/10
export const stampToLocale = (d) => toLocale(new Date(d))
export const createRow = (l) => {
    const arr = []
    for (let i = 0; i < l; i++) {
        arr.push(i)
    }
    return arr
}
export const filterArr = (keys) => keys.filter((v, i, arr) => arr.indexOf(v) === i)
export const filterArrOForm = (arr, k, e = 'data') => arr.filter((v, i, arr) => arr.indexOf(v) === i).map(v => v = {[k]: v, [e]: []}) 
export const mergeArr = (o, a, k, e = 'data') => {
    // console.log('a, k ：', o, a, k, e);
    a.map(v => {
        o.forEach(item => {
          if (item[k] === v[k]) {
            v[e].push(item)
          }
        })
        // console.log(' item ：', v[k], v[e])
        return v
    })
    return a
}
export const addProp = (arr, con, k, p) => arr.map(v => ({...v, [p]: con.filter(item => item[k] === v[k])[0][p]}))

export const findDOMNode = (d, c) => d.findDOMNode(c)

export const createLenArray = (l, w = []) => Array(l).fill(w)

export const wrapParams = p => ({company_id: getItems('company_id'), company_no: getItems('company_id'), user_id: getUserId('userInfo'), ...p})


export const dataFilter = (t, data, searchText, k, ) => {
    // console.log('k ：', data, k, searchText, );
    if (data.length && k != '') {
        const reg = new RegExp(searchText, 'gi');
        return data.map((record) => {
            // console.log('record ：', record);
            if (record[k] != undefined) {
                const match = record[k].match(reg);
                if (!match) {
                    return null;
                }
                return {
                    ...record, [k]: record[k],
                };
            } else {
                return {
                    ...record,
                };
            }
        }).filter(record => !!record)
    } else {
        return data
    }
}

export const customFilter = (t, k) => {
//   console.log('t11 ：', t, t.onInputChange);
  return {
    filterDropdown: (
      <div className="custom-filter-dropdown">
        <Input
          ref={ele => t.searchInput = ele}
          placeholder={`${INPUT_TXT}搜索文本`}
          value={t.state.searchText}
          onChange={t.onInputChange.bind(t, k)}
        />
        <Button type="primary" onClick={t.blur.bind(this, k)} className='m-r10'>關閉</Button>
        <Button type="primary" onClick={t.reset.bind(this, k)}>恢複數據</Button>
      </div>
    ),
    filterIcon: <Icon type="filter" style={{ color: t.state.filtered ? '#fff' : '#fff' }} />,
    filterDropdownVisible: t.state[`${k}Visible`],
    onFilterDropdownVisibleChange: (visible) => {
      t.setState({[`${k}Visible`]: visible, searchText: '', searchKey: '',}, () => t.searchInput.focus());
    },
  }
}

export const infoFilter = (a, p, k, u = 'unq', ) => a.find(item => item[u] === p)[k]

export const filterDatas = (arr, k) => Array.from(new Set(arr.map(v => v[k])))
export const filterTrimDatas = (arr, k) => Array.from(new Set(arr.map(v => v[k].trim())))

// -----

// export const addParams = (a, p, k, u = 'unq', ) => a.find(item => item[u] === p)[k]

// export const imgFilter = (v) => `${IMG_URL}${IMAGE_PREFIX[getItems('userInfo').company_id]}/Sketch${v.split('Sketch')[1]}`
// export const imgFilter = (v) => `${IMG_URL}${IMAGE_PREFIX[getItems('userInfo').company_id]}/${v.split('sc_data')[1]}`
export const imgFilter = (v) => `${IMG_URL}${IMAGE_PREFIX[getItems('userInfo').company_id]}/${v.split('Sketch')[1]}` 


export const valCompare = (v, k0, k1, ) => v[k0] > v[k1]

// export const createWith = (n = 10, arr = []) => {
//     console.log(' createWith ：', n, arr);
//     return arr.map((v, i) => {
//         console.log(' arr v ： ', v, ) 
//         return {...v, idx: `${i}`, idx: `${i}`}
//     })
// }

export const getMonth = () => new Date().getMonth() + 1
export const monthFilter = (v) => v > 12 ? v - 12 : v

export const dateSplits = (v, j = '') => v.split('-').join(j)
// export const dateSplits = (v) => v
export const dateFormat = (v) => `${v.substring(0, 4)}-${v.substring(4, 6)}-${v.substring(6, 8)}`




export const numToDate = (value, j = '') => {
    const str = value.split('').filter(v => v !== '-')
    const v1 = str.slice(0, 4).join('')
    const v2 = str.slice(4, 6).join('')
    const v3 = str.slice(6, 8).join('')
    console.log(' value formatter 格式化22  ： ', v1, v2, v3 , str, `${v1}${str.length > 4 ? '-' : ''}${v2}${str.length > 6 ? '-' : ''}${v3}`   )// 
    return `${v1}${str.length > 4 ? '-' : ''}${v2}${str.length > 6 ? '-' : ''}${v3}`
}







