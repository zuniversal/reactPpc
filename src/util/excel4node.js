const xlsx = require('excel4node');



function cellStyle(workbook, vertical, horizontal, size, fgColor, border) {
    let options = {};
    let alignment = {};
    let font = {};
    let fill = {};
    let borderStyle = {};

    if (vertical != undefined && vertical != null) alignment.vertical = vertical;
    if (horizontal != undefined && horizontal != null) alignment.horizontal = horizontal;
    if (size != undefined && size != null) font.size = size;
    if (fgColor != undefined && fgColor != null) fill = {
        fgColor: fgColor,
        type: 'pattern',
        patternType: 'solid'
    };

    if (border != undefined && JSON.stringify(border) != '{}') {
        if (border.top != undefined) borderStyle.top = {
            style: border.top,
            color: '#000000'
        };
        if (border.left != undefined) borderStyle.left = {
            style: border.left,
            color: '#000000'
        };
        if (border.bottom != undefined) borderStyle.bottom = {
            style: border.bottom,
            color: '#000000'
        };
        if (border.right != undefined) borderStyle.right = {
            style: border.right,
            color: '#000000'
        };
    }

    if (JSON.stringify(alignment) != '{}') options.alignment = alignment;
    if (JSON.stringify(font) != '{}') options.font = font;
    if (JSON.stringify(fill) != '{}') options.fill = fill;
    if (JSON.stringify(borderStyle) != '{}') options.border = borderStyle;

    if (JSON.stringify(options) != '{}') {
        let align = workbook.createStyle(options);
        return (align);
    }

}

export const fileModel = (data, employeeID, next) => {



    const wb = new xlsx.Workbook({
        defaultFont: {
            name: 'SimSun',
            size: 10
        }
    });

    // Add Worksheets to the workbook
    const ws = wb.addWorksheet('sheet1');

    // set the width of column
    ws.column(2).setWidth(15);
    ws.column(3).setWidth(28);
    ws.column(4).setWidth(15);
    ws.column(5).setWidth(28);


    ws.row(2).setHeight(28);
    for (var i = 4; i <= 25; i++) {
        ws.row(i).setHeight(27);
    }

    ws.cell(2, 2, 2, 5, true).string('公幹報銷申請表').style(cellStyle(wb, 'center', 'center', 20));
    ws.cell(4, 2).string('正楷全名：').style(cellStyle(wb, 'center', null, null, null, {
        top: 'double',
        left: 'double'
    }));
    ws.cell(4, 3).style(cellStyle(wb, 'center', null, null, null, {
        top: 'double'
    }));
    ws.cell(4, 4).string('遞交日期：').style(cellStyle(wb, 'center', null, null, null, {
        top: 'double'
    }));
    ws.cell(4, 5).style(cellStyle(wb, 'center', null, null, null, {
        top: 'double',
        right: 'double'
    }));
    ws.cell(5, 2).string('部門代碼：').style(cellStyle(wb, 'center', null, null, null, {
        left: 'double'
    }));
    ws.cell(5, 3).style(cellStyle(wb, 'center', null, null, null));
    ws.cell(5, 4).string('出數公司 :').style(cellStyle(wb, 'center', null, null, null));
    ws.cell(5, 5).style(cellStyle(wb, 'center', null, null, null, {
        right: 'double'
    }));
    ws.cell(6, 2).string('公幹日期：').style(cellStyle(wb, 'center', null, null, null, {
        left: 'double'
    }));
    ws.cell(6, 3).style(cellStyle(wb, 'center', null, null, null));
    ws.cell(6, 4).string('匯率：').style(cellStyle(wb, 'center', null, null, null));
    ws.cell(6, 5).style(cellStyle(wb, 'center', null, null, null, {
        right: 'double'
    }));
    ws.cell(7, 2).string('公幹原因：').style(cellStyle(wb, 'center', null, null, null, {
        left: 'double',
        bottom: 'double'
    }));
    ws.cell(7, 3).style(cellStyle(wb, 'center', null, null, null, {
        bottom: 'double'
    }));
    ws.cell(7, 4).string('客名：').style(cellStyle(wb, 'center', null, null, null, {
        bottom: 'double'
    }));
    ws.cell(7, 5).style(cellStyle(wb, 'center', null, null, null, {
        bottom: 'double',
        right: 'double'
    }));


    ws.cell(9, 2).string('日期').style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'double',
        left: 'double',
        bottom: 'thin',
        right: 'dashDot'
    }));
    ws.cell(9, 3).string('單據編號 註二').style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'double',
        left: 'dashDot',
        bottom: 'thin',
        right: 'dashDot'
    }));
    ws.cell(9, 4).string('項目 註一').style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'double',
        left: 'dashDot',
        bottom: 'thin',
        right: 'dashDot'
    }));
    ws.cell(9, 5).string('金額').style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'double',
        left: 'dashDot',
        bottom: 'thin',
        right: 'double'
    }));

    //blank expense detail table
    ws.cell(10, 2).style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'thin',
        left: 'double',
        bottom: 'dashDot',
        right: 'dashDot'
    }));
    ws.cell(10, 3).style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'thin',
        left: 'dashDot',
        bottom: 'dashDot',
        right: 'dashDot'
    }));
    ws.cell(10, 4).style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'thin',
        left: 'dashDot',
        bottom: 'dashDot',
        right: 'dashDot'
    }));
    ws.cell(10, 5).style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'thin',
        left: 'dashDot',
        bottom: 'dashDot',
        right: 'double'
    }));
    for (var j = 11; j < 25; j++) {
        ws.cell(j, 2).style(cellStyle(wb, 'center', 'center', null, null, {
            top: 'dashDot',
            left: 'double',
            bottom: 'dashDot',
            right: 'dashDot'
        }));
        ws.cell(j, 3).style(cellStyle(wb, 'center', 'center', null, null, {
            top: 'dashDot',
            left: 'dashDot',
            bottom: 'dashDot',
            right: 'dashDot'
        }));
        ws.cell(j, 4).style(cellStyle(wb, 'center', 'center', null, null, {
            top: 'dashDot',
            left: 'dashDot',
            bottom: 'dashDot',
            right: 'dashDot'
        }));
        ws.cell(j, 5).style(cellStyle(wb, 'center', 'center', null, null, {
            top: 'dashDot',
            left: 'dashDot',
            bottom: 'dashDot',
            right: 'double'
        }));
    }
    ws.cell(25, 2).style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'dashDot',
        left: 'double',
        bottom: 'double',
        right: 'dashDot'
    }));
    ws.cell(25, 3).style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'dashDot',
        left: 'dashDot',
        bottom: 'double',
        right: 'dashDot'
    }));
    ws.cell(25, 4).style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'dashDot',
        left: 'dashDot',
        bottom: 'double',
        right: 'dashDot'
    }));
    ws.cell(25, 5).style(cellStyle(wb, 'center', 'center', null, null, {
        top: 'dashDot',
        left: 'dashDot',
        bottom: 'double',
        right: 'double'
    }));

    var filename = employeeID + '.xlsx';

    wb.write(filename, function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log('success');
        }
    });
}

