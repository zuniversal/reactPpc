import {animate, createProperty, } from 'util'


export const isTest = process.env.NODE_ENV === 'development'

export const BASE_URL = "http://192.168.10.23:4000"

export const URL = isTest ? "http://192.168.10.23:4000" : BASE_URL 
// export const URL = isTest ? "http://192.168.10.23:4009" : BASE_URL 

// 服务器的地址
// export const URL = "http://192.168.10.24:4000"
// export const IMG_URL = "http://192.168.10.24:8080/ppc/"
// export const IMG_URL = "http://192.168.10.24:8080/ppc/"
export const IMG_URL = "http://192.168.10.24/assets/"
// export const TEST_URL = "http://192.168.108.165:4000/"

export const ACC_LOGIN_URL = "http://192.168.10.23:4001/login"
export const ACC_URL = "http://192.168.10.23:4001/master/"




export const TABPATH = "page/"
export const SELECT_TXT = "請選擇"
export const INPUT_TXT = "請輸入"
export const SIZE = 6
export const SIZE10 = 10
export const SIZE20 = 20
export const PAGE = 1
export const DEFAULT_FACTORY = 1

export const defaultFactoryType = 'I'
// export const defaultFactoryType = 'O'
export const capictyFactoryType = 'A'
export const defaultWorkNo = 1
// export const defaultWorkNo = 0
export const DEFAULT_WORK_NO = 0

export const USER_INFO = "USER_INFO"
export const LOUOUT = "LOUOUT"
export const LOAD = "LOAD"
export const STYLES = "STYLES"
export const IS_COLLPOSED = "IS_COLLPOSED"
export const FILTER_INFO = "FILTER_INFO"
export const COPY = "COPY"

export const ALL_CUSTOMER = "ALL_CUSTOMER"
export const ALL_STYLE_DATA = "ALL_STYLE_DATA"

export const IMAGE_PREFIX = {
    1: "pfimages",
    2: "pfimages",
    3: "vkimages",
}

export const NETWORK_ERROR = "請求超時，請刷新或者重新登錄，如果還是報錯，請聯系電腦部 ^_^"

export const LETTER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')


export const HEADER_HEIGHT = 70


const animations = [
    'bounce', 'flash', 'rubberBand', 'shake', 'headShake',
    'swing', 'tada', 'wobble', 'jello', 'bounceIn', 'bounceInDown',
    'bounceInLeft', 'bounceInRight', 'bounceOut', 'bounceOutDown', 'bounceOutLeft',
    'bounceOutLeft', 'bounceOutUp', 'fadeIn', 'fadeInDown', 'fadeInDownBig', 'fadeInLeft',
    'fadeInLeftBig', 'fadeInRight', 'fadeInRightBig', 'fadeInUp', 'fadeInUpBig', 'fadeOut',
    'fadeOutDown', 'fadeOutDownBig', 'fadeOutLeft', 'fadeOutLeftBig', 'fadeOutRight', 'fadeOutRightBig',
    'fadeOutUp', 'fadeOutUpBig', 'flipInX', 'flipInY', 'flipOutX', 'flipOutY',
    'lightSpeedIn', 'lightSpeedOut', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft',
    'rotateInUpRight', 'rotateOut', 'rotateOutDownLeft', 'rotateOutDownRight', 'rotateOutUpLeft', 'rotateOutUpRight',
    'hinge', 'jackInTheBox', 'rollIn', 'rollOut','zoomIn', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomInUp',
    'zoomOut', 'zoomOutDown', 'zoomOutLeft', 'zoomOutRight', 'zoomOutUp', 'slideInDown',
    'slideInLeft', 'slideInRight', 'slideInUp', 'slideOutDown', 'slideOutLeft', 'slideOutRight', 'slideOutUp'
];
export const ANIMATE = createProperty(animations, animate,)