import {post, get, noTipsPost} from "../index.js"

export const getProduct = p => noTipsPost('master/msfactory', p)
export const addProductLine = p => post('master/msfactory/addfactory', p)
export const editProcess = p => post('master/msfactory/updatefactory', p)
export const deleteProcess = p => post('master/msfactory/deletefactory', p)

export const addProcess = p => post('master/msfactory/addcapacity', p)
export const editCapacity = p => post('master/msfactory/updatecapacity', p)