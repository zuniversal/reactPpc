import "whatwg-fetch"
import "es6-promise"
import {URL} from 'constants'
import {getItem} from 'util'

export function get(urlSuffix, params) {
    const token = getItem('user_info') != undefined ? getItem('user_info').token : ''
    // console.log('paramsparams ：', params);
    let url = URL + urlSuffix
    let paramUrl = ''
    if (params) {
        Object.keys(params).map(key => {
        // console.log('key:', key)
            paramUrl += `&${key}=${params[key]}`
            return ''
        })
        if (paramUrl) {
            paramUrl = paramUrl.slice(1);
        }
        url = url + '?' + paramUrl
    }
    // console.log('paramsss:', url)
    let result = fetch(url, {
        mode: "cors",
        headers: {
            "Accept": "application/json, text/plain, */*",
            "Authorization": token
        }
    }) 
    return result;
}
