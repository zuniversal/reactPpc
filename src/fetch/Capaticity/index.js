import {post, get, noTipsPost} from "../index.js"

export const getCapaticity = p => noTipsPost('report/capacity_rpt', p)
export const getFactory = p => noTipsPost('report/capacity_rpt_filter', p)