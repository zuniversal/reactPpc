import {post, get, noTipsPost} from "../index.js"

export const getEvent = p => noTipsPost('event/getevent', p)
export const eventDetail = p => noTipsPost('event/yarninfo', p)
export const updateEvent = p => noTipsPost('event/updateevent', p)
export const getUpdateEvent = p => noTipsPost('event/updatepnrpt', p)


