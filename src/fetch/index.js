import {URL, LOAD, LOUOUT, NETWORK_ERROR, } from 'constants'
import {getToken, wrapParams, confirms, tipsConfirm, } from 'util'

import {loading, } from 'actions/login'// 

import axios from 'axios'
axios.defaults.baseURL = URL
// axios.defaults.timeout = 10000
axios.defaults.timeout = 30000
axios.defaults.headers.common['Authorization'] = getToken()
axios.interceptors.request.use((config) => {
    // console.log('getToken() ：', config, getToken());
    // config.headers.Authorization = getToken()
    // axios.store.dispatch({type: LOAD, data: true})
    // axios.store.dispatch('loading', {loading: 'loading',  })
    // axios.store.dispatch(loading())
    config.data = wrapParams(config.data)
    config.datas = wrapParams(config.method === 'get' ? config.params : config.data)
    // console.log(' 发送请求的信息 1：', config);
    return config;
}, (err) => Promise.reject(err));
axios.interceptors.response.use((res) => {
    // axios.store.dispatch({type: LOAD, data: false})
    console.log(' 请求回来的信息 2：', res, axios.store, axios.store.getState()  );
    tipsConfirm(res,  )
    return res;
}, (err) => {
    console.log(' 发生错误了：', err.response, err, err.message, typeof err.message, err.message.indexOf('timeout'), err.message.indexOf('403'));
    if (err.message.indexOf('timeout') > -1) {
        confirms(2, NETWORK_ERROR)
    } else {
        confirms(2, err.message)
    }
    if (err.response != undefined && err.response.data.code === 7) {
        confirms(2, '輔料系統時間獲取數據設置過短,token已過期,請重新登錄再點擊查看 !!!')
        // confirms(2, err.response.data.ForbiddenError)
        // window.location.href = '#login'
    }
    return Promise.reject(err)
});
export const get = (url, params) => {
    // console.log('axios get请求 ：', url, params, {params: params});
    return axios.get(url, {params: params})
}
export const post = (url, params) => {
    // console.log('apost(url, params) ：', url, params);
    return axios.post(url, params)
}
export const noTipsPost = (url, params) => axios.post(url, {...params, noTips: true})


// const urls = 'file://mh_sql2.pressfield.com/sc_data/Sketch/UP/A0102(TW)/H&M/2010580-1/front.jpg'

// const getss = (urls) => {
//     console.log('getssgetss get请求 ：', urls, );
//     return axios.get(urls)
// }
// getss(urls)
