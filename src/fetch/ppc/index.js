
import {post, noTipsPost, } from "../index.js"
import {TEST_URL} from 'constants'

export const getList = p => noTipsPost("ppc/ppc_list", p)
export const getPnList = p => noTipsPost("ppc/ppc_pn_list", p)
// export const getPnList = p => noTipsPost(`${TEST_URL}ppc/ppc_pn_list`, p)
export const getPnDetailList = p => noTipsPost("ppc/ppc_pn_click", p)
export const getBarcodeList = p => noTipsPost("ppc/ppc_bacode_click", p)

export const editBarcode = p => post("ppc/ppc_barcode_updatedate", p)
export const getFilter = p => noTipsPost("ppc/ppc_filter", p)
export const splitPnBarcode = p => post("ppc/ppc_barcode_split", p)
export const barcodeLock = p => post("ppc/ppc_barcode_lock", p)
export const finishDay = p => post("ppc/ppc_finish_date", p)
export const getRealDetail = p => noTipsPost("ppc/ppc_pn_list_detail", p)
export const deleteBarcode = p => post("ppc/ppc_barcode_delete", p)