import {post, get, noTipsPost} from "../index.js"

export const getPn = p => noTipsPost('master/outsidepn/pnget', p)
export const editPn = p => post('master/outsidepn/updatepn', p)
export const addPn = p => post('master/outsidepn/addpn', p)

export const getColorSize = p => noTipsPost('master/outsidepn/getcolorsize', p)
export const addColorSize = p => post('master/outsidepn/addcolorsize', p)
export const updateColorSize = p => post('master/outsidepn/updatecolorsize', p)
export const deleteColorSize = p => post('master/outsidepn/deletecolorsize', p)

export const getSidePnWork = p => noTipsPost('master/outsidepn/getsidepnwork', p)
export const saveSidePnWork = p => post('master/outsidepn/savesidepnwork', p)