import {post, get, noTipsPost} from "../index.js"

export const getStanderdDay = p => noTipsPost('master/msworkdays', p)
export const addWorkdays = p => post('master/msworkdays/insertworkdays', p)
export const updateWorkdays = p => post('master/msworkdays/updateworkdays', p)
export const deleteWorkdays = p => post('master/msworkdays/deleteworkdays', p)