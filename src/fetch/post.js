import "whatwg-fetch"
import "es6-promise"
import {URL, } from 'constants'
import {getItem, OPTIONS, getToken} from 'util'
import axios from 'axios'
axios.defaults.headers.common['Authorization'] = getToken()
export function post(url, paramsObj, isStringify) {
    const token = getItem('user_info') != undefined ? getItem('user_info').token : ''
    console.log('post   url：', url, paramsObj, );
    if (url !== 'login') {
        url = URL + url
    } else {
    }
    var result = fetch(url, {
        method: "POST",
        mode: "cors",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": token
        },
        body: isStringify ? JSON.stringify(paramsObj) : obj2params(paramsObj)
    }) 
    return result;
}
// export function post(url, paramsObj) {
//     console.log('url：', url, paramsObj);
//     url = URL + url
//     console.log('obj2params：', obj2params(paramsObj), JSON.stringify(paramsObj), JSON.parse(JSON.stringify(paramsObj)));
//     var result = fetch(url, {
//         method: "POST",
//         mode: "cors",
//         headers: {
//             "Content-Type": "application/x-www-form-urlencoded",
//             "Authorization": JSON.parse(localStorage.getItem('user_info')).token
//         },
//         body: JSON.stringify(paramsObj)
//     }) 
//     return result;
// }

function obj2params(obj) {
    // console.log('obj：', obj);
    var result = "";
    for (var item in obj) {
        result += "&" + item + "=" + obj[item];
    }
    if (result) {
        result = result.slice(1);
    }
    // console.log('result1111111111111111111111：', result);
    return result;
}
export function formPost(url, formdata) {
    const token = getItem('user_info') != undefined ? getItem('user_info').token : ''
    console.log('url：', url, formdata);
    url = URL + url
    // console.log('formPost-formdata:', formdata, formdata.get('file'), formdata.get('expenseType'), formdata.get('employeeID'));
    var result = fetch(url, {
        method: "POST",
        mode: "cors",
        headers: {
            "Authorization": token
        },
        body: formdata
    }) 
    return result;
}

axios.defaults.baseURL = URL;

export function aget(url, params) {
    console.log('axios get请求 ：', url, params,);
    return axios.get(url, {params: params})
}
export function apost(url, params) {
    console.log('apost(url, params) ：', url, params);
    return axios.post(url, params)
}
export function postJson(paramsObj) {
    // let myHeaders = new Headers();
    // const paramsObj = {
    //     a: '1',
    //     b: '2'
    // }
    const token = getItem('user_info') != undefined ? getItem('user_info').token : ''
    console.log('postJson   url：', paramsObj,JSON.stringify(paramsObj), token);
    const url = 'http://localhost:8086/test/json'
    var result = fetch(url, {
        method: "POST",
        mode: 'cors',
        headers: {
            "Authorization": token,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(paramsObj)
    }) 
    return result;
}