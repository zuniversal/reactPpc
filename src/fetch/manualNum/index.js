import {post, get, noTipsPost} from "../index.js"

// export const getManualNum = p => noTipsPost('ppc/pndayqty_open', p)

export const getPnDayData = p => noTipsPost('ppc/pndayqty_pnget', p)
export const getManualNum = p => noTipsPost('ppc/pndayqty_list', p)
export const newManualNum = p => post('ppc/pndayqty_new', p)
export const editManualNum = p => post('ppc/pndayqty_update', p)
export const deleteManualNum = p => post('ppc/pndayqty_delete', p)





