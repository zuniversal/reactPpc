import {post, get, noTipsPost} from "../index.js"

export const getProcedure = p => noTipsPost('master/mswork', p)
export const addWork = p => post('master/mswork/addwork', p)
export const updateWork = p => post('master/mswork/updatework', p)
export const deleteWork = p => post('master/mswork/deletework', p)