import * as TYPES from "constants"

const State = ''
export default (state = State, action) => {
    // console.log('COPY ：', state, action)
    switch (action.type) {
        case TYPES.COPY:
            return action.payload;
        default:
            return state;
    }
}