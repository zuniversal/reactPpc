import * as TYPES from "constants"

const State = false
export default (state = State, action) => {
    // console.log('collapse ：', state, action)
    switch (action.type) {
        case TYPES.IS_COLLPOSED:
            return action.payload;
        default:
            return state;
    }
}