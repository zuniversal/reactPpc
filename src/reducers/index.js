import {combineReducers} from "redux"
import userInfo from "./userInfo"
import collapse from "./collapse"
import filterInfo from "./filterInfo"
import customerData from "./customerData"
import styleNoData from "./styleNoData"
// import copy from "./copy"

export default combineReducers({
    userInfo,
    collapse,
    filterInfo,
    customerData,
    styleNoData,
    // copy,
})