import * as TYPES from "constants"

const State = []
export default (state = State, action) => {
    // console.log("customerData    reducer-action:", state, action)
    switch (action.type) {
        case TYPES.ALL_CUSTOMER:
            return action.payload;
        default:
            return state;
    }
}