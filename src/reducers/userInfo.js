import * as TYPES from "constants"
import {routeArr, authArr,   } from 'config'// 
import {getItems, } from 'util'

const userStarte = {right_id: [], seeAuth: {}, actionAuth: {},  }



const authHandle = (data,  ) => {
    // console.log(' SecureHocCom authHandle ： ', data,   )
    const userInfo = { ...userStarte, ...data,  }
    const {right_id,  } = data
    // console.log(' SecureHocCom authHandle ： ',  data, userInfo, right_id   )
    const rigthLen = right_id.length
    const isMe = data.user_id != undefined ? data.user_id.trim() === 'U029287' : false
    // const isMe = false
    const authData = [...routeArr, ...authArr, ]
    // console.log(' authData ： ', authData,  )// 
    routeArr.forEach((v, i) => {
      const allSeeAuth = rigthLen + v.seeAuth.length
    // console.log(' SecureHocCom v ： ', v, v.seeAuth, right_id )// 
      const calcSeeAuth = new Set([...right_id, ...v.seeAuth]).size
        // console.log(' authHandleauthHandle ： ', userInfo, userInfo.seeAuth, v.middle, allSeeAuth, calcSeeAuth,  )// 
    //   console.log(' SecureHocCom routeArr v ： ', v.seeAuth, right_id, allSeeAuth, calcSeeAuth, v.actionAuth, calcActionAuth, allActionAuth,  )
        userInfo.seeAuth[v.middle] = isMe ? true : allSeeAuth !== calcSeeAuth
        // userInfo.seeAuth[v.middle] = isMe ? true : allSeeAuth === calcSeeAuth
        if (Array.isArray(v.actionAuth)) {
            const allActionAuth = rigthLen + v.actionAuth.length
            const calcActionAuth = new Set([...right_id, ...v.actionAuth]).size
            // userInfo.actionAuth[v.middle] = allActionAuth !== calcActionAuth  
            userInfo.actionAuth[v.middle] = isMe ? true : allActionAuth !== calcActionAuth  
            // userInfo.actionAuth[v.middle] = true
        } else {
            // console.log(' SecureHocCom item11 ： ', right_id, v.actionAuth )
            Object.keys(v.actionAuth).forEach((item) => {
                // console.log(' SecureHocCom item ： ', item, v.actionAuth[item] )
                // userInfo.actionAuth[item] = right_id.some(items => items === v.actionAuth[item])
                userInfo.actionAuth[item] = isMe ? true : right_id.some(items => items === v.actionAuth[item])
                // userInfo.actionAuth[item] = true
            })
            // const allActionAuth = rigthLen + v.actionAuth.length
            // const calcActionAuth = new Set([...right_id, ...v.actionAuth]).size
        } 
    })
    console.log(' SecureHocCom userInfo ： ', userInfo,  )// 
    return userInfo 
}
  

export default (state = getItems('userInfo') ? getItems('userInfo') : userStarte, action) => {
    // console.log("***************userInfouserInfo-reduce SecureHocCom:",state,  action)
    switch (action.type) {
        case TYPES.USER_INFO:
            // console.log(' SecureHocCom this.authHandle(action.payload) ： ', authHandle(action.payload),  )// 
            return authHandle(action.payload);
            // return action.payload 
        case TYPES.LOUOUT:
            return {};
        case TYPES.LOAD:
            state.loading = action.payload;
            // console.log('**************state.loading ：', state, state.loading);
            return state
        default:
            // return state;
            // console.log(' SecureHocCom this.authHandle(action.payload) ： ', authHandle(state),  )// 
            return authHandle(state);
    }
}