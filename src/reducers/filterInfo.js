import * as TYPES from "constants"

const State = {}
export default (state = State, action) => {
    // console.log('FilterInfo ：', state, action)
    switch (action.type) {
        case TYPES.FILTER_INFO:
            return action.payload;
        default:
            return state;
    }
}