import * as TYPES from "constants"

const State = []
export default (state = State, action) => {
    // console.log("styleNoData    reducer-action:", state, action)
    switch (action.type) {
        case TYPES.ALL_STYLE_DATA:
            return action.payload;
        default:
            return state;
    }
}