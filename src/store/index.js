import {createStore, } from "redux"
import reducer from "Reducer"
// import thunk from 'redux-thunk'

export default state => createStore(reducer, state, window.devToolsExtension ? window.devToolsExtension() : undefined)