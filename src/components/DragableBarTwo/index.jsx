import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"

import './style.less'
import {wipe, ajax} from 'util'
import {grid} from 'config'
import { ANIMATE,  } from 'constants'
import Draggable from 'react-draggable'
import { Tooltip } from 'antd';
class DragableBar extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      dragAction: '', 
      activeDrags: 0,
      dragDirection: false,
      bounds: "parent",
      initPosition: {
        x: 0, y: 0
      },
      // 默认是不拖动的，当拖拽开始时才是dragging
      dragging: false,
    }
  }

  dbClick = (barcode) => {
    console.log('dbClick ：', barcode, this.props, );
    this.props.showBarcodeDialog(barcode)
  }
  widthAdd = (w) => {
    console.log('widthAdd ：', w);
    this.refs.dragBar.style.width = w + grid + 'px'
  }
  widthDecrease = (w) => {
    console.log('widthDecrease ：', w);
    this.refs.dragBar.style.width = w !== grid ? w - grid + 'px' : '60px'
  }

  handleDrag = (e, ui, d) => {
    const {dragAction} = this.state
    const barWidth = this.refs.dragBar.style.width
    const width = Number(wipe(barWidth))
    console.log('#bar拖动：', width, e, ui, dragAction, d.lastX , d.x, d, this.props);
    // this.props.short(this.props.config.barcode)
    if (dragAction !== '') {
      const isDragLeft = dragAction === 'dragLeft'
      // this.refs.dragBar.style.width = isDragLeft ? width - ui.deltaX + 'px' : width + ui.deltaX + 'px'
      if (isDragLeft) {
        console.log('左拖 ：', );
        if (d.lastX - d.x > 0) {
          console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
          this.widthAdd(width)
          // this.refs.dragBar.style.width = width + grid + 'px'
        } else {
          console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
          this.widthDecrease(width)
          // this.refs.dragBar.style.width = width !== grid ? width - grid + 'px' : '60px'
        }
      } else {
        console.log('右拖 ：', d.lastX, d.x);
        if (d.lastX - d.x < 0) {
          console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
          this.widthAdd(width)
          // this.refs.dragBar.style.width = width + grid + 'px'
        } else {
          console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
          this.widthDecrease(width)
          // this.refs.dragBar.style.width = width !== grid ? width - grid + 'px' : '60px'
        }
      }
    }
    const { x, y } = d;
    ajax(this.setState.bind(this), {
      dragging: true,
      initPosition: { x, y }
    })
    this.setState({
      dragging: true,
      initPosition: { x, y }
    })
  }

  dragAction = (v, a, d) => {
    const { x, y } = this.state.initPosition;
    const {dateLen} = this.props
    const {width, left} = this.props.config
    console.log('#bardragAction v ：', dateLen * grid, dateLen * grid - (left + width), x, y, width, left, v, a, d, this.state, this.props);
    // console.log('v ===ght:  ：', v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: y, right: left + width, bottom: 0})
    
    this.setState({
      dragAction: v,
      bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : "parent",
      // bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: 0, left: -width, right: left + width, bottom: 0},
    })
  }
  onStart = (a, b) => {
    console.log('#bar a：', a, b, );
    this.setState({ activeDrags: ++this.state.activeDrags,  });
  }

  onStop = (onStop, e) => {
    const barWidth = this.refs.dragBar.style.width
    const {barcode} = this.props.config
    const {dragging} = this.state
    const {data, pn_no, clickType} = this.props
    const width = Number(wipe(barWidth)) - grid
    console.log('#@@@@@@@@@@@@@@@@@@@#bar onStop：', barcode, pn_no, this.props, onStop, e, width, '是否是拖动：', this.state.dragging);
    const {lastX, lastY} = e
    const x = lastX / grid
    const y = lastY / grid
    const w = x + width / grid
    if (clickType === 'barClick' && dragging) {
      // ajax(this.props.stopDrag, {x, y, w, barcode, data, pn_no})
      // this.props.stopDrag(x, y, w, barcode)
    }
    this.setState({ 
      activeDrags: --this.state.activeDrags, 
      dragAction: '', 
      dragging: false,
      bounds: "parent",
    });
  }
  componentDidMount() {
    const { left, top } = this.props.config
    // console.log('componentDidMount ：',  this.props, )
  }

  barClick = (e) => {
    console.log('barClick ：', e, this.props)
    const {pn_no, clickType} = this.props
    const {barcode, txt} = this.props.config
    if (clickType === 'barClick') {
      const {work_no, } = this.props.data
      this.props.showBarcodeDialog(barcode, pn_no, work_no)
    } else {
      this.props.getPnDetailList(txt)
    }
  }

  handleDrags = (e, ui, d, t) => {
    const {dragAction} = this.state
    const barWidth = this.refs.dragBar.style.width
    const {width, left} = this.props.config
    console.log('#bar拖动########handleDrags：', e, ui, d, t, dragAction, d.lastX , d.x, this.refs.dragBar.style);
    if (e === 'dragRight') {
      if (d.x > 0) {
        this.refs.dragBar.style.width = d.x + width + 'px'
        this.refs.dragBar.style.transform = `translateX(${left + d.x}px)`
      } else {
        this.refs.dragBar.style.width = d.x + width + 'px'
        this.refs.dragBar.style.transform = `translateX(${left + d.x}px)`
      }
    } else {
      if (d.x > 0) {
        this.refs.dragBar.style.width = d.x + width + 'px'
      } else {
        this.refs.dragBar.style.width = -d.x + width + 'px'
      }
    }
    console.log('this.refs.dragBar ：', this.refs.dragBar, this.refs.dragBar.style.width, width)
  }


  render() {
    const { dragging, dragAction, bounds } = this.state;
    const { config, isAxis, isTop } = this.props
    const { left, width, top, txt, levels, barcode, shortDay, height } = config
    const dragHandlers = { onStart: dragAction === '' ? this.onStart : () => false, onStop: this.onStop };
    // console.log('DragableBar 组件 this.state, this.props ：', this.state, this.props, dragAction, dragAction === '' ? false : true)
    
    const style = {
      width: width + 'px',
      background: dragging ? 'rgba(111, 255, 228, 0.8)' : '',
      // zIndex: dragging ? 10 : 1
      // position: dragAction === '' ? 'absolute' : 'fixed'
    }
    const styles = {
      width: width + 'px',
      background: dragging ? 'rgba(111, 255, 228, 0.8)' : '',
      // zIndex: dragging ? 10 : 1
      // position: dragAction === '' ? 'absolute' : 'fixed'
      left: left + 'px'
    }
    const defaultPosition = { x: left, y: isTop ? top : 0}
    // console.log('shortDay ：', shortDay, config, defaultPosition)
    const shortBar = shortDay.length ? shortDay.map((v, i) => <div className="short" style={{left: v.shortDate}} key={i}></div>) : null
    return (
      <Draggable axis={isAxis} bounds={bounds} 
      defaultPosition={defaultPosition} grid={[grid, grid]} 
      onDrag={this.handleDrag.bind(this, barcode)} {...dragHandlers}>
        <div className='box' ref="dragBar" style={style}>
          <div className="dragBorder dragLeft">
          <Draggable axis={isAxis}  onDrag={this.handleDrags.bind(this, 'dragLeft')}
            defaultPosition={{ x: 0, y: isTop ? top : 0}} grid={[grid, grid]}  {...{onStart: () => {
              console.log('onStartonStartonStartonStart ：', )
              
            },onStop: () => 1}}>
            <div className="dragLefts dragBars"></div>
          </Draggable></div>
          {/* txt:{txt},-left:{left},-width:{width},-top:{top},-levels:{levels} */}
          <Tooltip placement="top" title={txt}>
            <div onClick={this.barClick.bind(this)} className={`machineName ${ANIMATE.bounceIn}`}>
            {/* left:{left} width:{width} */}
              {txt}
              </div>
          </Tooltip>
          <div className="work"></div>
          {/* <div className="bars" onMouseDown={this.dbClick.bind(this, barcode)}></div> */}
          {/* <Draggable defaultPosition={defaultPosition} grid={[30, 30]} bounds="parent" onDrag={this.handleDrag.bind(this, barcode)} {...dragHandlers}>
              <span>aa</span>
            </Draggable> */}
          {shortBar}
          <div className="dragBorder dragRight">
          <Draggable axis={isAxis}  onDrag={this.handleDrags.bind(this, 'dragRight')} grid={[grid, grid]}  {...{onStop: () => 1}}>
            <div className="dragRights dragBars"></div>
          </Draggable></div>

        </div>
      </Draggable>
    );
  }
}

export default DragableBar;