import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"

import './style.less'
import {wipe, ajax} from 'util'
import {grid} from 'config'
import { ANIMATE,  } from 'constants'
import Draggable from 'react-draggable'
import { Tooltip, Popover, Button } from 'antd';
class DragableBar extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      dragAction: '', 
      activeDrags: 0,
      dragDirection: false,
      bounds: "parent",
      initPosition: {
        x: 0, y: 0
      },
      // 默认是不拖动的，当拖拽开始时才是dragging
      dragging: false,
    }
  }

  dbClick = (barcode) => {
    console.log('dbClick ：', barcode, this.props, );
    this.props.showBarcodeDialog(barcode)
  }
  widthAdd = (w) => {
    console.log('widthAdd ：', w);
    this.refs.dragBar.style.width = w + grid + 'px'
  }
  widthDecrease = (w) => {
    console.log('widthDecrease ：', w);
    this.refs.dragBar.style.width = w !== grid ? w - grid + 'px' : '60px'
  }

  handleDrag = (e, ui, d) => {
    this.setState({
      dragging: true,
    })
    const {dragAction} = this.state
    const barWidth = this.refs.dragBar.style.width
    const width = Number(wipe(barWidth))
    console.log('#bar拖动：', width, e, ui, dragAction, d.lastX , d.x, d, this.props);
    // this.props.short(this.props.config.barcode)
    if (dragAction !== '') {
      const isDragLeft = dragAction === 'dragLeft'
      // this.refs.dragBar.style.width = isDragLeft ? width - ui.deltaX + 'px' : width + ui.deltaX + 'px'
      if (isDragLeft) {
        console.log('左拖 ：', );
        if (d.lastX - d.x > 0) {
          console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
          this.widthAdd(width)
          // this.refs.dragBar.style.width = width + grid + 'px'
        } else {
          console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
          this.widthDecrease(width)
          // this.refs.dragBar.style.width = width !== grid ? width - grid + 'px' : '60px'
        }
      } else {
        console.log('右拖 ：', d.lastX, d.x);
        if (d.lastX - d.x < 0) {
          console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
          this.widthAdd(width)
          // this.refs.dragBar.style.width = width + grid + 'px'
        } else {
          console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
          this.widthDecrease(width)
          // this.refs.dragBar.style.width = width !== grid ? width - grid + 'px' : '60px'
        }
      }
    }
    const { x, y } = d;
    ajax(this.setState.bind(this), {
      dragging: true,
      initPosition: { x, y }
    })
    this.setState({
      dragging: true,
      initPosition: { x, y }
    })
  }

  dragAction = (v, a, d) => {
    const { x, y } = this.state.initPosition;
    const {dateLen} = this.props
    const {width, left} = this.props.config
    console.log('#bardragAction v ：', dateLen * grid, dateLen * grid - (left + width), x, y, width, left, v, a, d, this.state, this.props);
    // console.log('v ===ght:  ：', v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: y, right: left + width, bottom: 0})
    
    this.setState({
      dragAction: v,
      // bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : "parent",
      bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: 0, left: 0, right: left + width - grid, bottom: 0},
    })
  }
  onStart = (a, b) => {
    console.log('#bar a：', a, b, this.props, );
    const {disabled, } = this.props
    if (disabled) {
      return false
    } else {
      this.setState({ activeDrags: ++this.state.activeDrags,  });
    }
  }

  onStop = (onStop, e) => {
    const barWidth = this.refs.dragBar.style.width
    const {barcode} = this.props.config
    const {dragging} = this.state
    const {data, pn_no, clickType} = this.props
    const width = Number(wipe(barWidth)) - grid
    console.log('#@@@@@@@@@@@@@@@@@@@#bar onStop：', barcode, pn_no, this.props, onStop, e, width, '是否是拖动：', this.state.dragging);
    const {lastX, lastY} = e
    const x = lastX / grid
    const y = lastY / grid
    const w = x + width / grid
    if (clickType === 'barClick' && dragging) {
      ajax(this.props.stopDrag, {x, y, w, barcode, data, pn_no})
      // this.props.stopDrag(x, y, w, barcode)
    }
    this.setState({ 
      activeDrags: --this.state.activeDrags, 
      dragAction: '', 
      dragging: false,
      bounds: "parent",
    });
  }
  componentDidMount() {
    const { left, top } = this.props.config
    // console.log('componentDidMount ：',  this.props, )
  }

  barClick = (e) => {
    console.log('barClick ：', e, this.props)
    const {pn_no, clickType} = this.props
    const {barcode, txt} = this.props.config
    if (clickType === 'barClick') {
      const {work_no, } = this.props.data
      this.props.showBarcodeDialog(barcode, pn_no, work_no)
    } else {
      this.props.getPnDetailList(txt)
    }
  }

  render() {
    const { dragging, dragAction, bounds } = this.state;
    const { config, isAxis, isTop, disabled } = this.props
    const { left, width, top, txt, levels, barcode, shortDay, height } = config
    const dragHandlers = { onStart: dragAction === '' ? this.onStart : () => false, onStop: this.onStop };
    // console.log('DragableBar 组件 this.state, this.props ：', this.state, this.props, dragAction, dragAction === '' ? false : true)
    
    const style = {
      width: width + 'px',
      background: dragging ? 'rgba(111, 255, 228, 0.8)' : '',
      // zIndex: dragging ? 10 : 1
      // position: dragAction === '' ? 'absolute' : 'fixed'
      // left: 0 + 'px',
    }
    const defaultPosition = { x: left, y: isTop ? top : 0}
    // console.log('shortDay ：', shortDay, config, defaultPosition)
    const shortBar = shortDay.length ? shortDay.map((v, i) => <div className="short" style={{left: v.shortDate}} key={i}></div>) : null
    return (
      <Draggable disabled={dragAction === '' ? false : true} axis={isAxis} bounds={bounds} 
      defaultPosition={defaultPosition} grid={[grid, grid]} 
      onDrag={this.handleDrag.bind(this, barcode)} {...dragHandlers}>
        <div className='box' ref="dragBar" style={style}>
          <Tooltip placement="top" title={'点击开始拖拽改变生产日期'}>
            <div className="dragBorder dragLeft" onMouseDown={this.dragAction.bind(this, 'dragLeft')}>
            </div>
          </Tooltip>
          {/* <Tooltip placement="top" title={txt}> */}

          <Popover content={<span className="barPopover">
            {txt}
            <Button type="primary" onClick={this.barClick.bind(this)} className='detailbtn'  size='small'>
              详情
            </Button>
            </span>}
          >
            {/* <span onClick={this.barClick.bind(this)}>{txt}</span> */}
            <div className={`machineName ${ANIMATE.bounceIn}`}>{txt}</div>
          </Popover>
          
          {/* </Tooltip> */}
          <div className="work"></div>
          {shortBar}
          <Tooltip placement="top" title={'点击开始拖拽改变生产日期'}>
            <div className="dragBorder dragRight" 
              onMouseDown={this.dragAction.bind(this, 'dragRight')}
            ></div>
          </Tooltip>
        </div>
      </Draggable>
    );
  }
}

export default DragableBar;