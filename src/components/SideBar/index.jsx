import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {hashHistory, Link, } from 'react-router'
import { Layout, Menu, Icon } from 'antd';
import {confirms,  } from 'util'// 
const SubMenu = Menu.SubMenu;
const { Sider } = Layout;

class SideBar extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  jump = (path,   ) => {
    const {userInfo, } = this.props
    const paths = path.slice(1, path.length) + '/'
    console.log(' jump ： ',  path, paths, userInfo, userInfo.seeAuth, userInfo.seeAuth[paths] )
    if (userInfo.seeAuth[paths]) {
      hashHistory.push(path)
    } else {
      confirms(2, '對不起，你沒有查看該頁面的權限 o(*￣︶￣*)o ')
    }
  }
  render() {
    const {name, word, collapsed, path, userInfo, } = this.props
    console.log('SideBar组件 this.props：', name, this.props, userInfo.seeAuth, );
    
    let defaultOpenKeys
    if (name === 'base') {
      defaultOpenKeys = 'base'
    } else if (name === 'schedue') {
      defaultOpenKeys = 'schedue'
    } else if (name === 'report') {
      defaultOpenKeys = 'report'
    }
    return (
      <Sider
        className={collapsed ? '' : 'yScroll'}
        trigger={null}
        collapsible
        collapsed={collapsed}
      >
        {word}
        <Menu
          defaultSelectedKeys={['1']}
          selectedKeys={[path]}
          defaultOpenKeys={[defaultOpenKeys]}
          mode="inline"
          theme={this.props.theme}
          inlineCollapsed={this.state.collapsed}
        >
          <Menu.Item key="/page/indexs"><div className='' onClick={() => this.jump('/page/indexs')}><Icon type="home" /><span>首页</span></div></Menu.Item>
          
          <SubMenu key="base" title={<span><Icon type="folder-open" /><span>基础资料</span></span>}>
            <Menu.Item key="/page/procedure"><div className='' onClick={() => this.jump('/page/procedure')} ><Icon type="contacts" /><span>工序</span></div></Menu.Item>
            <Menu.Item key="/page/productLine"><div className='' onClick={() => this.jump('/page/productLine')}><Icon type="layout" /><span>生产线</span></div></Menu.Item> 
            <Menu.Item key="/page/standerdDay"><div className='' onClick={() => this.jump('/page/standerdDay')}><Icon type="calendar" /><span>标准天数</span></div></Menu.Item> 
            <Menu.Item key="/page/poArrange"><div className='' onClick={() => this.jump('/page/poArrange')}><Icon type="layout" /><span>批号信息</span></div></Menu.Item> 
          </SubMenu>
          <SubMenu key="schedue" title={<span><Icon type="layout" /><span>排单</span></span>}>
            <Menu.Item key="/page/artLine"><div className='' onClick={() => this.jump('/page/artLine')}><Icon type="switcher" /><span>工艺路线</span></div></Menu.Item> 
            <Menu.Item key="/page/manualNum"><div className='' onClick={() => this.jump('/page/manualNum')}><Icon type="line-chart" /><span>人工上數</span></div></Menu.Item> 
            <Menu.Item key="/page/manualPn"><div className='' onClick={() => this.jump('/page/manualPn')}><Icon type="line-chart" /><span>批量復期</span></div></Menu.Item> 
            <Menu.Item key="/page/schedue"><div className='' onClick={() => this.jump('/page/schedue')}><Icon type="schedule" /><span>排单</span></div></Menu.Item> 
            <Menu.Item key="/page/pnNo"><div className='' onClick={() => this.jump('/page/pnNo')}><Icon type="database" /><span>产线排单展示</span></div></Menu.Item> 
              {/* <Menu.Item key="/page/schedue"><div className='' onClick={() => this.jump('/page/schedue')}><Icon type="home" /><span>排单（批号）</span></div></Menu.Item> */}
              {/* <Menu.Item key="/page/pnNos"><div className='' onClick={() => this.jump('/page/pnNos')}><Icon type="home" /><span>排单</span></div></Menu.Item> */}
              {/* <Menu.Item key="/page/Nos"><div className='' onClick={() => this.jump('/page/Nos')}><Icon type="home" /><span>排单（批号）</span></div></Menu.Item> */}
              
              {/* <Menu.Item key="/page/Trees"><div className='' onClick={() => this.jump('/page/Trees')}><Icon type="home" /><span>树状</span></div></Menu.Item> */}
          </SubMenu>
          <SubMenu key="report" title={<span><Icon type="idcard" /><span>报表</span></span>}>
            <Menu.Item key="/page/productionSchedue"><div className='' onClick={() => this.jump('/page/productionSchedue')}><Icon type="line-chart" /><span>生産進度表</span></div></Menu.Item> 
            <Menu.Item key="/page/capcityScatter"><div className='' onClick={() => this.jump('/page/capcityScatter')}><Icon type="line-chart" /><span>产能分布</span></div></Menu.Item> 
          </SubMenu>

        </Menu>
      </Sider>
    );
  }
}

export default SideBar;