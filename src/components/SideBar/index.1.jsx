import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { Link } from 'react-router'
import { Layout, Menu, Icon } from 'antd';
const SubMenu = Menu.SubMenu;
const { Sider } = Layout;

class SideBar extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  render() {
    const {name, word, collapsed, path, userInfo, } = this.props
    console.log('SideBar组件 this.props：', name, this.props, userInfo.seeAuth, );
    
    let defaultOpenKeys
    if (name === 'base') {
      defaultOpenKeys = 'base'
    } else if (name === 'schedue') {
      defaultOpenKeys = 'schedue'
    } else if (name === 'report') {
      defaultOpenKeys = 'report'
    }
    return (
      <Sider
        className={collapsed ? '' : 'yScroll'}
        trigger={null}
        collapsible
        collapsed={collapsed}
      >
        {word}
        <Menu
          defaultSelectedKeys={['1']}
          selectedKeys={[path]}
          defaultOpenKeys={[defaultOpenKeys]}
          mode="inline"
          theme={this.props.theme}
          inlineCollapsed={this.state.collapsed}
        >
          <Menu.Item key="/page/indexs"><Link to={'/page/indexs'}><Icon type="home" /><span>首页</span></Link></Menu.Item>
          
          <SubMenu key="base" title={<span><Icon type="folder-open" /><span>基础资料</span></span>}>
            {userInfo.seeAuth[`page/procedure/`] ? <Menu.Item key="/page/procedure"><Link to={'/page/procedure'}><Icon type="contacts" /><span>工序</span></Link></Menu.Item> : null}  
            {userInfo.seeAuth[`page/productLine/`] ? <Menu.Item key="/page/productLine"><Link to={'/page/productLine'}><Icon type="layout" /><span>生产线</span></Link></Menu.Item> : null}  
            {userInfo.seeAuth[`page/standerdDay/`] ? <Menu.Item key="/page/standerdDay"><Link to={'/page/standerdDay'}><Icon type="calendar" /><span>标准天数</span></Link></Menu.Item> : null}  
            {userInfo.seeAuth[`page/poArrange/`] ? <Menu.Item key="/page/poArrange"><Link to={'/page/poArrange'}><Icon type="layout" /><span>批号信息</span></Link></Menu.Item> : null}  
          </SubMenu>
          <SubMenu key="schedue" title={<span><Icon type="layout" /><span>排单</span></span>}>
            {userInfo.seeAuth[`page/artLine/`] ? <Menu.Item key="/page/artLine"><Link to={'/page/artLine'}><Icon type="switcher" /><span>工艺路线</span></Link></Menu.Item> : null}  
            {userInfo.seeAuth[`page/manualNum/`] ? <Menu.Item key="/page/manualNum"><Link to={'/page/manualNum'}><Icon type="line-chart" /><span>人工上數</span></Link></Menu.Item> : null}  
            {userInfo.seeAuth[`page/manualPn/`] ? <Menu.Item key="/page/manualPn"><Link to={'/page/manualPn'}><Icon type="line-chart" /><span>批量排單</span></Link></Menu.Item> : null}  
            {userInfo.seeAuth[`page/schedue/`] ? <Menu.Item key="/page/schedue"><Link to={'/page/schedue'}><Icon type="schedule" /><span>排单</span></Link></Menu.Item> : null}  
            {userInfo.seeAuth[`page/pnNo/`] ? <Menu.Item key="/page/pnNo"><Link to={'/page/pnNo'}><Icon type="database" /><span>产线排单展示</span></Link></Menu.Item> : null}  
              {/* <Menu.Item key="/page/schedue"><Link to={'/page/schedue'}><Icon type="home" /><span>排单（批号）</span></Link></Menu.Item> */}
              {/* <Menu.Item key="/page/pnNos"><Link to={'/page/pnNos'}><Icon type="home" /><span>排单</span></Link></Menu.Item> */}
              {/* <Menu.Item key="/page/Nos"><Link to={'/page/Nos'}><Icon type="home" /><span>排单（批号）</span></Link></Menu.Item> */}
              
              {/* <Menu.Item key="/page/Trees"><Link to={'/page/Trees'}><Icon type="home" /><span>树状</span></Link></Menu.Item> */}
          </SubMenu>
          <SubMenu key="report" title={<span><Icon type="idcard" /><span>报表</span></span>}>
            {userInfo.seeAuth[`page/capcityScatter/`] ? <Menu.Item key="/page/capcityScatter"><Link to={'/page/capcityScatter'}><Icon type="line-chart" /><span>产能分布</span></Link></Menu.Item> : null}  
          </SubMenu>

        </Menu>
      </Sider>
    );
  }
}

export default SideBar;