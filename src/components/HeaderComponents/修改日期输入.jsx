import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {hashHistory} from 'react-router'
import {filterOptionForm, initParams, DATE_FORMAT_BAR, filterOptionDate, } from 'config'
import ProduceForm from '@@@/ProduceForm'
import DateInput from '@@@@/DateInput'
import YearInput from '@@@@/YearInput'
import { Icon, Layout, Button, Tooltip  } from 'antd';
import {dateFormat, confirms,   } from 'util'// 
import moment from 'moment';
const { Header } = Layout;

// const formItemLayout = {
//   labelCol: { span: 6},
//   wrapperCol: { span: 18 },
// };
const formItemLayout = {
  labelCol: { span: 6},
  wrapperCol: { span: 18 },
};


class HeaderComponents extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      collapsed: false,
      filterOptionDate,
    }
  }
  filterOption = () => {
    this.forms.validateFields((err, values) => {
      console.log('开始过滤filterOption ：', err, values, this.state, );
      const {filterOptionDate, } = this.state// 
      const isValid = filterOptionDate.every((v) => {
        // console.log(' isValid v ： ', v, this.isValid(v.initVal), dateFormat(v.initVal), moment(v.initVal).format(DATE_FORMAT_BAR) ) 
        return this.isValid(v.initVal)
      })
      console.log(' isValid isValid ： ', isValid,  ) 
      if (!isValid) {
        confirms(2, '日期非法，去請重新填寫 ！！！', )
        return  
      }
      values.shipment_datefr = moment(filterOptionDate[0].initVal).format(DATE_FORMAT_BAR)
      values.shipment_dateto = moment(filterOptionDate[1].initVal).format(DATE_FORMAT_BAR)
      values.pn_no = values.pn_no != undefined ? values.pn_no : ''
      values.customer = values.customer != undefined ? values.customer: ''
      values.style_no = values.style_no != undefined ? values.style_no : ''
      // values.shipment_datefr = values.shipment_datefr != undefined ? moment(values.shipment_datefr).format(DATE_FORMAT_BAR) : ''
      // values.shipment_dateto = values.shipment_dateto != undefined ? moment(values.shipment_dateto).format(DATE_FORMAT_BAR) : ''
      console.log('values.customer.split ：', values, {...initParams, ...values})
      // values.shipment_datefr = "2018-08-01"
      // values.shipment_dateto = "2018-10-31"
      // return  
      this.props.scheduleAction.filterInfo({...initParams, ...values, isSearch: true, })
    })
  }
  logout = (v) => {
    console.log('v  logout ：', v  )
    hashHistory.push(`/login`)
  }
  dateChange = (v) => {
    console.log(' dateChange ： ',  v )
    const {filterOptionDate, } = this.state// 
    this.setState({
      filterOptionDate: filterOptionDate.map((item) => {
        console.log(' filterOptionDate item ： ', v.keys, item.key, item,  ) 
        return v.keys === item.key ? {...item, initVal: v.v, } : item 
      })
    })
  }
  isValid = (v) => moment(v).isValid() && v.length === 8
  render() {
    const {
      isDragPage, 
      filterInfo, 
      customerData,
      styleNoData,
      collapsed,
      toggle, 
    } = this.props
    const {filterOptionDate, } = this.state// 
    console.log('Header 组件 this.state, this.props ：', this.state, this.props, filterOptionDate, )
    return (
      <Header className='headerWrapepr' >
        <Icon
          className="trigger"
          type={collapsed ? 'menu-unfold' : 'menu-fold'}
          onClick={toggle}
        />
        {
          isDragPage ? <div className="filterInfoWrapper">
            <span className='' >起止日期: </span>
            {
              filterOptionDate.map((v, i) => {
                const Com = v.com === 'Year' ? YearInput : DateInput//  
                return <div className='d-ib ' key={i} >
                  {/* <Com className={`dateInput f1`}  */}
                  <DateInput className={`dateInput f1`} 
                    val={v.initVal} keys={v.key}
                    onChange={(v) => this.dateChange(v)}// 
                  /> 
                  {i === 0 ? <span>&nbsp;&nbsp;&nbsp;</span> : ''}
                </div>
              })
            }
            <ProduceForm customerData={customerData} styleNoData={styleNoData} ref={forms => this.forms = forms}
              selectData={[]} formItemLayout={formItemLayout} formLayout={'horizontal'}
              init={filterInfo} config={filterOptionForm} className='filterForm' 
            ></ProduceForm>
            <div className="filterBtn">
              <Button onClick={this.filterOption} icon='smile-o' className='f-r' type="primary">过滤</Button>
            </div>
          </div> : <div className="logoutWrapper">
            <Tooltip title="退出登錄">
              <Icon type="poweroff" onClick={this.logout} />
            </Tooltip>
          </div>
        }
      </Header>
    );
  }
}

export default HeaderComponents;