import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {hashHistory} from 'react-router'
import {filterOptionForm, initParams, DATE_FORMAT_BAR, filterOptionDate, } from 'config'
import ProduceForm from '@@@/ProduceForm'
import DateInput from '@@@@/DateInput'
import YearInput from '@@@@/YearInput'
import DatesInput from '@@@@/DatesInput'
import { Icon, Layout, Button, Tooltip  } from 'antd';
import {dateFormat, confirms,   } from 'util'// 
import moment from 'moment';
const { Header } = Layout;

// const formItemLayout = {
//   labelCol: { span: 6},
//   wrapperCol: { span: 18 },
// };
const formItemLayout = {
  labelCol: { span: 6},
  wrapperCol: { span: 18 },
};


class HeaderComponents extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      collapsed: false,
      filterOptionDate,
    }
  }
  filterOption = () => {
    this.forms.validateFields((err, values) => {
      const {filterOptionDate, } = this.state// 
      console.log('开始过滤filterOption ：', err, values, this.state, filterOptionDate, );
      // // const isValid = filterOptionDate.every((v) => {
      // //   console.log(' v ： ', v,  )// 
      // //   // console.log(' isValid v ： ', v, this.isValid(v.initVal), dateFormat(v.initVal), moment(v.initVal).format(DATE_FORMAT_BAR) ) 
      // //   return this.isValid(v.initVal)
      // // })
      // const lenRight = filterOptionDate.some(v => {
      //   // console.log(' v.initVal.replac ： ', v.initVal.replace('-', ''), v.initVal.replace('-', '').length, v.initVal.replace('-', '').length !== 4,  )// 
      //   return v.initVal.replace('-', '').length !== 4 
      // })
      // console.log(' 日期是否非法 lenRight ： ', lenRight,  )// 
      // if (lenRight) {
      //   confirms(2, '日期非法，請檢查 ！！！', )
      //   return  
      // }
      
      // const date1 = dateFormat(filterOptionDate[0].initVal + filterOptionDate[1].initVal.replace('-', ''))
      // const date2 = dateFormat(filterOptionDate[2].initVal + filterOptionDate[3].initVal.replace('-', ''))
      const date1 = filterOptionDate[0].initVal
      const date2 = filterOptionDate[1].initVal
      console.log(' date1 ： ', date1, date2,  )// 
      const isValid = [date1, date2, ].every((v, i) => {
        // console.log('  isValid v ： ', v, i, moment(v).isValid(),  )
        return moment(v).isValid()
      })
      console.log(' isValid isValid ： ', isValid, moment(date2).isAfter(date1) ) 
      if (!moment(date2).isAfter(date1)) {
        confirms(2, '搜索起止日期不正確，請檢查 ！！！', )
        return  
      }
      
      if (!isValid) {
        confirms(2, '日期非法，請重新填寫 ！！！', )
        return  
      }
      
      // values.shipment_datefr = moment(filterOptionDate[0].initVal).format(DATE_FORMAT_BAR)
      // values.shipment_dateto = moment(filterOptionDate[1].initVal).format(DATE_FORMAT_BAR)
      values.shipment_datefr = moment(date1).format(DATE_FORMAT_BAR)
      values.shipment_dateto = moment(date2).format(DATE_FORMAT_BAR)
      values.pn_no = values.pn_no != undefined ? values.pn_no : ''
      values.customer = values.customer != undefined ? values.customer: ''
      values.style_no = values.style_no != undefined ? values.style_no : ''
      // values.shipment_datefr = values.shipment_datefr != undefined ? moment(values.shipment_datefr).format(DATE_FORMAT_BAR) : ''
      // values.shipment_dateto = values.shipment_dateto != undefined ? moment(values.shipment_dateto).format(DATE_FORMAT_BAR) : ''
      console.log('values.customer.split ：', values, {...initParams, ...values})
      // values.shipment_datefr = "2018-08-01"
      // values.shipment_dateto = "2018-10-31"
      // return  
      this.props.scheduleAction.filterInfo({...initParams, ...values, isSearch: true, })
    })
  }
  logout = (v) => {
    console.log('v  logout ：', v  )
    hashHistory.push(`/login`)
  }
  dateChange = (v) => {
    console.log(' dateChange ： ',  v )
    const {date, filterOptionDate, } = this.state// 
    this.setState({
      filterOptionDate: filterOptionDate.map((item) => {
        // console.log(' filterOptionDate item ： ', v.keys, item.key, item, v.keys === item.key, v.keys === item.key ? {...item, initVal: v.v, } : item ,  ) 
        return v.keys === item.key ? {...item, initVal: v.v, } : item 
      })
    })
  }
  onChange = (e, key,  ) => {
    // console.log('onChange v ：', e, e.target.value, this.date.value, this.date.name, key );
    // this.setState({
    //   value: e.target.value,
    // })
    const {date, filterOptionDate, } = this.state// 
    this.setState({
      filterOptionDate: filterOptionDate.map((item) => {
        // console.log(' key === item.key ? {...item, initVal: e.target.value, } : item  ： ', key, item.key, key === item.key, key === item.key ? {...item, initVal: e.target.value, } : item, item , )// 
        return key === item.key ? {...item, initVal: e.target.value, } : item  
      })
    })
  }
  isValid = (v) => moment(v).isValid() && v.length === 8
  render() {
    const {
      isDragPage, 
      filterInfo, 
      customerData,
      styleNoData,
      collapsed,
      toggle, 
    } = this.props
    const {filterOptionDate, } = this.state// 
    console.log('Header 组件 this.state, this.props ：', this.state, this.props, filterOptionDate, )
    return (
      <Header className='headerWrapepr' >
        <Icon
          className="trigger"
          type={collapsed ? 'menu-unfold' : 'menu-fold'}
          onClick={toggle}
        />
        {
          isDragPage ? <div className="filterInfoWrapper">
            <span className='label' >起止日期: </span>
            <div className='flexCenter'>
              {
                filterOptionDate.map((v, i) => {
                  // console.log(' v.initVal ： ', v.initVal, v.key,  )// 
                  // const Com = v.com === 'Year' ? YearInput : DateInput//  
                  // const Com = YearInput//  
                  // const Com = DatesInput//  
                  // const Com = v.com === 'Year' ? YearInput : DatesInput//  
                  // return <div className='d-ib m-r5 ' key={v.key} >
                  //   <Com className={`dateInput f1`} 
                  //     val={v.initVal} keys={v.key}
                  //     onChange={(v) => this.dateChange(v)}// 
                  //   /> 
                  //   {i === 1 ? <span>&nbsp;-&nbsp;</span> : ''}
                  // </div>
                  // return <input type="date" ref={date => this.date = date} key={v.key} defaultValue={'2018-09-09'} ref={date => this.date = date} onChange={(e) => this.onChange(e)} />
                  return <input className={`antDateInput ${i === 0 ? 'm-r5' : ''}`} type="date" key={v.key} defaultValue={v.initVal} onChange={(e) => this.onChange(e, v.key, )} />
                })
              }
            </div>
            <ProduceForm customerData={customerData} styleNoData={styleNoData} ref={forms => this.forms = forms}
              selectData={[]} formItemLayout={formItemLayout} formLayout={'horizontal'}
              init={filterInfo} config={filterOptionForm} className='filterForm' 
            ></ProduceForm>
            <div className="filterBtn">
              <Button onClick={this.filterOption} icon='smile-o' className='f-r' type="primary">过滤</Button>
            </div>
          </div> : <div className="logoutWrapper">
            <Tooltip title="退出登錄">
              <Icon type="poweroff" onClick={this.logout} />
            </Tooltip>
          </div>
        }
      </Header>
    );
  }
}

export default HeaderComponents;