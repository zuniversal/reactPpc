const option = (data, ) => {
    // console.log('data ：', data)
    const legend = {
        data: [],
    }
    const series = []
    for (let i = 0, len = data.data.length; i < len; i++) {
        const {legendTxt, seriesData} = data.data[i]
        legend.data.push(legendTxt)
        series.push({
            name: legendTxt,
            type: 'bar',
            barWidth: 19,
            data: seriesData,
            label: {
                normal: {
                    show: true,
                    position: 'inside'
                }
            },
        })
    }
    // console.log('legend,  ：', legend, series)
    return {
        title: {
            show: true, 
            text: data.title,
        },
        textStyle: {
            color: 'black', 
            // fontWeight: 'bolder',
            fontSize: 16
        },
        axisLabel: {
            // color: 'red', 
            fontWeight: 'bolder',
            fontSize: 18
        },

        color: ['red', '#4e86ff', '#4cabce', '#e5323e'],
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                crossStyle: {
                    color: '#999'
                }
            }
        },
        toolbox: {
            feature: {
                dataView: { show: true, readOnly: false },
                magicType: { show: true, type: ['bar', 'line', 'stack', 'tiled'] },
                restore: { show: true },
                saveAsImage: { show: true }
            }
        },
        legend,
        xAxis: [
            {
                type: 'category',
                data: data.legend,
                axisPointer: {
                    type: 'shadow'
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '數量',
                axisLabel: {
                    formatter: '{value}'
                }
            },
            {
                type: 'value',
                name: '趨勢',
                axisLabel: {
                    formatter: '{value}'
                }
            }
        ],
        series
    }
};

export default option;