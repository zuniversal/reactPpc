import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import Inputs from '@@@@/Inputs'
import InputsNumber from '@@@@/InputsNumber'
import LimitDate from '@@@@/LimitDate'
import DateInput from '@@@@/DateInput'
import YearInput from '@@@@/YearInput'
import DatesInput from '@@@@/DatesInput'
import SecureHoc from '@@@@/SecureHoc'
import {dateForm, ajax, confirms, dateSplits, backupFn, } from 'util'
import { ANIMATE, } from 'constants'
import { DATE_FORMAT, pnDateArr, tagColorArr, } from 'config'
import { Table, Tag, Button, Icon, DatePicker, Select, InputNumber, Input, Switch,  } from 'antd';
import moment from 'moment';
const { RangePicker } = DatePicker;
const Option = Select.Option;

class ManualBarcodeTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    function showTotal(total) {
      return `Total ${total}`;
    }
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = 6
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      loading: false, 
      selectedRowKeys: [],
    };
  }
  render() {
    const { pagination, loading, selectedRowKeys,  } = this.state
    const {data, showPnIndex,  } = this.props// 
    console.log(' %c ManualBarcodeTable 组件 this.state, this.props ： ', `color: #333; font-weight: bold`, this.state, this.props,  )// 
    const rowKey = 'barcode'
    const columns = [
      // {
      //   title: '條碼', dataIndex: 'barcode',
      //   render: (t, v, i) => <Tag color={'blue'}>{t}</Tag>
      // },
      {
        title: '生產線', dataIndex: 'factory_name',
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '工序', dataIndex: 'work_name',
        render: (t, v, i) => {
          const matchItem = tagColorArr.find(item => item.work_no == v.work_no)
          return v.work_no !== 0 ? <Tag className={`${ANIMATE.bounceIn}`} color={matchItem != undefined ? matchItem.color : 'green'} >{t}</Tag> : null 
        }
      },
      {
        title: '數量', dataIndex: 'barcode_qty',
        sorter: (a, b) => a.barcode_qty - b.barcode_qty,
        render: (t, v, i) => <span>{t}</span>
      },

      {
        title: '開始日期', dataIndex: 's_date',
        render: (t, v, i) => <span>{v.s_date}</span>
      },
      {
        title: '結束日期', dataIndex: 'e_date',
        render: (t, v, i) => <span>{v.e_date}</span>
      },
      {
        title: '理由', dataIndex: 'reason',
        render: (t, v, i) => <span>{v.reason}</span>
      },
      {
        title: '操作', dataIndex: 'color',
        render: (t, v, i) => {
          // console.log(' t, v, i ： ', t, v, i,  )// 
          return <div>
            <Button onClick={() => this.props.startEditDay(t, v, i, showPnIndex, )} icon='edit' type="primary" >修改</Button>
            {/* <Button onClick={() => this.props.toggleLock(t, v, i)} icon='save' type="primary" className={'sub'}>切換鎖定</Button> */}
            <Switch defaultChecked={v.lock === 'T'} checkedChildren="解鎖" unCheckedChildren="鎖定" onChange={() => this.props.toggleLock(t, v, i)} />
          </div>
        }
      }
    ]

    return (
      <Table 
        columns={columns}
        loading={loading}
        dataSource={data}
        // pagination={pagination}
        pagination={false}
        // onRowClick={this.onRowClick}
        // onChange={this.handleTableChange}
        rowKey={record => record[rowKey]}
        rowClassName={(record, i) => `${ANIMATE.zoomIn} ${record.lock === 'T' ? 'disableBg' : ''}`}
        className='  m-b20' 
      />
    );
  }
}


// export default ManualBarcodeTable
export default SecureHoc(ManualBarcodeTable)

