import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {showTotal, dateForm, ajax, infoFilter, } from 'util'
import { ANIMATE, SIZE, } from 'constants'
import { DATE_FORMAT, poInfoConfig, } from 'config'
import Count from '@@@@/Count'
import InputsNumber from '@@@@/InputsNumber'
import { Table, Tag, Button, Icon, DatePicker, Select, Checkbox,  } from 'antd';
import moment from 'moment';

class ColorSizeTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      selectedRowKeys: [],
    };
  }
  onSelectChange = (selectedRowKeys) => {
    const {data, } = this.state 
    console.log('selectedRowKeys changed: ', selectedRowKeys, data, );
    // !selectedRowKeys.length && data.forEach((v) =>v.data.forEach((item) => item.status = 'F'))
    // selectedRowKeys.length === data.length && data.forEach((v) =>v.data.forEach((item) => item.status = 'T'))
    
    // const newData = !selectedRowKeys.length ? data.map((v) => ({...v, isCheckAll: 'F', data: v.data.map(item => {
    //   item.status = 'F'
    //   return item
    // })})) :
    // selectedRowKeys.length === data.length ? data.map((v) => ({...v, isCheckAll: 'T', data: v.data.map(item => {
    //   item.status = 'T'
    //   return item
    // })}))
    // : data.map((v) => ({...v, isCheckAll: selectedRowKeys.some(item => item === v.combo_seq) ? 'T' : 'F'}))
    if (!selectedRowKeys.length) {
      data.forEach(v => {
        v.isCheckAll = 'F'
        v.data.forEach(item => item.status = 'F')
      })
    } else if (selectedRowKeys.length === data.length) {
      data.forEach(v => {
        v.isCheckAll = 'T'
        v.data.forEach(item => item.status = 'T')
      })
    } else {
      data.forEach(v => {
        v.isCheckAll = selectedRowKeys.some(item => item === v.combo_seq) ? 'T' : 'F'
        v.data.forEach(item => item.status = v.isCheckAll)
      })
    }
    this.setState({ 
      selectedRowKeys, 
      data: [...data],
      // data: newData, 
      // data: data.map((v) => ({...v, isCheckAll: selectedRowKeys.some(item => item === v.combo_seq) ? 'T' : 'F'})),
    });
  }
  // inputing = (p,) => {
  //   console.log(' inputing ： ', p, )
  //   const {data, } = this.state 
  //   const {val, v} = p
  //   const {combo_seq, } = val 
  //   this.setState({
  //     data: data.map((item) => ({...item, qty: item.combo_seq === combo_seq ? v : item.qty})),
  //   })
  // }
  inputing = (p,) => {
    console.log(' inputing ： ', p, )
    const {data, } = this.state 
    const {val, combo_seq, size_id, v, } = p
    this.setState({
      data: data.map((item) => {
        // console.log(' item ： ', item, item.combo_seq, item.size_id, item.combo_seq === combo_seq, item.size_id === size_id )// 
        return {...item, data: item.data.map((items) => {
          // console.log(' items d： ', items, items.combo_seq, items.size_id, combo_seq, size_id, items.size_id === size_id, items.combo_seq === combo_seq, items.combo_seq === combo_seq && items.size_id === size_id ? v : items.pn_qty, ) 
          return {...items, qty: items.combo_seq === combo_seq && items.size_id === size_id ? v : items.pn_qty}
        })} 
      }),
    })
  }
  onChange = (o, items, c) => {
    const { data,  } = this.state
    console.log('checked = ', data, o, items, c.target.checked);
    const newData = data.map(v => {
      return {...v, status: v.data.map(item => {
        if (items.combo_seq === item.combo_seq && items.size_id === item.size_id) {
          console.log(' 相等 ： ',  )
          item.status = item.status === 'T' ? 'F' : 'T'
          return item
        }
      })}
    })
    // console.log(' newData ： ', newData,  )
    newData.forEach((v, i) => v.isCheckAll = v.data.every(v => v.status === 'T') ? 'T' : 'F')
    const selectedRowKeys = newData.filter((v, i) => v.isCheckAll === 'T').map(v => v.combo_seq)
    console.log(' selectedRowKeys ： ', newData, selectedRowKeys,  )
    this.setState({
      data: [...newData, ],
      selectedRowKeys,
    });
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    // const selectedRowKeys = data.filter((v, i) => v.isCheckAll === 'T')
    // data.forEach((v, i) => v.isCheckAll === 'T' && selectedRowKeys.push(v.combo_seq))
    // const selectedRowKeys = data.filter((v, i) => v.data.every(item => item.status === 'T')).map(v => v.combo_seq)
    const selectedRowKeys = data.filter((v, i) => v.data.every(item => item.qty !== 0)).map(v => v.combo_seq)
    console.log('ss componentDidMount 组件 this.state, this.props ：', this.state, this.props, data, selectedRowKeys, data.filter((v, i) => v.isCheckAll === 'T'), data.filter((v, i) => v.data.some(item => item.qty !== 0)),  )
    this.setState({
      data, 
      // data: data.filter((v, i) => v.data.every(item => item.qty !== 0)), 
      // data: data.filter((v, i) => v.data.some(item => item.qty !== 0)), 
      loading,
      selectedRowKeys,
    });
  }
  render() {
    const { pagination, loading, selectedRowKeys, data,  } = this.state
    const {residueColorData, } = this.props// 
    // const {data, barcode,  } = this.props
    // const {colorQtyObj,  } = this.props 
    console.log('ColorSizeTable 组件 this.state, this.props ：', data, this.state,  this.props,  )
    // data.forEach(v => {
    //   console.log(' ColorSizeTable ： ', v )// 
    //   return v .qtys !== 0 
    // })
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      onSelection: this.onSelection,
    };
    const rowKey = 'combo_seq'
    const columns = [
      {
        title: '顔色', dataIndex: 'combo_id',
        render: (v, o, i) => <Tag color={'#108ee9'}>{v}</Tag>
      },
      {
        title: '碼數列', dataIndex: 'data',
        render: (v, o, i) => <span>
          {
            v.map((items, i) => {
              // console.log(' items ： ', items,  )// items.status === 'T'
              const matchItem = residueColorData.find(v => v.combo_seq === items.combo_seq && v.size_id === items.size_id)
              console.log(' matchItem ： ', matchItem, items.qty !== 0,   )// 
              return (items.qty !== 0 && items.status === 'T') || (matchItem) ? <span key={items.size_id}>
              <Checkbox className='m-r5' 
                // checked={v.isCheckAll || items.status === 'T' || items.qty !== 0}
                checked={v.isCheckAll || items.qty !== 0}
                onChange={c => this.onChange(o, items, c)}
              ></Checkbox>
              <Tag color={'purple'}>{items.size_id}</Tag>  
              <Tag color={'#f50'}><Count end={items.pn_qty} duration={1}/></Tag>
              <InputsNumber keys='qty' max={items.pn_qty} val={{...items, }} data={v}
               combo_seq={items.combo_seq} size_id={items.size_id}
               className='max60 d-ib m-r20' inputing={this.inputing}></InputsNumber>
            </span> : null // ? : null
            }) 
          }
        </span>
      },
      {
        title: '生産數', dataIndex: 'qtys',
        render: (v, o, i) => <Count end={v} duration={1}/>
      },
      {
        title: '已排生産數', dataIndex: 'sch_qty',
        render: (v, o, i) => {
          // console.log(' v, o, i ： ', v, o, i,  )
          return <Count end={o.data.reduce((total, cur) => {
            // console.log(' cur ： ', total, cur,  )
            total += cur.qty
            return total
          }, v)} duration={1}/>
        }
      },
      // {
      //   title: '生産數', dataIndex: 'pn_qty',
      //   render: (v, o, i) => <Count end={v} duration={2}/>
      // },
      // {
      //   title: '已排生産數', dataIndex: 'sch_qty',
      //   render: (v, o, i) => <Count end={colorQtyObj[o.combo_seq]} duration={2}/>
      // },
      // {
      //   title: '排單數', dataIndex: 'qty',
      //   render: (v, o, i) => {
      //     // console.log(' v, o, i  ： ', v, o, i, colorQtyObj,  )//
      //     return <InputsNumber keys='qty' 
      //     // max={splitDataLen > 1 ? v.pn_qty - colorQtyObj[v.combo_seq] : Infinity}
      //     max={o.pn_qty - colorQtyObj[o.combo_seq]}
      //     val={{...o, }}
      //     //val={{...o, qty: o.pn_qty - colorQtyObj[o.combo_seq]}} 
      //    i={i} inputing={this.inputing} data={v}></InputsNumber>
      //   }
      // },
    ]
    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}// 
        // dataSource={data != undefined ? data.filter(v => v .qtys !== 0 ) : data}
        pagination={false}
        rowKey={record => record[rowKey]}
        rowClassName={(record, i) => ANIMATE.bounceIn}
        rowSelection={rowSelection}
        className='colorTable ' 
      />
    );
  }
}

export default ColorSizeTable