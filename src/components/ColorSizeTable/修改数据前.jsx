import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {showTotal, dateForm, ajax, infoFilter, } from 'util'
import { ANIMATE, SIZE, } from 'constants'
import { DATE_FORMAT, poInfoConfig, } from 'config'
import Count from '@@@@/Count'
import InputsNumber from '@@@@/InputsNumber'
import { Table, Tag, Button, Icon, DatePicker, Select,  } from 'antd';
import moment from 'moment';

class ColorSizeTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      selectedRowKeys: [],
    };
  }
  onSelectChange = (selectedRowKeys) => {
    const {data, } = this.state 
    console.log('selectedRowKeys changed: ', selectedRowKeys, data, );
    this.setState({ 
      selectedRowKeys, 
      data: data.map((v) => ({...v, status: selectedRowKeys.some(item => item === v.combo_seq) ? 'T' : 'F'})),
    });
  }
  inputing = (p,) => {
    console.log(' inputing ： ', p, )
    const {data, } = this.state 
    const {val, v} = p
    const {combo_seq, } = val 
    this.setState({
      data: data.map((item) => ({...item, qty: item.combo_seq === combo_seq ? v : item.qty})),
    })
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    const selectedRowKeys = data.filter((v, i) => v.status === 'T')
    data.forEach((v, i) => v.status === 'T' && selectedRowKeys.push(v.combo_seq))
    console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data, selectedRowKeys)
    this.setState({
      data, 
      loading,
      selectedRowKeys,
    });
  }
  render() {
    const { pagination, loading, selectedRowKeys, data,  } = this.state
    // const {data, barcode,  } = this.props
    const {colorQtyObj,  } = this.props 
    console.log('ColorSizeTable 组件 this.state, this.props ：', data, this.state,  this.props,  )
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      onSelection: this.onSelection,
    };
    const rowKey = 'combo_seq'
    const columns = [
      {
        title: '顔色', dataIndex: 'combo_id',
        render: (v, o, i) => <Tag color={'#f50'}>{v}</Tag>
      },
      {
        title: '生産數', dataIndex: 'pn_qty',
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
      {
        title: '已排生産數', dataIndex: 'sch_qty',
        render: (v, o, i) => <Count end={colorQtyObj[o.combo_seq]} duration={2}/>
      },
      {
        title: '排單數', dataIndex: 'qty',
        render: (v, o, i) => {
          // console.log(' v, o, i  ： ', v, o, i, colorQtyObj,  )//
          return <InputsNumber keys='qty' 
          // max={splitDataLen > 1 ? v.pn_qty - colorQtyObj[v.combo_seq] : Infinity}
          max={o.pn_qty - colorQtyObj[o.combo_seq]}
          val={{...o, }}
          //val={{...o, qty: o.pn_qty - colorQtyObj[o.combo_seq]}} 
         i={i} inputing={this.inputing} data={v}></InputsNumber>
        }
      },
    ]
    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={false}
        rowKey={record => record[rowKey]}
        rowClassName={(record, i) => ANIMATE.bounceIn}
        rowSelection={rowSelection}
        className='colorTable noMgTag' 
      />
    );
  }
}

export default ColorSizeTable