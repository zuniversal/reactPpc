import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {showTotal, dateForm, ajax, infoFilter, backupFn,   } from 'util'
import { ANIMATE, SIZE, SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS, } from 'constants'
import { DATE_FORMAT, poInfoConfig, } from 'config'
import Count from '@@@@/Count'
import InputsNumber from '@@@@/InputsNumber'
import { Table, Tag, Button, Icon, DatePicker, Select, Checkbox,  } from 'antd';
import moment from 'moment';

class ColorSizeTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      selectedRowKeys: [],
    };
  }
  onSelectChange = (selectedRowKeys) => {
    const {data, } = this.state 
    const {residueColorData, } = this.props// 
    console.log('selectedRowKeys changed: ', selectedRowKeys, data, );
    if (!selectedRowKeys.length) {
      console.log(' 没有 ： ',  )// 
      data.forEach(v => {
        v.isCheckAll = 'F'
        // v.data.forEach(item => item.status = 'F')
        // this.changeStatus(v, v.isCheckAll, )
        this.changeStatus(v, v.isCheckAll, selectedRowKeys,  )
      })
    } else if (selectedRowKeys.length === data.length) {
      console.log(' 有length11 ： ',  )// 
      data.forEach(v => {
        v.isCheckAll = 'T'
        // v.data.forEach(item => item.status = 'T')
        this.changeStatus(v, v.isCheckAll, selectedRowKeys, true, )
      })
    } else {
      console.log(' 其他 ： ',  )// 
      data.forEach(v => {
        v.isCheckAll = selectedRowKeys.some(item => item === v.combo_seq) ? 'T' : 'F'
        // v.data.forEach(item => item.status = v.isCheckAll)
        this.changeStatus(v, v.isCheckAll, selectedRowKeys, true, )
      })
    }
    this.setState({ 
      selectedRowKeys, 
      data: [...data],
    });
  }
  changeStatus = (v, status, selectedRowKeys, isAll, ) => {
    console.log(' changeStatus ： ', v, status, selectedRowKeys, isAll, this.state, this.props,   )
    const {residueColorData, } = this.props// 
    const everySame = v.data.every(v => v.status !== status) 
    v.data.forEach(item => {
      const matchItem = residueColorData.find(v => v.combo_seq === item.combo_seq && v.size_id === item.size_id)
      // console.log('  item.qty,  ： ',  item.qty,  matchItem  )// 
      // item.status = isAll ? status : everySame ? status : item.status
      // console.log('  selectedRowKeys) ： ',  selectedRowKeys.some(v => v=== item.combo_seq), matchItem, item.qty,  )// 
      item.status = selectedRowKeys.some(v => v=== item.combo_seq) || everySame ? status : item.status
      item.qty = item.status === 'T' && matchItem && item.qty === 0 ? matchItem.qty : item.qty
      // console.log(' matchItem ： ', matchItem, )// 
    })
  }
  // inputing = (p,) => {
  //   console.log(' inputing ： ', p, )
  //   const {data, } = this.state 
  //   const {val, v} = p
  //   const {combo_seq, } = val 
  //   this.setState({
  //     data: data.map((item) => ({...item, qty: item.combo_seq === combo_seq ? v : item.qty})),
  //   })
  // }
  inputing = (p,) => {
    console.log(' inputing ： ', p, )
    const {data, } = this.state 
    const {val, combo_seq, size_id, v, } = p
    this.setState({
      data: data.map((item) => {
        // console.log(' item ： ', item, item.combo_seq, item.size_id, item.combo_seq === combo_seq, item.size_id === size_id )// 
        return {...item, data: item.data.map((items) => {
          // console.log(' items dpn_： ', items, items.combo_seq, items.size_id, combo_seq, size_id, items.size_id === size_id, items.combo_seq === combo_seq, items.combo_seq === combo_seq && items.size_id === size_id ? v : items.pn_qty, ) 
          return {...items, qty: items.combo_seq === combo_seq && items.size_id === size_id ? v : items.qty}
        })} 
      }),
    })
  }
  onChange = (o, items, c, qty, ) => {
    const { data,  } = this.state
    console.log('checked = ', o, items, c.target.checked, qty, );
    const newData = data.map(v => {
      return {...v, status: v.data.map(item => {
        if (items.combo_seq === item.combo_seq && items.size_id === item.size_id) {
          // console.log(' 相等 ： ',  )
          item.status = item.status === 'T' ? 'F' : 'T'
          item.qty = qty
          return item
        }
      })}
    })
    // console.log(' newData ： ', newData,  )
    // newData.forEach((v, i) => v.isCheckAll = v.data.filter((v, i) => v.qty !== 0).every(v => v.status === 'T') ? 'T' : 'F')
    newData.forEach((v, i) => v.isCheckAll = v.data.filter((v, i) => v.qty !== 0).every((v, i) => v.qty !== 0 && v.status === 'T') ? 'T' : 'F')
    const selectedRowKeys = newData.filter((v, i) => v.isCheckAll === 'T').map(v => v.combo_seq)
    console.log(' selectedRowKeys ： ', newData, selectedRowKeys,  )
    this.setState({
      data: newData,
      selectedRowKeys,
    });
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    // const selectedRowKeys = data.filter((v, i) => v.isCheckAll === 'T')
    // data.forEach((v, i) => v.isCheckAll === 'T' && selectedRowKeys.push(v.combo_seq))
    // const selectedRowKeys = data.filter((v, i) => v.data.every(item => item.status === 'T')).map(v => v.combo_seq)
    // const datas = backupFn(data)
    const datas = backupFn(data).map((v) => ({...v, 
      data: v.data.map(item => ({...item, 
        qtyBk: item.qty, })
        )
      })
    )
    const selectedRowKeys = datas.filter((v, i) => v.data.every(item => item.qty !== 0 && item.status === 'T')).map(v => v.combo_seq)
    console.log(' ColorSizeTable  组componentDidMount 件 this.state, this.props ：', this.state, this.props, data, selectedRowKeys, data.filter((v, i) => v.isCheckAll === 'T'),   )
    this.setState({
      data: datas, 
      // data: data.map((v, i) => ({...v, isCheckAll: v.data.some(item => item.status === 'T')  ? 'T' : 'F'})), 
      loading,
      selectedRowKeys,
    });
  }
  render() {
    const { pagination, loading, selectedRowKeys, data,  } = this.state
    const {residueColorData, } = this.props// 
    // const {data, barcode,  } = this.props
    // const {colorQtyObj,  } = this.props 
    console.log('ColorSizeTable 组件 this.state, this.props ：', data, this.state,  this.props,  )
    // data.forEach(v => {
    //   console.log(' ColorSizeTable ： ', v )// 
    //   return v .qtys !== 0 
    // })
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      onSelection: this.onSelection,
    };
    const rowKey = 'combo_seq'
    const columns = [
      {
        title: '顔色', dataIndex: 'combo_id',
        render: (v, o, i) => <Tag color={'#108ee9'}>{v}</Tag>
      },
      {
        title: '碼數列', dataIndex: 'data',
        render: (v, o, i) => <span>
          {
            v.map((items, i) => {
              // console.log(' items ： ', items,  )// items.status === 'T' 
              const matchItem = residueColorData.find(v => v.combo_seq === items.combo_seq && v.size_id === items.size_id)
              const relQyt = matchItem && items.qty === 0 ? matchItem.qty : items.qtyBk
              const max = matchItem ? matchItem.qty + items.qtyBk : items.qtyBk
              // console.log(' matchItem items.qty !== 0 || (matchItem), ： ', max, matchItem, items.qty, items.qtyBk, items.qty !== 0, relQyt )// 
              // return items.qty !== 0 || (matchItem) ? <span key={items.size_id}>
              return !items.isUseOut || (matchItem) ? <span key={items.size_id}>
              <Checkbox className='m-r5' 
                checked={v.isCheckAll || items.status === 'T'}
                onChange={c => this.onChange(o, items, c, matchItem ? matchItem.qty: items.qty)}
              ></Checkbox>
              <Tag color={'purple'}>{items.size_id}</Tag>  
              <Tag color={'#f50'}><Count end={items.pn_qty} duration={1}/></Tag>
              <InputsNumber keys='qty' max={items.pn_qty} val={{...items,  
                // qty: relQyt,
              }} data={v}
               combo_seq={items.combo_seq} size_id={items.size_id} max={max}
               className='max60 d-ib m-r20' inputing={this.inputing}></InputsNumber>
            </span> : null // ? : null
            }) 
          }
        </span>
      },
      {
        title: '生産數', dataIndex: 'qtys',
        render: (v, o, i) => <Count end={v} duration={1}/>
      },
      {
        title: '已排生産數', dataIndex: 'sch_qty',
        // render: (v, o, i) => {
        //   // console.log(' v, o, i ： ', v, o, i,  )
        //   return <Count end={o.data.reduce((total, cur) => {
        //     // console.log(' cur ： ', total, cur,  )
        //     total += cur.qty
        //     return total
        //   }, v)} duration={1}/>
        // },
        render: (v, o, i) => <Count end={v} duration={1}/>
      },
      // {
      //   title: '生産數', dataIndex: 'pn_qty',
      //   render: (v, o, i) => <Count end={v} duration={2}/>
      // },
      // {
      //   title: '已排生産數', dataIndex: 'sch_qty',
      //   render: (v, o, i) => <Count end={colorQtyObj[o.combo_seq]} duration={2}/>
      // },
      // {
      //   title: '排單數', dataIndex: 'qty',
      //   render: (v, o, i) => {
      //     // console.log(' v, o, i  ： ', v, o, i, colorQtyObj,  )//
      //     return <InputsNumber keys='qty' 
      //     // max={splitDataLen > 1 ? v.pn_qty - colorQtyObj[v.combo_seq] : Infinity}
      //     max={o.pn_qty - colorQtyObj[o.combo_seq]}
      //     val={{...o, }}
      //     //val={{...o, qty: o.pn_qty - colorQtyObj[o.combo_seq]}} 
      //    i={i} inputing={this.inputing} data={v}></InputsNumber>
      //   }
      // },
    ]
    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}// 
        // dataSource={data != undefined ? data.filter(v => v .qtys !== 0 ) : data}
        pagination={false}
        rowKey={record => record[rowKey]}
        rowClassName={(record, i) => ANIMATE.bounceIn}
        rowSelection={rowSelection}
        className='colorTable ' 
      />
    );
  }
}

export default ColorSizeTable