import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {showTotal, dateForm, ajax} from 'util'
import { ANIMATE, SIZE, } from 'constants'
import { DATE_FORMAT, } from 'config'
import Inputs from '@@@@/Inputs'
import InputsNumber from '@@@@/InputsNumber'
import { Table, Tag, Button, Icon, DatePicker, Select,  } from 'antd';
import moment from 'moment';

class ColorTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      loading: false, 
      selectedRowKeys: [],
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    console.log('handleTableChange ：', pagination, filters, sorter)
    // const { path, option, level } = this.props
    // console.log('pagination, filters, sorter：', this.props, path, option, pagination, filters, sorter);
    // const { pageSize, current } = pagination
  }
  onRowClick = (record, index, event) => {
    event.stopPropagation()
    event.preventDefault()
  }
  addData = (v, o, i) => {
    console.log('addData v, o, i：', v, o, i, this.props);
    // this.props.addData(o, i)
  }
  editData = (v, o, i) => {
    console.log('editData v, o, i：', v, o, i, this.props);
    // this.props.editData(o, i)
  }
  showDetail = (v) => {
    console.log('v  showDetail ：', v  )
    // this.props.showDetail(v, o, i)
  }
  onSelectChange = (selectedRowKeys) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    // console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data)
    this.setState({
      loading,
      data,
    });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('Table props 属性的改变', nextProps)
    const { loading, pagination } = nextProps
    this.setState({
      loading,
      pagination,
    });
  }
  render() {
    const { pagination, data, loading, selectedRowKeys } = this.state
    // console.log('ColorTable 组件 this.state, this.props ：', data, this.props, dialogType )
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      onSelection: this.onSelection,
    };
    const rowKey = 'color'
    const columns = [
      {
        title: '顔色', dataIndex: 'color',
        render: (v, o, i) => <Tag color={'red'}>{v}</Tag>
      },
      {
        title: '尺碼', dataIndex: 'size',
        render: (v, o, i) => <div className='sizeWrapper' >
          <Tag color={'blue'} className='m-r10'>{v}</Tag>
          <Button onClick={() => this.editData(v, o, i)} shape="circle" type="primary" icon='edit' className="warn m-r10"></Button>
        </div>
      },
    ]
    return (
      <Table 
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={pagination}
        onRowClick={this.onRowClick.bind(this, )}
        onChange={this.handleTableChange.bind(this, )}
        rowKey={record => record[rowKey]}

        rowClassName={(record, i) => ANIMATE.bounceIn}
        // rowSelection={rowSelection}
        footer={() => <Button onClick={this.addData} type="primary" icon="plus" className="m-l">新增</Button>}
        className='colorTable tableFooterBtn' 
      />
    );
  }
}

export default ColorTable