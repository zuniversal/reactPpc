import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {showTotal, dateForm, ajax, infoFilter, } from 'util'
import { ANIMATE, SIZE, } from 'constants'
import { DATE_FORMAT, poInfoConfig, } from 'config'
import Inputs from '@@@@/Inputs'
import InputsNumber from '@@@@/InputsNumber'
import { Table, Tag, Button, Icon, DatePicker, Select,  } from 'antd';
import moment from 'moment';

class ColorTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      loading: false, 
      selectedRowKeys: [],
    };
  }
  onSelectChange = (selectedRowKeys) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys, this.state);
    this.setState({ selectedRowKeys });
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    // console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data)
    this.setState({
      loading,
    });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('Table props 属性的改变', nextProps)
    const { loading, pagination } = nextProps
    this.setState({
      loading,
      pagination,
    });
  }
  render() {
    const { pagination, loading, selectedRowKeys } = this.state
    const {rowKey, data, } = this.props
    console.log('ColorTable 组件 this.state, this.props ：', this.state, this.props,  )
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      onSelection: this.onSelection,
    };
    // 注意：tabel的dataIndex绑定的key如果改变了 render绑定的Inputs也会因此而发生重新渲染
    const columns = [
      {
        title: infoFilter(poInfoConfig, rowKey, 'title'), 
        dataIndex: infoFilter(poInfoConfig, rowKey, 'rowKey'),
        // dataIndex: rowKey,
        render: (v, o, i) => {
          // console.log('v, o, i 11111111：', infoFilter(poInfoConfig, rowKey, 'rowKey'), v, o, i)
          return o.editAble ? <Inputs className='t-c' data={''} keys={rowKey} val={o} i={i} inputing={this.props.inputing}></Inputs> 
          : <Tag color={infoFilter(poInfoConfig, rowKey, 'color')}>{v}</Tag>
        }
      },
    ]
    console.log('infoFilter(poInfoConf ：', infoFilter(poInfoConfig, rowKey, 'rowKey'))
    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={false}
        // pagination={pagination}
        rowKey={record => record[infoFilter(poInfoConfig, rowKey, 'rowKey')]}
        rowClassName={(record, i) => ANIMATE.bounceIn}

        rowSelection={rowSelection}
        footer={() => <div>
          <Button onClick={() => this.props.addData(rowKey)} type="primary" icon="plus" className="m-r10">新增</Button>
        </div>}
        className='colorTable tableFooterBtn noMgTag' 
      />
    );
  }
}

export default ColorTable