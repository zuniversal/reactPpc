import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { INPUT_TXT,  } from 'constants'
import { Form, Input, } from 'antd';
const FormItem = Form.Item;


class ArtLineForm extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      formLayout: 'vertical',
    }
  }
  componentDidMount() {
  }
  render() {
    console.log('ArtLineForm 组件this.state, this.props ：', this.state, this.props);
    const {formLayout} = this.state
    const {init, config} = this.props
    const haveInitial = Object.keys(init).length
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 20 },
    };
    const formItem = config.map((v, i) => { 
      const {label, key, formType, rules, } = v
      console.log(' config v ：', label, key, formType, rules, v, init[key]  )
      const val = haveInitial ? formType === 'Input' ? init[key] : init[key] : ''
      const initialValue = {initialValue: val, rules}
      return (<FormItem label={label + '：'} key={i}>
        {getFieldDecorator(key, initialValue)(
          <Input placeholder={INPUT_TXT + label} onChange={this.valueChange} />
        )}
      </FormItem>)
    })
    return (
      <Form className="inputs" layout={formLayout}>
        {formItem}
      </Form>
    );
  }
}
const WrappedDemo = Form.create()(ArtLineForm);

export default WrappedDemo;