import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { INPUT_TXT, ANIMATE,  } from 'constants'
import {filterOptionForm, initParams, DATE_FORMAT_BAR, } from 'config'
import ProduceForm from '@@@/ProduceForm'
import { Menu, Dropdown, Button, Icon, Select, DatePicker } from 'antd';
import moment from 'moment';
const { MonthPicker,  } = DatePicker
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const Option = Select.Option;
const MenuItem = Menu.Item

const formItemLayout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 19 },
};

class FilterPane extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  filterOption = () => {
    this.forms.validateFields((err, values) => {
      values.pn_no = values.pn_no != undefined ? values.pn_no : ''
      values.customer = values.customer != undefined ? values.customer: ''
      values.style_no = values.style_no != undefined ? values.style_no : ''
      values.shipment_datefr = values.shipment_datefr != undefined ?  moment(values.shipment_datefr).format(DATE_FORMAT_BAR) : ''
      values.shipment_dateto = values.shipment_dateto != undefined ?  moment(values.shipment_dateto).format(DATE_FORMAT_BAR) : ''
      console.log('values.customer.split ：', values, {...initParams, ...values})
      this.props.getData({...initParams, ...values})
    })
  }
  cancel = () => this.props.handleVisibleChange(false)
  
  render() {
    console.log('FilterPane 组件this.state, this.props ：', this.state, this.props);
    const {customerData, styleNoData, visible, } = this.props
    const menuItem = <Menu.Item className='menuItem'>
        {
          visible ? <ProduceForm customerData={customerData} styleNoData={styleNoData} ref={forms => this.forms = forms}
            selectData={[]} formItemLayout={formItemLayout} formLayout={'horizontal'} 
            init={{}} config={filterOptionForm} 
            extra={<Button onClick={this.filterOption.bind(this,)} icon='smile-o' className='f-r' type="primary">过滤</Button>}
            ></ProduceForm> : null
        }
      </Menu.Item>
    
    return (
      <Menu className={`filterOptionMenu ${ANIMATE.bounceIn}`}>
        {menuItem}
        <Menu.Item key="btn">
          <Button onClick={this.filterOption.bind(this,)} icon='smile-o' className='f-r' type="primary">过滤</Button>
          <Button onClick={this.cancel.bind(this,)} icon='meh-o' className='f-r m-r10'>取消</Button>
        </Menu.Item>
      </Menu>
    );
  }
}
export default FilterPane;