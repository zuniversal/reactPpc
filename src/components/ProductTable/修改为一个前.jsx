import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { ANIMATE, SIZE10, INPUT_TXT,  } from 'constants'
import {showTotal, dataFilter, customFilter, } from 'util'
import { expenseFilters, DATE_FORMAT_BAR, tagColorArr,  } from 'config'
import Count from '@@@@/Count'
import SecureHoc from '@@@@/SecureHoc'
import { Table, Tag, Tooltip, Button, Icon, Select, Input, } from 'antd';
import moment from 'moment';
const Option = Select.Option;

class Tables extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const {total} = this.props
    this.state = {
      data: [],
      loading: false,
      deleteItem: -1,
      len: 0,
      expandedRowKeys: [],
      expandedData: [],
      searchText: '',
      searchKey: '',
      filtered: false,
      filterDropdownVisible: false,
    };
  }
  onInputChange = (k, e) => this.setState({ searchText: e.target.value, searchKey: k });
  blur = (k) => this.setState({ [`${k}Visible`]: false,  });
  reset = (k) => this.setState({ [`${k}Visible`]: false, searchText: '', searchKey: '', });

  handleTableChange = (pagination, filters, sorter) => {
    const { path, option, level } = this.props
    console.log('pagination, filters, sorter：', this.props, path, option, pagination, filters, sorter);
    const { pageSize, current } = pagination
    if (path === 'settle') {
      this.props.getApprovel(pageSize, current)
    } else if (path === 'settledReport') {
      this.props.getSettle(pageSize, current)
    }
  }
  onExpandedRowsChange = (id) => {
    // console.log('onExpandedidsChange changed: ', id[id.length - 1], id, this.props, this.state.expandedRowKeys);
    const expandId = id[id.length - 1]
    let expandedData = []
    if (expandId != undefined) {
      const expandedRow = this.props.data.filter(item => item.factory_id === expandId)
      const { expandedRowKeys } = this.state
      expandedData = this.props.data.filter(v => v.factory_id === expandId)[0].historys
      // console.log('expandedData ：', expandedData);
    }
    // console.log('expandedData ：', expandedData);
    this.setState({ 
      expandedRowKeys: id.length ? [expandId] : [], 
      expandedData: expandedData != undefined ?  [...expandedData] : [], 
    });
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    // console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data)
    this.setState({
      loading,
      data,
    });
  }
  componentWillReceiveProps(nextProps) {
    console.log('$$$$$$$$$$$$$$$$$$$Table props 属性的改变', nextProps, this.state)
    const { loading, data,  } = nextProps
    const {len, expandedRowKeys, } = this.state
    if (expandedRowKeys.length) {
      data.forEach((item, i) => {
        // console.log(' data item ：', item, i, )
        if (item.factory_id === expandedRowKeys[0]) {
          // const expandedData = data.filter(v => v.factory_id === expandedRowKeys[0])
          console.log('已展开 ：', item, expandedRowKeys)
          this.setState({ 
            expandedData: [...item.historys],
          });
        } 
      })
    }
    if (len !== data.length) {
      this.setState({
        deleteItem: -1,
      })
    }
    this.setState({
      len: data.length,
      loading,
      data,
    });
  }
  render() {
    const { pagination, data, loading, expandedRowKeys, expandedData, searchText, searchKey,  } = this.state
    const {canDelete, factory_type, procedureData, processFilters, auth, } = this.props
    const isCompnay = factory_type !== 'O'
    console.log('ProductTable 组件 this.state, this.props ：', this.state, this.props, )
    const selfCompany = [
      {
        title: '工厂代號', dataIndex: 'factory_code',
        render: (t, v, i) => <span>{t}</span>,
        ...customFilter(this, 'factory_code',)
      },
      {
        title: '工厂名稱', dataIndex: 'factory_name',
        render: (t, v, i) => <span>{t}</span>,
        ...customFilter(this, 'factory_name',)
      },
      {
        title: '工序名稱', dataIndex: 'work_name',
        filterMultiple: false,
        filters: processFilters,
        onFilter: (value, record) => {
          // console.log('record.address.indexOf(value) === 0 ：', record, value, procedureData, record.work_name === value);
          return record.work_name === value
        },
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '針種', dataIndex: 'gauge',
        render: (v, o, i) => <span>{v != undefined ? v.trim() === 'C' ? '粗針' : v.trim()  === 'F' ? '幼針' : v : v}</span>,
        ...customFilter(this, 'gauge',)
      },
    ]
    const commonColumns = {
      title: '效率', dataIndex: 'prod_rate',
      sorter: (a, b) => a.prod_rate - b.prod_rate,
      render: (t, v, i) => <span>{t != undefined ? `${t * 100}%` : null}</span>,
    }
    const commonColumn = [
      {
        title: '工人数', dataIndex: 'workers',
        sorter: (a, b) => a.workers - b.workers,
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
      {
        title: '工时', dataIndex: 'pts',
        sorter: (a, b) => a.pts - b.pts,
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
      commonColumns,
      {
        title: '開始日期', dataIndex: 's_date',
        render: (v, o, i) => <span>{v != undefined ? moment(v).format(DATE_FORMAT_BAR) : v}</span>
      },
    ]

    const noSelfCompany = [
      {
        title: '可做工序', dataIndex: 'capatitys',
        filterMultiple: false,
        filters: processFilters,
        onFilter: (value, record) => {
          console.log('record.address.indexOf(value) === 0 ：', record, value, procedureData, record.capatitys.some((v, i) => v.work_name === value));
          return record.capatitys.some((v, i) => v.work_name === value)
        },
        render: (v, o, i) => {
          // console.log('v, o, i ：', v, o, i);
          return <span>
            {/* {
              v != undefined ? v.map((item, i) => <Tag className={`${ANIMATE.bounceIn}`} color={tagColorArr.filter(items => items.work_no === item.work_no)[0].color} key={i}>{item.work_name}</Tag>) : v 
            } */}
            {
              v.map((item, i) => <Tag className={`${ANIMATE.bounceIn}`} color={tagColorArr.filter(items => items.work_no === item.work_no)[0].color} key={i}>{item.work_name}</Tag>) 
            }
          </span>
        }
      },
    ]
    const actionColumn = {
      title: <div>操作列</div>, 
      dataIndex: 'actionCol',
      render: (v, o, i) => {
        // console.log('v, o, i ：', v, o, i);
        return (<div className="btnWrapper">
          {/* o.void === 'Y' ?  */}
          <Button onClick={() => this.props.beforeProcessAction('editProcess', o)} size='small' type="primary" className="succ m-r10">編輯工廠代號</Button>
          <Button onClick={() => this.props.beforeAction('addProcess', o, i)} size='small' type="primary" className="m-r10">新增産能</Button>
        </div>)
      }
    }
    auth && noSelfCompany.push(actionColumn)


    // data.length && console.log('ProductTable 组件this.state, this.props ：', data, this.state, this.props, expandedRowKeys)
    // const rowKey = 'gauge'
    const rowKey = 'factory_id'
    const columns = [
      ...commonColumn,
    ]

    const actionColumn2 = {
      title: <div>
        操作列
        {/* <Button onClick={() => this.props.beforeProcessAction('addProcess', info)} type="primary salmon m-r10" >新增産線</Button> */}
        {/* <Button onClick={() => this.props.beforeProcessAction('editProcess', info)} size='small' type="primary succ m-r10" >編輯産線</Button>
        <Button onClick={() => this.props.beforeProcessAction('deleteProcess', info)} size='small' type="primary warn" >删除産線</Button> */}
      </div>, 
      dataIndex: 'actionCol',
      render: (v, o, i) => {
        // console.log('v, o, i ：', v, o, i);
        return (<div className="btnWrapper">
          {/* o.void === 'Y' ?  */}
          <Button onClick={() => this.props.beforeProcessAction('editProcess', o)} size='small' type="primary" className="succ m-r5">編輯産線</Button>
          <Button onClick={() => this.props.beforeAction('addProcess', o, i)} size='small' type="primary" className="m-r5">新增産能</Button>
          {/* <Button onClick={() => this.props.beforeAction('editCapacity', o, i)} size='small' type="primary" className="salmon m-r10">編輯産能</Button> */}
          {/* <Button onClick={() => this.props.beforeProcessAction('deleteProcess', o)} size='small' type="primary" className="warn m-r10">禁用産線</Button> */}
          {/* {o.gauge != undefined ? null : <Button onClick={() => this.props.beforeAction('addProcess', o, i)} size='small' type="primary" className="m-r10">新增産能</Button>}*/} 
          {o.gauge != undefined ? <Button onClick={() => this.props.beforeAction('editCapacity', o, i)} size='small' type="primary" className="sub m-r5">編輯産能</Button> : null} 
        </div>)
      }
    }
    auth && columns.push(actionColumn2)

    const expandedRowRender = () => {
      const rowKey = 'seq_no'
      // // console.log('expandedData ：', expandedData);
      const columns = [
        ...commonColumn,
        {
          title: '結束日期', dataIndex: 'e_date',
          render: (v, o, i) => <span>{v != undefined ? moment(v).format(DATE_FORMAT_BAR) : ''}</span>
        },
      ];
      return (
        <Table
          rowKey={record => record[rowKey]}
          columns={columns}
          dataSource={expandedData}
          pagination={false}
          className={`${ANIMATE.bounceIn}`}
        />
      );
    }
    const slefExpand = isCompnay ? {
      expandedRowRender: expandedRowRender, 
      onExpandedRowsChange: this.onExpandedRowsChange, 
      expandedRowKeys: expandedRowKeys, 
    } : null
    return (
      <Table 
        bordered={true}
        columns={isCompnay ? [...selfCompany, ...columns, ] : [...selfCompany.slice(0, 2), commonColumns, ...noSelfCompany]}
        loading={loading}
        dataSource={dataFilter(this, data, searchText, searchKey, )}
        pagination={false}
        onChange={this.handleTableChange.bind(this, )}
        rowKey={record => record[rowKey]}
        // title={() => <div className='productTableHeader'>
        // {`${info.factory_code} ${info.factory_name} ${info.factory_type === 'I' ? '本公司' : '不是本公司'}`}
        // </div>}
        className='productTable m-b10' 
        {...slefExpand}
        rowClassName={(record, i) => `${ANIMATE.zoomIn} ${record.void === 'Y' ? 'disableRow' : ''}` }
      />
    );
  }
}

export default SecureHoc(Tables)