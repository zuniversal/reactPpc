import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { ANIMATE, SIZE10 } from 'constants'
import {showTotal, } from 'util'
import { purposeColor, typeColor, expenseFilters, purposeFilters, DATE_FORMAT_BAR,  } from 'config'
import { Table, Tag, Tooltip, Button, Icon, Select, Badge, Dropdown, Menu } from 'antd';
import moment from 'moment';
const Option = Select.Option;

const menu = (
  <Menu>
    <Menu.Item>
      Action 1
    </Menu.Item>
    <Menu.Item>
      Action 2
    </Menu.Item>
  </Menu>
);

class Tables extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const {total} = this.props
    this.state = {
      data: [],
      loading: false,
      deleteItem: -1,
      len: 0,
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    const { path, option, level } = this.props
    console.log('pagination, filters, sorter：', this.props, path, option, pagination, filters, sorter);
    const { pageSize, current } = pagination
    if (path === 'settle') {
      this.props.getApprovel(pageSize, current)
    } else if (path === 'settledReport') {
      this.props.getSettle(pageSize, current)
    }
  }
  editData = (v, o, i) => {
    console.log('editData v, o, i：', v, o, i, this.props);
    this.props.editData('editProcess', o, i)
  }
  deleteData = (v, o, i) => {
    console.log('deleteData v, o, i：', v, o, i, this.props);
    // this.setState({ deleteItem: i })
    this.props.deleteData('deleteData', o, i)
  }
  addProcess = (v) => {
    const {info, data} = this.props
    info.data = data
    console.log('addProcess ：', this.props);
    this.props.addProcess('addProcess', info)
  }
  deleteProcess = () => {
    const {info, } = this.props
    console.log('deleteProcess ：', this.props);
    this.props.deleteProcess('deleteProcess', info)
  }
  handleChange = (v, o, i, keys, s) => {
    console.log('handleChange ：', v, o, i, keys, s)
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    // console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data)
    this.setState({
      loading,
      data,
    });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('$$$$$$$$$$$$$$$$$$$Table props 属性的改变', nextProps, this.state)
    const { loading, data,  } = nextProps
    const {len, } = this.state
    if (len !== data.length) {
      this.setState({
        deleteItem: -1,
      })
    }
    this.setState({
      len: data.length,
      loading,
      data,
    });
  }
  render() {
    const { pagination, data, loading,  } = this.state
    const {canDelete, info, historyData} = this.props
    
    // console.log('ProductTable 组件this.state, this.props ：', this.state, this.props);
    const rowKey = 'seq_no'
    const columns = [
      {
        title: '工序名稱', dataIndex: 'work_name',
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '針種', dataIndex: 'gauge',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '工人数', dataIndex: 'workers',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '工时', dataIndex: 'pts',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '開始日期', dataIndex: 's_date',
        render: (v, o, i) => <span>{moment(v.s_date).format(DATE_FORMAT_BAR)}</span>
      },
      {
        title: <div>
          <Button onClick={(v) => this.addProcess(v)} size='small' type="primary succ m-r10" >新增</Button>
          <Button onClick={() => this.deleteProcess()} size='small' type="primary warn" >删除産線</Button>
        </div>, 
        dataIndex: 'editCol',
        render: (v, o, i) => (<div className="btnWrapper">
          <Button onClick={() => this.editData(v, o, i)} shape="circle" type="primary" icon='edit' className="m-r10"></Button>
          <Button onClick={() => this.deleteData(v, o, i)} shape="circle" type="primary" icon='delete' className="warn"></Button>
        </div>)
      },
    ]
    const expandedRowRender = () => {
      const columns = [
        {
          title: '工序名稱', dataIndex: 'work_name',
          render: (t, v, i) => <span>{t}</span>
        },
        {
          title: '針種', dataIndex: 'gauge',
          render: (v, o, i) => <span>{v}</span>
        },
        {
          title: '工人数', dataIndex: 'workers',
          render: (v, o, i) => <span>{v}</span>
        },
        {
          title: '工时', dataIndex: 'pts',
          render: (v, o, i) => <span>{v}</span>
        },
        {
          title: '開始日期', dataIndex: 's_date',
          render: (v, o, i) => <span>{moment(v.s_date).format(DATE_FORMAT_BAR)}</span>
        },

        { title: 'Status', key: 'state', render: () => <span><Badge status="success" />Finished</span> },
        {
          title: 'Action',
          dataIndex: 'operation',
          key: 'operation',
          render: () => (
            <span className="table-operation">
              <a href="#">Pause</a>
              <a href="#">Stop</a>
              <Dropdown overlay={menu}>
                <a href="#">
                  More <Icon type="down" />
                </a>
              </Dropdown>
            </span>
          ),
        },
      ];
      return (
        <Table 
          bordered={true}
          columns={columns}
          dataSource={historyData}
          pagination={false}
          rowClassName={(record, i) => i === this.state.deleteItem && canDelete ? ANIMATE.slideOutLeft : ANIMATE.zoomIn}
        />
      );
    }
    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={false}
        onChange={this.handleTableChange.bind(this, )}
        rowKey={record => record[rowKey]}
        title={() => <div className='productTableHeader'>
          {`${info.factory_code} ${info.factory_name} ${info.factory_type === 'I' ? '本公司' : '不是本公司'}`}
        </div>}
        className='productTable m-b10' 
        indentSize={40}
        // expandedRowRender={expandedRowRender}
        rowClassName={(record, i) => i === this.state.deleteItem && canDelete ? ANIMATE.slideOutLeft : ANIMATE.zoomIn}
      />
    );
  }
}

export default Tables