import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { ANIMATE } from 'constants'
import Collapses from '@@@/Collapses'

import { Icon, } from 'antd'

class EventBoard extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  showDetail = (v) => {
    console.log('showDetail ：', v);
    const {class_id, event_id, event_desc} = v
    const {data} = this.props
    this.props.eventDetail({class_id, event_id, event_desc, title: data.title})
  }
  render() {
    console.log('EventBoard 组件this.state, this.props ：', this.state, this.props);
    const {data} = this.props
    return (
      <Collapses title={data.title} className="eventBoard m-b10" noLimit extra={
        <div className="btnWrapper">
          {/* <Icon type="sync" onClick={this.refresh.bind(this, data)}/> */}
          {
            data.class_id !== 1 ? <Icon type="reload" onClick={() => this.props.refresh(data.class_id)} /> : null
          }
        </div>}
        >
        {
          data.event.map((v, i) => {
            {/* console.log(' v ：', v, v.num < 1)  */}
            return (<div className={`eventItem ${ANIMATE.flipInY}`} key={i} onClick={this.showDetail.bind(this, v)}>
              <Icon type={`${v.pn_count < 1 ? 'check-circle' : 'close-circle'}`} />
              <p className="eventDesc">{v.event_desc}</p>
            </div>)
          })
        }
      </Collapses>
    );
  }
}
export default EventBoard;