import React from "react"
import ReactDOM from 'react-dom';
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import ScheduleDay from '@@@/ScheduleDay'// 

class ScheduleDayRow extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    // console.log('########## ScheduleDayRow  shouldComponentUpdate  nextProps ：', nextProps, this.props, nextState, nextProps.dateLen, this.props.dateLen, )
    if (nextProps.dateLen === this.props.dateLen) {// 
      // console.log('########## ScheduleDayRow shouldComponentUpdate  nextProps ：', nextProps, this.props, nextState, nextProps.dateLen, this.props.dateLen, )
      return false
    } else {
      // console.log('shouldComponentUpdateshouldComponentUpdate  nextProps 不等 ：', nextProps, this.props, nextState, nextProps.dateLen, this.props.dateLen, )
      return true
    }
  }
    
  render() {
    const { monthArr, dateArr, isTxt, isMonth, isLasItem, } = this.props
    // console.log('ScheduleDayRow 组件 this.state, this.props ：', this.state, this.props, )
    return (
      <div className='flexWrappers' >
        {
          monthArr.map((v, i) => {
            // console.log(' monthArr v shouldComponentUpdate ：', this.state, ) // 
            return <ScheduleDay dateLen={dateArr} isTxt={isTxt} isMonth={isMonth} isLasItem={isLasItem} v={v} key={v.month + v.date.length}></ScheduleDay>
          })
        }
      </div>
    )
  }
}

export default ScheduleDayRow

