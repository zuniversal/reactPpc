import React from "react"
import ReactDOM from 'react-dom';
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { grid, } from 'config'
import DragableBar from '@@@/DragableSliders'
import { Button, Tooltip, Popover } from 'antd';

class ScheduleBody extends React.PureComponent {
  constructor(props) {
    super(props)
    // this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }

  // shouldComponentUpdate(nextProps, nextState) {
    // console.log('##########shouldComponentUpdate  nextProps ：', nextProps, nextState, )
        // return false
  //   // const {splitData} = this.state
  //   // console.log('@@@@@@@@shouldComponentUpdate  show ：', splitData)
  //   if (nextState.shou) {
  //     return false
  //   } else {
  //     return true
  // }
  componentDidMount() {
    console.log(' ScheduleBody 组件componentDidMount挂载 ： ', this.state, this.props,  )
    
  }
  
  render() {
    const { 
      data_total, dateArr, changeBarData, showEdit, isDragBack, monthArr,
      justSideBar, getPnDetailList, createDay, getRealDetail
    } = this.props
    console.log(' %c ScheduleBody 组件 this.state, this.props ： ', `color: #333; font-weight: bold`, this.state, this.props,  )
    // return <div>111111</div>
    return (
      data_total.map((v, i) => {
        // console.log(' data_total v ：', v, v.data.length) 
        return (
          <div className="contentRow" style={{height: v.data.length * grid}} key={v.pn_no}>
            {/* <div className="sideBarBox">
              <Tooltip placement="top" title={
                <div className="pnInfoWrapper">
                  <div className="pnInfo pnNos">{v.pn_no}</div>
                  <div className="pnInfo pnNos">{v.style_no}</div>
                  <div className="pnInfo">{v.cloth_style}</div>
                </div>
              }>
                <div className="pnDay">
                  {
                    v.data.length ? <Button onClick={getPnDetailList(v.pn_no)} type="primary">{v.pn_no}</Button> : null
                  }
                </div>
              </Tooltip>
              {v.data.length > 2 ? <div className="pnInfo pnNos">{v.style_no}</div>: null}
              {v.data.length > 2 ? <div className="pnInfo">{v.cloth_style}</div>: null}
            </div> */}
          </div>
        )
      })
    )
    return (
      data_total.map((v, i) => {
        // console.log(' data_total v ：', v, v.data.length) 
        return (
          <div className="contentRow" style={{height: v.data.length * grid}} key={v.pn_no}>
            <div className="sideBarBox">
              <Tooltip placement="top" title={
                <div className="pnInfoWrapper">
                  <div className="pnInfo pnNos">{v.pn_no}</div>
                  <div className="pnInfo pnNos">{v.style_no}</div>
                  <div className="pnInfo">{v.cloth_style}</div>
                </div>
              }>
                <div className="pnDay">
                  {
                    v.data.length ? <Button onClick={getPnDetailList(v.pn_no)} type="primary">{v.pn_no}</Button> : null
                  }
                </div>
              </Tooltip>
              {v.data.length > 2 ? <div className="pnInfo pnNos">{v.style_no}</div>: null}
              {v.data.length > 2 ? <div className="pnInfo">{v.cloth_style}</div>: null}
            </div>
            {
              justSideBar ? null : (<div className="rowWrapper">
                {
                  v.data.map((item, index) => {
                    const { left, width, levels, pn_no, barcode, shortDay, work_name, factory_name, s_date, e_date, real_qty, open_date, finish_date, rate, realStart, realWidth, factory_id, zIndex, } = item
                    const top = (levels - 1) * 30
                    const isLasItem = index === v.data.length - 1 
                    {/* const widths = dateArr.length * grid
                    const lefts = Math.abs(left) */}
                    {/* index < 50 && console.log('item ：', v, item, index, left, width, barcode, shortDay) */}
                    const realStyle = {zIndex, left: realStart, width: realWidth, position: 'absolute'}
                    {/* const rates = rate * 100 > 100 ? 100 : rate * 100 */}
                    const rates = (rate * 100).toFixed(2)
                    return <div className="pnRow" key={barcode + work_name + factory_name}>
                      {/* {createDay(false, false, isLasItem)}  */}
                      {/* <Slider className='sliders' range 
                      tipFormatter={this.formatter}
                      //step={1} 
                      step={grid / widths * 100}
                      defaultValue={[(lefts) /( widths) * 100, (lefts + width) / (widths) * 100]}  
                      onChange={this.onChange.bind(this, left, width, )} onAfterChange={this.onAfterChange.bind(this, left, width, )} /> */}
                      
                      {/* {realStart != undefined ? <Tooltip placement="top" title={`${barcode + work_name} / ${factory_name} - 實際生産：${real_qty}
                      ${<Button type="primary" onClick={this.getRealDetail} className='detailbtn' size='small'>详情</Button>}`}>
                        <div className="real" style={realStyle}>{real_qty}</div></Tooltip> : null} */}
                        
                      {/* {realStart != undefined ? <Popover content={<span className="barPopover">
                        {barcode + work_name} / {factory_name} - 實際總生産數：{real_qty} - 百分比 {rates}%  <Button type="primary" onClick={this.props.getRealDetail(barcode, pn_no, factory_id)} size='small'>详情</Button></span>}
                      ><div className="real" style={realStyle} onClick={() => this.props.zIndex(item, i, index)}>{rates}%</div>
                      </Popover> : null} */}
                        
                      {/* {realStart != undefined ? <Tooltip placement="top" title={`${barcode + work_name} / ${factory_name} - 實際生産：${real_qty}`}>
                      <div className="real" style={realStyle}>{real_qty}</div></Tooltip> : null} */}
                      
                      {/* <DragableBar 
                        isDragBack={isDragBack} 
                        showEdit={showEdit} 
                        changeBarData={changeBarData} 
                        changeBar={this.props.changeBar} 
                        s_date={s_date} 
                        e_date={e_date} 
                        dateArr={dateArr} 
                        dateLen={dateArr.length} 
                        ref={dragBar => this[barcode] = dragBar} 
                        clickType='barClick' 
                        key={barcode + work_name + factory_name} 
                        pn_no={pn_no} 
                        data={item} 
                        isAxis={'x'} 
                        showBarcodeDialog={this.props.showBarcodeDialog} 
                        stopDrag={this.props.stopDrag} 
                        indexes={v.indexes}
                        config={{ left, width, top, txt: `${work_name} / ${factory_name}`, levels, barcode, shortDay, }}
                        //config={{ left, width, top, txt: `${barcode + work_name} / ${factory_name}`, levels, barcode, shortDay, }}
                      ></DragableBar> */}
                    </div>
                  })
                }
              </div>)
            }
          </div>
        )
      })
    )
  }
}

export default ScheduleBody