import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import BarcodeInfo from '@@@@/BarcodeInfo'
import BarcodeTable from '@@@@/BarcodeTable'
import {bacodeInfo,   } from 'config'
class BarcodeContent extends React.Component {
  constructor(props) {
    super(props)
    // // this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  
  render() {
    const {splitData, barcodeData, dialogType, pn_info, isShowBarcode, } = this.props
    console.log('表格啊啊啊啊啊啊啊啊啊啊this.props ：', this.props, barcodeData[0] )
    // 
    return (
      <div className=''>
        <BarcodeInfo bacodeInfoConfig={bacodeInfo} data={isShowBarcode ? barcodeData[0] : pn_info} middle={barcodeData[0].work_no} {...this.props}></BarcodeInfo>
        <BarcodeTable data={barcodeData} {...this.props} middle={barcodeData[0].work_no}></BarcodeTable>
        { 
          isShowBarcode && splitData.length !== 0 ? <BarcodeTable barcodeData={barcodeData} data={splitData} {...this.props}></BarcodeTable> : null
        }
      </div>
    );
  }
}
export default BarcodeContent;