import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { Steps, Icon } from 'antd';
const Step = Steps.Step;
class ArtLineStep extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  render() {
    console.log('ArtLineStep 组件this.state, this.props ：', this.state, this.props);
    return (
      <Steps direction="vertical" current={1}>
    <Step title="Finished" description="This is a description." />
    <Step title="In Progress" description="This is a description." />
    <Step title="Waiting" description="This is a description." />
  </Steps>
    );
  }
}
export default ArtLineStep;