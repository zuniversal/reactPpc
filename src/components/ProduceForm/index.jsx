import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { INPUT_TXT, SELECT_TXT,  } from 'constants'
import { purposeColor, typeColor, expenseFilters, purposeFilters, DATE_FORMAT_BAR, MONTH_FORMAT, } from 'config'
import { getItem, ajax, ts, isArray  } from 'util'
import { Form, Input, Checkbox, DatePicker, Select, InputNumber, Row, Col,  } from 'antd';
import moment from 'moment';
const { MonthPicker, RangePicker } = DatePicker;
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;

class ProduceForm extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      isRecive: false,
      reciveKey: '',
      disableKey: '',
    }
  }
  dateChange = (k, v) => {
    const {isRecive, reciveKey, } = this.state
    console.log('选择日期init === nextProps.init ：', v.substr(0, 4), v.substr(5, 2) , moment(moment([v.substr(0, 4), (v.substr(5, 2) -1).toString()]).add(1, 'months')), moment(v.split('-')), v.split('-'), moment(['2018', ('11' -1).toString()]).add(0, 'months'),  ('11' -1).toString(), k, v, this.props, isRecive, reciveKey, )
    if (isRecive) {
      console.log('确实有改变 ：', isRecive)
      this.props.form.setFieldsValue({ [reciveKey]: moment(moment([v.substr(0, 4), (v.substr(5, 2) -1).toString()]).add(2, 'months')) })
    }
    // isRecive && this.props.form.setFieldsValue({ [reciveKey]: moment(moment(v.split('-')).add(1, 'months')) })
  }
  valueChange = (e, key, ) => {
    const {config, } = this.props
    const inputItem = config.find(v => v.key === key)
    console.log(' valueChange ： ', e, key, this.props, inputItem,  )// 
    inputItem.inputCb != undefined && this.props[inputItem.inputCb]({v: e.target.value, key, ...this.props, })
  }
  rangeDateChange = (v, key) => {
    const {config, } = this.props
    const macthItem = config.find(v => v.key === key)
    console.log(' rangeDateChange ： ', macthItem, v, key, this.props )
    macthItem.dateCb != undefined && this.props[macthItem.dateCb]({v, props: macthItem.isCbParam ? this.props : null, })
    
  }
  selectChange = (k, v) => {
    const {selectCb, disableKey, disableOption, config, } = this.props
    const macthItem = config.find(v => v.key === k)
    console.log('selectChange ：', k, v, disableKey, this.props, macthItem,  )
    if (k === disableKey && disableKey != undefined) {
      this.setState({
        [disableKey]: Number(v),
      }, () => {// 
        console.log('回调 ：', ) // 
        // this.props.form.setFieldsValue({gauge: ['values'] })
        // this.props.form.validateFields((err, values) => {
        //   console.log('values ：', err, values, Object.keys(values), disableOption);
        //   Object.keys(values).some(v => v === disableOption) && this.props.form.setFieldsValue({[disableOption]: undefined})
        // })
        config.some(v => v.key === disableOption) && this.props.form.setFieldsValue({[disableOption]: undefined})
        console.log('getFieldValuegetFieldValue ：', this.props.form.getFieldValue('gauge'), this.props.form.getFieldValue('disableOption'))
      })
    }
    // selectCb(k, v)
    if (macthItem != undefined) {
      const {resetKey, } = macthItem
      macthItem.selectCb != undefined && this.props[macthItem.selectCb]({v, props: macthItem.isCbConfig ? macthItem : null, k, })
      console.log(' resetKey.isArray ： ', Array.isArray(resetKey), Array.isArray({}), resetKey != undefined, macthItem,   )// 
      if (resetKey != undefined) {
        console.log(' undefine ： ',    )// 
        if (Array.isArray(resetKey)) {
          console.log(' isArrayisArrayisArray ： ',    )// 
          resetKey.forEach((item, i) => this.props.form.setFieldsValue({[item]: undefined}))
        } else{
          console.log(' undefineundefineundefine ： ',    )// 
          resetKey != undefined && this.props.form.setFieldsValue({[resetKey]: undefined})
        }
      }
    }
    
  }
  filterOption = (input, option) => option.props ? option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : -1
  
  componentDidMount() {
    const {config, isLoadSelect, init, disableKey,  } = this.props
    const isRecive = config.some(v => v.isRecive)
    if (isRecive) {
      this.setState({isRecive, reciveKey: config.filter(v => v.isRecive)[0].key, })
    }
    if (isLoadSelect && Object.keys(init).length) {
      console.log(' isLoadSelect ： ', this.props, this.props.loadFn(init), disableKey )
      // const {work_no, } = init
      // this.selectChange('work_no', this.props.loadFn(work_no))
      this.setState({
        [disableKey]: this.props.loadFn(init),
      })
      // this.props.form.setFieldsValue({work_no: `${this.props.loadFn(work_no)}`})
    }
    
    // console.log('$$$$$$$$$$$$$$$$$$$ProduceForm 组件componentDidMount挂载 ：', this.props, config, isRecive )
  }
  
  componentWillReceiveProps(nextProps) {
    // console.log('%%%%%%%%%%%%%%ProduceForm  componentWillReceiveProps组件获取变化 ：', nextProps.isClear, nextProps, this.props,  )
    const {init} = this.props
    // console.log('表单接收数据变化 init !== nextProps.init ：', init, nextProps.init, init !== nextProps.init, !nextProps.isClear);
    if (init !== nextProps.init || nextProps.isClear) {
      console.log('清空 ：', )
      // this.props.form.resetFields()
    }
  }
    
  render() {
    console.log('###########ProduceForm 组件this.state, this.props ：', this.state, this.props);
    const {
      init, 
      config, 
      selectData, 
      formLayout, 
      formItemLayout, 
      customerData, 
      styleNoData, 
      className, 
      disableAll, 
      disableDay, 
      dateDisable, 
      isRow, 
    } = this.props
    const haveInitial = Object.keys(init).length
    const { getFieldDecorator } = this.props.form;
    const formItem = config.map((v, i) => { 
      // console.log('formItem ：', v)
      const {
        label, key, formType, rules, disabled, 
        optionTxt, isRecive, noLabel, datakey, haveInit, 
        initVal, optionkey, selectDisable, disableKey, 
        disableFuc, optionDisable, format, hide, optMore, 
        plSpecial, span = 24, disableRange, 
      } = v
      // console.log(' selectDisableselectDisable ： ', label, key, this.props[datakey]  )
      // console.log('this.props[datakey] ：', v, datakey, this.props[datakey]);// // 
      const selectOption = this.props[datakey] != undefined && Object.keys(this.props[datakey]).length ? this.props[datakey].map((v, i) => {
        // console.log('v[disableKey] ：', v[optionTxt], v[optionkey], selectDisable, v[disableKey], this.state[disableKey], v[disableKey] !== this.state[disableKey]);
        
        if (selectDisable) {
          if (v[disableKey] === this.state[disableKey]) {
            return (<Option key={`${v[optionkey]}`} disabled={this.props[disableFuc](v[optionkey])}>{v[optionTxt]}</Option>)
          }
        } else {
          // console.log(' v[optionTxt] ： ', optionDisable, disableFuc, v, v[optionkey], v[optionTxt],  )// 
          return (<Option key={`${v[optionkey]}`} disabled={optionDisable && this.props[disableFuc](v[optionkey])}>{v[optionTxt]}</Option>)
          // return (<Option key={`${v[optionkey]}`} disabled={optionDisable && this.props[disableFuc](v[optionkey])}>{`${optMore ? v[optionTxt] - v[optionkey] : v[optionTxt]}`}</Option>)
        }
      }) : null
      const isInput = formType === 'Input'
      const isInputNum = formType === 'InputNum'
      const isCheckbox = formType === 'Checkbox'
      const isSingleDate = formType === 'SingleDate'
      const isMonthDate = formType === 'MonthDate'
      const isRangeDate = formType === 'RangeDate'
      const isSelect = formType === 'Select'
      const isText = formType === 'Text'

      function range(start, end) {
        const result = [];
        for (let i = start; i < end; i++) {
          result.push(i);
        }
        return result;
      }
      function disabledDate(current) {
        // console.log('current && current.valueOf() < Date.now() ：', current, current.valueOf(), current.valueOf() < Date.now(),)
        // return current && current.valueOf() < Date.now();
        // console.log('dateDisable ：', dateDisable);
        if (disableRange !== '') {
          const disableDate = Date.parse(new Date(disableRange))
          // console.log(' disableRangedisableRangedisableRange ： ', disableRange, disableDate,  )// 
          return current && current.valueOf() < disableDate;
        }
        
        if (dateDisable || dateDisable == undefined) {
          return false
        } else {
          const disableDate = Date.parse(new Date(moment(disableDay).format(DATE_FORMAT_BAR)))
          // console.log('current && current.valueOf() < init[key] ：', dateDisable, current && current.valueOf() < disableDate, disableDay, current && current.valueOf() < disableDay,  current.valueOf(), init[key], Date.now(), current.valueOf() < init[key],)
          return current && current.valueOf() < disableDate;
        }
      }
      function disabledDateTime() {
        // console.log('disabledDateTime ：', range(0, 24).splice(4, 20), range(30, 60),)
        return {
          disabledHours: () => range(0, 24).splice(4, 20),
          disabledMinutes: () => range(30, 60),
          disabledSeconds: () => [55, 56],
        };
      }
      // console.log(' selectOption ： ', selectOption,  )// 
      
      let val = haveInitial ? init[key] != undefined ? init[key] : null : haveInit ? initVal : null
      // console.log('val ：', label, key, formType, val, val != undefined ? 111 : 222)
      val = isSingleDate ? moment(val != undefined ? val : null, DATE_FORMAT_BAR) : val 
      val = isMonthDate ? moment(val != undefined ? val : null, MONTH_FORMAT) : val
      // console.log(' val != undefined ： ', val != undefined, val )// 
      // 解决 日期 非法问题
      val = isSingleDate ? val._isValid ? val : null : val
      // console.log(' val != undefined222 ： ', val != undefined, val )// 
      const initialValue = isSelect && val == undefined ?  {rules} : {initialValue: val, rules}
      isCheckbox ? initialValue.valuePropName = 'checked' : null
      // initialValue.initialValue = initialValue.initialValue._isValid != undefined > initialValue.initialValue : null
      // console.log('initialValue ：', isSingleDate, initialValue)
      // console.log('isSingleDate ：', initialValue, haveInitial, haveInit, init[key], init[key] != undefined, initVal, isSingleDate ? moment(val != undefined ? val : null, DATE_FORMAT_BAR) : null, val,   )

      const container = isInput ? <Input placeholder={INPUT_TXT + label} onChange={(e) => this.valueChange(e, key, v, )} disabled={disabled || disableAll} /> : 
      isCheckbox ? <Checkbox disabled={disabled || disableAll}></Checkbox> : 
      isSingleDate ? <DatePicker className='calendarCover' disabledDate={disabledDate} 
        disabledTime={disabledDateTime} onChange={this.dateChange} 
        disabled={disabled || disableAll} 
        format={format != undefined ? format : DATE_FORMAT_BAR}/> : isMonthDate ? <MonthPicker onChange={this.dateChange} format={format != undefined ? format : MONTH_FORMAT} disabled={disabled || disableAll} /> : isRangeDate ? <RangePicker 
        format={DATE_FORMAT_BAR}  className='rangePicker' disabledDate={disabledDate}
        disabled={disabled || disableAll}
        ranges={{ Today: [moment(), moment()], 'This Month': [moment(), moment().endOf('month')] }} 
        onChange={(v) => this.rangeDateChange(v, key, )}
      /> : isSelect ? 
      <Select disabled={disabled || disableAll}
        showSearch
        allowClear={true}
        optionFilterProp="children"
        onChange={this.selectChange.bind(this, optionkey)}
        placeholder={SELECT_TXT + label}
        filterOption={this.filterOption}
      >
        {selectOption}
      </Select> : isInputNum ? <InputNumber
        min={0}
        max={100}
        formatter={v => `${v}%`}
        parser={v => v.replace('%', '')}
      /> : isText ? <TextArea rows={4} /> : ''
      // console.log('initialValue ：', haveInitial, formType, container, initialValue, noLabel ? ' ' : `${label}：`);
      const isHasFeedback = {hasFeedback: isInput}
      const formItems = <FormItem 
          //label={noLabel ? ' ' : `${label}`}
          label={`${label}`}
          key={i}
          {...formItemLayout}
          {...isHasFeedback}
          className={`${hide ? 'hide' : ''}`} 
        >
          {getFieldDecorator(key, initialValue)(
            container
          )}
        </FormItem>
      return isRow ? (
        <Col span={span} key={i} >
          {formItems}
        </Col>
      ) : formItems 
    })
    return (
      <Form className={className != undefined ? className : ''} layout={formLayout != undefined ? formLayout : 'vertical'}>
       {isRow ?<Row>
          {formItem}
        </Row>: formItem}
      </Form>
    );
  }
}
const WrappedDemo = Form.create()(ProduceForm);
export default WrappedDemo;