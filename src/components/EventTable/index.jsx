import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import Inputs from '@@@@/Inputs'
import InputsNumber from '@@@@/InputsNumber'
import {showTotal, dateForm, ajax, infoFilter, } from 'util'
import { ANIMATE, } from 'constants'
import { DATE_FORMAT, poInfoConfig, } from 'config'
import { Table, Tag, Button, Icon, DatePicker, Select,  } from 'antd';
import moment from 'moment';

class EventTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = 10
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      loading: false, 
      selectedRowKeys: [],
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    console.log('handleTableChange ：', pagination, filters, sorter)
    // const { path, option, level } = this.props
    // console.log('pagination, filters, sorter：', this.props, path, option, pagination, filters, sorter);
    // const { pageSize, current } = pagination
  }
  onRowClick = (record, index, event) => {
    event.stopPropagation()
    event.preventDefault()
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    // console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data)
    this.setState({
      loading,
      data,
    });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('Table props 属性的改变', nextProps)
    const { loading, pagination } = nextProps
    this.setState({
      loading,
      pagination,
    });
  }
  render() {
    const { pagination, data, loading, selectedRowKeys,  } = this.state
    const {event_id, } = this.props
    console.log('EventTable 组件 this.state, this.props ：', data, this.props,   )
    const rowKey = 'pn_no'
    const isEvent5 = event_id === 5
    const columnsPrefix = [
      {
        title: '批號', dataIndex: 'pn_no',
        className: 'minPnWidth',
        render: (t, v, i) => <Tag color={'red'}>{t}</Tag>
      },
      {
        title: '訂單號', dataIndex: 'sc_no',
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '款號', dataIndex: 'style_no',
        className: 'minStyleWidth',
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '營業針種', dataIndex: 'sc_gauge',
        render: (t, v, i) => <span>{t}</span>
      },
    ]
    const noEvent5 = [
      {
        title: event_id === 3 || event_id === 4 ? '後整開始期' : '開機日期', dataIndex: 's_date',
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: event_id === 3 || event_id === 4 ? '洗水嘜送貨期' : '預計毛期', dataIndex: 'yarn_date',
        render: (t, v, i) => <span>{t != undefined ? t : 'null'}</span>
      },
    ]
    const columnsSuffix = [
      {
        title: '生産完成期', dataIndex: 'prod_date',
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '數量', dataIndex: 'qty',
        sorter: (a, b) => a.qty - b.qty,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '客户名称', dataIndex: 'cust_name',
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '款式', dataIndex: 'cloth_style',
        className: 'minClothWidth',
        render: (t, v, i) => <span>{t}</span>
      },
    ]
    const columns = isEvent5 ? [
      ...columnsPrefix,
      ...columnsSuffix,
    ] : [
      ...columnsPrefix,
      ...noEvent5,
      ...columnsSuffix,
    ] 
    console.log('columns ：', columns, isEvent5, event_id, event_id === 5)
    return (
      <Table 
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={pagination}
        onRowClick={this.onRowClick.bind(this, )}
        onChange={this.handleTableChange.bind(this, )}
        rowKey={record => record[rowKey]}

        rowClassName={(record, i) => ANIMATE.bounceIn}
        className='eventTable' 
      />
    );
  }
}

export default EventTable