import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"

import './style.less'
import {wipe, ajax} from 'util'
import {grid} from 'config'
import { ANIMATE,  } from 'constants'
import Draggable from 'react-draggable'
import { Tooltip, Slider } from 'antd';
class DragableSlider extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      dragAction: '', 
      activeDrags: 0,
      dragDirection: false,
      bounds: "parent",
      initPosition: {
        x: 0, y: 0
      },
      // 默认是不拖动的，当拖拽开始时才是dragging
      dragging: false,
      isDrag: true,
      dayRange: [],
    }
  }

  dbClick = (barcode) => {
    console.log('dbClick ：', barcode, this.props, );
    this.props.showBarcodeDialog(barcode)
  }
  widthAdd = (w) => {
    console.log('widthAdd ：', w);
    this.refs.dragBar.style.width = w + grid + 'px'
  }
  widthDecrease = (w) => {
    console.log('widthDecrease ：', w);
    this.refs.dragBar.style.width = w !== grid ? w - grid + 'px' : '60px'
  }

  handleDrag = (e, ui, d) => {
    const {dragAction} = this.state
    const barWidth = this.refs.dragBar.style.width
    const width = Number(wipe(barWidth))
    console.log('#bar拖动：', width, e, ui, dragAction, d.lastX , d.x, d, this.props);
    // this.props.short(this.props.config.barcode)
    if (dragAction !== '') {
      const isDragLeft = dragAction === 'dragLeft'
      // this.refs.dragBar.style.width = isDragLeft ? width - ui.deltaX + 'px' : width + ui.deltaX + 'px'
      if (isDragLeft) {
        console.log('左拖 ：', );
        if (d.lastX - d.x > 0) {
          console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
          this.widthAdd(width)
          // this.refs.dragBar.style.width = width + grid + 'px'
        } else {
          console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
          this.widthDecrease(width)
          // this.refs.dragBar.style.width = width !== grid ? width - grid + 'px' : '60px'
        }
      } else {
        console.log('右拖 ：', d.lastX, d.x);
        if (d.lastX - d.x < 0) {
          console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
          this.widthAdd(width)
          // this.refs.dragBar.style.width = width + grid + 'px'
        } else {
          console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
          this.widthDecrease(width)
          // this.refs.dragBar.style.width = width !== grid ? width - grid + 'px' : '60px'
        }
      }
    }
    const { x, y } = d;
    ajax(this.setState.bind(this), {
      dragging: true,
      initPosition: { x, y }
    })
    this.setState({
      dragging: true,
      initPosition: { x, y }
    })
  }

  dragAction = (v, a, d) => {
    const { x, y } = this.state.initPosition;
    const {dateLen} = this.props
    const {width, left} = this.props.config
    this.setState({
      isDrag: false,
    })
    
    console.log('#bardragAction v ：', dateLen * grid, dateLen * grid - (left + width), x, y, width, left, v, a, d, this.state, this.props);
    // console.log('v ===ght:  ：', v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: y, right: left + width, bottom: 0})
    
    // this.setState({
    //   dragAction: v,
    //   // bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : "parent",
    //   bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: 0, left: 0, right: left + width - grid, bottom: 0},
    // })
  }
  onStart = (a, b) => {
    console.log('#bar a：', a, b, );
    this.setState({ activeDrags: ++this.state.activeDrags,  });
  }

  onStop = (onStop, e) => {
    const barWidth = this.refs.dragBar.style.width
    const {barcode} = this.props.config
    const {dragging} = this.state
    const {data, pn_no, clickType} = this.props
    const width = Number(wipe(barWidth)) - grid
    console.log('#@@@@@@@@@@@@@@@@@@@#bar onStop：', barcode, pn_no, this.props, onStop, e, width, '是否是拖动：', this.state.dragging);
    const {lastX, lastY} = e
    const x = lastX / grid
    const y = lastY / grid
    const w = x + width / grid
    if (clickType === 'barClick' && dragging) {
      // ajax(this.props.stopDrag, {x, y, w, barcode, data, pn_no})
      // this.props.stopDrag(x, y, w, barcode)
    }
    this.setState({ 
      activeDrags: --this.state.activeDrags, 
      dragAction: '', 
      dragging: false,
      bounds: "parent",
    });
  }
  componentWillMount() {
    const { left, top, width } = this.props.config
    this.setState({
      left, width
    })
    
    // console.log('componentDidMount ：',  this.props, )
  }

  barClick = (e) => {
    console.log('barClick ：', e, this.props)
    const {pn_no, clickType} = this.props
    const {barcode, txt} = this.props.config
    if (clickType === 'barClick') {
      const {work_no, } = this.props.data
      this.props.showBarcodeDialog(barcode, pn_no, work_no)
    } else {
      this.props.getPnDetailList(txt)
    }
  }

  onChange = (value) => {
    const { dateArr } = this.props
    const days0 = dateArr[value[0]] 
    const days1 = dateArr[value[1]]
    this.setState({
      dayRange: value,
    })
    
    console.log('onChange: ', value, days0, days1);
    return `起止日期：${days0} - ${days1}`
  }
  
  onAfterChange = (value) => {
    const { dateLen, dateArr } = this.props
    const {dayRange} = this.state
    // const days0 = dateArr[dayRange[0]].dateOrigin 
    // const days1 = dateArr[dayRange[1]].dateOrigin
    // if (dayRange.length) {
    //   this.setState({
    //     isDrag: true,
    //     dayRange: [],
    //     // width: (value[1] - value[0] - 1) * grid,
    //   })
    // } 
    // if (value[0] !== days0 || value[1] !== days1) {
    // }
    console.log('onAfterChange: ', value, this.state, (value[1] - value[0] - 1) / dateLen);
  }
  formatter = (value, e) => {
    // console.log('formatter: ', value, e, );
    const { dateArr, s_date, e_date } = this.props
    const {dayRange} = this.state
    if (dayRange.length) {
      const days0 = dateArr[dayRange[0]].dateOrigin 
      const days1 = dateArr[dayRange[1]].dateOrigin
      // const days0 = dateArr[value] 
      // console.log('formatter: ', days0, dayRange);
      return `原起止日期：${s_date} - ${e_date} 现在起止日期：${days0} - ${days1} `
    } else {
      return `起止日期：${1} - ${2}`
    }
  }

  render() {
    const { dragging, dragAction, bounds, isDrag } = this.state;
    const { config, isAxis, isTop, dateLen } = this.props
    const { top, txt, levels, barcode, shortDay, height } = config
    const {left, width} = this.state
    
    const dragHandlers = { onStart: dragAction === '' ? this.onStart : () => false, onStop: this.onStop };
    // console.log('DragableSlider 组件 this.state, this.props ：', this.state, this.props, dragAction, dragAction === '' ? false : true)
    
    const style = {
      width: width + 'px',
      background: dragging ? 'rgba(111, 255, 228, 0.8)' : '',
      // zIndex: dragging ? 10 : 1
      // position: dragAction === '' ? 'absolute' : 'fixed'
      // left: 0 + 'px',
    }
    const defaultPosition = { x: left, y: isTop ? top : 0}
    const widths = dateLen * grid
    const lefts = Math.abs(left)
    
    
    // console.log('shortDay ：', shortDay, config, defaultPosition, lefts, widths,lefts /( widths), (lefts + width) / (widths))
    const shortBar = shortDay.length ? shortDay.map((v, i) => <div className="short" style={{left: v.shortDate}} key={i}></div>) : null
    return (
      isDrag ? (
        <Draggable disabled={true} axis={isAxis} bounds={bounds} 
        defaultPosition={defaultPosition} grid={[grid, grid]} 
        onDrag={this.handleDrag.bind(this, barcode)} {...dragHandlers}>
          <div className='box' ref="dragBar" style={style}>
            <div className="dragBorder dragLeft" onMouseDown={this.dragAction.bind(this, 'dragLeft')}>
            </div>
            <Tooltip placement="top" title={txt}>
              <div onClick={this.barClick.bind(this)} className={`machineName ${ANIMATE.bounceIn}`}>
                {txt}
                </div>
            </Tooltip>
            <div className="work"></div>
            {shortBar}
            <div className="dragBorder dragRight" onMouseDown={this.dragAction.bind(this, 'dragRight')}></div>
          </div>
        </Draggable>) : (<Slider className='sliders' range 
        tipFormatter={this.formatter}
        step={1} 
        //step={grid / widths * 100}
        defaultValue={[(lefts) /( widths) * 100, (lefts + width) / (widths) * 100]} 
        onChange={this.onChange} onAfterChange={this.onAfterChange} />)
    );
  }
}

export default DragableSlider;