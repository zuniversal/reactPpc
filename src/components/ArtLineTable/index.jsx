import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { ANIMATE, SIZE10 } from 'constants' 
import {showTotal, } from 'util'
import { purposeColor, typeColor, expenseFilters, purposeFilters } from 'config'
import Inputs from '@@@@/Inputs'
import Count from '@@@@/Count'
import SecureHoc from '@@@@/SecureHoc'
import { Table, Tag, Tooltip, Button, Icon, Input, } from 'antd';
const { TextArea } = Input;

class Tables extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const {total} = this.props
    pagination.pageSize = SIZE10
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      data: [],
      pagination,
      loading: false,
      deleteItem: -1,
      len: 0,
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    const { path, option, level } = this.props
    console.log('pagination, filters, sorter：', this.props, path, option, pagination, filters, sorter);
    const { pageSize, current } = pagination
    if (path === 'settle') {
      this.props.getApprovel(pageSize, current)
    } else if (path === 'settledReport') {
      this.props.getSettle(pageSize, current)
    }
  }
  onRowClick = (record, index, event) => {
    // event.stopPropagation()
    // event.preventDefault()
    // const { path, level } = this.props
    // console.log('record, index, event：', record, index, event, this.props);
    // this.props.showReportDetail(record.reportID)
  }
  saveData = (v, o, i) => {
    console.log('v, o, i：', v, o, i, this.props);
    this.props.showDialog()
  }
  editData = (v, o, i) => {
    console.log('v, o, i：', v, o, i, this.props);
    this.props.editData(v, o, i)
  }
  editDataAction = (v, o, i) => {
    console.log('v, o, i：', v, o, i, this.props);
    this.props.editDataAction(v, o, i)
  }
  remarkChange = (v, o, i, e, ) => {
    // console.log('remarkChange v, o, i：', v, o, i, e, e.target.value, this.props);
    this.props.editData({v: e.target.value, keys: 'remark'}, o, i)
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data)
    this.setState({
      loading,
      data,
    });
  }
  componentWillReceiveProps(nextProps) {
    console.log('$$$$$$$$$$$$$$$$$$$Table props 属性的改变', nextProps, this.state)
    const { loading, data, pagination } = nextProps
    const {len} = this.state
    if (len !== data.length) {
      this.setState({
        deleteItem: -1,
      })
    }
    this.setState({
      len: data.length,
      loading,
      data,
      pagination,
    });
  }
  render() {
    const { pagination, data, loading, } = this.state
    const {canDelete, auth, } = this.props
    
    console.log('ArtLineTable 组件this.state, this.props ：', this.state, this.props);
    const rowKey = 'work_no'
    const columns = [
      {
        title: '工序', dataIndex: 'work_no',
        render: (v, o, i) => <Tag color={'#f50'}>{o.work_name}</Tag>
      },
      {
        title: 'SAM(秒)', dataIndex: 'work_sam',
        sorter: (a, b) => a.work_sam - b.work_sam,
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
      {
        title: '日产(人/台', dataIndex: 'day_qty',
        sorter: (a, b) => a.day_qty - b.day_qty,
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
      {
        title: '标准天数', dataIndex: 'work_days',
        sorter: (a, b) => a.work_days - b.work_days,
        render: (v, o, i) => <Count tag={true} color={'pink'} end={v} duration={2}/>
      },
      {
        title: '总计人/台数', dataIndex: 't_workers',
        sorter: (a, b) => a.t_workers - b.t_workers,
        render: (v, o, i) => <Count tag={true} color={'green'} end={v} duration={2}/>
      },
      {
        title: '偏移天数', dataIndex: 'dev_days',
        sorter: (a, b) => a.dev_days - b.dev_days,
        render: (v, o, i) => o.editable ? <Inputs keys={'dev_days'} noRule={true} className='txtCenter' data={v} val={v} size={'default'} inputing={this.props.editData}></Inputs> : <Count tag={true} color='red' end={v} duration={2}/>
      },
      {
        title: '标准日产', dataIndex: 't_day_qty',
        sorter: (a, b) => a.t_day_qty - b.t_day_qty,
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
      {
        title: '每天需人数(台数)', dataIndex: 'workers',
        sorter: (a, b) => a.workers - b.workers,
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
      {
        title: '人数(尾期)', dataIndex: 'last_workers',
        sorter: (a, b) => a.last_workers - b.last_workers,
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
      {
        title: '件数(尾期)', dataIndex: 'last_qty',
        sorter: (a, b) => a.last_qty - b.last_qty,
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
      {
        title: '備注', dataIndex: 'remark',
        render: (v, o, i) => o.editable ? <TextArea defaultValue={v} className={`${ANIMATE.bounceIn}`} onChange={this.remarkChange.bind(this, v, o, i)} rows={2} /> : <span>{v}</span>
        // render: (v, o, i) => <TextArea onChange={this.remarkChange.bind(this, v, o, i)} rows={2} />
      },
    ]
    const actionColumn = {
      title: '編輯', dataIndex: 'editable',
      className: 'standerdEditCol', 
      render: (t, v, i) => {
        // console.log('t, v, i ：', t, v, i);
        return <Button onClick={() => t ? this.saveData(t, v, i) : this.editDataAction(t, v, i)} type="primary" icon={`${t ? 'check' : 'edit'}`} className={`m-r10 ${t ? 'succ' : ''}`}>{t ? '保存' : '編輯'}</Button>
        // return <Button onClick={() => this.editData(t, v, i)} shape="circle" type="primary" icon='edit' className='succ'></Button>
      }
    }
    auth && columns.push(actionColumn)

    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={pagination}
        onRowClick={this.onRowClick.bind(this, )}
        onChange={this.handleTableChange.bind(this, )}
        rowKey={record => record[rowKey]}
        
        className={`${ANIMATE.slideInUp}`}
        rowClassName={(record, i) => i === this.state.deleteItem && canDelete ? ANIMATE.slideOutLeft : ANIMATE.zoomIn}
      />
    );
  }
}

export default SecureHoc(Tables)