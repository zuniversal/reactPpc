import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import SecureHoc from '@@@@/SecureHoc'
import { Card, Collapse, Icon } from 'antd';
import { ANIMATE } from 'constants'
const Panel = Collapse.Panel;

class Collapses extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      activeKey: '1',
      isClose: false,
    }
  }
  callback = _ => {
    let { activeKey } = this.state
    activeKey = activeKey === '1' ? '2' : '1'
    this.setState({ activeKey })
    console.log(activeKey);
  }
  close = _ => {
    this.setState({ isClose: true, activeKey: '2' })
  }
  render() {
    const { activeKey, isClose } = this.state
    const { title, children, className, extra, animate, noAnimate, auth, noLimit,  } = this.props
    // console.log('Collapses组件 ：', this.props, activeKey, noAnimate, noAnimate ? 11 : 22, auth, auth && !noLimit, auth || noLimit ? extra : null);
    const animation = !noAnimate ? animate != undefined ? animate : ANIMATE.flipinX : '' 
    return (
      isClose ? null :
        <Card title={title} className={`${animation} ${className} ${activeKey === '1' ? 'collapse expend' : 'collapse noExpend'}`}
          extra={
            <div className="clearfix">
              {auth || noLimit ? extra : null}&nbsp;&nbsp;&nbsp;
              <Icon type={`${activeKey === '1' ? 'up' : 'down'}`} onClick={this.callback} />
              &nbsp;&nbsp;&nbsp;<Icon type='close-circle-o' onClick={this.close} /></div>
          }
        >
          <Collapse activeKey={activeKey} onChange={this.callback} accordion>
            <Panel key="1">{children}</Panel>
          </Collapse>
        </Card>
    );
  }
}

export default SecureHoc(Collapses)