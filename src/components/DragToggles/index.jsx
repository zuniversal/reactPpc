import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { findDOMNode, ajax } from 'util'
import ReactDOM from 'react-dom'
import Draggable from 'react-draggable'

class DragToggle extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      activeDrags: 0,
      controlledPosition: {
        x: 0, y: 0
      },
      which: '',
      sideBarScroll: 0,
    }
  }

  // 横向滚动条拖动事件
  handleDrag = (e, ui, d) => {
    const { controlledPosition, which } = this.state
    console.log('handleDrag', e, ui, d);
    controlledPosition[e] = d[e]
    this.setState({ controlledPosition: { ...controlledPosition } });
  }
  onStart = (a) => {
    console.log('onStart', a, );
    this.setState({ activeDrags: ++this.state.activeDrags, which: a, });
  }
  onStop = (onStop, d) => {
    const { controlledPosition, which } = this.state
    const {tableHeight, tableWidth} = this.props
    console.log('onStop：', this.props,onStop, d, this.state, which, controlledPosition, controlledPosition[onStop], d[onStop]);
    if (which === 'x') {
      if (d[which] < 0) {
        d[which] = Math.max(0, d[which])
      } else {
        d[which] = Math.min(d[which], tableWidth)
      }
    } else {
      if (d[which] < 0) {
        d[which] = Math.max(0, d[which])
      } else {
        d[which] = Math.min(d[which], tableHeight)
      }
    }
    controlledPosition[which] = d[which]
    console.log('d[onStop] ：', d[which], which ,d);
    this.setState({ controlledPosition: { ...controlledPosition } });
  }
  
  setScrollTop = (scrollTop) => {
    const {sideBarScroll, } = this.state
    const {scrollTopPos,} = this.props
    // console.log('@@@@@@@@@setScrollTop ：', scrollTopPos, scrollTop, sideBarScroll)
    // this.setState({
    //   sideBarScroll: scrollTop, 
    // })
    // ajax(this.setState.bind(this), {sideBarScroll: scrollTop, })

    ajax(this.props.sideBarScroll, scrollTop)
  
    // this.props.sideBarScroll(scrollTop)
    // ajax(this.props.sideBarScroll, scrollTop > sideBarScroll ? scrollTopPos + scrollTop : scrollTopPos - scrollTop, 300)
  }

  componentDidMount() {
    console.log(' 组件componentDidMount挂载 ：', this.state, this.props, )
    const domRef = findDOMNode(ReactDOM, this.sideBar)
    domRef.onscroll = (e) => { 
     // console.log('同步拖拽 组件 this.state, this.props ：', this.state, this.props, domRef)
      const {scrollTop} = domRef
      const {sideBarScroll, } = this.state
      // if (sideBarScroll !== scrollTop) {
        // ajax(this.setScrollTop.bind(this), scrollTop, 300)
        this.setScrollTop(scrollTop)
      // }
    }
  }
  // componentWillReceiveProps(nextProps) {
  //   console.log('同步拖拽 组件 componentWillReceiveProps组件获取变化 ：', nextProps, this.props, this.state,  )
  //   if (nextProps.scrollTopPos !== this.props.scrollTopPos) {
  //     this.setState({
  //       sideBarScroll: sideBarScroll
  //     })
  //   }
  // }
  
  

  render() {
    const dragHandlers = { onStart: this.onStart, onStop: this.onStop };
    const { children, tableHeight, tableWidth, collapse, 
      scrollLeftPos, scrollTopPos,  
    } = this.props
    const { sideBarScroll, controlledPosition, which } = this.state;
    // const style = {'zIndex': 100}
    const leftPos= {'left': collapse ? '74px' : '210px'}
    
    // console.log('########## ：', this.props, sideBarScroll, controlledPosition, collapse, scrollLeftPos, scrollTopPos);
    return (
      <div className="dragToggle">
        {/* 主体 */}
        <Draggable bounds="parent" onStart={() => false}>
          {children[2]}
        </Draggable>
        {/* 固定头 */}
        <div className={`headerFixs`} style={leftPos}>
          <Draggable bounds="parent" axis="x" position={{ x: -scrollLeftPos, y: 0 }} disabled={true} onStart={() => false}>
            {children[0]}
          </Draggable>
        </div>
        {/* 固定列 */}
        <div className={`sideBarFixs`} style={leftPos} ref={sideBar => this.sideBar = sideBar}>
          {/* <Draggable bounds="parent" axis="y" position={{ x: 0, y: -scrollTopPos }} disabled={true} onStart={() => false}> */}
          <Draggable bounds="parent" axis="y" disabled={true} onStart={() => false}>
            {children[1]}
          </Draggable>
        </div>
        {/* 主体 */}
        <Draggable bounds="parent" position={{ y: -sideBarScroll }} onStart={() => false}>
          <div className="contentWrapper">
              {children[3]}
          </div>
        </Draggable>
        {/* 固定角 */}
        <div className="fixBox" style={leftPos}>
          {children[4]}
        </div>
        {children[5]}
      </div>
    );
  }
}

export default DragToggle;