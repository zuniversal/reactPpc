import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {showTotal, dateForm, ajax, infoFilter, } from 'util'
import { ANIMATE, SIZE, } from 'constants'
import { statusInfo, } from 'config'
import Count from '@@@@/Count'
import { Table, Tag, Button, Icon, DatePicker, Select,  } from 'antd';
import moment from 'moment';

class Tables extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
    };
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data, )
    this.setState({
      data, 
      loading,
    })
  }
  render() {
    const { pagination,  } = this.state
    const {data, } = this.props
    console.log('Tables 组件 this.state, this.props ：', data, this.state,  this.props,  )
    const rowKey = 'ids'
    const columns = [
      // {
      //   title: 'ids', dataIndex: 'ids',
      //   render: (v, o, i) => <Tag color={'#108ee9'}>{v}</Tag>
      // },
      // {
      //   title: 'pn_acc_id', dataIndex: 'pn_acc_id',
      //   render: (v, o, i) => <Tag color={'#108ee9'}>{v}</Tag>
      // },
      // {
      //   title: 'acc_id', dataIndex: 'acc_id',
      //   render: (v, o, i) => <Tag color={'#108ee9'}>{v}</Tag>
      // },
      {
        title: '齊倉', dataIndex: 'acc_status',
        render: (v, o, i) => {
          const status = statusInfo.find(item => item.k === v)
          return <Tag color={status.color}>{status.t}</Tag>
        }
      },
      {
        title: '批號', dataIndex: 'pn_no',
        render: (v, o, i) => <Tag color={'#108ee9'}>{v}</Tag>
      },
      {
        title: '生産單數', dataIndex: 'pn_qty',
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
      {
        title: '客戶名', dataIndex: 'customer_id',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '品種', dataIndex: 'acc_name',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '采購方式', dataIndex: 'com_position',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '訂購詳細', dataIndex: 'po_desc_dtl',
        render: (v, o, i) => { 
          return <div className='max180' >
            {v != undefined ? v.split(';').filter(v => v.trim() !== '').map((item, i) => <div  key={i} >
              <Tag color={`${i % 2 === 0 ? 'salmon' : 'cyan'}`}>{item}</Tag>
            </div>) : null}
            {/* {v != undefined ? v : null} */}
          </div> 
        }
      },
      {
        title: '送貨日期', dataIndex: 'deviler_date',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '入倉日期', dataIndex: 'stock_date',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '入倉詳細', dataIndex: 'stock_desc_dtl',
        render: (v, o, i) => <span className='max180'>{v}</span>
      },
      {
        title: '領料狀態', dataIndex: 'transfer_status', 
        render: (v, o, i) => v != undefined ? <Tag color={'purple'}>{v}</Tag> : null 
      },
      {
        title: '工廠', dataIndex: 'work_shop',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '品種詳細', dataIndex: 'acc_desc',
        render: (v, o, i) => <span>{v}</span>
        // render: (v, o, i) => { 
        //   return <div className='max180' >
        //     {v.split(';').filter(v => v.trim() !== '').map((item, i) => <div  key={i} >
        //       <Tag color={`${i % 2 === 0 ? 'cyan' : 'salmon'}`}>{item}</Tag>
        //     </div>)}
        //   </div> 
        // }
      },
    ]
    return (
      <Table 
        bordered={true}
        columns={columns}
        dataSource={data}
        pagination={false}
        rowKey={record => record[rowKey]}
        rowClassName={(record, i) => ANIMATE.bounceIn}
        className='pnDetialTable ' 
      />
    );
  }
}

export default Tables