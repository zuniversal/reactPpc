import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import Inputs from '@@@@/Inputs'
import SecureHoc from '@@@@/SecureHoc'
import {showTotal, dateForm, ajax, infoFilter, confirms, } from 'util'
import { ANIMATE, SIZE, } from 'constants'
import { DATE_FORMAT, DATE_FORMAT_BAR, poTableConfig, actionType, } from 'config'
import { Table, Tag, Button, Icon, DatePicker, Select, Modal,  } from 'antd';
import moment from 'moment';
const confirm = Modal.confirm;

class Tables extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      loading: false, 
      selectedRowKeys: [],
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    console.log('handleTableChange ：', pagination, filters, sorter)
    const { pageSize, current } = pagination
    this.props.getPn(pageSize, current)
  }
  renderColumns = (v, o, i, key) => {
    // console.log('v, o, i, key ：', v, o, i, key, key.indexOf('date'))
    return o.editAble ? <Inputs noRule={true} className='t-c' data={v} keys={key} val={o} i={i} size={'default'} inputing={this.props.tableInput}></Inputs> 
    : key.indexOf('date') > 0 ? moment(v).format(DATE_FORMAT_BAR) : <span>{v}</span>  
  }
  copy = (v) => {
    console.log(' copy ： ', v, this.props )
  }
  deletePn = (v) => {
    console.log(' deletePn ： ', v, this.props, this.state,  )
    confirms(2, '暫未开发 !', )
  }
  // v, o, i, e, 'deletePn', 'delete', `批號
  showConfirm = (v, o, i, e, t, action = 'delete', item = '' ) => {
    console.log('showConfirm ：', v, o, i, e, t, action,  );
    e.stopPropagation()// 
    confirm({
      title: `刪除操作 `,
      content: `確認 ${actionType[action]} ${item} 嗎 ?`,
      width: 480,
      onOk: () => this[t](v, o, i, e),
      onCancel() {
        console.log('取消刪除操作  ');
      },
    });
  }
    
  componentDidMount() {
    const { loading, data, total,  } = this.props
    const {pagination, } = this.state 
    pagination.total = total
    // console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data)
    this.setState({
      loading,
      pagination,
    });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('Table props 属性的改变', nextProps)
    const { loading, total } = nextProps
    const {pagination, } = this.state 
    pagination.total = total
    this.setState({
      loading,
      pagination,
    });
  }
  render() {
    const { pagination, loading, selectedRowKeys } = this.state
    const {data, auth, } = this.props
    console.log('PoArrangeTable 组件 this.state, this.props ：', data, this.props,  )
    const rowKey = 'pn_no'
    const columns = [
      {
        title: '批號', dataIndex: 'pn_no',
        render: (v, o, i) => <Tag color={'red'} onClick={() => this.copy(v)}>{v}</Tag>
      },
      {
        title: '針種', dataIndex: 'gauge_no',
        render: (t, v, i) => this.renderColumns(t, v, i, 'gauge_no',)
      },
      {
        title: '縫盤針種', dataIndex: 'sew_gauge',
        render: (t, v, i) => this.renderColumns(t, v, i, 'sew_gauge',)
      },
      {
        title: '毛期', dataIndex: 'yarn_date',
        render: (t, v, i) => this.renderColumns(t, v, i, 'yarn_date',)
      },
      {
        title: '輔料期', dataIndex: 'acs_date',
        render: (t, v, i) => this.renderColumns(t, v, i, 'acs_date',)
      },
      {
        title: '預計開機期', dataIndex: 'yj_mc_date',
        render: (t, v, i) => this.renderColumns(t, v, i, 'yj_mc_date',)
      },
      {
        title: '生産完成期', dataIndex: 'prod_date',
        render: (t, v, i) => this.renderColumns(t, v, i, 'prod_date',)
      },
      {
        title: '客戶名', dataIndex: 'cust_name',
        render: (t, v, i) => this.renderColumns(t, v, i, 'cust_name',)
      },
      {
        title: '數量', dataIndex: 'qty',
        render: (t, v, i) => this.renderColumns(t, v, i, 'type_id',)
      },
      {
        title: '數量明細', dataIndex: 'qty_dtl',
        className: 'min180',
        render: (v, o, i) => {
        //   return v != undefined ? v.split(';').map((vv, i) => <div className='tagWrapper' key={i}>
        //   {vv === '' ? null : <Tag color={'pink'} >{vv.split('--')[0]}</Tag>} 
        //   {vv === '' ? null : <Tag color={'#108ee9'} >{vv.split('--')[1]}：</Tag>} 
        //   {vv === '' ? null : <Tag color={'red'} >{vv.split('--')[2]}</Tag>} 
        // </div>) : null
          return v != undefined ? v.split(';').map((vv, i) => vv !== '' ? <div className='tagWrapper' key={i}> 
          {infoFilter(poTableConfig, 'colorSize', 'tagItem').map((item, index) => <Tag color={item} key={item} >
            {vv.split('--')[index]}{index !== 2  ? ':' : ''}
          </Tag> )}
        </div> : null) : null
        }
      },
      {
        title: '工序安排', dataIndex: 'work_dtl',
        className: 'min120',
        render: (v, o, i) => {
          // console.log('v != undefined ? ：', v != undefined ? v.split(';')[0] === '' : 1111111)
        //   return v != undefined ? v.split(';').map((vv, i) => <div className='tagWrapper' key={i}>
        //   {vv === '' ? null : <Tag color={'#f50'} >{vv.split('--')[0]}：</Tag> } 
        //   {vv === '' ? null : <Tag color={'purple'} >{vv.split('--')[1]}</Tag>} 
        // </div>) : null
          return v != undefined ? v.split(';').map((vv, i) => vv !== '' ? <div className='tagWrapper' key={i}>
          {infoFilter(poTableConfig, 'process', 'tagItem').map((item, index) => <Tag color={item} key={item} >
            {vv.split('--')[index]}{index !== 1  ? '：' : null}
          </Tag> )}
          {/* <Tag color={'#f50'} >{vv.split('--')[0]}：</Tag> 
          <Tag color={'purple'} >{vv.split('--')[1]}</Tag>  */}
        </div> : null) : null
        }
      },
    ]
    const actionColumn = {
      title: '操作列', dataIndex: 'editable',
      render: (v, o, i) => {
        // console.log(' v, o, i ： ', v, o, i, o.pn_no  )// 
        return (<div className="btnWrapper">
        {/* <Button onClick={() => o.editAble ? this.saveData(v, o, i) : this.editData('編輯批號', o, i)} 
          shape="circle" type="primary" icon={`${o.editAble ? 'check' : 'edit'}`} className={`${o.editAble ? 'succ' : 'warn'} m-r10`}></Button> */}
        <Button onClick={() => this.props.editData('編輯批號', o, i)} shape="circle" icon={`edit`} type="primary" className="succ m-r10"></Button>
        <Button onClick={(e) => this.showConfirm(v, o, i, e, 'deletePn', 'delete', `批號 ${o.pn_no}`)} shape="circle" type="primary" icon='delete' className="warn"></Button>
        <Button onClick={() => this.props.getColorSize('colorDetail', o, i)} type="primary" className="m-r10">色码表</Button>
        <Button onClick={() => this.props.getSidePnWork('processDetail', o, i)} type="primary" className="">工序表</Button>
      </div>)
      }
    }
    auth && columns.push(actionColumn)

    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={pagination}
        rowKey={record => record[rowKey]}
        rowClassName={(record, i) => ANIMATE.bounceIn}

        onChange={this.handleTableChange}
        // className={`poArrangeTable ${auth ? 'minTd' : ''}`}
        className={`poArrangeTable `}
      />
    );
  }
}

export default SecureHoc(Tables)