import React from "react"
import ReactDOM from 'react-dom';
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {wipe, ajax, findDOMNode, confirms, } from 'util'
import {grid} from 'config'
import { ANIMATE, HEADER_HEIGHT, } from 'constants'
import Draggable from 'react-draggable'
import { Tooltip, Slider, Popover, Button, Icon, } from 'antd';

class DragableSlider extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      dragAction: '', 
      activeDrags: 0,
      dragDirection: false,
      bounds: "parent",
      initPosition: {
        x: 0, y: 0
      },
      // 默认是不拖动的，当拖拽开始时才是dragging
      dragging: false,
      isDrag: '',
      dayRange: [],
      changeBar: '',
      visible: true,
      zIndex: 1,
    }
  }
  // widthAdd = (w) => {
  //   console.log('widthAdd ：', w);
  //   this.refs.dragBar.style.width = w + grid + 'px'
  // }
  // widthDecrease = (w) => {
  //   console.log('widthDecrease ：', w);
  //   this.refs.dragBar.style.width = w !== grid ? w - grid + 'px' : '60px'
  // }

  handleDrag = (e, ui, d) => {
    this.setState({
      dragging: true,
    })
    const {dragAction, initPosition} = this.state
    const barWidth = this.refs.dragBar.style.width
    const width = Number(wipe(barWidth))
    // console.log('#bar拖动：', width, e, ui, dragAction, d.lastX , d.x, d, initPosition, this.props, );

    // if (dragAction !== '') {
    //   const isDragLeft = dragAction === 'dragLeft'
    //   if (isDragLeft) {
    //     console.log('左拖 ：', );
    //     if (d.lastX - d.x > 0) {
    //       console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
    //       this.widthAdd(width)
    //     } else {
    //       console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
    //       this.widthDecrease(width)
    //     }
    //   } else {
    //     console.log('右拖 ：', d.lastX, d.x);
    //     if (d.lastX - d.x < 0) {
    //       console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
    //       this.widthAdd(width)
    //     } else {
    //       console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
    //       this.widthDecrease(width)
    //     }
    //   }
    // }

    const { x, y } = d;
    ajax(this.setState.bind(this), {
      dragging: true,
      initPosition: { x, y }
    })
    // this.setState({
    //   // dragging: true,
    //   initPosition: { x, y }
    // })
  }

  dragAction = (v, a, d) => {
    console.log('#bardragAction v ：', v, a, d, this.state, this.props);
    const {noAction, } = this.props// 
    const { x, y } = this.state.initPosition;
    const {data} = this.props
    if (noAction) {
      return  
    }
    
    if (data.lock === 'T' || data.finish_status === 'T') {
      confirms(0, '請先解鎖再進行操作！',  )
    } else {
      this.setState({
        isDrag: v,
        changeBar: data.barcode,
      })
      this.props.changeBar(data)
    }
    // console.log('v ===ght:  ：', v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: y, right: left + width, bottom: 0})
    
    // this.setState({
    //   dragAction: v,
    //   // bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : "parent",
    //   bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: 0, left: 0, right: left + width - grid, bottom: 0},
    // })
  }
  onStart = (a, b) => {
    console.log('#bar a：', a, b, );
    this.setState({ activeDrags: ++this.state.activeDrags,  });
  }

  onStop = (onStop, e) => {
    const barWidth = this.refs.dragBar.style.width
    const {barcode} = this.props.config
    const {dragging, isDrag, left} = this.state
    const {data, pn_no, clickType, indexes, barcodeData, s_date, e_date, } = this.props
    const width = Number(wipe(barWidth)) - grid
    console.log('#@@@@@@@@@@@@@@@@@@@#bar onStop：', barcode, pn_no, this.props, onStop, e, width, '是否是拖动：', dragging, isDrag, this.state, this.props, );
    const {lastX, lastY} = e
    const x = lastX / grid
    const y = lastY / grid
    const w = x + width / grid
    const newBarcode = {
      ...barcodeData,
      s_date, e_date
    }
    // if (clickType === 'barClick' && dragging) {
    // 两个页面都可以拖动
    if (dragging) {
      // console.log('拖动参数 ：', x * grid, this.state.left, {x, y, w, barcode, data, pn_no})
      ajax(this.props.stopDrag, {x, y, w, barcode, data, pn_no, barcodeData: newBarcode, indexes, dragAction: x * grid > this.state.left ? 'dragRight' : 'dragleft'})
      this.props.changeBar(data)
      // this.props.stopDrag(x, y, w, barcode)
    }
    // console.log('onStop：onStop：{x, y, w, barcode, data, pn_no, dragA ：', x * grid, width, {x, y, w, barcode, data, pn_no, dragAction: 'dragging'})
    this.setState({ 
      left: x * grid, 
      width: width + grid,
      activeDrags: --this.state.activeDrags, 
      dragAction: '', 
      dragging: false,
      bounds: "parent",
    });
  }
  componentWillMount() {
    const { left, width } = this.props.config
    this.setState({
      left, width
    })
    // console.log('componentDidMount ：', )
  }

  barClick = (e) => {
    console.log('barClick22：', e, this.props, this.state)
    const {pn_no, clickType, indexes} = this.props
    const {barcode, txt} = this.props.config
    if (clickType === 'barClick') {
      const {work_no, } = this.props.data
      this.props.showBarcodeDialog(barcode, pn_no, work_no, indexes)
    } else {
      this.props.getPnDetailList(txt)
    }
  }

  onChange = (value) => {
  }
  zIndex = (v) => {
    // console.log(' zIndex ： ', this.state.zIndex )
    let {zIndex, } = this.state 
    this.setState({
      zIndex: zIndex+1,
    })
  }
  
  changeBar = (data) => {
    console.log('changeBar  data ：', data, this.props)
    this.setState({
      changeBarData: data,
      isDragBack: false,
      barcode: data.barcode,
    })
  }
  onAfterChange = (value) => {
    const {barcode} = this.props.config
    const { data, pn_no, dateLen, dateArr, indexes, barcodeData, s_date, e_date, } = this.props
    const {dayRange, isDrag} = this.state
    // const {left, width} = this.props.config
    const {left, width} = this.state
    const widths = dateLen * grid
    const daySubVal = dayRange[0] - value[0]
    const daySub2Val = dayRange[1] - value[1]
    const daySub = daySubVal !== 0 ? daySubVal > 0 ? '左边拉长' : '左边缩短' : '左边不变'
    const daySub2 = daySub2Val !== 0 ? daySub2Val > 0 ? '右边缩短' : '右边拉长' : '右边不变'
    const leftSteps = daySubVal / (grid / widths * 100)
    const rightStep = daySub2Val / (grid / widths * 100)// 
    console.log('isNaN(daySubVal) ：', daySubVal, isNaN(daySubVal), leftSteps, rightStep, this.state,  this.props)
    const newBarcode = {
      ...barcodeData,
      s_date, e_date
    }
    let changeLeft, changeWidth
    if (isDrag === 'dragLeft') {
      // console.log('dragLeft ：', )
      if (!isNaN(daySubVal)) {
        changeLeft = left - Math.round(leftSteps) * grid
        changeWidth = width + Math.round(leftSteps) * grid
        // console.log('左边变化 ：', leftSteps, '距离', left, left - Math.round(leftSteps) * grid, '宽度',width, width + Math.round(leftSteps) * grid, )
        this.setState({
          left: changeLeft, 
          width: changeWidth, 
        })
        const params = {x: changeLeft / grid, y: 0, w: (changeLeft + changeWidth - grid) / grid, barcodeData: newBarcode, indexes, barcode, data, pn_no, dragAction: isDrag}
        this.props.stopDrag(params)
      }
    } else if (isDrag === 'dragRight') {
      // console.log('dragLeft右边 ：', )
      if (!isNaN(daySub2Val)) {
        changeLeft = left
        changeWidth = width - Math.round(rightStep) * grid
        // console.log('右边变化 ：', rightStep, '距离', left, left - Math.round(rightStep) * grid, '宽度',width, width + Math.round(rightStep) * grid, )
        this.setState({
          width: width - Math.round(rightStep) * grid, 
        })
        const params = {x: changeLeft / grid, y: 0, w: (changeLeft + changeWidth - grid) / grid, barcodeData: newBarcode, indexes, barcode, data, pn_no, dragAction: isDrag}
        this.props.stopDrag(params)
      }
    } else {
      this.setState({
        isDrag: '',
      })
      
    }
    console.log('onAfterChange: ',  changeLeft / grid, changeWidth / grid,  `歩长:${daySub} ${daySub2} ${leftSteps} ${Math.round(leftSteps)}left: ${left}width: ${width} `, daySubVal, daySub2Val, value, value[0] / dateLen, value[1]/ dateLen);
    this.setState({
      dayRange: value,
    })
  }
  formatter = (value, e) => {// 
    const { dateArr, s_date, e_date } = this.props
    // console.log('formatter: ', value, e, dateArr, s_date, e_date, this.state);
    return (<div className="formatWrapper">
      <div className="formatCon">原起止日期：{s_date} 到 {e_date}</div>
    </div>)
  }

  componentWillReceiveProps(nextProps) {
    // console.log('发生变化的bar dragging componentWillReceiveProps组件获取变化 ：', nextProps, this.props,  this.props.config.barcode)
    if (nextProps.changeBarData.barcode === this.props.config.barcode) {
      const {isDragBack, showEdit } = nextProps
      const {left, width,  } = nextProps.changeBarData
      const {isDrag, changeBar, } = this.state 
      // console.log('是同一条bar  222发生变化的bar dragging componentWillReceiveProps组件获取变化 ：', isDrag === '', this.refs.draggableRefs, changeBar, nextProps, this.props, this.state )
      if (isDragBack && isDrag === '') {
        // console.log('拖拽回去 left, width ：', left, width)
        // findDOMNode(ReactDOM, this.refs.dragBar).style.transform = `translate(${left}px, 0px)`
        this.setState({
          left, 
          width,
          changeBar: '',
          isDrag: '',
          dayRange: [],
          initPosition: { x: left, y: 0 },
        })
        // console.log('this.refs.draggableRefs ：', this.refs.draggableRefs)
        
        this.refs.draggableRefs.state.x = left
      }
      if (showEdit) {
        // console.log('去除状态 ：', )
        this.setState({
          changeBar: '',
          isDrag: '',
          dayRange: [],
        })
      }
    } else {
      // console.log('不是同一条bar ：', )
      this.setState({
        isDrag: '',
      })
    }
  }
  

  render() {
    const { dragging, dragAction, bounds, isDrag, zIndex,  } = this.state;
    const { config, isAxis, isTop, dateLen, data, clickType, noAction,  } = this.props
    const { top, txt, levels, barcode, shortDay, } = config
    const { real_qty, open_date, rate, realStart, realWidth, finish_status, isOutDay, } = data
    const isLock = data.lock === 'F' || data.lock == undefined 
    const isFinish = finish_status === 'F' || finish_status == undefined 
    // const isLock = true
    // const {left, width} = config
    const {left, width} = this.state
    const dragHandlers = { onStart: (dragAction === '' && isLock && isFinish) && !noAction ? this.onStart : () => false, onStop: this.onStop };
    // console.log('DragableSlider 组件 this.state, this.props ：', data.lock, finish_status, this.state, this.props, data.realStart, dragAction, dragAction === '' ? false : true)
    
    // if (left > 0 && barcode === '100000884') {
    //   console.log('当前组件bar ：', this.state)
    // }
    // console.log('left > 0 &&当前组件bar ：', this.state, this.props)
    const style = {
      zIndex,
      width: width + 'px',
      background: dragging ? 'rgba(111, 255, 228, 0.8)' : '',
      // zIndex: dragging ? 10 : 1
      // position: dragAction === '' ? 'absolute' : 'fixed'
      // left: 0 + 'px',
    }
    const defaultPosition = { x: left, y: isTop ? top : 0}
    const widths = dateLen * grid
    const lefts = Math.abs(left)
    // const sliderStyle = isDrag !== '' ? {width: widths, ...clickType !== 'barClick' ? {top: (tableHeight + levels - 1) * grid + HEADER_HEIGHT} : null} : null
    const sliderStyle = isDrag !== '' ? {width: widths, ...clickType !== 'barClick' ? {top: (levels - 1) * grid} : null} : null
    
    // const realBar = shortDay.length ? shortDay.map((v, i) => <Tooltip placement="top" title={`${v.s_date}日 - 實際生産：${v.qty}`} key={i}><div className="real" style={{left: v.shortDate + 240}}>
    //   {/* {v.qty} */}
    //   </div></Tooltip>) : null
    
    // const realBar = realStart != undefined ? <Tooltip placement="top" title={`${open_date} 日 - 實際生産：${real_qty}`}>
    //   <div className="real" style={{left: realStart, width: realWidth, position: 'absolute'}}></div>
    // </Tooltip> : null
    const shortBar = shortDay.length && isOutDay ? shortDay.map((v, i) => <div className="short" style={{left: v.shortDate}} key={i}></div>) : null
    // console.log('shortDay ：', shortDay, shortBar)
    return (
      isDrag === '' ? (
        <Draggable axis={isAxis} bounds={bounds} ref="draggableRefs"
        defaultPosition={defaultPosition} grid={[grid, grid]} 
        onDrag={this.handleDrag.bind(this, barcode)} {...dragHandlers}>
          <div className={
            // !isLock || 
            !isFinish ? 'lockBar box' : 'box' }
            onClick={this.zIndex} ref="dragBar" style={style}>
            <Tooltip placement="top" title={'点击开始拖拽改变生产日期'}>
              <div className="dragBorder dragLeft" onMouseDown={this.dragAction.bind(this, 'dragLeft')}>
                {/* <Slider className='sliders' range 
          tipFormatter={this.formatter}
          //step={1} 
          step={grid / widths * 100}
          defaultValue={[(lefts) /( widths) * 100, (lefts + width) / (widths) * 100]}  
          onChange={this.onChange} onAfterChange={this.onAfterChange} /> */}
              </div>
            </Tooltip>
            {/* <Tooltip placement="top" title={txt}> */}
              
            <Popover content={<span className="barPopover">
              {
                // !isFinish || 
                !isLock ? <Icon type="lock" className='barcodeLock' /> : null} {txt}
              <Button type="primary" onClick={this.barClick.bind(this)} className='detailbtn'  size='small'>
                详情
              </Button>
              </span>}
            >
              {/* <span onClick={this.barClick.bind(this)}>{txt}</span> */}
              <div className={`machineName ${ANIMATE.bounceIn}  ${isOutDay != undefined && !isOutDay && isFinish ? 'isOutDay' : ''} `}>{
                // !isFinish || 
                !isLock ? <Icon type="lock" className='barcodeLock' /> : null} {txt}</div>
            </Popover>
              
            {/* </Tooltip> */}
            {isFinish ? <div className={`work`} ></div> : null}
            
            {/* {isOutDay ? null :<div className={`isOutDay`}></div> }  */}
            {/* {realBar} */}
            {shortBar}
            <Tooltip placement="top" title={'点击开始拖拽改变生产日期'}>
              <div className="dragBorder dragRight" 
                onMouseDown={this.dragAction.bind(this, 'dragRight')}
              ></div>
            </Tooltip>
          </div>
        </Draggable>) : (<Slider className={`sliders ${isDrag}`} style={sliderStyle} range 
        tipFormatter={this.formatter}
        //step={1} 
        step={grid / widths * 100} 
        defaultValue={[(lefts) / (widths) * 100, (lefts + width) / (widths) * 100]}  
        onChange={this.onChange} onAfterChange={this.onAfterChange} />)
    );
  }
}

export default DragableSlider;