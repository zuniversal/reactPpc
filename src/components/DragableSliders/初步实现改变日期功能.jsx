import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"

import './style.less'
import {wipe, ajax} from 'util'
import {grid} from 'config'
import { ANIMATE,  } from 'constants'
import ProductDialog from '@@@/ProductDialog'
import Draggable from 'react-draggable'
import { Tooltip, Slider } from 'antd';
class DragableSlider extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      dragAction: '', 
      activeDrags: 0,
      dragDirection: false,
      bounds: "parent",
      initPosition: {
        x: 0, y: 0
      },
      // 默认是不拖动的，当拖拽开始时才是dragging
      dragging: false,
      isDrag: '',
      dayRange: [],
      show: false,
    }
  }

  dbClick = (barcode) => {
    console.log('dbClick ：', barcode, this.props, );
    this.props.showBarcodeDialog(barcode)
  }
  widthAdd = (w) => {
    console.log('widthAdd ：', w);
    this.refs.dragBar.style.width = w + grid + 'px'
  }
  widthDecrease = (w) => {
    console.log('widthDecrease ：', w);
    this.refs.dragBar.style.width = w !== grid ? w - grid + 'px' : '60px'
  }

  handleDrag = (e, ui, d) => {
    const {dragAction} = this.state
    const barWidth = this.refs.dragBar.style.width
    const width = Number(wipe(barWidth))
    console.log('#bar拖动：', width, e, ui, dragAction, d.lastX , d.x, d, this.props);
    // this.props.short(this.props.config.barcode)
    if (dragAction !== '') {
      const isDragLeft = dragAction === 'dragLeft'
      // this.refs.dragBar.style.width = isDragLeft ? width - ui.deltaX + 'px' : width + ui.deltaX + 'px'
      if (isDragLeft) {
        console.log('左拖 ：', );
        if (d.lastX - d.x > 0) {
          console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
          this.widthAdd(width)
          // this.refs.dragBar.style.width = width + grid + 'px'
        } else {
          console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
          this.widthDecrease(width)
          // this.refs.dragBar.style.width = width !== grid ? width - grid + 'px' : '60px'
        }
      } else {
        console.log('右拖 ：', d.lastX, d.x);
        if (d.lastX - d.x < 0) {
          console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
          this.widthAdd(width)
          // this.refs.dragBar.style.width = width + grid + 'px'
        } else {
          console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
          this.widthDecrease(width)
          // this.refs.dragBar.style.width = width !== grid ? width - grid + 'px' : '60px'
        }
      }
    }
    const { x, y } = d;
    ajax(this.setState.bind(this), {
      dragging: true,
      initPosition: { x, y }
    })
    this.setState({
      dragging: true,
      initPosition: { x, y }
    })
  }

  dragAction = (v, a, d) => {
    const { x, y } = this.state.initPosition;
    const {dateLen, data} = this.props
    const {width, left} = this.props.config
    
    this.setState({
      isDrag: v,
    })
    // this.props.changeBar(data)
    console.log('#bardragAction v ：', v, a, d, this.state, this.props);
    // console.log('v ===ght:  ：', v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: y, right: left + width, bottom: 0})
    
    // this.setState({
    //   dragAction: v,
    //   // bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : "parent",
    //   bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: 0, left: 0, right: left + width - grid, bottom: 0},
    // })
  }
  onStart = (a, b) => {
    console.log('#bar a：', a, b, );
    this.setState({ activeDrags: ++this.state.activeDrags,  });
  }

  onStop = (onStop, e) => {
    const barWidth = this.refs.dragBar.style.width
    const {barcode} = this.props.config
    const {dragging} = this.state
    const {data, pn_no, clickType} = this.props
    const width = Number(wipe(barWidth)) - grid
    console.log('#@@@@@@@@@@@@@@@@@@@#bar onStop：', barcode, pn_no, this.props, onStop, e, width, '是否是拖动：', this.state.dragging);
    const {lastX, lastY} = e
    const x = lastX / grid
    const y = lastY / grid
    const w = x + width / grid
    if (clickType === 'barClick' && dragging) {
      // ajax(this.props.stopDrag, {x, y, w, barcode, data, pn_no})
      // this.props.stopDrag(x, y, w, barcode)
    }
    this.setState({ 
      activeDrags: --this.state.activeDrags, 
      dragAction: '', 
      dragging: false,
      bounds: "parent",
    });
  }
  componentWillMount() {
    const { left, top, width } = this.props.config
    this.setState({
      left, width
    })
    
    // console.log('componentDidMount ：',  this.props, )
  }

  barClick = (e) => {
    console.log('barClick ：', e, this.props)
    const {pn_no, clickType} = this.props
    const {barcode, txt} = this.props.config
    if (clickType === 'barClick') {
      const {work_no, } = this.props.data
      this.props.showBarcodeDialog(barcode, pn_no, work_no)
    } else {
      this.props.getPnDetailList(txt)
    }
  }

  onChange = (value) => {
    // const { dateLen, dateArr } = this.props
    // const {dayRange} = this.state
    // const widths = dateLen * grid
    // // const days0 = dateArr[value[0]] 
    // // const days1 = dateArr[value[1]]
    // const days0 = dayRange[0]
    // const days1 = dayRange[1]
    // const oldLen = dayRange[1] - dayRange[0]
    // const newLen = value[1] - value[0]
    // const daySubVal = dayRange[0] - value[0]
    // const daySub2Val = dayRange[1] - value[1]
    // const daySub = daySubVal !== 0 ? daySubVal > 0 ? '左边拉长' : '左边缩短' : '左边不变'
    // const daySub2 = daySub2Val !== 0 ? daySub2Val > 0 ? '右边缩短' : '右边拉长' : '右边不变'
    // const steps = daySubVal / (grid / widths * 100)
    // // console.log('onChange: ',`条歩长:${grid / widths * 100}`,newLen, oldLen, value[0], value[1], days0, days1, value[0] / dateLen, value[1]/ dateLen);
    // console.log('onChange: ',    `歩长:${daySub} ${daySub2} ${steps} ${Math.round(steps)}`, daySubVal, daySub2Val, value, days0, days1, value[0] / dateLen, value[1]/ dateLen);
    // this.setState({
    //   dayRange: value,
    // })
  }
  
  onAfterChange = (value) => {
    const { dateLen, dateArr } = this.props
    const {dayRange, isDrag} = this.state
    // const {left, width} = this.props.config
    const {left, width} = this.state
    const widths = dateLen * grid
    const days0 = dayRange[0]
    const days1 = dayRange[1]
    const oldLen = dayRange[1] - dayRange[0]
    const newLen = value[1] - value[0]
    const daySubVal = dayRange[0] - value[0]
    const daySub2Val = dayRange[1] - value[1]
    const daySub = daySubVal !== 0 ? daySubVal > 0 ? '左边拉长' : '左边缩短' : '左边不变'
    const daySub2 = daySub2Val !== 0 ? daySub2Val > 0 ? '右边缩短' : '右边拉长' : '右边不变'
    const steps = daySubVal / (grid / widths * 100)
    const rightStep = daySub2Val / (grid / widths * 100)
    console.log('isNaN(daySubVal) ：', isNaN(daySubVal), rightStep)
    let changeLeft, changeWidth
    if (isDrag === 'dragLeft') {
      console.log('dragLeft ：', )
      if (daySubVal !== 0 && !isNaN(daySubVal)) {
        changeLeft = left - Math.round(steps) * grid
        changeWidth = width + Math.round(steps) * grid
        console.log('左边变化 ：', steps, '距离', left, left - Math.round(steps) * grid, '宽度',width, width + Math.round(steps) * grid, )
        this.setState({
          left: changeLeft, 
          width: changeWidth, 
          isDrag: '',
          show: true,
        })
      }
    } else {
      console.log('dragLeft右边 ：', )
      if (daySub2Val !== 0 && !isNaN(daySub2Val)) {
        changeLeft = left
        changeWidth = width - Math.round(rightStep) * grid
        console.log('右边变化 ：', rightStep, '距离', left, left - Math.round(rightStep) * grid, '宽度',width, width + Math.round(rightStep) * grid, )
        this.setState({
          width: width - Math.round(rightStep) * grid, 
          isDrag: '',
          show: true,
        })
      }
    }
    
    // if (daySubVal !== 0 && !isNaN(daySubVal)) {
    //   console.log('左边变化 ：', steps, '距离', left, left - Math.round(steps) * grid, '宽度',width, width + Math.round(steps) * grid, )
    //   this.setState({
    //     left: left - Math.round(steps) * grid, 
    //     width: width + Math.round(steps) * grid, 
    //     isDrag: '',
    //   })
    //   // if (daySubVal > 0) {
    //   //   console.log('左边拉长 ：', left - Math.round(steps) * grid)
    //   //   this.setState({
    //   //     left: left - Math.round(steps) * grid, 
    //   //     width: width + Math.round(steps) * grid, 
    //   //     isDrag: true,
    //   //   })
    //   // } else {
    //   //   console.log('左边缩短 ：', left + Math.round(steps) * grid)
    //   //   this.setState({
    //   //     left: left - Math.round(steps) * grid, 
    //   //     width: width + Math.round(steps) * grid, 
    //   //     isDrag: true,
    //   //   })
    //   // }
    // } else if (daySub2Val !== 0 && !isNaN(daySub2Val)) {
    //   console.log('右边变化 ：', steps, '距离', left, left - Math.round(steps) * grid, '宽度',width, width + Math.round(steps) * grid, )
    //   this.setState({
    //     left: left - Math.round(steps) * grid, 
    //     width: width + Math.round(steps) * grid, 
    //     isDrag: '',
    //   })
    // }
    console.log('onAfterChange: ',  changeLeft, changeWidth,  `歩长:${daySub} ${daySub2} ${steps} ${Math.round(steps)}left: ${left}width: ${width} `, daySubVal, daySub2Val, value, days0, days1, value[0] / dateLen, value[1]/ dateLen);
    this.setState({
      dayRange: value,
    })
  }
  formatter = (value, e) => {
    // console.log('formatter: ', value, e, );
    // const { dateArr } = this.props
    // const {dayRange} = this.state
    // if (dayRange.length) {
    //   const days0 = dateArr[dayRange[0]].dateOrigin 
    //   const days1 = dateArr[dayRange[1]].dateOrigin
    //   // const days0 = dateArr[value] 
    //   // console.log('formatter: ', days0, dayRange);
    //   return `起止日期：${days0} - ${days1}`
    // } else {
      return `起止日期：${1} - ${2}`
    // }
  }
  handleOk = () => {
    console.log('handleOk ：', )
  }
  close = () => {
    console.log('close ：', )
    this.setState({
      show: false,
    })
    
  }

  render() {
    const { dragging, dragAction, bounds, isDrag, show,  } = this.state;
    const { config, isAxis, isTop, dateLen } = this.props
    const { top, txt, levels, barcode, shortDay, height } = config
    // const {left, width} = config
    const {left, width} = this.state
    
    const dragHandlers = { onStart: dragAction === '' ? this.onStart : () => false, onStop: this.onStop };
    // console.log('DragableSlider 组件 this.state, this.props ：', this.state, this.props, dragAction, dragAction === '' ? false : true)
    
    const style = {
      width: width + 'px',
      background: dragging ? 'rgba(111, 255, 228, 0.8)' : '',
      // zIndex: dragging ? 10 : 1
      // position: dragAction === '' ? 'absolute' : 'fixed'
      // left: 0 + 'px',
    }
    const defaultPosition = { x: left, y: isTop ? top : 0}
    const widths = dateLen * grid
    const lefts = Math.abs(left)
    
    const title = `是否确认修改条码 ${barcode} 的生产时间`
    const content = 'content'
    
    // console.log('shortDay ：', isDrag, isDrag !== '' ? 11 : 22,shortDay, config, defaultPosition, grid / widths * 100, lefts /( widths), (lefts + width) / (widths))
    const shortBar = shortDay.length ? shortDay.map((v, i) => <div className="short" style={{left: v.shortDate}} key={i}></div>) : null
    return (
      isDrag === '' ? (
        <Draggable disabled={true} axis={isAxis} bounds={bounds} 
        defaultPosition={defaultPosition} grid={[grid, grid]} 
        onDrag={this.handleDrag.bind(this, barcode)} {...dragHandlers}>
          <div className='box' ref="dragBar" style={style}>
            <div className="dragBorder dragLeft" onMouseDown={this.dragAction.bind(this, 'dragLeft')}>
              {/* <Slider className='sliders' range 
        tipFormatter={this.formatter}
        //step={1} 
        step={grid / widths * 100}
        defaultValue={[(lefts) /( widths) * 100, (lefts + width) / (widths) * 100]}  
        onChange={this.onChange} onAfterChange={this.onAfterChange} /> */}
            </div>
            <Tooltip placement="top" title={txt}>
              <div onClick={this.barClick.bind(this)} className={`machineName ${ANIMATE.bounceIn}`}>
                {txt}
                </div>
            </Tooltip>
            <div className="work"></div>
            {shortBar}
            <div className="dragBorder dragRight" onMouseDown={this.dragAction.bind(this, 'dragRight')}></div>
            {show ? <ProductDialog handleOk={this.handleOk} width={'40%'} show={show} close={this.close} title={title}>{content}</ProductDialog> : null}
          </div>
        </Draggable>) : (<Slider className='sliders' range 
        tipFormatter={this.formatter}
        //step={1} 
        step={grid / widths * 100}
        defaultValue={[(lefts) /( widths) * 100, (lefts + width) / (widths) * 100]}  
        onChange={this.onChange} onAfterChange={this.onAfterChange} />)
    );
  }
}

export default DragableSlider;