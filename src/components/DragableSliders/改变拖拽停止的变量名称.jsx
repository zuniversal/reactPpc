import React from "react"
import ReactDOM from 'react-dom';
import PureRenderMixin from "react-addons-pure-render-mixin"

import './style.less'
import {wipe, ajax, findDOMNode} from 'util'
import {grid} from 'config'
import { ANIMATE,  } from 'constants'
import Draggable from 'react-draggable'
import { Tooltip, Slider, Popover, Button,  } from 'antd';
class DragableSlider extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      dragAction: '', 
      activeDrags: 0,
      dragDirection: false,
      bounds: "parent",
      initPosition: {
        x: 0, y: 0
      },
      // 默认是不拖动的，当拖拽开始时才是dragging
      dragging: false,
      isDrag: '',
      dayRange: [],
      changeBar: '',
      visible: true,
    }
  }
  // widthAdd = (w) => {
  //   console.log('widthAdd ：', w);
  //   this.refs.dragBar.style.width = w + grid + 'px'
  // }
  // widthDecrease = (w) => {
  //   console.log('widthDecrease ：', w);
  //   this.refs.dragBar.style.width = w !== grid ? w - grid + 'px' : '60px'
  // }

  handleDrag = (e, ui, d) => {
    this.setState({
      dragging: true,
    })
    const {dragAction, initPosition} = this.state
    const barWidth = this.refs.dragBar.style.width
    const width = Number(wipe(barWidth))
    // console.log('#bar拖动：', width, e, ui, dragAction, d.lastX , d.x, d, initPosition, this.props, );

    // if (dragAction !== '') {
    //   const isDragLeft = dragAction === 'dragLeft'
    //   if (isDragLeft) {
    //     console.log('左拖 ：', );
    //     if (d.lastX - d.x > 0) {
    //       console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
    //       this.widthAdd(width)
    //     } else {
    //       console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
    //       this.widthDecrease(width)
    //     }
    //   } else {
    //     console.log('右拖 ：', d.lastX, d.x);
    //     if (d.lastX - d.x < 0) {
    //       console.log('拉长 ：', d.lastX , d.x, width, d.deltaX);
    //       this.widthAdd(width)
    //     } else {
    //       console.log('缩短 ：', d.lastX , d.x, width, d.deltaX);
    //       this.widthDecrease(width)
    //     }
    //   }
    // }

    const { x, y } = d;
    ajax(this.setState.bind(this), {
      dragging: true,
      initPosition: { x, y }
    })
    // this.setState({
    //   // dragging: true,
    //   initPosition: { x, y }
    // })
  }

  dragAction = (v, a, d) => {
    const { x, y } = this.state.initPosition;
    const {data} = this.props
    
    this.setState({
      isDrag: v,
      changeBar: data.barcode,
    })
    this.props.changeBar(data)
    console.log('#bardragAction v ：', v, a, d, this.state, this.props);
    // console.log('v ===ght:  ：', v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: y, right: left + width, bottom: 0})
    
    // this.setState({
    //   dragAction: v,
    //   // bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : "parent",
    //   bounds: v === 'dragRight' ? {top: y, left: x, right: 0, bottom: 0} : {top: 0, left: 0, right: left + width - grid, bottom: 0},
    // })
  }
  onStart = (a, b) => {
    console.log('#bar a：', a, b, );
    this.setState({ activeDrags: ++this.state.activeDrags,  });
  }

  onStop = (onStop, e) => {
    const barWidth = this.refs.dragBar.style.width
    const {barcode} = this.props.config
    const {dragging, isDrag} = this.state
    const {data, pn_no, clickType} = this.props
    const width = Number(wipe(barWidth)) - grid
    console.log('#@@@@@@@@@@@@@@@@@@@#bar onStop：', barcode, pn_no, this.props, onStop, e, width, '是否是拖动：', dragging, isDrag, this.state);
    const {lastX, lastY} = e
    const x = lastX / grid
    const y = lastY / grid
    const w = x + width / grid
    if (clickType === 'barClick' && dragging) {
      console.log('拖动参数 ：', {x, y, w, barcode, data, pn_no})
      ajax(this.props.stopDrag, {x, y, w, barcode, data, pn_no, dragAction: 'dragging'})
      this.props.changeBar(data)
      // this.props.stopDrag(x, y, w, barcode)
    }
    this.setState({ 
      activeDrags: --this.state.activeDrags, 
      dragAction: '', 
      dragging: false,
      bounds: "parent",
    });
  }
  componentWillMount() {
    const { left, width } = this.props.config
    this.setState({
      left, width
    })
    // console.log('componentDidMount ：', )
  }

  barClick = (e) => {
    console.log('barClick22：', e, this.props, this.state)
    const {pn_no, clickType} = this.props
    const {barcode, txt} = this.props.config
    if (clickType === 'barClick') {
      const {work_no, } = this.props.data
      this.props.showBarcodeDialog(barcode, pn_no, work_no)
    } else {
      this.props.getPnDetailList(txt)
    }
  }

  onChange = (value) => {
    // const { dateLen, dateArr } = this.props
    // const {dayRange} = this.state
    // const widths = dateLen * grid
    // // const days0 = dateArr[value[0]] 
    // // const days1 = dateArr[value[1]]
    // const days0 = dayRange[0]
    // const days1 = dayRange[1]
    // const oldLen = dayRange[1] - dayRange[0]
    // const newLen = value[1] - value[0]
    // const daySubVal = dayRange[0] - value[0]
    // const daySub2Val = dayRange[1] - value[1]
    // const daySub = daySubVal !== 0 ? daySubVal > 0 ? '左边拉长' : '左边缩短' : '左边不变'
    // const daySub2 = daySub2Val !== 0 ? daySub2Val > 0 ? '右边缩短' : '右边拉长' : '右边不变'
    // const steps = daySubVal / (grid / widths * 100)
    // // console.log('onChange: ',`条歩长:${grid / widths * 100}`,newLen, oldLen, value[0], value[1], days0, days1, value[0] / dateLen, value[1]/ dateLen);
    // console.log('onChange: ',    `歩长:${daySub} ${daySub2} ${steps} ${Math.round(steps)}`, daySubVal, daySub2Val, value, days0, days1, value[0] / dateLen, value[1]/ dateLen);
    // this.setState({
    //   dayRange: value,
    // })
  }
  
  onAfterChange = (value) => {
    const {barcode} = this.props.config
    const { data, pn_no, dateLen, dateArr } = this.props
    const {dayRange, isDrag} = this.state
    // const {left, width} = this.props.config
    const {left, width} = this.state
    const widths = dateLen * grid
    const daySubVal = dayRange[0] - value[0]
    const daySub2Val = dayRange[1] - value[1]
    const daySub = daySubVal !== 0 ? daySubVal > 0 ? '左边拉长' : '左边缩短' : '左边不变'
    const daySub2 = daySub2Val !== 0 ? daySub2Val > 0 ? '右边缩短' : '右边拉长' : '右边不变'
    const steps = daySubVal / (grid / widths * 100)
    const rightStep = daySub2Val / (grid / widths * 100)
    console.log('isNaN(daySubVal) ：', daySubVal, isNaN(daySubVal), rightStep, this.state,  this.props)
    let changeLeft, changeWidth
    if (isDrag === 'dragLeft') {
      console.log('dragLeft ：', )
      if (daySubVal !== 0 && !isNaN(daySubVal)) {
        changeLeft = left - Math.round(steps) * grid
        changeWidth = width + Math.round(steps) * grid
        console.log('左边变化 ：', steps, '距离', left, left - Math.round(steps) * grid, '宽度',width, width + Math.round(steps) * grid, )
        this.setState({
          left: changeLeft, 
          width: changeWidth, 
          // isDrag: '',
        })
        const params = {x: changeLeft / grid, y: 0, w: (changeLeft + changeWidth - grid) / grid, barcode, data, pn_no, dragAction: isDrag}
        this.props.stopDrag(params)
      }
    } else if (isDrag === 'dragRight') {
      console.log('dragLeft右边 ：', )
      if (daySub2Val !== 0 && !isNaN(daySub2Val)) {
        changeLeft = left
        changeWidth = width - Math.round(rightStep) * grid
        console.log('右边变化 ：', rightStep, '距离', left, left - Math.round(rightStep) * grid, '宽度',width, width + Math.round(rightStep) * grid, )
        this.setState({
          width: width - Math.round(rightStep) * grid, 
          // isDrag: '',
        })
        const params = {x: changeLeft / grid, y: 0, w: (changeLeft + changeWidth - grid) / grid, barcode, data, pn_no, dragAction: isDrag}
        this.props.stopDrag(params)
      }
    }
    console.log('onAfterChange: ',  changeLeft / grid, changeWidth / grid,  `歩长:${daySub} ${daySub2} ${steps} ${Math.round(steps)}left: ${left}width: ${width} `, daySubVal, daySub2Val, value, value[0] / dateLen, value[1]/ dateLen);
    this.setState({
      dayRange: value,
    })
  }
  formatter = (value, e) => {
    // console.log('formatter: ', value, e, this.state);
    const { dateArr, s_date, e_date } = this.props
    // const {dayRange} = this.state
    // if (dayRange.length) {
    //   const days0 = dateArr[dayRange[0]].dateOrigin 
    //   const days1 = dateArr[dayRange[1]].dateOrigin
    //   // const days0 = dateArr[value] 
    //   // console.log('formatter: ', days0, dayRange);
    //   return `起止日期：${days0} - ${days1}`
    // } else {
      return (<div className="formatWrapper">
        <div className="formatCon">原起止日期：{s_date} 到 {e_date}</div>
        {/* <div className="formatCon">现起止日期：{s_date} 到 {e_date}</div> */}
      </div>)
    // }
  }

  componentWillReceiveProps(nextProps) {
    // console.log('发生变化的bar dragging componentWillReceiveProps组件获取变化 ：', nextProps, this.props,  this.props.config.barcode)
    if (nextProps.changeBarData.barcode === this.props.config.barcode) {
      const {isDragBack, showEdit } = nextProps
      const {left, width,  } = nextProps.changeBarData
      const {isDrag, changeBar, } = this.state
      console.log('是同一条bar  222发生变化的bar dragging componentWillReceiveProps组件获取变化 ：', this.refs.draggableRefs, changeBar, nextProps, this.props, this.state )
      if (isDragBack && isDrag === '') {
        console.log('拖拽回去 left, width ：', left, width)
        // findDOMNode(ReactDOM, this.refs.dragBar).style.transform = `translate(${left}px, 0px)`
        this.setState({
          left, 
          width,
          changeBar: '',
          isDrag: '',
          dayRange: [],
          initPosition: { x: left, y: 0 },
        })
        console.log('this.refs.draggableRefs ：', this.refs.draggableRefs)
        
        this.refs.draggableRefs.state.x = left
      }
      if (showEdit) {
        this.setState({
          changeBar: '',
          isDrag: '',
          dayRange: [],
        })
      }
    } else {
      // console.log('不是同一条bar ：', )
      this.setState({
        isDrag: '',
      })
    }
  }
  

  render() {
    const { dragging, dragAction, bounds, isDrag,  } = this.state;
    const { config, isAxis, isTop, dateLen } = this.props
    const { top, txt, levels, barcode, shortDay, } = config
    // const {left, width} = config
    const {left, width} = this.state
    const dragHandlers = { onStart: dragAction === '' ? this.onStart : () => false, onStop: this.onStop };
    // console.log('DragableSlider 组件 this.state, this.props ：', this.state, this.props, dragAction, dragAction === '' ? false : true)
    
    const style = {
      width: width + 'px',
      background: dragging ? 'rgba(111, 255, 228, 0.8)' : '',
      // zIndex: dragging ? 10 : 1
      // position: dragAction === '' ? 'absolute' : 'fixed'
      // left: 0 + 'px',
    }
    const defaultPosition = { x: left, y: isTop ? top : 0}
    const widths = dateLen * grid
    const lefts = Math.abs(left)

    if (left > 0 && barcode === '100000884') {
      console.log('当前组件bar ：', this.state)
    }
    // console.log('shortDay ：', shortDay)
    
    const content = (
      <div>
        <p>Content</p>
        <p>Content</p>
      </div>
    );

    const shortBar = shortDay.length && shortDay != undefined ? shortDay.map((v, i) => <div className="short" style={{left: v.shortDate}} key={i}></div>) : null
    return (
      isDrag === '' ? (
        <Draggable axis={isAxis} bounds={bounds} ref="draggableRefs"
        defaultPosition={defaultPosition} grid={[grid, grid]} 
        onDrag={this.handleDrag.bind(this, barcode)} {...dragHandlers}>
          <div className='box' ref="dragBar" style={style}>
            <Tooltip placement="top" title={'点击开始拖拽改变生产日期'}>
              <div className="dragBorder dragLeft" onMouseDown={this.dragAction.bind(this, 'dragLeft')}>
                {/* <Slider className='sliders' range 
          tipFormatter={this.formatter}
          //step={1} 
          step={grid / widths * 100}
          defaultValue={[(lefts) /( widths) * 100, (lefts + width) / (widths) * 100]}  
          onChange={this.onChange} onAfterChange={this.onAfterChange} /> */}
              </div>
            </Tooltip>
            {/* <Tooltip placement="top" title={txt}> */}
              <div className={`machineName ${ANIMATE.bounceIn}`}>
                <Popover content={<span className="barPopover">
                  {txt}
                  <Button type="primary" onClick={this.barClick.bind(this)} className='detailbtn'  size='small'>
                    详情
                  </Button>
                  </span>}>
                  <span onClick={this.barClick.bind(this)}>{txt}</span>
                </Popover>
              </div>
            {/* </Tooltip> */}
            <div className="work"></div>
            {shortBar}
            <div className="dragBorder dragRight" 
            onMouseDown={this.dragAction.bind(this, 'dragRight')}
            ></div>
          </div>
        </Draggable>) : (<Slider className='sliders' range 
        tipFormatter={this.formatter}
        //step={1} 
        step={grid / widths * 100} 
        defaultValue={[(lefts) /( widths) * 100, (lefts + width) / (widths) * 100]}  
        onChange={this.onChange} onAfterChange={this.onAfterChange} />)
    );
  }
}

export default DragableSlider;