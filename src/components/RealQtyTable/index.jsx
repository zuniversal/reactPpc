import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {ANIMATE, } from 'constants'
import { Row, Col, Icon } from 'antd'

class RealQtysTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    this.state = {
    }
  }
  render() {
    // console.log('RealQtysTable 组件this.state, this.props ：', this.state, this.props)
    const {data, } = this.props 
    return (
      <Row className={'realQtyTable'}>
        {
          data.map((v, i) => (<Col className={`${ANIMATE.bounceIn}`} key={i} span={2}>
            <div className={`th td `}>{v.sf_date}</div>
            <div className={`td ${i === data.length - 1 ? 'total' : ''}`}>{v.real_qty}</div>
          </Col>))
        }
    </Row>
    )
  }
}

export default RealQtysTable