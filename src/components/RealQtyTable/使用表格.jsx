import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import Inputs from '@@@@/Inputs'
import SecureHoc from '@@@@/SecureHoc'
import {showTotal, dateForm, ajax, infoFilter, } from 'util'
import { ANIMATE, SIZE10, } from 'constants'
import { DATE_FORMAT, DATE_FORMAT_BAR, poTableConfig, } from 'config'
import Count from '@@@@/Count'
import { Table, Tag, Button, Icon, DatePicker, Select,  } from 'antd';
import moment from 'moment';

class RealQtyTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE10
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      loading: false, 
      selectedRowKeys: [],
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    console.log('handleTableChange ：', pagination, filters, sorter)
    // const { pageSize, current } = pagination
    // this.props.getPn(pageSize, current)
  }
  renderColumns = (v, o, i, key) => {
    // console.log('v, o, i, key ：', v, o, i, key, key.indexOf('date'))
    return o.editAble ? <Inputs noRule={true} className='t-c' data={v} keys={key} val={o} i={i} size={'default'} inputing={this.props.tableInput}></Inputs> 
    : key.indexOf('date') > 0 ? moment(v).format(DATE_FORMAT_BAR) : <span>{v}</span>  
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    // console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data)
    this.setState({
      loading,
    });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('Table props 属性的改变', nextProps)
    const { loading, pagination } = nextProps
    this.setState({
      loading,
      pagination,
    });
  }
  render() {
    const { pagination, loading, selectedRowKeys } = this.state
    const {data, auth, } = this.props
    console.log('PoArrangeTable 组件 this.state, this.props ：', data, this.props,  )
    const rowKey = 'real_qty'
    // const columns = data.map(item => {
    //   return {
    //     title: item.sf_date, dataIndex: 'sf_date', 
    //     render: (v, o, i) => <Tag key={item.sf_date} color={'red'}>{o.real_qty}</Tag>
    //   }
    // })
    
    const columns = [
      {
        title: '日期', dataIndex: 'sf_date',
        render: (v, o, i) => <Tag color={'red'}>{v}</Tag>
      },
      {
        title: '數量', dataIndex: 'real_qty',
        render: (v, o, i) => <Count end={v} duration={2}/>
      },
    ]
    
    // const columns = [
    //   {
    //     title: '批號', dataIndex: 'pn_no',
    //     render: (v, o, i) => <Tag color={'red'}>{v}</Tag>
    //   },
    //   {
    //     title: '數量', dataIndex: 'qty',
    //     render: (t, v, i) => this.renderColumns(t, v, i, 'type_id',)
    //   },
    // ]

    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={pagination}
        rowKey={record => record[rowKey]}
        rowClassName={(record, i) => ANIMATE.bounceIn}

        // onChange={this.handleTableChange}
        // className={`poArrangeTable ${auth ? 'minTd' : ''}`}
        className={` `}
      />
    );
  }
}

export default RealQtyTable