import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { SELECT_TXT,  } from 'constants'
import { Select, Form, } from 'antd';
const Option = Select.Option;

class SelectForm extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  static defaultProps = {
    ph: '關鍵詞',
    data: [],
    dv: null,
    option: {
      key: '',
      txt: '',
    },
  }
  onChange = (v) => this.props.onChange(v)
  filterOption = (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
  render() {
    // console.log('SelectForm 组件this.state, this.props ：', this.state, this.props);
    const {data, option, ph, dv, className, } = this.props
    return (
      <Select 
        showSearch
        allowClear={true}
        placeholder={SELECT_TXT + ph}
        optionFilterProp="children"
        defaultValue={dv} 
        value={dv} 
        onChange={this.onChange} 
        filterOption={this.filterOption}
        // className="selectForm "
        className={`m-r5 ${className}`}
      >
        {data.map((v, i) => <Option value={v[option.key].toString()} key={i}>{v[option.txt]}</Option>)}
      </Select>
    );
  }
}
const WrappedDemo = Form.create()(SelectForm);

export default WrappedDemo;