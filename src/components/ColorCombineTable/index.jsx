import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {showTotal, dateForm, ajax, infoFilter, } from 'util'
import { ANIMATE, SIZE, } from 'constants'
import { DATE_FORMAT, poInfoConfig, } from 'config'
import Inputs from '@@@@/Inputs'
import InputsNumber from '@@@@/InputsNumber'
import { Table, Tag, Button, Icon, DatePicker, Select,  } from 'antd';
import moment from 'moment';

class ColorCombineTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      loading: false, 
      selectedRowKeys: [],
    };
  }
  deleteData = (v, o, i) => {
    console.log('deleteData v, o, i：', v, o, i, this.props);
    this.props.deleteCTData(o, i)
  }
  onSelectChange = (selectedRowKeys) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    // console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data)
    this.setState({
      loading,
    });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('Table props 属性的改变', nextProps)
    const { loading, pagination } = nextProps
    this.setState({
      loading,
      pagination,
    });
  }
  render() {
    const { pagination, loading, selectedRowKeys } = this.state
    const {data,  } = this.props
    console.log('ColorCombineTable 组件 this.state, this.props ：', data, this.props,  )
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      onSelection: this.onSelection,
    };
    const rowKey = 'compose'
    const columns = [
      {
        title: '顔色', dataIndex: 'combo_id',
        render: (v, o, i) => <Tag color={'#f50'}>{v}</Tag>
      },
      {
        title: '尺碼', dataIndex: 'size_no',
        render: (v, o, i) => <Tag color={'#108ee9'}>{v}</Tag>
      },
      {
        title: '數量', dataIndex: 'qty',
        className: 'inputMaxWidth',
        // render: (v, o, i) => o.status === 'O' && !editAble ? <span>{v}</span> : <Inputs keys={'compose'} noRule={true} className='t-c' data={v} val={o} size={'default'} inputing={this.props.inputing}></Inputs> 
        render: (v, o, i) => <Inputs keys={'compose'} noRule={true} className='t-c' data={v} val={o} size={'default'} inputing={this.props.inputing}></Inputs> 
      },
      {
        title: '狀態', dataIndex: 'status',
        render: (v, o, i) => <Tag color={infoFilter(poInfoConfig, v, 'color')}>{v}</Tag>
      },
      {
        title: '操作列', dataIndex: 'editable',
        className: 'minWidth',
        render: (v, o, i) => {
          return (<div className="btnWrapper">
          <Button onClick={() => this.deleteData(v, o, i)} shape="circle" type="primary" icon='delete' className="warn"></Button>
        </div>)
        }
      },
    ]
    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={false}
        rowKey={record => record[rowKey]}
        rowClassName={(record, i) => ANIMATE.bounceIn}

        footer={() => <div>
          <Button onClick={() => this.props.newData(rowKey)} type="primary" icon="check" className="succ">保存</Button>
        </div>}
        className='colorTable tableFooterBtn noMgTag' 
      />
    );
  }
}

export default ColorCombineTable