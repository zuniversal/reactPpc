import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { ANIMATE, SIZE10, } from 'constants'
import {showTotal, } from 'util'
import { purposeColor, typeColor, expenseFilters, purposeFilters, unitArr, standardFilterTxt, tagColorArr,  } from 'config'
import Inputs from '@@@@/Inputs'
import SecureHoc from '@@@@/SecureHoc'
import { Table, Tag, Tooltip, Button, Icon, Select } from 'antd';
const Option = Select.Option;

class Tables extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    // const pagination = {}
    // const {total} = this.props
    // pagination.pageSize = SIZE10
    // pagination.total = total
    // pagination.showSizeChanger = true
    // pagination.showTotal = showTotal
    this.state = {
      data: [],
      // pagination,
      loading: false,
      deleteItem: -1,
      len: 0,
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    console.log('handleTableChange ：', pagination, filters, sorter)
    // const { path, option, level } = this.props
    // console.log('pagination, filters, sorter：', this.props, path, option, pagination, filters, sorter);
    // const { pageSize, current } = pagination
  }
  onRowClick = (record, index, event) => {
    event.stopPropagation()
    event.preventDefault()
  }
  selectChange = (o, k, v) => {
    console.log(`selectChange`, o, k, v);
    this.props.editData({...o, v, keys: k})
  }
  saveData = (t, v, i) => {
    console.log('saveData t, v, i：', t, v, i, this.props);
    this.props.showDialog()
  }
  addData = (t, v, i) => {
    console.log('addData t, v, i：', t, v, i, this.props);
    this.props.addDataAction(t, v, i)
  }
  editDataAction = (t, v, i) => {
    console.log('editDataAction t, v, i：', t, v, i, this.props);
    this.props.editDataAction(v)
  }
  deleteData = (t, v, i) => {
    // event.stopPropagation()
    // event.preventDefault()
    console.log('deleteData t, v, i：', t, v, i, this.props);
    this.setState({ deleteItem: i })
    this.props.deleteData(v)
  }
  renderColumns = (t, v, i, key) => {
    const {procedureData, work_no, isDisable, } = this.props
    // console.log('￥￥￥￥￥￥￥￥renderColumns ：', t, v, i, key, )
    if ((v.editable || v.newing) && key !== 'work_name' && key !== 'type_id') {
      // if (key === 'work_name') {
      //   return <Select defaultValue={t} onChange={this.selectChange.bind(this, v, 'work_no')}>
      //         {procedureData.map((v, i) => <Option value={v.work_no.toString()} key={i}>{v.work_name}</Option>)}
      //       </Select>
      // } else 
      if (key === 'unit') {
        console.log('unitArr[work_no].unit 选择器：', unitArr[work_no], unitArr, work_no);
        return <Select defaultValue={standardFilterTxt[t.trim()]} onChange={this.selectChange.bind(this, v, key)}>
              {unitArr[work_no].map((v, i) => <Option value={v.toString()} key={i}>{standardFilterTxt[v]}</Option>)}
            </Select>
      } else {
        // return <Inputs isDisable={isDisable && !v.newing && key === 'qty_fr'} noRule={true} className='txtCenter' data={t} keys={key} val={v} i={i} size={'default'} inputing={this.props.editData}></Inputs>
        return <Inputs isDisable={i === 0 && key === 'qty_fr'} noRule={true} className='txtCenter' data={t} keys={key} val={v} i={i} size={'default'} inputing={this.props.editData}></Inputs>
      }
    } else {
      if (key === 'unit') {
        // console.log('unitArr[work_no].unit 文本 ：', standardFilterTxt[t],  t, v, i, key);
        return <span>{t != undefined ? standardFilterTxt[t.trim()] : t}</span>
      } else {
        return <span>{t}</span>
      }
      // return <span>{t}</span>
    }
  }
  componentDidMount() {
    // const {pagination, } = this.state
    const { loading, data,  } = this.props
    // pagination.pageSize = data.length
    this.setState({
      loading,
      data,
      // pagination,
    });
  }
  componentWillReceiveProps(nextProps) {
    console.log('Table props 属性的改变', nextProps)
    const { data,  } = nextProps
    const {len, } = this.state
    if (len !== data.length) {
      this.setState({
        deleteItem: -1,
      })
    }
    this.setState({
      len: data.length,
      data,
      // pagination,
    });
  }
  render() {
    const { data, loading,  } = this.state
    const {canDelete, typeFilters, unitFilters, auth, } = this.props
    console.log('StanderdDayTable 组件this.state, this.props ：', this.state, this.props);
    const rowKey = 'seq_no'
    const columns = [
      {
        title: '序号', dataIndex: 'seq_no',
        render: (t, v, i) => <div>{t}</div>
      },
      {
        title: '難易度', dataIndex: 'type_id',
        filterMultiple: false,
        filters: typeFilters,
        onFilter: (value, record) => {
          console.log('record.address.indexOf(value) === 0 ：', record, value, );
          return record.type_id === value
        },
        render: (t, v, i) => this.renderColumns(t, v, i, 'type_id',)
      },
      // {
      //   title: '工序編碼', dataIndex: 'work_no',
      //   render: (t, v, i) => v.editable ? <Inputs className='txtCenter' data={t} keys='work_no' val={v} i={i} inputing={this.props.inputing}></Inputs> : <span>{t}</span>
      // },
      {
        title: '工序名', dataIndex: 'work_name',
        // render: (t, v, i) => this.renderColumns(t, v, i, 'work_name',)
        render: (t, v, i) => v.work_no !== 0 ? <Tag className={`${ANIMATE.bounceIn}`} color={tagColorArr.find(item => item.work_no == v.work_no).color} >{t}</Tag> : null 
      },
      {
        title: '最小數量', dataIndex: 'qty_fr',
        sorter: (a, b) => a.qty_fr - b.qty_fr,
        render: (t, v, i) => this.renderColumns(t, v, i, 'qty_fr',)
      },
      {
        title: '最大數量', dataIndex: 'qty_to',
        sorter: (a, b) => a.qty_to - b.qty_to,
        render: (t, v, i) => this.renderColumns(t, v, i, 'qty_to',)
      },
      {
        title: '數量單位', dataIndex: 'unit',
        className: 'unitWidth',
        filterMultiple: false,
        filters: unitFilters,
        onFilter: (value, record) => {
          console.log('record.address.indexOf(value) === 0 ：', record, value, );
          return record.unit.trim() === value
        },
        render: (t, v, i) => this.renderColumns(t, v, i, 'unit',)
      },
      {
        title: '標准天數', dataIndex: 'work_days',
        sorter: (a, b) => a.work_days - b.work_days,
        render: (t, v, i) => this.renderColumns(t, v, i, 'work_days',)
      },
    ]
    const actionColumn = {
      title: '編輯', dataIndex: 'editable',
      className: 'standerdEditCol', 
      render: (t, v, i) => {
        // console.log('t, v, i ：', t, v, i);
        return (<div className="btnWrapper">
        { i !== 0 ? <Button onClick={() => v.newing ? this.saveData(t, v, i) : this.addData(t, v, i)} shape="circle" type="primary" icon={`${v.newing ? 'check' : 'plus'}`} className={`m-r10 ${v.newing ? 'succ' : 'warn'}`}></Button> : null}
        <Button onClick={() => v.editable ? this.saveData(t, v, i) : this.editDataAction(t, v, i)} shape="circle" type="primary" icon={`${v.editable ? 'check' : 'edit'}`} className={`m-r10 ${v.editable ? 'succ' : ''}`}></Button>
        {/* <Button onClick={() => this.saveData(t, v, i)} shape="circle" type="primary" icon={'check'} className={`m-r10 succ`}></Button> 
        <Button onClick={() => this.editDataAction(t, v, i)} shape="circle" type="primary" icon={'edit'} className={`m-r10`}></Button>    */}
        {
          i === data.length - 1 ? <Button onClick={() => this.deleteData(t, v, i)} shape="circle" type="primary" icon='delete' className="salmon"></Button> : null
        }
        { v.newing ? <Button onClick={this.props.cancelData} shape="circle" type="primary" icon={'delete'} className={`m-r10 warn`}></Button> : null}
      </div>)
      }
    }
    auth && columns.push(actionColumn)


    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={false}
        onRowClick={this.onRowClick.bind(this, )}
        onChange={this.handleTableChange.bind(this, )}
        rowKey={record => record[rowKey]}

        rowClassName={(record, i) => i === this.state.deleteItem && canDelete ? ANIMATE.slideOutLeft : ANIMATE.bounceIn}
        className={`standerdDayTable ${auth ? 'minTd' : ''}`}
      />
    );
  }
}

export default SecureHoc(Tables)