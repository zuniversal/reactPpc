import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {showTotal, dateForm, ajax} from 'util'
import { ANIMATE, SIZE, } from 'constants'
import { DATE_FORMAT, } from 'config'
import Inputs from '@@@@/Inputs'
import InputsNumber from '@@@@/InputsNumber'
import { Table, Tag, Button, Icon, DatePicker, Select,  } from 'antd';
import moment from 'moment';

class FactoryTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      loading: false, 
      selectedRowKeys: [],
    };
  }
  onSelectChange = (selectedRowKeys) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys, this.state);
    this.setState({ selectedRowKeys });
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    const selectItem = data.filter(v => v.work_status === 'T').map(v => v.work_no)
    console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data, selectItem)
    this.setState({
      loading,
      selectedRowKeys: [...selectItem],
    });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('Table props 属性的改变', nextProps)
    const { loading, pagination } = nextProps
    this.setState({
      loading,
      pagination,
    });
  }
  render() {
    const { pagination, loading, selectedRowKeys } = this.state
    const {data, } = this.props
    const rowKey = 'work_no'
    console.log('FactoryTable 组件 this.state, this.props ：', data, this.props,  )
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      onSelection: this.onSelection,
    };
    const columns = [
      {
        title: '工序名', dataIndex: 'work_name',
        render: (v, o, i) => <Tag color={'#f50'}>{v}</Tag>
      },
      {
        title: '日産數量', dataIndex: 'day_qty',
        className: 'inputMaxWidth',
        render: (v, o, i) => {
          // console.log('v, o, i ：', v, o, i, selectedRowKeys, selectedRowKeys.every(v => v !== o.work_no))
          return <Inputs className='t-c' isDisable={selectedRowKeys.every(v => v !== o.work_no)} data={v} keys='work_no' val={o} i={i} inputing={this.props.inputing}></Inputs> 
        }
      },
    ]
    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={false}
        rowKey={record => record[rowKey]}
        rowClassName={(record, i) => ANIMATE.bounceIn}

        rowSelection={rowSelection}
        className='factoryTable ' 
      />
    );
  }
}

export default FactoryTable