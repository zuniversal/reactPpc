import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"

import './style.less'

import Draggable from 'react-draggable'

class Dragable extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      activeDrags: 0,
      deltaPosition: {
        x: 0, y: 0
      },
      controlledPosition: {
        x: -400, y: 200
      }
    }
  }
  // onStart = () => {
  //   this.setState({
  //     activeDrags: this.state.activeDrags++
  //   })
  // }
  // onStop = () => {
  //   this.setState({
  //     activeDrags: this.state.activeDrags--
  //   })
  // }
  // handleDrag = () => {
  //   const {x, y} = this.state.activeDrags
  // }

  // render() {
  //   return (
  //     <div>Draggable
  //       {/* <Draggable></Draggable> */}
  //     </div>
  //   );
  // }



  handleDrag = (e, ui) => {
    console.log('ui：', ui, this.state);
    const {x, y} = this.state.deltaPosition;
    this.setState({
      deltaPosition: {
        x: x + ui.deltaX,
        y: y + ui.deltaY,
      }
    })
  }

  onStart = (a) => {
    // console.log('a：', this.refs.a.style);
    this.setState({activeDrags: ++this.state.activeDrags});
  }

  onStop = (onStop) => {
    console.log('onStop：', onStop);
    this.setState({activeDrags: --this.state.activeDrags});
  }

  // For controlled component
  adjustXPos = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const {x, y} = this.state.controlledPosition;
    this.setState({controlledPosition: {x: x - 10, y}});
  }
  drag = () => {
    console.log('drag ：', );
    this.setState({
      action: 'drag'
    })
  }

  adjustYPos = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const {controlledPosition} = this.state;
    const {x, y} = controlledPosition;
    this.setState({controlledPosition: {x, y: y - 10}});
  }

  onControlledDrag = (e, position) => {
    console.log('e, position：', e, position);
    const {x, y} = position;
    this.setState({controlledPosition: {x, y}});
  }

  onControlledDragStop = (e, position) => {
    this.onControlledDrag(e, position);
    this.onStop();
  }

  render() {
    const dragHandlers = {onStart: this.onStart, onStop: this.onStop};
    const {deltaPosition, controlledPosition} = this.state;
    const handle = 'strong'
    return (
      <div>
        <p>Active DragHandlers: {this.state.activeDrags}</p>
        <p>
          <a href="https://github.com/mzabriskie/react-draggable/blob/master/example/index.html">Demo Source</a>
        </p>
        <Draggable {...dragHandlers}>
          <div className="box">I can be dragged anywhere</div>
        </Draggable>
        <Draggable axis="x" {...dragHandlers}>
          <div className="box cursor-x">I can only be dragged horizonally (x axis)</div>
        </Draggable>
        <Draggable axis="y" {...dragHandlers}>
          <div className="box cursor-y">I can only be dragged vertically (y axis)</div>
        </Draggable>
        <Draggable onStart={() => false}>
          <div className="box">I don't want to be dragged</div>
        </Draggable>
        <Draggable axis="x" onDrag={this.handleDrag} {...dragHandlers}>
          <div className="box">
            <div>I track my deltas</div>
            <div>x: {deltaPosition.x.toFixed(0)}, y: {deltaPosition.y.toFixed(0)}</div>
          </div>
        </Draggable>
        <Draggable handle={handle} onDrag={this.handleDrag} onStart={() => false} {...dragHandlers}>
          <div className="box no-cursor">
            <strong className="cursor" onDrag={this.handleDrag} onClick={this.drag}><div>Drag here</div></strong>
            <div>You must click my handle to drag me</div>
            <strong className="cursor"><div>Drag here</div></strong>
            <p className="cursor">You</p>
          </div>
        </Draggable>
        <Draggable bounds={{top: -100, left: -100, right: 100, bottom: 100}} {...dragHandlers}>
          <div className="box"> bounds=1</div>
        </Draggable>
        {/* <Draggable cancel="strong" {...dragHandlers}>
          <div className="box">
            <strong className="no-cursor">Can't drag here</strong>
            <div>Dragging here works</div>
          </div>
        </Draggable> */}
        {/* <Draggable grid={[25, 25]} {...dragHandlers}>
          <div className="box">I snap to a 25 x 25 grid</div>
        </Draggable>
        <Draggable grid={[40, 40]} onDrag={this.handleDrag} {...dragHandlers}>
          <div className="box">
            <div>I snap to a 50 x 50 grid</div>
            <div>x: {deltaPosition.x.toFixed(0)}, y: {deltaPosition.y.toFixed(0)}</div>
          </div>
        </Draggable>
        <Draggable bounds={{top: -100, left: -100, right: 100, bottom: 100}} {...dragHandlers}>
          <div className="box">I can only be moved 100px in any direction.</div>
        </Draggable>
        <Draggable bounds="body" {...dragHandlers}>
          <div className="box">
            I can only be moved within the confines of the body element.
          </div>
        </Draggable>
        <Draggable {...dragHandlers}>
          <div className="box" style={{position: 'absolute', bottom: '100px', right: '100px'}}>
            I already have an absolute position.
          </div>
        </Draggable>
        <Draggable defaultPosition={{x: 25, y: 25}} {...dragHandlers}>
          <div className="box">
            {"I have a default position of {x: 25, y: 25}, so I'm slightly offset."}
          </div>
        </Draggable>
        <Draggable position={controlledPosition} {...dragHandlers} onDrag={this.onControlledDrag}>
          <div className="box">
            My position can be changed programmatically. <br />
            I have a drag handler to sync state.
            <p>
              <a href="#" onClick={this.adjustXPos}>Adjust x ({controlledPosition.x})</a>
            </p>
            <p>
              <a href="#" onClick={this.adjustYPos}>Adjust y ({controlledPosition.y})</a>
            </p>
          </div>
        </Draggable>
        <Draggable position={controlledPosition} {...dragHandlers} onStop={this.onControlledDragStop}>
          <div className="box">
            My position can be changed programmatically. <br />
            I have a dragStop handler to sync state.
            <p>
              <a href="#" onClick={this.adjustXPos}>Adjust x ({controlledPosition.x})</a>
            </p>
            <p>
              <a href="#" onClick={this.adjustYPos}>Adjust y ({controlledPosition.y})</a>
            </p>
          </div>
        </Draggable> */}

      </div>
    );
  }



}

export default Dragable;