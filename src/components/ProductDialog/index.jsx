import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { Table, Icon, notification, Modal, Button, Tag  } from 'antd';

class ProductDialog extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    };
  }
  handleOk = (e) => {
    // console.log(e);
    this.props.handleOk()
  }
  handleCancel = (e) => {
    // console.log(e);
    this.props.close()
  }
  render() {
    const { show, children, title, width, className, noJustify, isHideOk, okTxt, extra, cancelTxt, maskClosable,   } = this.props
    // console.log('show ：', show); width="70%"
    // console.log('ProductDialog 组件this.state, this.props ：', this.state, this.props, this);
    console.log('okTxt != undefined ? okT：', okTxt, okTxt != undefined ? okTxt : '确认');
    return (
      <div>
        <Modal className={`${className} ${noJustify ? '' : 'textJustify'}`}
          title={title}
          width={width != undefined ? width : '60%'}
          visible={show}
          onOk={this.handleOk}
          onCancel={this.handleCancel} 
          maskClosable={maskClosable}
          footer={[
            <Button key="cancel" onClick={this.handleCancel} icon="meh-o">{cancelTxt != undefined ? cancelTxt : '取消'}</Button>,
            extra != undefined ? extra : null,
            !isHideOk ? <Button key="sure" onClick={this.handleOk} type="primary" icon="smile-o">{okTxt != undefined ? okTxt : '確認'}</Button> : null,
          ]}
        >
          {children}
        </Modal>
      </div>
    );
  }
}

export default ProductDialog;