import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import Draggable from 'react-draggable'

class DragToggle extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      activeDrags: 0,
      deltaPosition: {
        x: 0, y: 0
      },
      controlledPosition: {
        x: 130, y: 0
      },
      which: ''
    }
  }

  handleDrag = (e, ui, d) => {
    const { controlledPosition, which } = this.state
    console.log('handleDrag', e, ui, d );
    controlledPosition[e] = d[e]
    this.setState({ controlledPosition: { ...controlledPosition } });
  }

  onStart = (a) => {
    console.log('onStart', a, );
    this.setState({ activeDrags: ++this.state.activeDrags, which: a });
  }

  onStop = (onStop, d) => {
    const { controlledPosition, which } = this.state
    console.log('onStop：', onStop, d, this.state, which, controlledPosition, controlledPosition[onStop], d[onStop]);
    controlledPosition[onStop] = d[onStop]
    this.setState({ controlledPosition: { ...controlledPosition } });
  }

  // For controlled component
  adjustXPos = (e) => {
    e.preventDefault();
    e.stopPropagation();
    console.log('adjustXPos', e, );
    const { x, y } = this.state.controlledPosition;
    this.setState({ controlledPosition: { x: x - 10, y } });
  }

  adjustYPos = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const { controlledPosition } = this.state;
    const { x, y } = controlledPosition;
    console.log('adjustYPos', e, );
    this.setState({ controlledPosition: { x, y: y - 10 } });
  }

  onControlledDrag = (e, position) => {
    const { x, y } = position;
    this.setState({ controlledPosition: { x, y } });
  }

  render() {
    const dragHandlers = { onStart: this.onStart, onStop: this.onStop };
    const { children } = this.props
    const { deltaPosition, controlledPosition } = this.state;
    // bounds={{ top: 0, left: 130, right: 200, bottom: 0 }}
    //  bounds={{ top: 75, left: 0, right: 0, bottom: 200 }}
    return (
      <div className="dragToggle">
      {/* <Draggable bounds="parent"onStart={() => false}>
        <div className="boxs">header</div>
      </Draggable>
        <Draggable bounds="parent" axis="x" onDrag={this.handleDrag.bind(this, 'x')} onStart={this.onStart.bind(this, 'x')} onStop={this.onStop.bind(this, 'x')} position={controlledPosition}>
          <div className="boxs headerBars">
            axisx
            <p>
              <a href="#" onClick={this.adjustXPos}>Adjust x ({controlledPosition.x})</a>
            </p>
            <p>
              <a href="#" onClick={this.adjustYPos}>Adjust y ({controlledPosition.y})</a>
            </p>
          </div>
        </Draggable> */}
        {/* <Draggable bounds="parents" {...dragHandlers}>
          <div className="boxs">
            I can only be moved within the confines of the body element.
          </div>
        </Draggable> */}
        <Draggable bounds="parent" axis="x" position={controlledPosition} onDrag={this.handleDrag.bind(this, 'x')} onStart={this.onStart.bind(this, 'x')}>
          <div className="boxs">
            <p>
              <a href="#" onClick={this.adjustXPos}>Adjust x ({controlledPosition.x})</a>
            </p>
            <p>
              <a href="#" onClick={this.adjustYPos}>Adjust y ({controlledPosition.y})</a>
            </p>
          </div>
        </Draggable>
        <Draggable bounds="parent" position={controlledPosition} {...dragHandlers} onStop={this.onControlledDragStop}>
          <div className="boxs">
            <p>
              <a href="#" onClick={this.adjustXPos}>Adjust x ({controlledPosition.x})</a>
            </p>
            <p>
              <a href="#" onClick={this.adjustYPos}>Adjust y ({controlledPosition.y})</a>
            </p>
          </div>
        </Draggable>
        {/* <Draggable bounds={{ top: 75, left: 0, right: 0, bottom: 200 }} axis="y" onDrag={this.handleDrag.bind(this, 'y')} onStart={this.onStart.bind(this, 'y')} onStop={this.onStop.bind(this, 'y')} position={controlledPosition}>
          <div className="boxs">
            axisy
            <p>
              <a href="#" onClick={this.adjustXPos}>Adjust x ({controlledPosition.x})</a>
            </p>
            <p>
              <a href="#" onClick={this.adjustYPos}>Adjust y ({controlledPosition.y})</a>
            </p>
          </div>
        </Draggable> */}
        {/* <Draggable position={{x: 0, y: controlledPosition.y}} onStart={() => false}>
          <div className="boxs">sidebar</div>
        </Draggable> */}
      </div>

    );
  }



}

export default DragToggle;