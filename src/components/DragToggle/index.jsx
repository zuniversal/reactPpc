import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import Draggable from 'react-draggable'

class DragToggle extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      activeDrags: 0,
      deltaPosition: {
        x: 0, y: 0
      },
      controlledPosition: {
        x: 0, y: 0
      },
      which: '',
      dragging: false
    }
  }

  // 横向滚动条拖动事件
  handleDrag = (e, ui, d) => {
    const { controlledPosition, which } = this.state
    console.log('handleDrag', e, ui, d);
    controlledPosition[e] = d[e]
    this.setState({ controlledPosition: { ...controlledPosition } });
  }

  onStart = (a) => {
    console.log('onStart', a, );
    this.setState({ activeDrags: ++this.state.activeDrags, which: a, dragging: true });
  }

  onStop = (onStop, d) => {
    const { controlledPosition, which } = this.state
    const {tableHeight, tableWidth} = this.props
    console.log('onStop：', this.props,onStop, d, this.state, which, controlledPosition, controlledPosition[onStop], d[onStop]);
    if (which === 'x') {
      if (d[which] < 0) {
        d[which] = Math.max(0, d[which])
      } else {
        d[which] = Math.min(d[which], tableWidth)
      }
    } else {
      if (d[which] < 0) {
        d[which] = Math.max(0, d[which])
      } else {
        d[which] = Math.min(d[which], tableHeight)
      }
    }
    controlledPosition[which] = d[which]
    console.log('d[onStop] ：', d[which], which ,d);
    this.setState({ controlledPosition: { ...controlledPosition } });
  }

  // 滚动范围限制函数
  range = (k) => {
    const {tableHeight, tableWidth, offsetHeight, offsetWidth} = this.props
    const {controlledPosition} = this.state
    const v = controlledPosition[k]
    // console.log('range ：', k, tableHeight, tableWidth, controlledPosition, v);
    let p = 0
    if (v !== 0 && k === 'x') {
      p = - Math.min((((v + 60) / (offsetWidth + 130)) * (tableWidth)), tableWidth - offsetWidth)
    } else if (v !== 0 && k === 'y') {
      p = - Math.min(((v / (offsetHeight - 100)) * (tableHeight)), tableHeight - offsetHeight)
    } else {
      p = 0
    }
    return p
  }

  onControlledDrag = (e, position) => {
    const { x, y } = position;
    this.setState({ controlledPosition: { x, y } });
  }

  filterPos = (k) => {
    const { controlledPosition, dragging } = this.state;
    // console.log('filterPos ：', k, v, controlledPosition, dragging);
    return dragging ? controlledPosition[k] : 0
  }

  render() {
    const dragHandlers = { onStart: this.onStart, onStop: this.onStop };
    const { children, tableHeight, tableWidth, scrollHeight, collapse } = this.props
    const { deltaPosition, controlledPosition, dragging, which } = this.state;
    const style = {'zIndex': 100}
    console.log('##########dragging ：', this.props, controlledPosition, scrollHeight, collapse);
    return (
      <div className="dragToggle">
        {/* 主体 */}
        <Draggable bounds="parent" position={{ x: this.range('x'), y: this.range('y') }} onStart={() => false}>
          {children[2]}
        </Draggable>
        {/* 固定头 */}
        <div className={`headerFixs ${scrollHeight ? 'fix' : ''}`} style={which !== '' ? which === 'x' ? {} : style : {}} style={{'left': collapse ? '74px' : '260px'}}>
          <Draggable bounds="parent" axis="x" position={{ x: this.range('x'), y: 0 }}>
            {children[0]}
          </Draggable>
        </div>
        {/* 固定列 */}
        <div className={`sideBarFixs`} style={which !== '' ? which === 'y' ? {} : style : {}}>
          <Draggable bounds="parent" axis="y" position={{ x: 0, y: this.range('y') }}>
            {children[1]}
          </Draggable>
        </div>
        {/* 横向bar */}
        <div className="headerFix" style={{'left': collapse ? '74px' : '260px'}}>
          <Draggable bounds="parent" onStop={this.onStop} axis="x" position={{ x: this.filterPos('x'), y: 0 }} onDrag={this.handleDrag.bind(this, 'x')} onStart={this.onStart.bind(this, 'x')}>
            <div className="HeadBar">
            </div>
          </Draggable>
        </div>
        {/* 纵向bar */}
        {/* <div className="sideBarFix">
          <Draggable bounds="parent" onStop={this.onStop} axis="y" position={{ x: 0, y: dragging ? controlledPosition.y : 0 }} onDrag={this.handleDrag.bind(this, 'y')} onStart={this.onStart.bind(this, 'y')}>
            <div className="SideBar">
            </div>
          </Draggable>
        </div> */}
        {/* 固定角 */}
        <div className="fixBox" style={{'left': collapse ? '74px' : '260px'}}>
        {children[3]}
        </div>
      </div>

    );
  }



}

export default DragToggle;