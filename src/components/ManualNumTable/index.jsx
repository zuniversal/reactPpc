import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { ANIMATE, SIZE20, } from 'constants'
import {showTotal, } from 'util'
import { purposeColor, typeColor, expenseFilters, purposeFilters, unitArr, 
  standardFilterTxt, DATE_FORMAT_BAR, tagColorArr, } from 'config'
import Inputs from '@@@@/Inputs'
import SecureHoc from '@@@@/SecureHoc'
import Count from '@@@@/Count'
import { Table, Tag, Tooltip, Button, Icon, Select, Checkbox,  } from 'antd';
import moment from 'moment'
const Option = Select.Option;

class ManualNumTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const {total} = this.props
    pagination.pageSize = SIZE20
    // pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      data: [],
      pagination,
      deleteItem: -1,
      len: 0,
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    console.log('handleTableChange ：', pagination, filters, sorter)
    // const { path, option, level } = this.props
    // console.log('pagination, filters, sorter：', this.props, path, option, pagination, filters, sorter);
    // const { pageSize, current } = pagination
  }
  onRowClick = (record, index, event) => {
    event.stopPropagation()
    event.preventDefault()
  }
  selectChange = (o, k, v) => {
    console.log(`selectChange`, o, k, v);
    this.props.editData({...o, v, keys: k})
  }
  saveData = (t, v, i) => {
    console.log('saveData t, v, i：', t, v, i, this.props);
    this.props.showDialog()
  }
  editDataAction = (t, v, i) => {
    console.log('editDataAction t, v, i：', t, v, i, this.props);
    this.props.editDataAction(v)
    // this.props.editData(v)
  }
  deleteData = (t, v, i) => {
    // event.stopPropagation()
    // event.preventDefault()
    console.log('deleteData t, v, i：', t, v, i, this.props);
    this.setState({ deleteItem: i })
    this.props.deleteData({f: 'deleteManualNum', v, item: `${v.pn_no} - ${v.factory_name} - ${v.factory_code}`, action: 'Delete'})
  }
  renderColumns = (t, v, i, key) => {
    const {procedureData, work_no, isDisable, } = this.props
    // console.log('￥￥￥￥￥￥￥￥renderColumns ：', t, v, i, key, )
    if ((v.editable || v.newing) && (key === 'qty' || key === 'remark')) {
      if (key === 'unit') {
        console.log('unitArr[work_no].unit 选择器：', unitArr[work_no], unitArr, work_no);
        return <Select defaultValue={standardFilterTxt[t.trim()]} onChange={this.selectChange.bind(this, v, key)}>
              {unitArr[work_no].map((v, i) => <Option value={v.toString()} key={i}>{standardFilterTxt[v]}</Option>)}
            </Select>
      } else {
        return <Inputs noRule={true} className='txtCenter' data={t} keys={key} val={v} i={i} size={'default'} inputing={this.props.editData}></Inputs>
      }
    } else {
      if (key === 'unit') {
        // console.log('unitArr[work_no].unit 文本 ：', standardFilterTxt[t],  t, v, i, key);
        return <span>{t != undefined ? standardFilterTxt[t.trim()] : t}</span>
      } else if (key === 'work_name') {
        // console.log('unitArr[work_no].unit 文本 ：', standardFilterTxt[t],  t, v, i, key);
        return <Tag className={`${ANIMATE.bounceIn}`} color={tagColorArr.find(item => item.work_no == v.work_no).color} >{t}</Tag> 
      } else {
        return <span>{key === 'r_date' ? moment(t).format(DATE_FORMAT_BAR) : t}</span>
      }
      // return <span>{t}</span>
    }
  }
  componentDidMount() {
    // const {pagination, } = this.state
    const {  data,  } = this.props
    // pagination.pageSize = data.length
    this.setState({
      data,
      // pagination,
    });
  }
  componentWillReceiveProps(nextProps) {
    console.log('Table props 属性的改变', nextProps)
    const { data,  } = nextProps
    const {len, } = this.state
    if (len !== data.length) {
      this.setState({
        deleteItem: -1,
      })
    }
    this.setState({
      len: data.length,
      data,
      // pagination,
    });
  }
  render() {
    const { data, pagination,   } = this.state
    const {canDelete, unitFilters, auth, total, } = this.props
    console.log('ManualNumTable 组件this.state, this.props ：', this.state, this.props);
    const rowKey = 'seq_no'
    const columns = [
      {
        title: 'barcode', dataIndex: 'barcode',
        sorter: (a, b) => a.barcode - b.barcode,
        render: (t, v, i) => this.renderColumns(t, v, i, 'barcode',)
      },
      {
        title: '序號', dataIndex: 'index',
        sorter: (a, b) => a.index - b.index,
        render: (t, v, i) => this.renderColumns(t, v, i, 'index',)
      },
      {
        title: '日期', dataIndex: 'r_date',
        sorter: (a, b) => a.r_date - b.r_date,
        render: (t, v, i) => this.renderColumns(t, v, i, 'r_date',)
      },
      {
        title: '批號', dataIndex: 'pn_no',
        sorter: (a, b) => a.pn_no - b.pn_no,
        render: (t, v, i) => this.renderColumns(t, v, i, 'pn_no',)
      },
      {
        title: '工序', dataIndex: 'work_name',
        sorter: (a, b) => a.work_name - b.work_name,
        render: (t, v, i) => this.renderColumns(t, v, i, 'work_name',)
      },
      // {
      //   title: '工序', dataIndex: 'factory_code',
      //   sorter: (a, b) => a.factory_code - b.factory_code,
      //   render: (t, v, i) => this.renderColumns(t, v, i, 'factory_code',)
      // },
      // {
      //   title: '工區', dataIndex: 'factory_id',
      //   sorter: (a, b) => a.factory_id - b.factory_id,
      //   render: (t, v, i) => this.renderColumns(t, v, i, 'factory_id',)
      // },
      {
        // title: '工厂名稱', dataIndex: 'factory_name',
        title: '工區', dataIndex: 'factory_name',
        sorter: (a, b) => a.factory_name - b.factory_name,
        render: (t, v, i) => this.renderColumns(t, v, i, 'factory_name',)
      },
      {
        title: '數量', dataIndex: 'qty',
        sorter: (a, b) => a.qty - b.qty,
        render: (t, v, i) => this.renderColumns(t, v, i, 'qty',)
      },
      {
        title: '備注', dataIndex: 'remark',
        render: (t, v, i) => this.renderColumns(t, v, i, 'remark',)
      },
    ]
    const actionColumn = {
      title: '編輯', dataIndex: 'editable',
      className: 'standerdEditCol', 
      render: (t, v, i) => {
        // console.log('t, v, i ：', t, v, i);
        return (<div className="btnWrapper">
        <Button onClick={() => v.editable ? this.saveData(t, v, i) : this.editDataAction(t, v, i)} shape="circle" type="primary" icon={`${v.editable ? 'check' : 'edit'}`} className={`m-r5 ${v.editable ? 'succ' : ''}`}></Button>
        {/* <Button onClick={() => this.editDataAction(t, v, i)} shape="circle" type="primary" icon={`${'edit'}`} className={`m-r5 ${'succ'}`}></Button> */}
        <Button onClick={() => this.deleteData(t, v, i)} shape="circle" type="primary" icon='delete' className="warn m-r5"></Button>
        <Checkbox checked={v.finish_status === 'T'} onChange={(e, ) => this.props.finishDay(e, t, v, i)}>確認清貨</Checkbox>
      </div>)
      }
    }
    auth && columns.push(actionColumn)


    return (
      <Table 
        bordered={true}
        columns={columns}
        dataSource={data}
        pagination={pagination}
        onRowClick={this.onRowClick.bind(this, )}
        onChange={this.handleTableChange.bind(this, )}
        rowKey={record => record[rowKey]}

        rowClassName={(record, i) => `${i === this.state.deleteItem && canDelete ? ANIMATE.slideOutLeft : ANIMATE.bounceIn} ${record.finish_status === 'T' ? 'disableBg' : ''}`}
        className={`manualNumTable ${auth ? 'minTd' : ''}`}
        footer={() => <div>
          <span className='footerLabel' >總數:</span>
          <Count className='totalNum'  end={total} duration={1}/>
        </div>}
      />
    );
  }
}

export default SecureHoc(ManualNumTable)
