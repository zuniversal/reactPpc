import React from "react"
import ReactDOM from 'react-dom';
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { grid, initParams, DATE_FORMAT_BAR, MONTH_FORMAT, FORMAT_DAY, } from 'config'
import { ANIMATE, LETTER, } from 'constants'
// import DragableBar from '@@@/DragableBar'
// import DragableBar from '@@@/DragableSlider'
import DragableBar from '@@@/DragableSliders'
import ProductDialog from '@@@/ProductDialog'
import { Dropdown, Icon, Card, Button, Row, Col, Tooltip, Slider } from 'antd';

class ScheduleDay extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  // componentDidMount() {
  //   console.log(' ScheduleDay 组件componentDidMount挂载 ： ', this.state, this.props,  )
    
  // }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.dateLen === this.props.dateLen) {// 
      console.log('##########shouldComponentUpdate  nextProps ：', nextProps, this.props, nextState, nextProps.dateLen, this.props.dateLen, )
      return false
    } else {
      // console.log('shouldComponentUpdateshouldComponentUpdate  nextProps 不等 ：', nextProps, this.props, nextState, nextProps.dateLen, this.props.dateLen, )
      return true
    }
  }
    
  

  render() {
    const { isTxt, isMonth, isLasItem, v } = this.props
    // console.log('ScheduleDay 组件 this.state, this.props ：', this.state, this.props, )
    
    return (
      !isMonth ? <div className="dayWrapper" style={{maxWidth: v.date.length * grid }}>
          {
            v.date.map((item, index) => {
              return <div className={`${item.isSunday ? 'isSunday dateItem' : 'dateItem'} 
              ${isLasItem ? 'boldDate' : ''} ${item.today ? 'today' : ''}`} key={item.dateOrigin + v.date.length}>{isTxt ? item.date : null}</div>
            })
          }
        </div>
        : <div className="headerWrapper">
            <div className="monthWrapper">
              {isMonth ? <div className={`month ${v.date.length === 1 ? 'oneDate' : ''} ${v.date.length === 2 ? 'twoDate' : ''}`}>{v.month}</div> : null}
              <div className="dayWrapper">
                {
                  v.date.map((item, index) => {
                    // console.log(' item.date item ：', item, ) 
                    return <div className={`${item.isSunday ? 'isSunday dateItem' : 'dateItem'}  ${item.today ? 'today' : ''}`} key={item.dateOrigin + v.date.length}>{isTxt ? item.date : null}</div>
                  })
                }
              </div>
            </div>
          </div>
    )
  }
}

export default ScheduleDay

