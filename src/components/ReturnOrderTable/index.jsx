import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {showTotal, dateForm, ajax, infoFilter, } from 'util'
import { ANIMATE, SIZE, } from 'constants'
import Count from '@@@@/Count'
import { Table, Tag, Button, Icon, DatePicker, Select,  } from 'antd';
import moment from 'moment';

class ReturnOrderTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = SIZE
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
    };
  }
  componentDidMount() {
    const { loading, data,  } = this.props
    console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data, )
    this.setState({
      data, 
      loading,
    })
  }
  render() {
    const { pagination,  } = this.state
    const {data, } = this.props
    console.log('ReturnOrderTable 组件 this.state, this.props ：', data, this.state,  this.props,  )
    
    const rowKey = 'index'
    const columns = [
      {
        title: '批號', dataIndex: 'pn_no',
        render: (v, o, i) => <Tag color={'#f50'}>{v}</Tag>
      },
      {
        title: '款號', dataIndex: 'style_no',
        render: (v, o, i) => <Tag color={'green'}>{v}</Tag>
      },
      {
        title: '價格', dataIndex: 'price',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '數量', dataIndex: 'qty',
        render: (v, o, i) => <Count end={v} duration={1}/>
      },
      {
        title: '客戶名', dataIndex: 'customer_name',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '工廠名', dataIndex: 'factory_name',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '備注', dataIndex: 'remark',
        render: (v, o, i) => <span>{v}</span>
      },
    ]
    return (
      <Table 
        bordered={true}
        columns={columns}
        dataSource={data}
        pagination={false}
        rowKey={record => record[rowKey]}
        rowClassName={(record, i) => ANIMATE.bounceIn}
        className='returnOrderTable ' 
      />
    );
  }
}

export default ReturnOrderTable