import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { ANIMATE, SIZE10 } from 'constants'
import {showTotal, } from 'util'
import { purposeColor, typeColor, expenseFilters, purposeFilters, tagColorArr } from 'config'
import SecureHoc from '@@@@/SecureHoc'
import { Table, Tag, Tooltip, Button, Icon } from 'antd';

class Tables extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const pagination = {}
    const {total} = this.props
    pagination.pageSize = SIZE10
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      data: [],
      pagination,
      loading: false,
      deleteItem: -1,
      len: 0,
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    const { path, option, level } = this.props
    console.log('pagination, filters, sorter：', this.props, path, option, pagination, filters, sorter);
    const { pageSize, current } = pagination
    if (path === 'settle') {
      this.props.getApprovel(pageSize, current)
    } else if (path === 'settledReport') {
      this.props.getSettle(pageSize, current)
    }
  }
  onRowClick = (record, index, event) => {
    // event.stopPropagation()
    // event.preventDefault()
    // const { path, level } = this.props
    // console.log('record, index, event：', record, index, event, this.props);
    // this.props.showReportDetail(record.reportID)
  }
  editData = (v, o, i) => {
    console.log('v, o, i：', v, o, i, this.props);
    this.props.editData(o, i)
  }
  deleteWork = (v, o, i) => {
    console.log('deleteWork v, o, i：', v, o, i, this.props);
    this.setState({ deleteItem: i })
    this.props.deleteWork(o, i)
  }
  // showConfirm = (f, user, i) => {
  //   console.log('showConfirm ：', f, user, i)
  //   const {emp_id} = user
  //   confirm({
  //     title: `是否確認刪除工序 - ${emp_name.zh} - ${emp_id} ?`,
  //     content: "請檢查是否確認刪除工序",
  //     width: 550,
  //     onCancel: () => console.log('取消'),
  //     onOk: () => this.delete(user, i)
  //   })
  // }
  componentDidMount() {
    const { loading, data,  } = this.props
    console.log('componentDidMount 组件 this.state, this.props ：', this.state, this.props, data)
    this.setState({
      loading,
      data,
    });
  }
  componentWillReceiveProps(nextProps) {
    console.log('$$$$$$$$$$$$$$$$$$$Table props 属性的改变', nextProps, this.state)
    const { loading, data, pagination } = nextProps
    const {len} = this.state
    if (len !== data.length) {
      this.setState({
        deleteItem: -1,
      })
    }
    this.setState({
      len: data.length,
      loading,
      data,
      pagination,
    });
  }
  render() {
    console.log('ProcedureTables 组件this.state, this.props ：', this.state, this.props);
    const { pagination, data, loading,  } = this.state
    const {canDelete, auth, } = this.props
    const rowKey = 'work_no'
    const columns = [
      {
        title: '工序編碼', dataIndex: 'work_no',
        sorter: (a, b) => a.work_no - b.work_no,
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '工序代號', dataIndex: 'work_code',
        render: (v, o, i) => <span>{v}</span>
      },
      {
        title: '工序名稱', dataIndex: 'work_name',
        // render: (v, o, i) => <Tag color={'blue'}>{v}</Tag>
        render: (t, v, i) => {
          const matchItem = tagColorArr.find(item => item.work_no == v.work_no)
          return v.work_no !== 0 ? <Tag className={`${ANIMATE.bounceIn}`} color={matchItem != undefined ? matchItem.color : 'green'} >{t}</Tag> : null 
        }
      },
    ]
    const actionColumn = {
      title: '編輯', dataIndex: 'editCol',
      render: (v, o, i) => (<div className="btnWrapper">
        <Button onClick={() => this.editData(v, o, i)} shape="circle" type="primary" icon='edit' className="m-r10"></Button>
        <Button onClick={() => this.deleteWork(v, o, i)} shape="circle" type="primary" icon='delete' className="warn"></Button>
      </div>)
    }
    auth && columns.push(actionColumn)
    return (
      <Table 
        bordered={true}
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={pagination}
        onRowClick={this.onRowClick.bind(this, )}
        onChange={this.handleTableChange.bind(this, )}
        rowKey={record => record[rowKey]}
        className='procedureTable' 
        
        rowClassName={(record, i) => i === this.state.deleteItem && canDelete ? ANIMATE.slideOutLeft : ANIMATE.zoomIn}
      />
    );
  }
}

export default SecureHoc(Tables)