import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {bacodeInfo, DATE_FORMAT_BAR, } from 'config'
import { ANIMATE,  } from 'constants'
import {backupFn, getItem, confirms, } from 'util'
import { Row, Col, Button, Checkbox, DatePicker, } from 'antd'
import moment from 'moment'
const user_id = getItem('user_id')

class ClearInfo extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    this.state = {
      isClear: true,
      date: null,
    }
  }
  clearChange = () => {
    const {isClear, date} = this.state 
    console.log('ClearInfo  clear ：', date != undefined, this.state, this.props, moment(date).format(DATE_FORMAT_BAR))
    if (date == undefined) {
      confirms(2, '清貨日期不能爲空 o(*￣︶￣*)o！')
      return 
    } 
    this.setState({
      isClear: !isClear, 
    })
    // this.props.clearChange(isClear, user_id)
    this.props.finishAction(!isClear ? 'T' : 'F', moment(date).format(DATE_FORMAT_BAR),)
  }
  dateChange = (v) => {
    console.log(' dateChange ： ',  v )
    const {isClear, } = this.state 
    this.setState({
      date: v,
      // isClear: v == undefined ? false : isClear, 
      isClear: false, 
    })
  }
  componentDidMount() {
    console.log(' ClearInfo 组件componentDidMount挂载 ： ', this.state, this.props,  )
    const { init, finish_date } = this.props
    console.log(' finish_date ： ', finish_date, moment(finish_date), moment(finish_date, DATE_FORMAT_BAR))
    this.setState({
      // date: finish_date != undefined ? moment(finish_date, DATE_FORMAT_BAR) : null,
      date: finish_date != undefined ? finish_date : null,
      isClear: init !== 'F',
    })
  }

  render() {
    const { init, finish_date } = this.props
    const {lock, isClear, date, } = this.state
    // console.log(' date datedatedatedatedatedatedatedatedatedate： ', date, this.state, this.props, typeof date)
    return (
      <Col span={24} className={`infoItem ${ANIMATE.fadeIn}`}>
        <Row gutter={10}>
          <Col span={24} className="infoItem">
            {/* <Col span={3} className="infoLabel ellipsis">
              當前用戶： 
            </Col>
            <Col span={9} className="infoValue ellipsis">
              {user_id}
            </Col> */}
            <Col span={4} className="infoLabel ellipsis">
              清貨日期： 
            </Col>
            <Col span={16} className="infoValue ellipsis">
              <Checkbox checked={isClear} onChange={this.clearChange}>打鈎確認清貨</Checkbox>
              {
                //typeof date !== 'string' ? 
                <DatePicker className=''
                defaultValue={date != undefined ? moment(date, DATE_FORMAT_BAR) : null}
                onChange={this.dateChange} 
                key={date} 
                format={DATE_FORMAT_BAR}/> 
                //: null
              }
            </Col>
            {/* <Col span={12} className="infoLabel ellipsis">
              <Button onClick={this.clear} type="primary" className={`m-r10 edit`}>清貨</Button>
            </Col> */}
          </Col>
        </Row>
        {/* <div span={24} className="infoItem ">
          <div span={3} className="infoLabel ellipsis">
            當前用戶： 
          </div>
          <div span={9} className="infoValue ellipsis">
            {user_id}
          </div>
          <div span={12} className="infoValue ellipsis">
            <Checkbox checked={isClear} onChange={this.clearChange}>{moment().format('YYYY-MM-DD')}</Checkbox>
          </div>
        </div> */}
      </Col>
    )
  }
}

export default ClearInfo