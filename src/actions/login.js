import * as TYPES from "constants"

export const login = payload => {
    return { type: TYPES.USER_INFO, payload }
}
export const logout = payload => {
    // console.log('LOUOUT      payload ：', payload);
    return { type: TYPES.LOUOUT, payload }
}
export const collapse = payload => {
    return { type: TYPES.IS_COLLPOSED, payload }
}

export const copy = payload => {
    return { type: TYPES.COPY, payload }
}

export const loading = payload => {
    return { type: TYPES.LOAD, payload }
}

