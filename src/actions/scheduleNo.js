import * as TYPES from "constants"

export function filterInfo(payload) {
    return { type: TYPES.FILTER_INFO, payload }
}
export function customerData(payload) {
    return { type: TYPES.ALL_CUSTOMER, payload }
}
export function styleNoData(payload) {
    return { type: TYPES.ALL_STYLE_DATA, payload }
}