import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import Inputs from '@@@@/Inputs'
import InputsNumber from '@@@@/InputsNumber'
import LimitDate from '@@@@/LimitDate'
import {dateForm, ajax, confirms, } from 'util'
import { ANIMATE, } from 'constants'
import { DATE_FORMAT, } from 'config'
import { Table, Tag, Button, Icon, DatePicker, Select,  } from 'antd';
import moment from 'moment';
const { RangePicker } = DatePicker;
const Option = Select.Option;

class BarcodeTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    function showTotal(total) {
      return `Total ${total}`;
    }
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = 6
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      loading: false, 
      selectedRowKeys: [],
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    // const { path, option, level } = this.props
    // console.log('pagination, filters, sorter：', this.props, path, option, pagination, filters, sorter);
    // const { pageSize, current } = pagination
  }
  onRowClick = (record, index, event) => {
    event.stopPropagation()
    event.preventDefault()
  }
  split = (t, v, i) => {
    console.log('split e ：', t, v, i);
    this.props.splitAction(v, i)
  }
  lock = (t, v, i) => {
    console.log('lock e ：', t, v, i, this.props);
    this.props.lock(t, v, i)
  }
  dateChange = (t, v, i, s) => {
    console.log('dateChange ：', t, v, i, s, this.date )
    const {barcode} = v
    const keys = 'date'
    ajax(this.props.inputing, {v: s, barcode, keys})
  }
  
  
  onChange = (selectedRowKeys, selectedRows) => {
    const {data} = this.state
    // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows, data);
    // let keys, inde
    // const idnexs =data.forEach((v, i) => {
    //   const noEQ = selectedRowKeys.every(item => {
    //     console.log(' selectedRowKeys some item ：', item, v.barcode, item !== v.barcode )
    //     return item !== v.barcode 
    //   })
    //   console.log('noEQ ：', noEQ, i, v.barcode )
    //   if (noEQ) {
    //     keys = v.barcode
    //     inde = i
    //   }
    // })
    // console.log('idnexs ：', keys, inde)
    
    // this.setState({
    //   selectedRowKeys: [...selectedRowKeys]
    // })
    // this.props.lock(t, v, i)
  }
  filterOption = (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
  handleChange = (t, v, i, keys, s) => {
    // console.log('handleChange ：', t, v, i, keys, s)
    // if (keys === 'factory_id') {
    //   s = s.split('-')[0]
    // }
    const {productData, } = this.props
    const result = productData.find(v => v.factory_name === s)
    const matchItem = result != undefined ? result.factory_id : ''
    // console.log('matchItem ：', matchItem)
    const {barcode} = v
    ajax(this.props.inputing, {v: matchItem, barcode, keys})
  }
  showColor = (t, v, i) => {
    const {colorData, } = this.props 
    console.log(' showColor ： ',  t, v, i, this.props , )
    if (Object.keys(colorData).length) {
      this.props.showColor(t, v, i)
    } else {
      confirms(2, '沒有色碼信息，o(╥﹏╥)o ！', )
    }
  }
  deleteBar = (t, v, i) => {
    console.log(' deleteBar ： ',  t, v, i )
    this.props.deleteBar(t, v, i)
  }
  delete = (t, v, i) => {
    console.log(' delete ： ',  t, v, i )
    this.props.delete(t, v, i)
  }
  componentDidMount() {
    const { loading,  } = this.props
    // const selectedRowKeys = []
    // data.forEach(v => selectedRowKeys.push(v.barcode))
    this.setState({
      loading,
      // selectedRowKeys: [...selectedRowKeys]
    });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('Table props 属性的改变', nextProps)
    const { loading, pagination } = nextProps
    this.setState({
      loading,
      pagination,
    });
  }
  render() {
    const { pagination, loading, selectedRowKeys} = this.state
    const { data, isSpliting, dialogType, productData, barcodeData, disableDay, colorSizeDataObj, isShowBarcode,   } = this.props
    const {isSplitData} = data[0]
    const isStatic = dialogType === 'showPnCode' || !isSplitData
    console.log('BarcodeTable 组件 this.state, this.props ：', data, isSplitData, dialogType === 'showPnCode', !isSplitData,  this.state, isSpliting, this.props, dialogType )
    const rowSelection = {
      onChange: this.onChange, 
      selectedRowKeys
    };
    const rowKey = 'barcode'
    const columns = [
      {
        title: '工序', dataIndex: 'work_name',
        render: (t, v, i) => <Tag color={'red'}>{t}</Tag>
      },
      {
        title: '生產線', dataIndex: 'factory_name',
        className: 'productLineSelect',
        // 注意：不能使用mode="combobox"，不然的话点击不会自动清除已有的内容
        render: (t, v, i) => isStatic ? <span>{t}</span> : <Select  
          //size='small' 
          // mode="combobox"
          showSearch
          allowClear={true}
          optionFilterProp="children"
          defaultValue={t}
          onChange={this.handleChange.bind(this, t, v, i, 'factory_id')}
          filterOption={this.filterOption}
        > 
          {productData.length ? productData.map((v, i) => {
            {/* console.log('v, i ：', v, i, v.factory_id + '-' + v.work_no) */}
            return <Option key={v.factory_name} title={v.factory_name}>{v.factory_name}</Option>
          }) : null}
        </Select>
      },
      {
        title: 'SAM', dataIndex: 'sam',
        // sorter: (a, b) => a.sam - b.sam,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '日產(人/台)', dataIndex: 'day_qty',
        // sorter: (a, b) => a.day_qty - b.day_qty,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '起止日期', dataIndex: 's_date',
        className: 'dateColumn',
        // render: (t, v, i) => isStatic ? <span>{dateForm(v.s_date, '/')} - {dateForm(v.e_date, '/')}</span> : <RangePicker
        //   value={[moment(v.s_date, DATE_FORMAT), moment(v.e_date, DATE_FORMAT)]} ref={date => this.date = date}
        //   format={DATE_FORMAT} onChange={this.dateChange.bind(this, t, v, i)}
        //   // showTime={{ hideDisabledOptions: true, defaultValue: [moment('00:00:00', 'HH:mm:ss'), moment('11:59:59', 'HH:mm:ss')], }}/>
        //   />
        render: (t, v, i) => isStatic ? <span>{dateForm(v.s_date, '/')} - {dateForm(v.e_date, '/')}</span> : <LimitDate
           disableDay={disableDay} data={v} onChange={this.dateChange.bind(this, t, v, i)} k1={'s_date'} k2={'e_date'}
          />
      },
      // {
      //   title: '預計天數', dataIndex: 'work_days',
      //   render: (t, v, i) => isStatic ? <span>{t}</span> : <Inputs keys='work_days' val={v} i={i} inputing={this.props.inputing} data={t}></Inputs>
      // },
      {
        title: '預計天數', dataIndex: 'work_days',
        // sorter: (a, b) => a.work_days - b.work_days,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '總人(台)數', dataIndex: 't_workers',
        // sorter: (a, b) => a.t_workers - b.t_workers,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '總日產', dataIndex: 't_day_qty',
        // sorter: (a, b) => a.t_day_qty - b.t_day_qty,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '人數(尾期)', dataIndex: 'last_wokers',
        // sorter: (a, b) => a.last_wokers - b.last_wokers,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '數量(尾期)', dataIndex: 'last_qty',
        // sorter: (a, b) => a.last_qty - b.last_qty,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '數量', dataIndex: 'qty',
        className: 'numColumn',
        // sorter: (a, b) => a.qty - b.qty,
        render: (t, v, i) => <span>{t}</span>
        // render: (t, v, i) => {
        //   const newQty = isShowBarcode ? colorData[v.barcode] != undefined ? colorData[v.barcode].reduce((total, cuv, ) => total += cuv.status === 'T' ? cuv.qty : 0, 0) : v.qty : v.qty
        //   // console.log(' v.qty ： ', v.qty,  )
        //   return isStatic ? <span>{t}</span> : <InputsNumber key='qty' keys='qty' val={{...v, qty: newQty}} i={i} inputing={this.props.inputing} data={t}></InputsNumber>
        // }
      },
      // {
      //   title: '色碼信息', dataIndex: 'colorData',
      //   className: 'width250',
      //   render: (t, v, i) => {
      //     // console.log(' t, v, i ： ', t, v, i, colorData[v.barcode.trim()], this.props ,  )
      //     return <div>
      //       {
      //         colorData[v.barcode.trim()] != undefined ? <div>
      //           {
      //             colorData[v.barcode.trim()].map((item, index) => {
      //               const colorTag = <div key={index} >
      //                 {['combo_id', 'qty'].map((v, i) => <Tag key={i} color={i % 2 ? '#108ee9' : '#f50'}>{`${item[v]}`}</Tag>)}
      //               </div>
      //               return item.status === 'T' ? colorTag : '...'
      //               return !isStatic || item.status === 'T' ? colorTag : '...'
      //             })}
      //         </div> : '...'
      //       }
      //     </div>
      //     // return <span>{t.map((item, i) => item.status === 'T' && <Tag key={i} color={i % 2 ? '#f50' : '#108ee9'}>{`${item.combo_id} - ${item.qty};`}</Tag>)}</span>
      //   }
      // },
      {
        title: '色碼信息', dataIndex: 'colorData',
        className: 'width250',
        render: (t, v, i) => {
          console.log(' t, v, i ： ', t, v, i, colorSizeDataObj[v.barcode.trim()], this.props ,  )
          return <div>
            {
              colorSizeDataObj[v.barcode.trim()] != undefined ? <div>
                {
                  Object.values(colorSizeDataObj[v.barcode.trim()]).map((item, index) => <div key={index}>
                    <div className='m-b5' ><Tag color={'#108ee9'}>{`${item[0].combo_id}`}</Tag></div>
                    {
                      item.map((items, i) => <span key={items.pn_qty}>
                        <Tag color={'#f50'}>{`${items.size_id}`}</Tag>
                        <Tag color={'purple'}>{`${items.pn_qty}`}</Tag>
                      </span>)
                    }
                  </div>)}
              </div> : '...'
            }
          </div>
        }
      },
      // {
      //   title: '數量', dataIndex: 'qty',
      //   className: 'numColumn',
      //   render: (t, v, i) => isStatic ? <span>{t}</span> : <Inputs keys='qty' val={v} i={i} inputing={this.props.inputing} data={t}></Inputs>
      // },
    ]
    // if (isStatic && dialogType !== 'showPnCode') {
      // columns.push(...[{
      //   title: '剩余', dataIndex: 'originQty',
      //   render: (t, v, i) => <span>{t}</span>
      // },])
    // }
    if (!isStatic) {
      columns.push({
        title: '操作', dataIndex: 'color',
        render: (t, v, i) => {
          // icon={`${t ? 'check' : 'edit'}`} 
          // return <Button onClick={() => this.showColor(t, v, i, t ? 'add' : 'edit')} type="primary" className={`${t ? 'succ' : ''}`}>{t ? '增加' : '修改'}</Button>
          return <div>
            <Button onClick={() => this.showColor(t, v, i)} type="primary" className={'succ'}>色碼</Button>
            <Button onClick={() => this.deleteBar(t, v, i)} shape="circle" type="primary" icon='delete' className="warn"></Button>
          </div>
        }
      })
    }
    // !isShowBarcode && columns.push({
    //   title: '操作', dataIndex: 'action',
    //   render: (t, v, i) => <Button onClick={() => this.delete(t, v, i)} type="primary" className={'warn'}>删除</Button>
    // },)

    return (
      <Table 
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={pagination}
        onRowClick={this.onRowClick.bind(this, )}
        onChange={this.handleTableChange.bind(this, )}
        rowKey={record => record[rowKey]}
        
        rowClassName={(record, i) => ANIMATE.zoomIn}
        className='barcodeTable noPagination m-b20' 
      />
    );
  }
}


export default BarcodeTable