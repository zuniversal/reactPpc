import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import Inputs from '@@@@/Inputs'
import InputsNumber from '@@@@/InputsNumber'
import LimitDate from '@@@@/LimitDate'
import DateInput from '@@@@/DateInput'
import {dateForm, ajax, confirms, dateSplits, } from 'util'
import { ANIMATE, } from 'constants'
import { DATE_FORMAT, pnDateArr, } from 'config'
import { Table, Tag, Button, Icon, DatePicker, Select, InputNumber, Input,  } from 'antd';
import moment from 'moment';
const { RangePicker } = DatePicker;
const Option = Select.Option;

class BarcodeTable extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    function showTotal(total) {
      return `Total ${total}`;
    }
    const pagination = {}
    const { total } = this.props
    pagination.pageSize = 6
    pagination.total = total
    pagination.showSizeChanger = true
    pagination.showTotal = showTotal
    this.state = {
      pagination,
      loading: false, 
      selectedRowKeys: [],
      isInputing: false, 
      barcode: '',
      isEditting: false, 
    };
  }
  handleTableChange = (pagination, filters, sorter) => {
    // const { path, option, level } = this.props
    // console.log('pagination, filters, sorter：', this.props, path, option, pagination, filters, sorter);
    // const { pageSize, current } = pagination
  }
  onRowClick = (record, index, event) => {
    event.stopPropagation()
    event.preventDefault()
  }
  split = (t, v, i) => {
    console.log('split e ：', t, v, i);
    this.props.splitAction(v, i)
  }
  lock = (t, v, i) => {
    console.log('lock e ：', t, v, i, this.props);
    this.props.lock(t, v, i)
  }
  dateChange = (t, v, i, s) => {
    console.log('dateChange ：', t, v, i, s, this.props, this.state, )
    const {barcode} = v
    const keys = 'date'
    const {dialogType, } = this.props// 
    const isStatic = dialogType === 'showPnCode'
    ajax(this.props.inputing, {v: s, barcode, keys})
  }
  editDay = (v) => {
    const {isSplit, productData,   } = this.props// 
    const {barcode, keys, type, factory_name  } = v
    console.log(' editDay222222222 ： ', barcode, keys, v, this.props, this.state, isSplit, )
    if (type === 'productLine') {
      const result = productData.find(v => v.factory_name === factory_name)
      const matchItem = result != undefined ? result.factory_id : ''
      v.v = matchItem
      console.log(' result ： ', result, matchItem, v )// 
    } 
    // !isSplit ? this.props.editDay(v) : ajax(this.props.inputing, {v: v.v, barcode, keys})

    // !isSplit ? this.props.editDay(v) : this.props.inputing(v)
    // 是否拆分 触发不同的修改
    !isSplit ? ajax(this.props.editDay, v) : this.props.inputing(v)
  }
  onEvent = (barcode) => {
    console.log(' onEvent ： ', this.state.isInputing, barcode )
    this.setState({
      barcode: barcode,
      isInputing: !this.state.isInputing,
    })
  }
  onChange = (selectedRowKeys, selectedRows) => {
    const {data} = this.state
    // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows, data);
    // let keys, inde
    // const idnexs =data.forEach((v, i) => {
    //   const noEQ = selectedRowKeys.every(item => {
    //     console.log(' selectedRowKeys some item ：', item, v.barcode, item !== v.barcode )
    //     return item !== v.barcode 
    //   })
    //   console.log('noEQ ：', noEQ, i, v.barcode )
    //   if (noEQ) {
    //     keys = v.barcode
    //     inde = i
    //   }
    // })
    // console.log('idnexs ：', keys, inde)
    
    // this.setState({
    //   selectedRowKeys: [...selectedRowKeys]
    // })
    // this.props.lock(t, v, i)
  }
  filterOption = (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
  handleChange = (t, v, i, keys, s) => {
    console.log('handleChange ：', t, v, i, keys, s)// 
    // if (keys === 'factory_id') {
    //   s = s.split('-')[0]
    // }
    const {productData, } = this.props
    const result = productData.find(v => v.factory_name === s)
    const matchItem = result != undefined ? result.factory_id : ''
    // console.log('matchItem ：', matchItem)
    const {barcode} = v
    ajax(this.props.inputing, {v: matchItem, barcode, keys})
  }
  showColor = (t, v, i) => {
    const {colorData, } = this.props 
    console.log(' showColor ： ',  t, v, i, this.props , )
    // if (Object.keys(colorData).length) {
    //   this.props.showColor(t, v, i)
    // } else {
    //   confirms(2, '沒有色碼信息，o(╥﹏╥)o ！', )
    // }
    this.props.showColor(t, v, i)
  }
  deleteBar = (t, v, i) => {
    console.log(' deleteBar ： ',  t, v, i )
    this.props.deleteBar(t, v, i)
  }
  delete = (t, v, i) => {
    console.log(' delete ： ',  t, v, i )
    this.props.delete(t, v, i)
  }
  startEditDay = (t, v, i) => {
    console.log(' startEditDay ： ', t, v, i )
    this.setState({ isEditting: true, })
    this.props.startEditDay(t, v, i)
  }
  componentDidMount() {
    const { loading,  } = this.props
    // const selectedRowKeys = []
    // data.forEach(v => selectedRowKeys.push(v.barcode))
    this.setState({
      loading,
      // selectedRowKeys: [...selectedRowKeys]
    });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('Table props 属性的改变', nextProps)
    const { loading, pagination } = nextProps
    this.setState({
      loading,
      pagination,
    });
  }
  render() {
    const { pagination, loading, selectedRowKeys, isInputing, barcode, isEditting, } = this.state
    const { data, isSpliting, dialogType, productData, barcodeData, disableDay, colorSizeDataObj, isShowBarcode, isSplit,   } = this.props
    const {isSplitData, endDay } = data[0]
    const isStatic = dialogType === 'showPnCode' || !isSplitData// 
    console.log('BarcodeTable 组件 this.state, this.props ：', isInputing, endDay, data, isSplitData, dialogType === 'showPnCode', !isSplitData,  this.state, isSpliting, this.props, dialogType )
    const rowSelection = {
      onChange: this.onChange, 
      selectedRowKeys
    };
    const rowKey = 'barcode'
    const columns = [
      // {
      //   title: 'barcode', dataIndex: 'barcode',
      //   render: (t, v, i) => <Tag color={'red'}>{t}</Tag>
      // },
      {
        title: '工序', dataIndex: 'work_name',
        render: (t, v, i) => <Tag color={'red'}>{t}</Tag>
      },
      {
        title: '生產線', dataIndex: 'factory_name',
        className: 'productLineSelect',
        // 注意：不能使用mode="combobox"，不然的话点击不会自动清除已有的内容
        // render: (t, v, i) => isStatic ? <span>{t}</span> : <Select  
        // render: (t, v, i) => (isShowBarcode && isEditting) || isSplit ? <Select  
        render: (t, v, i) => {
          const factorySelect = <Select 
            //size='small' 
            // mode="combobox"
            showSearch
            allowClear={true}
            optionFilterProp="children"
            defaultValue={t}
            onChange={
              isSplit ? this.handleChange.bind(this, t, v, i, 'factory_id') : (factory_name) => this.editDay({t, v, i, factory_name, keys: 'factory_id', type: 'productLine', })
            }
            filterOption={this.filterOption}
          > 
            {productData.length ? productData.map((v, i) => {
              return <Option key={v.factory_name} title={v.factory_name}>{v.factory_name}</Option>
            }) : null}
          </Select>
          return isStatic ? <div>
            <div className='m-b5 noWrap'>{isShowBarcode ? '原生產線：' : ''} {t}</div>
              {
                isEditting ? !isSplit && isShowBarcode ? factorySelect : null : null
              }
            </div> : factorySelect
        }
      },
      {
        title: 'SAM', dataIndex: 'sam',
        // sorter: (a, b) => a.sam - b.sam,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '日產(人/台)', dataIndex: 'day_qty',
        // sorter: (a, b) => a.day_qty - b.day_qty,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '起止日期', dataIndex: 's_date',
        className: 'dateColumn',
        // render: (t, v, i) => isStatic ? <span>{dateForm(v.s_date, '/')} - {dateForm(v.e_date, '/')}</span> : <RangePicker
        //   value={[moment(v.s_date, DATE_FORMAT), moment(v.e_date, DATE_FORMAT)]} ref={date => this.date = date}
        //   format={DATE_FORMAT} onChange={this.dateChange.bind(this, t, v, i)}
        //   // showTime={{ hideDisabledOptions: true, defaultValue: [moment('00:00:00', 'HH:mm:ss'), moment('11:59:59', 'HH:mm:ss')], }}/>
        //   />
        render: (t, v, i) => {
          // const limitDate = <LimitDate
          // // disableDay={disableDay} disableDay={endDay}
          //  data={v} onChange={this.dateChange.bind(this, t, v, i)} k1={'s_date'} k2={'e_date'}
          // />dateInput
          const limitDate = pnDateArr.map((item, i) => {
            return <DateInput key={i} className={`dateInput d-ib ${i === 0 ? 'm-ro': ''}`} 
              val={v[`new_${item}`]} keys={item}
              barcode={v.barcode}
              onChange={this.editDay}// 
            /> 
          })
          // const limitDate = pnDateArr.map((item, i) => {
          //   // console.log(' Inputs item eidtKey={`new_${item}`} ： ', item, isInputing ? dateSplits(v[item]) : v[item]) 
          //   return <Inputs key={i} wrapCls={`dateInput d-ib ${i === 0 ? 'm-ro': ''}`} keys={item} noRule={true} 
          //     // data={isInputing ? dateSplits(v[item]) : v[`new_${item}`]} 
          //     data={isInputing && barcode === v.barcode ? dateSplits(v[`new_${item}`]) : v[`new_${item}`]} 
          //     val={v} size={'default'} inputing={(e) => this.editDay(e)} 
          //     onFocus={() => this.onEvent(v.barcode)}// 
          //     onBlur={() => this.onEvent(v.barcode)}// 
          //     // onFocus={(e) => this.onEvent(e)}// 
          //     // onBlur={(e) => this.onEvent(e)}// 
          //     /> 
          //     // !isInputing ? : <Input defaultValue={v[item]} key={i} 
          //     // onFocus={(e) => this.onEvent(e)} // 
          //     // // onBlur={(e) => this.onEvent(e)}// 
          //     //  />
          // })
          return isStatic ? <div>
            <div className='orginDate noWrap'>{isShowBarcode ? '原日期': ''}{dateForm(v.s_date, '/')} - {dateForm(v.e_date, '/')}</div>
            {/* {dateInput} */}
            {isShowBarcode ? <div className='c-g noWrap m-b5' >輸入框輸入日期的格式 如: 20180808</div> : null}
            {isShowBarcode && !isEditting && !isSplit ? <Button onClick={() => this.startEditDay(t, v, i)} type="primary" className={'succ'}>修改起止日期</Button> : null} 
            {/* {
              !isSplit ? pnDateArr.map((item, i) => {
                // console.log(' Inputs item eidtKey={`new_${item}`} ： ', item, )  
                return <Inputs key={i}  wrapCls={`d-ib ${i === 0 ? 'm-r5': ''}`} keys={item} noRule={true} className='max100' data={dateSplits(v[item])}
                  val={v} size={'default'} inputing={(e) => this.props.editDay(e)}/>
              }) : null 
            } */}
            {
              isEditting ? !isSplit && isShowBarcode ? limitDate : null : null 
            }
            {/* {limitDate} */}
          </div> : limitDate
        }
      },
      // {
      //   title: '預計天數', dataIndex: 'work_days',
      //   render: (t, v, i) => isStatic ? <span>{t}</span> : <Inputs keys='work_days' val={v} i={i} inputing={this.props.inputing} data={t}></Inputs>
      // },
      {
        title: '預計天數', dataIndex: 'work_days',
        // sorter: (a, b) => a.work_days - b.work_days,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '總人(台)數', dataIndex: 't_workers',
        // sorter: (a, b) => a.t_workers - b.t_workers,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '總日產', dataIndex: 't_day_qty',
        // sorter: (a, b) => a.t_day_qty - b.t_day_qty,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '人數(尾期)', dataIndex: 'last_wokers',
        // sorter: (a, b) => a.last_wokers - b.last_wokers,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '數量(尾期)', dataIndex: 'last_qty',
        // sorter: (a, b) => a.last_qty - b.last_qty,
        render: (t, v, i) => <span>{t}</span>
      },
      {
        title: '數量', dataIndex: 'qty',
        className: 'numColumn',
        // sorter: (a, b) => a.qty - b.qty,
        render: (t, v, i) => <span>{t}</span>
        // render: (t, v, i) => {
        //   const newQty = isShowBarcode ? colorData[v.barcode] != undefined ? colorData[v.barcode].reduce((total, cuv, ) => total += cuv.status === 'T' ? cuv.qty : 0, 0) : v.qty : v.qty
        //   // console.log(' v.qty ： ', v.qty,  )
        //   return isStatic ? <span>{t}</span> : <InputsNumber key='qty' keys='qty' val={{...v, qty: newQty}} i={i} inputing={this.props.inputing} data={t}></InputsNumber>
        // }
      },
      // {
      //   title: '色碼信息', dataIndex: 'colorData',
      //   className: 'width250',
      //   render: (t, v, i) => {
      //     // console.log(' t, v, i ： ', t, v, i, colorData[v.barcode.trim()], this.props ,  )
      //     return <div>
      //       {
      //         colorData[v.barcode.trim()] != undefined ? <div>
      //           {
      //             colorData[v.barcode.trim()].map((item, index) => {
      //               const colorTag = <div key={index} >
      //                 {['combo_id', 'qty'].map((v, i) => <Tag key={i} color={i % 2 ? '#108ee9' : '#f50'}>{`${item[v]}`}</Tag>)}
      //               </div>
      //               return item.status === 'T' ? colorTag : '...'
      //               return !isStatic || item.status === 'T' ? colorTag : '...'
      //             })}
      //         </div> : '...'
      //       }
      //     </div>
      //     // return <span>{t.map((item, i) => item.status === 'T' && <Tag key={i} color={i % 2 ? '#f50' : '#108ee9'}>{`${item.combo_id} - ${item.qty};`}</Tag>)}</span>
      //   }
      // },
      {
        title: '色碼信息', dataIndex: 'colorData',
        className: 'width350',
        render: (t, v, i) => {
          // console.log(' t, v,c i items.： ', colorSizeDataObj, t, v, i, colorSizeDataObj[v.barcode.trim()], this.props ,  )
          console.log(' this.props.colorSizeHandle(v.barcode.trim()) ： ', v.barcode.trim(),  )// 
          return <div>
            {
              // colorSizeDataObj[v.barcode.trim()] != undefined ? <div>
              this.props.colorSizeHandle(v.barcode.trim()) != undefined ? <div>
                {
                  // Object.values(colorSizeDataObj[v.barcode.trim()]).map((item, index) => item.data
                  Object.values(this.props.colorSizeHandle(v.barcode.trim())).map((item, index) => item.data
                    // .some(v => v.status === 'T') ? <div key={index}>
                    .some(v => v.status === 'T') ? <div key={index}>
                      {/* <div className='m-b5' > */}
                      { item.justShow ? item.data.map((items, i) => items.status === 'T' ? <span key={i}>
                          <Tag color={'#108ee9'}>{`${items.combo_id}`}</Tag>
                          <Tag color={'#f50'}>{`${items.qty}`}</Tag>
                        </span> : null) : <div key={index}>
                          <div className='m-b5' >
                            <Tag color={'#108ee9'}>{`${item.combo_id}`}{`${item.status}`}</Tag>
                          </div>
                          {
                            item.data.map((items, i) => items.status === 'T' ? <span key={i}>
                              <Tag color={'purple'}>{`${items.size_id}`}</Tag>
                              <Tag color={'#f50'}>{`${items.qty}`}</Tag>
                            </span> : null)
                          }
                        </div> 
                        }
                      {/* </div> */}
                    </div> 
                  : null
                  )
                }
                {/* {
                  Object.values(colorSizeDataObj[v.barcode.trim()]).map((item, index) => item.data.some(v => v.status === 'T') ? <div key={index}>
                    <div className='m-b5' >
                    { item.justShow ? item.data.map((items, i) => items.status === 'T' ? <span key={i}>
                        <Tag color={'#108ee9'}>{`${items.combo_id}`}</Tag><Tag color={'#f50'}>{`${items.qty}`}</Tag>
                      </span> : null) : <Tag color={'#108ee9'}>{`${item.combo_id}`}{`${item.status}`}</Tag>
                      }
                    </div>
                    {
                      item.data.map((items, i) => items.status === 'T' ? <span key={i}>
                        <Tag color={'purple'}>{`${items.size_id}`}</Tag>
                        <Tag color={'#f50'}>{`${items.qty}`}</Tag>
                      </span> : null)
                    }
                  </div> : null
                  )
                } */}
              </div> : '...'
            }
          </div>
        }
      },
      // {
      //   title: '數量', dataIndex: 'qty',
      //   className: 'numColumn',
      //   render: (t, v, i) => isStatic ? <span>{t}</span> : <Inputs keys='qty' val={v} i={i} inputing={this.props.inputing} data={t}></Inputs>
      // },
    ]
    // if (isStatic && dialogType !== 'showPnCode') {
      // columns.push(...[{
      //   title: '剩余', dataIndex: 'originQty',
      //   render: (t, v, i) => <span>{t}</span>
      // },])
    // }
    if (!isStatic) {
      columns.push({
        title: '操作', dataIndex: 'color',
        render: (t, v, i) => {
          // icon={`${t ? 'check' : 'edit'}`} 
          // return <Button onClick={() => this.showColor(t, v, i, t ? 'add' : 'edit')} type="primary" className={`${t ? 'succ' : ''}`}>{t ? '增加' : '修改'}</Button>
          return <div>
            <Button onClick={() => this.showColor(t, v, i)} type="primary" className={'succ'}>色碼</Button>
            <Button onClick={() => this.deleteBar(t, v, i)} shape="circle" type="primary" icon='delete' className="warn"></Button>
          </div>
        }
      })
    }
    // !isShowBarcode && columns.push({
    //   title: '操作', dataIndex: 'action',
    //   render: (t, v, i) => <Button onClick={() => this.delete(t, v, i)} type="primary" className={'warn'}>删除</Button>
    // },)

    return (
      <Table 
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={pagination}
        onRowClick={this.onRowClick.bind(this, )}
        onChange={this.handleTableChange.bind(this, )}
        rowKey={record => record[rowKey]}
        
        rowClassName={(record, i) => ANIMATE.zoomIn}
        className='barcodeTable noPagination m-b20' 
      />
    );
  }
}


export default BarcodeTable