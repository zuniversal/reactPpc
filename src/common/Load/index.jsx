import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import fly from 'static/img/fly.jpg'

class NotFound extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }

  render() {
    return (
      <section className="loadingWrapper">
        <div className="loadingPages">
          <div className="fly"><img src={fly} alt="" /></div>
          <div className="loadingTxt">
            努力加載數據中
          </div>
        </div>
      </section>
    );
  }
}

export default NotFound;
