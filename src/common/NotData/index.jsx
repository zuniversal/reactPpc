import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import fly from 'static/img/fly.jpg'
class NotData extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  render() {
    return (
      <div className='noDatas'>
        <p className='noData'>Not Data o(╥﹏╥)o!</p>
        <img src={fly} className='noImg' />
      </div>
    );
  }
}
export default NotData;