import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {items} from 'config'
class Dashboard extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      actives: 3
    }
  }
  clickChart = i => {
    this.setState({actives: i})
    let transform1 = this.refs[this.state.actives].style.transform
    let transform2 = this.refs[i].style.transform
    this.refs[this.state.actives].style.transform = transform2
    this.refs[i].style.transform = transform1
  }
  render() {
    console.log('Dashboard：', this.props.children);
    return (
        <div className="itemWrapper">{items.map((item, i) => <div key={i} ref={i} className={`item ${this.state.actives === i ? 'actives' : ''}`} onClick={this.clickChart.bind(this, i)} style={item}>{this.props.children[i]}</div>)}</div>  
    );
  }
}
export default Dashboard;