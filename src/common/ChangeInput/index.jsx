import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {ajax} from 'util'
import { ANIMATE, } from 'constants'
import { Form, Input,  } from 'antd';
const FormItem = Form.Item;

class ChangeInput extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      value: '',
    }
  }
  valueChange = (v) => {
    console.log('valueChange v ：', v.target.value, this.props, this.props.form.getFieldValue('data'));
    // const { currency } = this.props
    const {val, i, keys, inputAction, } = this.props
    const {barcode} = val
    console.log('val, i ：', keys, val, i, barcode, )
    this.setState({ value: v.target.value })
    // ajax(this.props.inputing, {v: v.target.value, barcode, keys})
    if (inputAction === 'split') {
      ajax(this.props.inputing, {v: v.target.value, barcode, keys})
    } else {
      ajax(this.props.inputing, {v: v.target.value, ...val, keys})
    }
  }
  componentWillReceiveProps(nextProps) {
    const {show} = this.props
    // if (show) {
    //   this.props.form.resetFields()
    // }
  }
  render() {
    const {disabled, data, keys, val, className, size, noRule, isDisable, } = this.props
    const { getFieldDecorator } = this.props.form;
    // console.log('ChangeInput组件  ：', this.props, val[keys]);
    return (
      <Form className="ChangeInput" >
        <FormItem>
          {getFieldDecorator(keys, 
          {
            rules: noRule != undefined ? undefined : [{
              required: true,
              type: 'string',
              message: 'Require String',
            }], initialValue: data != undefined ? data : ''
          }) (
            <Input disabled={isDisable} className={`${className != undefined ? className : ''} ${ANIMATE.bounceIn}`} size={size != undefined ?  size : "small"}  onChange={this.valueChange} />
            )}
        </FormItem>
      </Form>)
  }
}
const WrappedDemo = Form.create()(ChangeInput);
export default WrappedDemo;