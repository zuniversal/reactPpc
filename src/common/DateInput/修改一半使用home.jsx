import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {numToDate, dateSplits, ajax,   } from 'util'
import { InputNumber } from 'antd';

class DateInput extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  onChange = (v) => {
    const {keys, } = this.props
    // console.log('onChange v ：', this.props, v, dateSplits(v.toString()), dateSplits(v.toString()).length < 9    );
    this.props.onChange({v: dateSplits(v.toString()), keys, ...this.props, })// 
  }
  render() {
    const {val, className,  } = this.props// 
    // console.log(' %c DateInput 组件 this.state, this.props ： ', `color: #333; font-weight: bold`, this.state, this.props,  )// 
    return (
      <InputNumber 
        className={`${className} `}
        // defaultValue={val}
        value={val}
        // formatter={v => numToDate(v)}
        formatter={v => {
          const str = v.split('').filter(v => v !== '-')
          const v1 = str.slice(0, 2).join('')
          const v2 = str.slice(2, 4).join('')
          // const v3 = str.slice(6, 8).join('')//
           console.log(' value formatter 格式化22 str.slice(0, 2) ： ', v, this.props.keys, this.props, v1, v2,  str,   )// 
          return `${v1}${str.length > 2 ? '-' : ''}${v2}` 
        }}
        // parser={value => value}
        parser={value => {
          // console.log(' parser value ： ', value,  )// 
          this.onChange(value)
          return value
        }}
        // onChange={this.onChange}
      />
    )
  }
}

export default DateInput
