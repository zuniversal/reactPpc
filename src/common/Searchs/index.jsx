import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { hashHistory } from 'react-router'
import { INPUT_TXT, ANIMATED } from 'constants'
import { Button, Row, Col, Input, Icon, } from 'antd';
class Searchs extends React.Component {
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      txt: ''
    }
  }
  // onChange = (key, employeeID) => ajax(this.props.searchApprover, { key, employeeID })
  onChange = (e) => this.setState({ txt: e.target.value })
  clickHandle = () => this.props.search(this.state.txt)
  render() {
    return (
      <div className="searchWrapper">
        <Input placeholder={`${INPUT_TXT}${this.props.lable}`} onChange={this.onChange} addonAfter={<Icon type="search" onClick={this.clickHandle} />} />
        {/* <Button onClick={this.clickHandle} type="primary" icon="search" className="m-l5"></Button> */}
      </div>
    );
  }
}
export default Searchs;