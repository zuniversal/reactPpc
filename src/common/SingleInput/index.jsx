import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { Form, Input,  } from 'antd';
const FormItem = Form.Item;

class SingleInput extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      value: '',
    }
  }
  valueChange = (v) => {
    console.log('valueChange v ：', v.target.value, this.props, this.props.form.getFieldValue('data'));
    // const { currency } = this.props
    // this.setState({ value: v })
    // this.props.rateInput(currency, v)
  }
  componentWillReceiveProps(nextProps) {
    const {show} = this.props
    console.log('SingleInput componentWillReceiveProps ：', nextProps, this.props, show )
    // if (show) {
    //   this.props.form.resetFields()
    // }
  }
  render() {
    const {disabled, data, } = this.props
    const { getFieldDecorator } = this.props.form;
    console.log('WrappedDemo ：', this.props);
    return (
    <div>
      <Form className="inputs">
        <FormItem>
          {getFieldDecorator('data', {
            rules: [{
              required: true,
              type: 'string',
              message: 'Require String',
            }], initialValue: data != undefined ? data : ''
          })(
            <Input size="small" onChange={this.valueChange} />
            )}
        </FormItem>
      </Form>
    </div>)
  }
}
const WrappedDemo = Form.create()(SingleInput);
export default WrappedDemo;