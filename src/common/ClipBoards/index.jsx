import React from 'react';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import { confirms, } from 'util'
import { connect } from "react-redux"
import { Button, } from 'antd';
import './style.less'

class ClipBoards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  // onCopy = (v, r, t) => {
  //   console.log(' onCopy ： ', v, r, t, this.props )
  //   const {copy, } = this.props 
  //   confirms(1, `${copy} 文本複制成功！ O(∩_∩)O~`)
  // }
  onCopy = (v, r, t) => {
    console.log(' onCopy ： ', v, r, t, this.props )
    confirms(1, `${v} 文本複制成功！ O(∩_∩)O~`)
  }
  // componentWillReceiveProps(nextProps) {//
  //    console.log(`%t ClipBoards componentWillReceiveProps 改变 `, nextProps.copy,nextProps, this.props, this.state,  )
  //   const {copy, } = nextProps
  //   if (copy !== this.props.copy) {
  //     this.onCopy(copy)
  //   } 
  // }
  componentWillUpdate(nextProps) {
    console.log(' ClipBoards componentWillUpdate 将更新 ： ', nextProps, this.props, this.state,  )
    const {copy, } = nextProps
    if (copy !== this.props.copy) {
      this.setState({
        text: copy,
      })
      this.onCopy(copy)
    } 
  }
  
  
  render() {
    const { children, copy } = this.props
    const {text, } = this.state 
    
    console.log(' %c ClipBoards 组件 this.state, this.props ： ', `color: #333; font-weight: bold`, this.state, this.props,  )
    return (
      <CopyToClipboard text={copy} 
      //onCopy={this.onCopy}
      ><div className='copyWrapper' >
        <Button onClick={this.props.onClick} type="primary">{children}</Button>
      </div></CopyToClipboard>
    )
    return (
      <CopyToClipboard text={copy} 
      //onCopy={this.onCopy}
      ><div className='copyWrapper' >
        {children}
      </div></CopyToClipboard>
    )
  }
}

export default ClipBoards
// export default connect(s => s, {})(ClipBoards)
