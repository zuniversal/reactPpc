import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import Inputs from '@@@@/Inputs'
import InputsNumber from '@@@@/InputsNumber'
class EditableCeil extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      isEnter: false,
    }
  }
  mouseEnter = (e) => this.setState({isEnter: true})
  mouseLeave = (v) => {
    console.log('mouseLeave ：', v);
    this.setState({isEnter: false})
  }
  render() {
    const {isEnter} = this.state
    const {v, type} = this.props
    return (
      <div className='editableCeil'>
        {
          isEnter ?  type === "input" ? <Inputs data={v} onMouseLeave={this.mouseLeave}></Inputs> : <InputsNumber data={v} onMouseLeave={this.mouseLeave}></InputsNumber> : <p className="itemTxt" onMouseEnter={this.mouseEnter}>{v}</p>
        }
      </div>
    );
  }
}
export default EditableCeil;