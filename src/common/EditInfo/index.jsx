import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { Icon,  } from 'antd';

class EditInfo extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    };
  }

  render() {

    return (
      <div className="modalContent">
        <Icon type="question-circle" />
        <div className='editTxt' >是否確認修改數據！</div>
      </div>
    );
  }
}

export default EditInfo;