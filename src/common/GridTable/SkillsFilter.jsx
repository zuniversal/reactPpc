import React from 'react';
import RefData from './RefData';

// 技能过滤器
// the skills filter component. this can be laid out much better in a 'React'
// way. there are design patterns you can apply to layout out your React classes.
// however, i'm not worried, as the intention here is to show you ag-Grid
// working with React, and that's all. i'm not looking for any awards for my
// React design skills.
export default class SkillsFilter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            android: false,
            css: false,
            html5: false,
            mac: false,
            windows: false
        };
    }

    // 可供外界调用的方法
    getModel() {
        console.log('技能过滤器getModel ：', )
        return {
            android: this.state.android,
            css: this.state.css,
            html5: this.state.html5,
            mac: this.state.mac,
            windows: this.state.windows
        }
    }

    // 可供外界调用的方法
    setModel(model) {
        console.log('技能过滤器setModel ：', model)
        this.setState({
            android: model ? model.android : null,
            css: model ? model.css : null,
            html5: model ? model.html5 : null,
            mac: model ? model.mac : null,
            windows: model ? model.windows : null
        });
    }

    // 被自动调用的过滤方法
    // called by agGrid
    doesFilterPass(params) {
        console.log('技能过滤器doesFilterPass ：', params)
        const rowSkills = params.data.skills;
        let passed = true;

        RefData.IT_SKILLS.forEach((skill) => {
            if (this.state[skill]) {
                if (!rowSkills[skill]) {
                    passed = false;
                }
            }
        });

        return passed;
    };

    // 可供外界调用的方法
    getModel() {
        console.log('技能过滤器getModel11111111111111 ：', )
        return ''
    }

    // 可供外界调用的方法
    // called by agGrid
    isFilterActive() {
        console.log('技能过滤器isFilterActive ：', this.state)
        const somethingSelected = this.state.android || this.state.css ||
            this.state.html5 || this.state.mac || this.state.windows;
        return somethingSelected;
    };

    // 设置当前选择器过滤器的值
    onSkillChanged(skill, event) {
        console.log('技能过滤器onSkillChanged skill, event ：', skill, event.target.checked)
        const newValue = event.target.checked;
        const newModel = {};
        newModel[skill] = newValue;
        // set the state, and once it is done, then call filterChangedCallback
        this.setState(newModel, this.props.filterChangedCallback);
    }

    // 可供外界调用的方法
    helloFromSkillsFilter() {
        console.log('技能过滤器helloFromSkillsFilter ：', 'Hello From The Skills Filter!')
        // alert("Hello From The Skills Filter!");
    }

    render() {

        const skillsTemplates = [];
        RefData.IT_SKILLS.forEach((skill, index) => {

            const skillName = RefData.IT_SKILLS_NAMES[index];
            const template = (
                <label key={skill}
                       style={{border: '1px solid lightgrey', margin: 4, padding: 4, display: 'inline-block'}}>
                    <span>
                        <div style={{textAlign: 'center'}}>{skillName}</div>
                        <div>
                            <input type="checkbox" onClick={this.onSkillChanged.bind(this, skill)}/>
                            <img src={'images/skills/' + skill + '.png'} width={30}/>
                        </div>
                    </span>
                </label>
            );

            skillsTemplates.push(template);
        });
        console.log('SkillsFilter 组件 this.state, this.props ：', this.state, this.props, )
        return (
            <div style={{width: 380}}>
                <div style={{
                    textAlign: 'center',
                    background: 'lightgray',
                    width: '100%',
                    display: 'block',
                    borderBottom: '1px solid grey'
                }}>
                    {/* <b>Custom Skills Filter</b> */}
                    <b>自定义技能过滤器</b>
                </div>
                {skillsTemplates}
            </div>
        );
    }

    // these are other method that agGrid calls that we
    // could of implemented, but they are optional and
    // we have no use for them in this particular filter.
    //afterGuiAttached(params) {}
    //onNewRowsLoaded() {}
    //onAnyFilterChanged() {}
}
