import React from 'react';
import * as PropTypes from 'prop-types';
import { Icon, } from 'antd'


// 自定义表头组件 Employee列的表头 当点击了显示隐藏列的时候会触发
// Header component to be used as default for all the columns.
export default class HeaderGroupComponent extends React.Component {

    constructor(props) {
        super(props);
        this.props.columnGroup.getOriginalColumnGroup().addEventListener('expandedChanged', this.onExpandChanged.bind(this));
        this.state = {
            expanded: null
        };
    }

    // 
    componentDidMount() {
        this.onExpandChanged();
    }

    render() {
        // 设置点击展开隐藏列的箭头的class
        let arrowClassName = "customExpandButton " + (this.state.expanded ? " expanded" : " collapsed");
        console.log('自定义表头组件HeaderGroupComponent 组件 this.state, this.props ：', this.state, this.props, )
        return <div>
                <div className="customHeaderLabel">自头 {this.props.displayName}</div>
                <div onClick={this.expandOrCollapse.bind(this)} className={arrowClassName}>
                {/* <i className="fa fa-arrow-right"/> */}
                <Icon type="arrow-right" />
                </div>
            </div>
    }
    // 
    expandOrCollapse() {
        console.log('expandOrCollapse ：', this.state)
        this.props.setExpanded(!this.state.expanded);
    };

    // 点击隐藏列的函数
    onExpandChanged() {
        console.log('onExpandChanged ：', this.state, this.props.columnGroup.getOriginalColumnGroup().isExpanded())
        this.setState({
            expanded: this.props.columnGroup.getOriginalColumnGroup().isExpanded()
        })
    }
}

// the grid will always pass in one props called 'params',
// which is the grid passing you the params for the cellRenderer.
// this piece is optional. the grid will always pass the 'params'
// props, so little need for adding this validation meta-data.
HeaderGroupComponent.propTypes = {
    params: PropTypes.object
};