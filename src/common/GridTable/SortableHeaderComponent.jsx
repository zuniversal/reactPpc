import React from "react";
import * as PropTypes from "prop-types";
import { Icon, } from 'antd'

// 公共可排序头部组件
// Header component to be used as default for all the columns.
export default class SortableHeaderComponent extends React.Component {

    constructor(props) {
        super(props);

        // this.sortChanged = this.onSortChanged.bind(this);
        this.props.column.addEventListener('sortChanged', this.onSortChanged);

        //The state of this component contains the current sort state of this column
        //The possible values are: 'asc', 'desc' and ''
        this.state = {
            sorted: ''
        }
    }

    componentWillUnmount() {
        this.props.column.removeEventListener('sortChanged', this.onSortChanged);
    }

    render() {
        let sortElements = [];
        if (this.props.enableSorting) {
            let downArrowClass = "customSortDownLabel " + (this.state.sorted === 'desc' ? " active" : "");
            let upArrowClass = "customSortUpLabel " + (this.state.sorted === 'asc' ? " active" : "");
            let removeArrowClass = "customSortRemoveLabel " + (this.state.sorted === '' ? " active" : "");

            sortElements.push(<div key={`up${this.props.displayName}`} className={downArrowClass}
                                   onClick={this.onSortRequested.bind(this, 'desc')}><Icon type="arrow-down" /></div>);
            sortElements.push(<div key={`down${this.props.displayName}`} className={upArrowClass}
                                   onClick={this.onSortRequested.bind(this, 'asc')}><Icon type="arrow-up" /></div>);
            sortElements.push(<div key={`minus${this.props.displayName}`} className={removeArrowClass}
                                   onClick={this.onSortRequested.bind(this, '')}><Icon type="close" /></div>)
        }


        let menuButton = null;
        if (this.props.enableMenu) {
            menuButton =
                <div ref="menuButton" className="customHeaderMenuButton" onClick={this.onMenuClick.bind(this)}><i
                    className={"fa " + this.props.menuIcon}/></div>
        }
        // console.log('SortableHeaderComponent 组件 this.state, this.props ：', this.state, this.props, )
        return <div>
            {/* 菜单按钮 */}
            {menuButton}
            {/* 显示列名 */}
            <div className="customHeaderLabel">{this.props.displayName}</div>
            {/* 排序组件 */}
            {sortElements}
        </div>
    }

    // 开始排序请求
    onSortRequested(order, event) {
        console.log('开始排序onSortRequested ：', order, event)
        this.props.setSort(order, event.shiftKey);
    };

    // 监听排序
    onSortChanged = () => {
        console.log('监听排序onSortChanged ：', this.props.displayName, this.props.column.isSortAscending(), this.props.column.isSortDescending(), )
        if (this.props.column.isSortAscending()) {
            this.setState({
                sorted: 'asc'
            })
        } else if (this.props.column.isSortDescending()) {
            this.setState({
                sorted: 'desc'
            })
        } else {
            this.setState({
                sorted: ''
            })
        }
    };

    // 点击展开菜单栏
    onMenuClick() {
        console.log('点击菜单栏onMenuClick ：', )
        this.props.showColumnMenu(this.refs.menuButton);
    };

}

// the grid will always pass in one props called 'params',
// which is the grid passing you the params for the cellRenderer.
// this piece is optional. the grid will always pass the 'params'
// props, so little need for adding this validation meta-data.
SortableHeaderComponent.propTypes = {
    params: PropTypes.object
};