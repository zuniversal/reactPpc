import React from 'react';

const PROFICIENCY_NAMES = ['No Filter', 'Above 40%', 'Above 60%', 'Above 80%'];

// 技能熟练度bar过滤器
// the proficiency filter component. this demonstrates how to integrate
// a React filter component with ag-Grid.
export default class ProficiencyFilter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: PROFICIENCY_NAMES[0]
        };
    }

    // 自定义的过滤方法给组件实例调用
    // called by agGrid
    doesFilterPass(params) {
        const value = this.props.valueGetter(params);
        const valueAsNumber = parseFloat(value);
        console.log('技能熟练度bar过滤器params ：', params, value, valueAsNumber, )
        switch (this.state.selected) {
            case PROFICIENCY_NAMES[1] :
                return valueAsNumber >= 40;
            case PROFICIENCY_NAMES[2] :
                return valueAsNumber >= 60;
            case PROFICIENCY_NAMES[3] :
                return valueAsNumber >= 80;
            default :
                return true;
        }
    };

    // 自定义的过滤方法给组件实例调用
    // called by agGrid
    isFilterActive() {
        console.log('技能熟练度bar过滤器isFilterActive ：', this.state, PROFICIENCY_NAMES[0])
        return this.state.selected !== PROFICIENCY_NAMES[0];
    };

    // 设置选择当前的单选框值
    onButtonPressed(name) {
        const newState = {selected: name};
        console.log('技能熟练度bar过滤器newState ：', name)
        // set the state, and once it is done, then call filterChangedCallback
        this.setState(newState, this.props.filterChangedCallback);
    }

    // 可供外界调用的方法
    getModel() {
        console.log('技能熟练度bar过滤器getModelgetModel ：', )
        return ''
    }

    // 可供外界调用的方法
    setModel(model) {
        console.log('技能熟练度bar过滤器setModel ：', model)
    }

    render() {
        const rows = [];
        console.log('ProficiencyFilter 组件 this.state, this.props ：', this.state, this.props, )
        PROFICIENCY_NAMES.forEach((name) => {
            const selected = this.state.selected === name;
            rows.push(
                <div key={name}>
                    <label style={{paddingLeft: 4}}>
                        <input type="radio" checked={selected} name={Math.random()}
                               onChange={this.onButtonPressed.bind(this, name)}/>
                        {name}
                    </label>
                </div>
            );
        });

        return (
            <div>
                <div style={{
                    textAlign: 'center',
                    background: 'lightgray',
                    width: '100%',
                    display: 'block',
                    borderBottom: '1px solid grey'
                }}>
                    {/* <b>Custom Proficiency Filter</b> */}
                    <b>技能熟练度bar过滤器</b>
                </div>
                {rows}
            </div>
        );
    }

    // these are other method that agGrid calls that we
    // could of implemented, but they are optional and
    // we have no use for them in this particular filter.
    //getApi() {}
    //afterGuiAttached(params) {}
    //onNewRowsLoaded() {}
    //onAnyFilterChanged() {}
}
