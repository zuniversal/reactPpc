import React, {Component} from "react";
import {AgGridColumn, AgGridReact} from "ag-grid-react";
import RowDataFactory from "./RowDataFactory";
import DateComponent from "./DateComponent.jsx";
import SkillsCellRenderer from './SkillsCellRenderer.jsx';
import NameCellEditor from './NameCellEditor.jsx';
import ProficiencyCellRenderer from './ProficiencyCellRenderer.jsx';
import RefData from './RefData';
import SkillsFilter from './SkillsFilter.jsx';
import ProficiencyFilter from './ProficiencyFilter.jsx';
import HeaderGroupComponent from './HeaderGroupComponent.jsx';
import SortableHeaderComponent from './SortableHeaderComponent.jsx';
import {valCompare, isAfter, } from 'util'
import moment from 'moment'
import { defaultNow, dangerDateConfig,  } from 'config'
import { Icon, Tooltip, } from 'antd'

// import "./RichGridDeclarativeExample.css";

// take this line out if you do not want to use ag-Grid-Enterprise

// 注意 如果没有 引入 方法 会导致无法导出 excel 文件  exportDataExcel
import "ag-grid-enterprise";
import "ag-grid/dist/styles/ag-grid.css";
import "ag-grid/dist/styles/theme-fresh.css";
import "ag-grid/dist/styles/ag-theme-balham.css";

import './index.less'

export default class RichGridDeclarativeExample extends Component {

    constructor(props) {
        super(props);
        console.log('props构造函数 ：', props)
        this.state = {
            quickFilterText: null,
            showToolPanel: false,
            // rowData: new RowDataFactory().createRowData(),
            rowData: props.data,
            icons: {
                columnRemoveFromGroup: '<i class="fa fa-remove"/>',
                // 过滤的图标
                // filter: '<Icon type="filter" />',
                filter: '<i class="anticon anticon-filter"></i>',
                sortAscending: <Icon type="arrow-down" />,
                sortDescending: <Icon type="arrow-up" />,
                groupExpanded: '<i class="fa fa-minus-square-o"/>',
                groupContracted: '<i class="fa fa-plus-square-o"/>'
            }
        };
    }

    // 当表格组件加载完毕自动调用
    /* Grid Events we're listening to */
    onGridReady = (params) => {
        console.log('onGridReady ：', params)
        this.api = params.api;
        this.columnApi = params.columnApi;
    };

    // 点击单元格触发
    onCellClicked = (event) => {
        console.log('onCellClicked: ', event, event.data.name + ', col ' + event.colIndex);
    };

    // 当有行被选中是触发
    onRowSelected = (event) => {
        console.log('onRowSelected: ', event, event.node.data.name);
    };

    /* Demo related methods */
    onToggleToolPanel = (event) => {
        console.log('onToggleToolPanel ：', event, event.target.checked)
        this.setState({showToolPanel: event.target.checked});
    };

    // 调用表格组件实例的方法触发行取消全选功能
    deselectAll() {
        console.log('deselectAll ：', this.api)
        // this.api.deselectAll();
        this.api.exportDataAsExcel();
    }

    onQuickFilterText = (event) => {
        console.log('invokeSkillsFilterMethod ：', event, event.target.checked)
        this.setState({quickFilterText: event.target.value});
    };

    // 刷新数据
    onRefreshData = () => {
        console.log('onRefreshData ：', new RowDataFactory().createRowData())
        this.setState({
            rowData: new RowDataFactory().createRowData()
        });
    };

    // 得到表格组件实例的方法 拿到要使用的列实例 调用该实例的方法
    invokeSkillsFilterMethod = () => {
        let skillsFilter = this.api.getFilterInstance('skills');
        let componentInstance = skillsFilter.getFrameworkComponentInstance();
        componentInstance.helloFromSkillsFilter();
        console.log('invokeSkillsFilterMethod ：', skillsFilter, componentInstance)
    };

    dobFilter = () => {
        let dateFilterComponent = this.api.getFilterInstance('dob');
        dateFilterComponent.setFilterType('equals');
        dateFilterComponent.setDateFrom('2000-01-01');
        // console.log('dateFilterComponent ：', dateFilterComponent)
        // as the date filter is a React component, and its using setState internally, we need
        // to allow time for the state to be set (as setState is an async operation)
        // simply wait for the next tick
        setTimeout(() => {
            this.api.onFilterChanged();
        }, 0)
    };

    // 生成国家旗帜列渲染的静态方法
    // 直接调用方法 没有传入参数 参数应该是实例
    static countryCellRenderer(params) {
        // console.log('生成国家旗帜的静态方法countryCellRenderer params ：', params)
        if (params.value) {
            return `<img border='0' width='15' height='10' style='margin-bottom: 2px' src='http://flags.fmcdn.net/data/flags/mini/${RefData.COUNTRY_CODES[params.value]}.png'> ${params.value}`;
        } else {
            return null;
        }
    }

    // 如果某个列一开始是养藏的只有该组件显示了才会调用该方法
    // 生成日期组件列渲染的静态方法
    static dateCellRenderer(params) {
        // console.log('日期排序静态方法dateCellRenderer params ：', params, params.value)
        return RichGridDeclarativeExample.pad(params.value.getDate(), 2) + '/' +
            RichGridDeclarativeExample.pad(params.value.getMonth() + 1, 2) + '/' +
            params.value.getFullYear();
    }
    static dateChange (data, colDef,  ) {
        // console.log('    dateChange ： ', data, colDef,    )
        return data.qty_status === 'T' && colDef.field === 'qty' || data.prod_status === 'T' && colDef.field === 'prod_date' ? 'warning' : '' 
    }
    static cellRenderer(params) {
        // const {isCapcity, } = this.props 
        const {colDef, value, data, } = params
        const {isCapcity, lastwork_e_date, isDateChange,  } = data
        const {isDanger, isLock,  } = colDef
        // console.log('静态方法 cellRenderer params ：', params, data, colDef, colDef.idx, isDanger, value )
        if (isDanger) {
            // console.log(' data[isDanger] ： ', data[isDanger], moment(data[isDanger]).isAfter(defaultNow) )// 
            return `<div class="${ isDanger && !moment(data[isDanger]).isAfter(defaultNow) && value !== '' ? 'danger' : ''}">${value}</div>`
        }
        if (isCapcity) {
            return `<div class="${valCompare(data, `done_capacity${colDef.idx}`, `basic_capacity${colDef.idx}`) ? 'danger' : ''}">${value}</div>`
        }
        if (isDateChange) {
            console.log(' isDateChangeisDateChange ： ', colDef, data, data.qty_status   )// 
            return `<div class="${RichGridDeclarativeExample.dateChange(data, colDef,)}">${value}</div>`
        }
        if (isLock) {
            return `<div class="${ isLock && !moment(data[isLock]).isAfter(defaultNow) && value !== 'T' ? 'danger' : ''}">${value}</div>`
        }
        // return value
        return `<div title="${value}">
            ${value}
        </div>`
        // return `<div class="tooltip">
        //     ${value}
        //     <span class="tooltiptext">${value}</span>
        // </div>`
        // return <div class="tooltip">鼠标移动到这
        //   <span class="tooltiptext">提示文本</span>
        // </div>
        // return `<Tooltip placement="top" title="${value}">
        //     ${value}
        // </Tooltip>`
        // return `<Popover title="${value}">
        //     ${value}
        // </Popover>`
    }


    // 将数字改成两位的字符串数字
    static pad(num, totalStringSize) {
        let asString = num + "";
        while (asString.length < totalStringSize) asString = "0" + asString;
        // console.log('num, totalStringSize ：', num, totalStringSize, asString)
        return asString;
    }
    

    render() {
        console.log('RichGridDeclarativeExample 组件 this.state, this.props ：', this.state, this.props, )
        const {config, fixColmun, defaultExportParams, } = this.props
        return (
            <div style={{marginTop: 10}}>
                <div style={{height: 600, width: '100%'}} id="myGrid" className="ag-fresh agGridTable ag-theme-balham">
                    <AgGridReact
                        // 列
                        // sideBar = {true}
                        ag-row-hover 
                        ag-column-hover 
                        // // equivalent detailed long form
                        // sideBar = {{
                        //     toolPanels: [
                        //         {
                        //             id: 'columns',
                        //             labelDefault: 'Columnssss',
                        //             labelKey: 'columns',
                        //             iconKey: 'columns',
                        //             toolPanel: 'agColumnsToolPanel',
                        //         },
                        //         {
                        //             id: 'filters',
                        //             labelDefault: 'Filters',
                        //             labelKey: 'filters',
                        //             iconKey: 'filter',
                        //             toolPanel: 'agFiltersToolPanel',
                        //         }
                        //     ],
                        //     defaultToolPanel: 'columns',
                        //     // defaultToolPanel: 'null',
                        // }}


                        // columnDefs= {[
                        //     {
                        //       field: "pn_no",
                        //       rowDrag: true
                        //     },
                        //     // { field: "country" },
                        //     // { field: "year" },
                        //     // { field: "date" },
                        //     // { field: "sport" },
                        //     // { field: "gold" },
                        //     // { field: "silver" },
                        //     // { field: "bronze" }
                        // ]}
  
                        // 列定义数组
                        // columnDefs

                        // 一个默认列 的定义
                        // defaultColGroupDef

                        // 一个默认列 的定义。
                        // defaultColDef

                        // 禁止列的自动调整大小列。换句话说，双击列标题边缘将不会自动调整大小。
                        suppressAutoSize={true}

                        // 在 自动调整大小计算后，需要将多少个像素添加到列宽中。默认值是4个像素。如果你想添加额外的空间来容纳（例如）排序图标或标题的其他动态性质
                        //autoSizePadding

                        // 如果为true，ag-column-moving则列在移动时不会添加到网格中。在默认主题中，这会导致移动列没有动画。
                        //suppressColumnMoveAnimation

                        // 	设置为true以禁止列移动。换句话说，设置为true以使列固定位置。
                        // suppressMovableColumns


                        // 如果为true，则address.firstline字段名称中的点（例如）不被视为深度参考。如果您愿意，可以在字段名称中使用点。
                        // suppressFieldDotNotation

                        // 设置为true以显示“无排序”图标
                        // unSortIcon

                        // 	设置为true可以在用户按住Shift键单击列标题时禁止多重排序。
                        // suppressMultiSort

                        // 设置为true时始终显示列菜单按钮，而不是仅当鼠标悬停在列标题上时才显示。
                        // suppressMenuHide

                        // 	如果您对默认值不满意，允许指定组的“自动列”。如果分组，则将此列def作为网格中的第一列定义。如果不分组，则不包括此列。
                        // autoGroupColumnDef

                        // 分类和过滤器

                        // 排序和过滤

                        // 	使用客户端行模型启用 行排序时设置为true 。单击列标题将导致网格对数据进行排序。
                        enableSorting

                        // 使用无限， 服务器端或 视口行模型启用 行排序时设置为true 。单击列标题将导致数据源再次通过新排序顺序询问数据。
                        // enableServerSideSorting

                        // 使用客户端行模型启用 行过滤时设置为true 。
                        enableFilter

                        // 设置为true时，使用无限， 服务器端或 视口行模型启用 行过滤。过滤器的更改将导致数据源再次通过新过滤器询问数据。
                        // enableServerSideFilter

                        // 
                        // quickFilterText

                        // listening for events监听事件
                        onGridReady={this.onGridReady}
                        // 
                        onRowSelected={this.onRowSelected}
                        // 
                        onCellClicked={this.onCellClicked}

                        // binding to simple properties绑定简单的属性
                        showToolPanel={this.state.showToolPanel}
                        // 
                        quickFilterText={this.state.quickFilterText}

                        // binding to an object property绑定图标数据
                        icons={this.state.icons}

                        // binding to array properties绑定数据
                        rowData={this.state.rowData}

                        // 
                        // no binding, just providing hard coded strings for the properties
                        // boolean properties will default to true if provided (ie enableColResize => enableColResize="true")
                        suppressRowClickSelection
                        defaultExportParams={defaultExportParams}
                        // 
                        rowSelection="multiple"
                        // 设置为true以允许通过在列标题边缘拖动鼠标来调整列的大小。
                        enableColResize
                        // enableColResize={false}
                        // 
                        
                        // 
                        floatingFilter
                        // 
                        groupHeaders
                        // 
                        rowHeight="32"

                        // setting grid wide date component设置网格的日期组件
                        dateComponentFramework={DateComponent}

                        // setting default column properties设置默认的列属性
                        defaultColDef={{
                            headerComponentFramework: SortableHeaderComponent,
                            headerComponentParams: {
                                // 传递菜单按钮图标
                                menuIcon: 'fa-bars'
                            }
                        }}

                        toolPanelSuppressValues={`工具栏`}
                    >
                        <AgGridColumn headerName="選擇" width={50} checkboxSelection suppressSorting suppressMenu suppressFilter pinned></AgGridColumn>
                        {/* 固定在左边的列 */}
                        {/* <AgGridColumn headerName="Employee" headerGroupComponentFramework={HeaderGroupComponent}>
                            <AgGridColumn field="pn_no" width={120} filter="text" enableRowGroup enablePivot pinned editable cellEditorFramework={NameCellEditor}></AgGridColumn>
                        </AgGridColumn>
                        <AgGridColumn headerName="Employee" headerGroupComponentFramework={HeaderGroupComponent}>
                            
                            <AgGridColumn field="pn_no" width={120} filter="text" pinned  cellEditorFramework={NameCellEditor}></AgGridColumn>
                        </AgGridColumn> */}
                        {
                            config.map(v => {
                                console.log('v.wrapperConfi ：', v, v.wrapperConfig, {...v.wrapperConfig})
                                return <AgGridColumn pinned={v.field === 'pn_no1' ? true : false}  {...v.wrapperConfig} key={v.wrapperConfig.headerName}>
                                {v.column.map(item => <AgGridColumn cellRenderer={RichGridDeclarativeExample.cellRenderer} key={item.field} {...item} filter="text"></AgGridColumn>) }
                            </AgGridColumn>
                            })
                        }
                    </AgGridReact>
                </div>
            </div>
        );
    }
}
