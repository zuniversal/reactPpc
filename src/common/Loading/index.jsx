import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'

class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    // console.log('loadingloading componentWillReceiveProps ：', nextProps, this.props, nextProps.loading, this.props.loading, )
    
  }
  render() {
    // const {userInfo} = this.props
    // const {loading} = userInfo
    const {loading} = this.props
    // console.log('^^^^^^^^^^^^^^^^^^^^^^loading ：', this.props, loading, loading ? '' : 'hide');
    return (
      <section className={`loading-overlay ${loading ? '' : 'hide'}`}>
        <div className="sk-three-bounce">
          <div className="sk-child sk-bounce1"></div>
          <div className="sk-child sk-bounce2"></div>
          <div className="sk-child sk-bounce3"></div>
        </div>
      </section>
    );
  }
}
export default Loading;