import React from 'react';
import CountUp, { startAnimation } from 'react-countup';
import { Tag, } from 'antd';
import './style.less'

class Count extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      start: 0,
      end: 547875.0319
    }
  }
  onComplete = () => {
    // console.log('Completed! 👏');
    // console.log('this.myCountUp：', this.myCountUp);
  };
  onStart = () => {
    // console.log('Started! 💨');
  };
  render() {
    const { end, className, duration, noPrefix, tag, color,  } = this.props
    // console.log('CountCountCount ：', this.props);
    const content = <CountUp
      className={`${className != undefined ? className : null}`}
      start={0}
      end={end}
      duration={duration != undefined ? duration : 4}
      useEasing={true}
      useGrouping={true}
      separator=","
      decimals={0}
      prefix={''}
      onComplete={this.onComplete}
      onStart={this.onStart}
      ref={(countUp) => {
        this.myCountUp = countUp;
      }}
    />
    return (
      tag != undefined ? <Tag color={color}>{content}</Tag> : content
    )
  }
}
export default Count;