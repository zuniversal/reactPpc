import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"

import './style.less'

import { Switch, Icon } from 'antd';



class SettingTheme extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      paneActive: false,
      test: false,
    }
  }
  themeChange(v, i) {
    console.log('变换主题', v, i, this.state.themes, this.state.themes[i].checked)
    // const themes = this.state.themes
    // themes[i].checked = !themes[i].checked
    //     console.log('变换主题itemitem222', this.state.themes)
    // v.checked = !v.checked
    this.setState({
      themes: this.state.themes.map((item, index) => {
        if (item.bg === v.bg) {
          item.checked = !item.checked
          localStorage.removeItem('theme')
          if (item.checked === true) {
            localStorage.setItem('theme', v.bg)
          }
        } else {
          item.checked = false
        }
        return item
      }),
      theme: (v.checked && v.bg) || ''
    }, () => {
      localStorage.removeItem('themes')
      localStorage.setItem('themes', JSON.stringify(this.state.themes))
      console.log('变换主题ssssss', JSON.parse(localStorage.getItem('themes')))
    })
    console.log('变换', this.state.themes)
    // setTimeout( _ =>{

    // console.log('变换主题11', this.state.themes, this.state.theme, localStorage.getItem('theme'))
    // }, 1000);
  }
  componentDidMount() {
  }
  activePane = () => {
    this.setState({
      paneActive: !this.state.paneActive
    }, () => {

    })
    console.log('变换主题themeArr', this.state.paneActive)
  }

  render() {


    return (
      <div className={`paneWrapper ${this.state.paneActive ? 'active' : ''}`}>
        <div className="settingPane" onClick={this.activePane}>
          <a href="javascript:;" onClick={this.changeTheme}><Icon type="setting"></Icon></a>
        </div>

        <div className="settingContent">
        </div>
      </div>
    );
  }
}

export default SettingTheme;