import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { DATE_FORMAT, DATE_FORMAT_BAR,  } from 'config'
import { DatePicker,  } from 'antd';
import moment from 'moment';
const { RangePicker } = DatePicker;

const range = (start, end) => {
  const result = [];
  for (let i = start; i < end; i++) {
    result.push(i);
  }
  return result;
}
const disabledDateTime = () => {
  // console.log('disabledDateTime ：', range(0, 24).splice(4, 20), range(30, 60),)
  return {
    disabledHours: () => range(0, 24).splice(4, 20),
    disabledMinutes: () => range(30, 60),
    disabledSeconds: () => [55, 56],
  };
}

class LimitDate extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  render() {
    // console.log(' %c LimitDate 组件 this.state, this.props ： ', `color: #333; font-weight: bold`, this.state, this.props,  )
    const {
      k1, 
      k2, 
      data, 
      disableDate, 
      start, 
      end,
      disableDay, 
    } = this.props 
    
    const disabledDate = (current) => {
      const disableDate = Date.parse(new Date(moment(disableDay).format(DATE_FORMAT_BAR)))// 
      // console.log('current && current.valueOf() < init[key] ：', disableDay, current && current.valueOf() < disableDay,  )
      return current && current.valueOf() > disableDate;
    }
    return (
      <RangePicker
        value={[moment(data[k1], DATE_FORMAT), moment(data[k2], DATE_FORMAT)]} 
        ref={date => this.date = date}
        //可以传递数据 但是传递props属性需要自己放 
        format={DATE_FORMAT} onChange={v => this.props.onChange(v)}
        disabledDate={disabledDate} 
        disabledTime={disabledDateTime} 
        allowClear={false}
        // showTime={{ hideDisabledOptions: true, defaultValue: [moment('00:00:00', 'HH:mm:ss'), moment('11:59:59', 'HH:mm:ss')], }}/>
      />
    )
  }
}
export default LimitDate