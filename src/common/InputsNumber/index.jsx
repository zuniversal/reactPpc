import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {dateForm, ajax} from 'util'
import { Form, Input, InputNumber } from 'antd';
const FormItem = Form.Item;

class InputsNumber extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      value: '',
    }
  }
  rateClick = (e) => {
    e.stopPropagation()
    e.preventDefault()
  }
  valueChange = (v) => {
    console.log('InputsNumber valueChange  v ：', v, this.props);
    const {val, i, keys, max, } = this.props
    const {barcode} = val
    // this.props.inputing(v, barcode, keys)
    // ajax(this.props.inputing, {v, barcode, keys})
    // ajax(this.props.inputing, {v, barcode, ...this.props})
    // ajax(this.props.inputing, {v: max != undefined ? v > max ? max : v : v, barcode, ...this.props})
    this.props.inputing({v: max != undefined ? v > max ? max : v : v, barcode, ...this.props})
  }
  componentWillReceiveProps(nextProps) {
    const {show, keys, val} = nextProps
    // console.log(' InputsNumber componentWillReceiveProps ：', nextProps, this.props, show )
    if (val[keys] !== this.props.val[keys]) {
      console.log(' 不等 ： ',  )
      this.props.form.setFieldsValue({
        [keys]: val[keys]
      })
    }
  }
  render() {
    const {disabled, data, keys, val, max, className,  } = this.props
    const { getFieldDecorator } = this.props.form;// // 
    const otherConfig = {
      // ...{max: max != undefined ? max : Infinity},
    } // 
    // console.log('数字输入框 ：', this.props, val[keys], otherConfig,);
    return (<Form className={`${className} inputs`}>
      <FormItem>
        {getFieldDecorator(keys, {
          rules: [{
            required: true,
            type: 'number',
            message: 'Require Number',
          }], initialValue: val[keys]
          //}], initialValue: 555
        })(
          <InputNumber min={0} max={max != undefined ? max : Infinity}
          {...otherConfig} // 
          onClick={this.rateClick} onChange={this.valueChange} disabled={disabled} />
          )}
      </FormItem>
    </Form>)
  }
}
const WrappedDemo = Form.create()(InputsNumber);
export default WrappedDemo;