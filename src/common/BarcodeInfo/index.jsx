import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {bacodeInfo, bacodeCopyInfo, DATE_FORMAT_BAR,  } from 'config'
import {backupFn, getItems, } from 'util'
import ClearInfo from '@@@/ClearInfo'
import ClipBoard from '@@@@/ClipBoard'
import SecureHoc from '@@@@/SecureHoc'
import { Row, Col, Button, Checkbox } from 'antd';
import moment from 'moment';

class BarcodeInfo extends React.Component {
  constructor(props) {
    super(props)
    // this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      lock: 'F',
      canSplit: 'F',
    };
  }
  split = () => {// 
    console.log('BarcodeInfo  split e ：', this.props, );
    const {data} = this.props
    // this.props.splitAction(backupFn(data))
    this.props.splitAction(backupFn({...data, new_s_date: data.s_date, new_e_date: data.e_date}))
  }
  lock = (e) => {
    // console.log(`checked = ${e.target.checked}`);
    const {data} = this.props
    const {barcode} = data
    const {lock, } = this.state
    const isLock = lock !== 'T' ? 'T' : 'F'
    // console.log('this.state.lock ：', this.state.lock, this.state.lock !== 'T')
    this.props.lock(barcode, isLock)
    this.setState({
      lock: isLock,
    })
  }
  delete = () => {
    console.log('BarcodeInfo  delete ：', this.props, )
    // const {data} = this.props
    // const {barcode} = data
    this.props.delete()
  }
  componentDidMount() {
    const {  data } = this.props
    const {lock, finish_status, } = data
    console.log('bacodeInfos  componentDidMount组件挂载 ：', this.props, lock, finish_status, lock === 'T', finish_status === 'T', )
    this.setState({
      lock, 
      canSplit: lock === 'T' || finish_status === 'T', 
    })// 
    // this.split()
  }
  componentWillReceiveProps(nextProps) {
    const {  data } = nextProps
    const {lock, finish_status, } = data
    // console.log(`%c bacodeInfos componentWillReceiveProps 改变 ：`, `color: #03A9F4`, nextProps, this.props, this.state, lock, finish_status, lock === 'T', finish_status === 'T', ) 
    this.setState({
      lock, 
      canSplit: lock === 'T' || finish_status === 'T', 
    })// 
  }
  

  render() {
    const { data, dialogType, isShowBarcode, finishAction, getAccPnInfo, auth, hideAction, 
      bacodeInfoConfig,
    } = this.props
    const {lock, canSplit, } = this.state
    const {barcode, finish_date, finish_status, } = data
    const isLock = lock !== 'F'
    console.log('BarcodeInfo 组件 this.state, this.props ：', getItems('company_id'), this.state, this.props, isLock, canSplit, )
    // console.log('isLock 重排 ：', isLock, lock)
    const bacodeInfos = bacodeInfoConfig.map((item, i) => {
      // console.log('bacodeInfoKey item ：', item, item.label, item.k, Array.isArray(item.k) ? item.k.map((v) => data[v]).join(' → ') : data[item.k]);
      return (
        <Col span={8} key={i} className="infoItem animated zoomIn">
          <Col span={8} className="infoLabel ellipsis">{item.label}</Col>
          <Col span={16} className="infoValue ellipsis">&nbsp;{  
            bacodeCopyInfo.some(v => v === item.k) ? <ClipBoard className='ellipsis'  type={'btn'}>{data[item.k]}</ClipBoard> : 
            item.k === 'sew_gauge' ? data[item.k] === 'F' ? '幼针' : '粗针' : ['yarn_date', 'acc_date'].some(v => v === item.k) && data[item.k] != undefined ? moment(data[item.k]).format(DATE_FORMAT_BAR) : Array.isArray(item.k) ? item.k.map((v) => data[v]).join(' → ') : data[item.k]
          }</Col>
        </Col>
      )
    })

    return (
      <Row gutter={10}>
        {bacodeInfos}
        {
          isShowBarcode && !hideAction ? <Col span={8} className="infoItem animated zoomIn">
              {
                auth ? <div className='btnWrapper' >
                  <Button onClick={this.split} disabled={canSplit} type="primary" className={`m-r10 ${canSplit ? 'disabled' : ''}`}>拆分</Button>
                  <Button onClick={this.delete} disabled={canSplit} type="primary" className={`warn m-r10 ${canSplit ? 'disabled' : ''}`} >删除</Button>
                </div> : null
              }
              <Button onClick={getAccPnInfo} type="primary" className={'succ'}  >輔料詳情</Button>
              {
                auth ? <Checkbox checked={isLock} onChange={this.lock}>锁定</Checkbox> : null
              }
              {/* <Row><ClearInfo init={finish_status} finishAction={finishAction} ref={info => this.info = info} /></Row> */}
          </Col> : null
        }
        {
          isShowBarcode && auth && !hideAction ? <ClearInfo finish_date={finish_date} init={finish_status} finishAction={finishAction} ref={info => this.info = info} />   : null
        }
        {/* {
          isShowBarcode ? 
          <Col span={24} className={`infoItem ${''}`}>
            <ClearInfo init={finish_status} finishAction={finishAction} ref={info => this.info = info} />  
          </Col>  : null
        } */}
      </Row>
    );
  }
}

// export default BarcodeInfo;
export default SecureHoc(BarcodeInfo)

