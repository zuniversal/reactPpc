import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {bacodeInfo, bacodeCopyInfo, } from 'config'
import {backupFn, getItems, } from 'util'
import ClearInfo from '@@@/ClearInfo'
import ClipBoard from '@@@@/ClipBoard'
import { Row, Col, Button, Checkbox } from 'antd';

class BarcodeInfo extends React.Component {
  constructor(props) {
    super(props)
    // this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      lock: 'F',
    };
  }
  split = () => {
    // console.log('BarcodeInfo  split e ：', this.props, );
    const {data} = this.props
    this.props.splitAction(backupFn(data))
  }
  lock = (e) => {
    // console.log(`checked = ${e.target.checked}`);
    const {data} = this.props
    const {barcode} = data
    const {lock, } = this.state
    const isLock = lock !== 'T' ? 'T' : 'F'
    // console.log('this.state.lock ：', this.state.lock, this.state.lock !== 'T')
    this.props.lock(barcode, isLock)
    this.setState({
      lock: isLock,
    })
  }
  delete = () => {
    console.log('BarcodeInfo  delete ：', this.props, )
    // const {data} = this.props
    // const {barcode} = data
    this.props.delete()
  }
  componentDidMount() {
    // console.log('bacodeInfos  componentDidMount组件挂载 ：', this.props, )
    const {  data } = this.props
    const {lock, } = data
    this.setState({
      lock 
    })
    // this.split()
  }

  render() {
    const { data, dialogType, isShowBarcode, finishAction,  } = this.props
    const {lock, } = this.state
    const {barcode, finish_date, finish_status, } = data
    console.log('BarcodeInfo 组件 this.state, this.props ：', this.state, this.props, )
    const isLock = lock !== 'F'
    // console.log('isLock ：', isLock, lock)
    const bacodeInfos = bacodeInfo.map((item, i) => {
      // console.log('bacodeInfoKey item ：', item, item.label, item.k, );
      return (
        <Col span={8} key={i} className="infoItem animated zoomIn">
          <Col span={8} className="infoLabel ellipsis">{item.label}</Col>
          <Col span={16} className="infoValue ellipsis">&nbsp;{  
            bacodeCopyInfo.some(v => v === item.k) ? <ClipBoard type={'btn'}>{data[item.k]}</ClipBoard> : 
            item.k === 'sew_gauge' ? data[item.k] === 'F' ? '幼针' : '粗针' : data[item.k]
          }</Col>
        </Col>
      )
    })

    return (
      <Row gutter={10}>
        {bacodeInfos}
        {
          isShowBarcode ? <Col gutter={10} span={24} className="infoItem animated zoomIn">
            <Col span={8} className="infoItem animated zoomIn">
              <ClearInfo finish_date={finish_date} init={finish_status} finishAction={finishAction} ref={info => this.info = info} />
            </Col>
            <Col span={10} className="infoItem animated zoomIn">
                <Button onClick={this.split} disabled={isLock} type="primary" className={`m-r10 ${isLock ? 'disabled' : ''}`}>重排</Button>
                <Button onClick={this.delete} disabled={isLock} type="primary" className={`warn m-r10 ${isLock ? 'disabled' : ''}`} >删除</Button>
                <Checkbox checked={isLock} onChange={this.lock}>锁定</Checkbox>
                {/* <Row><ClearInfo init={finish_status} finishAction={finishAction} ref={info => this.info = info} /></Row> */}
            </Col>
          </Col> : null
        }
        {/* {
          isShowBarcode ? 
          <Col span={24} className={`infoItem ${''}`}>
            <ClearInfo init={finish_status} finishAction={finishAction} ref={info => this.info = info} />  
          </Col>  : null
        } */}
      </Row>
    );
  }
}

export default BarcodeInfo;