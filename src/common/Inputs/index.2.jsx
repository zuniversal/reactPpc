import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {ajax} from 'util'
import { ANIMATE, } from 'constants'
import { Form, Input,  } from 'antd';
const FormItem = Form.Item;

class Inputs extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      value: '',
    }
  }
  valueChange = (v) => {
    // console.log('valueChange v ：', v.target.value, this.props, this.props.form.getFieldValue('data'));
    // const { currency } = this.props
    const {val, i, keys, inputAction, } = this.props
    const {barcode} = val
    // console.log('val, i ：', keys, val, i, barcode, )
    const {value, } = v.target
    // let vals
    // if (value.length === 4 || value.length === 6 || value.length === 8) {
    //   console.log(' v 超过4 ： ', value, value.length,  )// 
    //   vals = value + '/'
    // }
    // console.log(' v超过4 ： ', value, value.length, val, vals,  )// 
    this.setState({ value: v.target.value })// 
    // ajax(this.props.inputing, {v: v.target.value, barcode, keys})
    if (inputAction === 'split') {
      ajax(this.props.inputing, {v: vals, barcode, keys}, 2000)
    } else {
      ajax(this.props.inputing, {v: vals, ...val, keys}, 2000)
    }
  }
  componentWillReceiveProps(nextProps) {
    const {show} = this.props
    // if (show) {
    //   this.props.form.resetFields()
    // }
  }
  render() {
    const {disabled, data, keys, val, className, size, noRule, isDisable, wrapCls, 
      onFocus, onBlur, 
    } = this.props
    const { getFieldDecorator } = this.props.form;
    // console.log('Inputs组件  ：', this.props, val[keys], keys);// // 
    return (
      <Form className={`${wrapCls} inputs`} >
        <FormItem>
          {getFieldDecorator(keys, 
          {
            rules: noRule != undefined ? undefined : [{
              required: true,
              type: 'string',
              message: 'Require String',
            }], initialValue: data != undefined ? data : ''
          }) (
            <Input onFocus={onFocus} onBlur={onBlur} disabled={isDisable} 
              className={`${className != undefined ? className : ''} ${ANIMATE.bounceIn}`} 
              size={size != undefined ?  size : "small"}  onChange={this.valueChange} />
            )}
        </FormItem>
      </Form>)
  }
}
const WrappedDemo = Form.create()(Inputs);
export default WrappedDemo;