import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { Form, Input,  } from 'antd';
const FormItem = Form.Item;

class Inputs extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      value: '',
    }
  }
  valueChange = (v) => {
    console.log('valueChange v ：', v.target.value, this.props, this.props.form.getFieldValue('data'));
    this.setState({
      value: v.target.value
    })
    // const { currency } = this.props
    // this.setState({ value: v })
    // this.props.rateInput(currency, v)
    // setTimeout(() => {
    //   console.log('this.props ：', this.props)
    // }, 2000)
  }
  componentDidMount() {
    console.log('componentDidMount ：',  )
    this.setState({
      value: this.props.data
    })
  }
  componentWillReceiveProps(nextProps) {
    const {show} = this.props
    console.log('Inputs componentWillReceiveProps ：', nextProps, this.props, show )
    // if (show) {
    //   this.props.form.resetFields()
    // }
  }
  render() {
    const {disabled, data} = this.props
    const {value} = this.state
    const { getFieldDecorator } = this.props.form;
    console.log('WrappedDemo ：', this.props, value);
    return (
          <Input size="small" onChange={this.valueChange} defaultValue={data} value={value} />)
  }
}
const WrappedDemo = Form.create()(Inputs);
export default WrappedDemo;