import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import {numToDate, dateSplits, ajax,   } from 'util'
import { InputNumber } from 'antd';

class YearInput extends React.Component {
  constructor(props) {
    super(props)
    // this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }
  shouldComponentUpdate(nextProps) {
    // console.log(' %c nextProps 组件  ： ', `color: #333; font-weight: bold`, this.state, this.props, nextProps, this.props === nextProps, this.props.val !== nextProps.val   )// 
    return this.props.val !== nextProps.val
  }
  onChange = (v) => {
    const {keys, } = this.props
    // console.log('YearInput onChange v ：', this.props, v, dateSplits(v.toString()), dateSplits(v.toString()).length < 9    );
    this.props.onChange({v: dateSplits(v.toString()), keys, ...this.props, })// 
  }
  render() {
    const {val, className,  } = this.props// 
    // console.log(' %c YearInput 组件 this.state, this.props ： ', `color: #333; font-weight: bold`, this.state, this.props,  )// 
    return (
      <InputNumber 
        className={`${className} w-80 `}
        // defaultValue={val}
        value={val}
        // formatter={v => numToDate(v)}
        formatter={v => {
          // const str = v.split('').filter(v => v !== '-')
          // const v1 = str.slice(0, 4).join('')
          // const v2 = str.slice(2, 5).join('')
          // const v3 = str.slice(4, 8).join('')//
          //  console.log(' value formatter 格式化22  ： ', this.props.keys, this.props, v1, v2, v3 , str, `${v1}${str.length > 4 ? '-' : ''}${v2}${str.length > 6 ? '-' : ''}${v3}`   )// 
          // return `${v1}` 
          return val
        }}
        // parser={value => value}
        parser={value => {
          // console.log(' parser value ： ', value,  )// 
          this.onChange(value)
          return value
        }}
        // onChange={this.onChange}
      />
    )
  }
}

export default YearInput