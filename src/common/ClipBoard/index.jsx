import React from 'react';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import { Button, message, Icon, } from 'antd';
import './style.less'

class ClipBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    }
  }
  static defaultProps = {
    onClick: () => {}
  }
  // onCopy = (v, r, t) => {
  //   console.log('ClipBoard onCopy ： ', v, r, t, this.props )
  //   const {text, onClick} = this.props 
  //   this.setState({
  //     value: text,
  //   })
  //   confirms(1, `${text}複制成功！ O(∩_∩)O~`)
  // }
  // onCopy = (v, r, t) => {
  //   console.log('ClipBoard onCopy ： ', v, r, t, this.props )
  //   const {text, onClick, children} = this.props 
  //   // this.setState({
  //   //   copy: children,
  //   // })
  //   // message.success(`${text}複制成功！ O(∩_∩)O~`)
  //   // onClick()
  // }
  onCopys = (v, r, t) => {
    console.log('ClipBoard onCopys22 ： ', v, r, t, this.props )
    const {text, onClick, children, noTips, } = this.props 
    noTips && message.success(`${v}  文本複制成功！ O(∩_∩)O~ `)
    // onClick()
  }
  render() {
    const { text, children, type } = this.props
    const { copy } = this.state
    // console.log(' className='mcolor' %c ClipBoard 组件 this.state, copy, this.props ： ', `color: #333; font-weight: bold`, this.state, this.props,  )
    return (
      <CopyToClipboard 
        text={children} 
        onCopy={this.onCopys}
      >
        {type === 'div' ? <div {...this.props}>{children}</div> : type === 'btn' ? 
        <div className='d-ib' >{children} <Icon type={`copy`} ></Icon> </div> : <Button {...this.props}>{children}</Button>}
        {/* <div className='d-ib' >{children} <Button shape="circle" icon={`copy`} type="primary" className="m-l5 "></Button> </div> : <Button {...this.props}>{children}</Button>} */}
        {/* {type === 'div' ?  <div {...this.props}>{children}</div> : <Button {...this.props}>{children}</Button>} */}
      </CopyToClipboard>
    )
  }
}

export default ClipBoard
