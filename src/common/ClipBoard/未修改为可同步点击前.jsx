import React from 'react';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import { confirms, } from 'util'
import { Button, } from 'antd';
import './style.less'

class ClipBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    }
  }
  // onCopy = (v, r, t) => {
  //   console.log('ClipBoard onCopy ： ', v, r, t, this.props )
  //   const {text, onClick} = this.props 
  //   this.setState({
  //     value: text,
  //   })
  //   confirms(1, `${text}複制成功！ O(∩_∩)O~`)
  // }
  onCopy = (v, r, t) => {
    console.log('ClipBoard onCopy ： ', v, r, t, this.props )
    const {text, onClick, children} = this.props 
    this.setState({
      copy: children,
    })
    confirms(1, `${text}複制成功！ O(∩_∩)O~`)
    onClick()
  }
  render() {
    const { text, children, type } = this.props
    const { copy } = this.state
    console.log(' %c ClipBoard 组件 this.state, copy, this.props ： ', `color: #333; font-weight: bold`, this.state, this.props,  )
    // return (
    //   <div>
    //     <input value={this.state.value}
    //       onChange={({target: {value}}) => this.setState({value, copied: false})} />

    //     <CopyToClipboard text={this.state.value}
    //       onCopy={() => this.setState({copied: true})}>
    //       <span>Copy to clipboard with span</span>
    //     </CopyToClipboard>
    //     <CopyToClipboard text={this.state.value}
    //       onCopy={this.onCopy}>
    //       <button>Copy to clipboard with button</button>
    //     </CopyToClipboard>
    //     {this.state.copied ? <span style={{color: 'red'}}>Copied.</span> : null}
    //   </div>
    // )
    // return (
    //   <CopyToClipboard text={copy} 
    //   //onCopy={this.onCopy}
    //   ><div className='copyWrapper' >
    //     {children}
    //     <Button onClick={this.onCopy} className='copyBtn edit' shape="circle" type="primary" icon={`copy`} ></Button>
    //   </div></CopyToClipboard>
    // )
    return (
      <CopyToClipboard text={copy} 
      //onCopy={this.onCopy}
      >
        {type === 'div' ? <div {...this.props} onClick={this.onCopy}>{children}</div> : <Button {...this.props} onClick={this.onCopy}>{children}</Button>}
      {/*<div className='copyWrapper'>
        {/* {children}
        <Button onClick={this.onCopy} className='copyBtn edit' shape="circle" type="primary" icon={`copy`} ></Button>
       </div>*/}
       </CopyToClipboard>
    )
  }
}

export default ClipBoard
