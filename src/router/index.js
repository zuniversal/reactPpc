import React from "react"
import {Router, Route, hashHistory} from "react-router"
import Root from '@@'
import App from '@/App'
import Login from '@@/Login'
import NotFound from '@@/NotFound'
import {routeArr} from 'config'
import {logins} from 'util'

// console.log('routeArrrouteArrrouteArr ：', routeArr);
class RouterMap extends React.Component {
    logins = (nextState, replace, cb) => {
        // console.log('logins(nextState, replace, cb) ：', nextState );
        return logins(nextState, replace, cb)
    }
    
    // requireAuth(nextState, replace, callback) {
    //     console.log('nextState, replace, callback============= ：', nextState,);
    //     var isLogined = false
    // //  onChange={this.requireAuth}
    //     // if (isLogined) {
    //     //     console.log('false')
    //     //     replace('/city');
    //     // } else {
    //     //     console.log('true')
    //     //     replace('/login');
    //     // }

    //     if (!isLogined) {
    //         console.log('false')
    //         replace('/login');
    //     }
    //     callback();
    // }
    render() {
        // const ROUTE = routeArr.map((item, i) => {
        //     // console.log('item ：', item, ) 
        //     const { path, components, name, middle, } = item
        //     return <Route path={path} components={components} name={name} middle={middle} key={path}></Route>
        // })
        // const ROUTE = routeArr.map(v => <Route {...v} key={v.path}></Route>)
        return (
            <Router history={hashHistory}>
                {/* <Route path={'/'} components={App} name="/"> */}
                <Route path={'/'} onEnter={this.logins} components={App} name="/">
                    {/* <IndexRoute components={App}></IndexRoute> */} 
                    <Route path={'404'} components={NotFound}></Route>
                    <Route path={'page'} components={Root}>{routeArr.map(v => <Route {...v} key={v.path}></Route>)}</Route>
                    {/* <Route path={'pnNo'} components={Homes}></Route> */}
                </Route>
                <Route path={'login'} components={Login}></Route>
                <Route path="*" component={NotFound}></Route>
            </Router>
        )
    }
}

export default RouterMap;