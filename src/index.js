import React from 'react';
// import ReactDOM from 'react-dom';
import './index.less';
// import 'antd/dist/antd.css'
import 'static/css/animate.css'
import registerServiceWorker from './registerServiceWorker';
import Router from 'Router'
import {Provider} from "react-redux"
import { AppContainer } from 'react-hot-loader'
import {render} from 'util'
import Store from "store"
import axios from 'axios'
import { LocaleProvider } from 'antd';
import zhCN from 'antd/lib/locale-provider/zh_TW';

// console.log(' zhCN ： ', zhCN,  )// 

const store = Store();
axios.store = store
const Providers = <LocaleProvider><Provider store={store}><Router/></Provider></LocaleProvider>
// const Providers = <LocaleProvider locale={zhCN}><Provider store={store}><Router/></Provider></LocaleProvider>
render(AppContainer, Providers)
console.log(' registerServiceWorker ： ', module.hot, )//
if (module.hot) module.hot.accept('Router', () => render(AppContainer, Providers))
registerServiceWorker(); 