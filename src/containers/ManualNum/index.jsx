import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getManualNum, getPnDayData, newManualNum, editManualNum, deleteManualNum,  } from 'api/manualNum'
import { getProcedure, } from 'api/Procedure'
import { finishDay, } from 'api/ppc'
import {unitFilters, manualNumForm, manualNumFormExtra, 
  manualNumSearchFormExtra, rules, manualNumInit, manualDate, 
  DATE_FORMAT_BAR, defaultNow, } from 'config'
import { ANIMATE, SELECT_TXT, defaultWorkNo, } from 'constants'
import {backupFn, confirms, filterDatas, } from 'util'
import Collapses from '@@@/Collapses'
import ProductDialog from '@@@/ProductDialog'
import ManualNumTable from '@@@/ManualNumTable'
import ProduceForm from '@@@/ProduceForm'
import EditInfo from '@@@@/EditInfo'
import SecureHoc from '@@@@/SecureHoc' 
import { Button, Icon, Select, message, Row, Col, Modal,   } from 'antd';
import moment from 'moment'
const Option = Select.Option;
const confirm = Modal.confirm;
const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

class ManualNum extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      procedureData: [],
      editData: {},
      editOrigin: {},
      show: false,
      isNew: false,
      isAdd: false,
      addData: {},
      modalType: '',
      title: '提示框',
      manualNumInit: {...manualNumInit, },
      manualNumData: [],
      manualNumFormData: [],
      factoryData: [],
      formWorkData: [],
      formFactoryData: [],
      total: 0,
      
    }
  }
  showDialog = () => this.setState({ show: true, })
  cancelData = () => {
    console.log('cancelData ：', this.state)
    const {editOrigin, isAdd, addData, manualNumData,  } = this.state
    const {seq_no, } = editOrigin
    const indexes = manualNumData.findIndex(v => v.seq_no === seq_no)
    console.log('修改后的数据manualNumData ：', indexes, manualNumData, )
    if (isAdd) {
      const addItem = manualNumData.findIndex(v => v.seq_no === addData.seq_no)
      console.log('改变增加addItem ：', addItem);
      manualNumData.splice(addItem, 1)
    } else {
      console.log('改变修改 ：', );
      manualNumData[indexes] = editOrigin
    }
    console.log('manualNumData ：', manualNumData)
    this.setState({
      manualNumData: [...manualNumData],
      ...this.resetPramas(),
    })
  }
  resetPramas = (v) => {
    return {
      editData: {},
      show: false,
      isAdd: false,
      isNew: false,
      modalType: '',
      title: '提示框',
    } 
  }
  close = () => {
    console.log('取消 ：', )
    this.cancelData()
  }
  filterOption = (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
  editContent = () => {
    const {editData, modalType, manualNumInit, manualNumFormData, formWorkData, formFactoryData,  } = this.state
    console.log('editContent ：', editData, )
    if (modalType === 'newManualNum' ) {
      const commonConfig = {
        formItemLayout,
        formLayout: 'horizontal',
        ref: forms => this.forms = forms,
      }
      const config = [...manualNumForm.map((v) => ({...v, rules, })), ...manualNumFormExtra.map((v) => ({...v, rules: v.key !== 'remark'  ? rules : null , })), ]
      return <ProduceForm 
        {...commonConfig}
        init={{...manualNumInit, r_date: defaultNow, }} 
        config={config}
        pn_no_data={manualNumFormData}
        work_no_data={formWorkData}
        factory_id_data={formFactoryData}
        pn_noSelectCb={this.pn_noSelectCb}
        workSelectCb={this.workSelectCb}
      />
    }
    return <EditInfo/>
  }
  editDataAction = (v) => {
    console.log('编辑ss ', v, this.props, this.state);
    const {manualNumData, editData, isNew, } = this.state
    if (Object.keys(editData).length || isNew) {
      confirms(2, '請先保存上一條數據再繼續操作！', );
    } else {
      console.log('编辑modalTypess ', );
      let editData 
      const manualNumDatas = manualNumData.map(item => {
        if (item.seq_no === v.seq_no)  {
          console.log('匹配的 manualNumData item ：', item, ) 
          item.editable = true
          editData = item
        }
        return item 
      })
      this.setState({
        manualNumData: manualNumDatas,
        editData: {...editData,},
        editOrigin: backupFn({...editData, editable: false}),
        modalType: 'editManualNum',
      })
    }
  }
  handleOk = () => {
    console.log('handleOk ：', this.state)
    const {modalType, manualNumFormData, formFactoryData, editData,  } = this.state// 
    if (modalType === 'newManualNum') {
      this.forms.validateFields((err, values) => {
        console.log('values ：', err, values, )
        if (!err) {
          values.r_date = moment(values.r_date).format(DATE_FORMAT_BAR)
          values.work_no = Number(values.work_no)
          values.factory_id = Number(values.factory_id)
          values.remark =  values.remark  != undefined ? values.remark : ''
          const matchItem = manualNumFormData.find(v => v.pn_no === values.pn_no)
          const matchBarcodeItem = formFactoryData.find(v => v.factory_id == values.factory_id)
          console.log(' =====  matcharcodeItem ： ', matchItem, matchBarcodeItem,  )
          values.qty = Number(values.qty)
          values.gauge = matchBarcodeItem.gauge
          values.factory_code = matchBarcodeItem.factory_code
          values.barcode = matchBarcodeItem.barcode
          this.newManualNumAction(values)
        }
      })
    } else if (modalType === 'editManualNum') {
      console.log('editManualNumeditManualNum ：', )
      this.editManualNum(editData)
    }
    
  }
  newManualNumAction = (v) => {
    console.log(' newManualNumAction ： ',  v )
    // return  
    newManualNum(v).then(res => {
      console.log('newManualNum res ：', res.data);
      const {code, data, mes, } = res.data
      if (code === 1) {
        this.setState({
          ...this.resetPramas(),
        })
      }
    })
  }
  editManualNum = (v) => {
    console.log(' editManualNum ： ',  v )
    // return  
    editManualNum(v).then(res => {
      console.log('editManualNum res ：', res.data);
      const {code, data, mes, } = res.data
      if (code === 1) {
        const {manualNumData, } = this.state// 
        this.setState({
          manualNumData: manualNumData.map((item) => ({...item, 
            seq_no: item.seq_no === v.seq_no ? v.seq_no : item.seq_no,
            remark: item.remark === v.remark ? v.remark : item.remark,
            editable: false,
          })),
          ...this.resetPramas(),
        })
      }
    })
  }
  deleteManualNum = (v) => {
    console.log(' deleteManualNum ： ',  v )
    // return  
    deleteManualNum(v).then(res => {
      console.log('deleteManualNum res ：', res.data);
      const {code, data, mes, } = res.data
      if (code === 1) {
        const {manualNumData, } = this.state// 
        // this.setState({
        //   canDelete: true,
        // })
        // this.timer = setTimeout(() => this.setState({manualNumData: manualNumData.filter((item) => item.seq_no !== v.seq_no), }), 500);
        this.setState({
          manualNumData: manualNumData.filter((item) => item.seq_no !== v.seq_no),
          ...this.resetPramas(),
        })
      }
    })
  }
  editData = (data) => {
    console.log('editData213 ：', data, this.state)
    // return  
    const {manualNumData, isAdd } = this.state
    const {v, keys, seq_no, } = data
    // manualNumData[keys] = Number(v)
    let editData
    const manualNumDatas = manualNumData.map(item => {
      if (item.seq_no === seq_no)  {
        editData = item
        console.log('匹配的 manualNumData item ：', item, ) 
        item[keys] = Number(v)
      }
      return item
    })
    const newState = {
      manualNumData: manualNumDatas,
      editData: {...editData,}
    }
    console.log('manualNumData ：', this.state, newState)
    this.setState(newState)
  }
  showConfirm = ({f, v, item = '', action = 'Delete', }) => {
    console.log('showConfirm ：', f, v, action, );
    confirm({
      title: `提示框`,
      content: `你確信要 ${action === 'Delete' ? '刪除' : action} ${item} ？`,
      width: 480,
      onOk: () => this[f](v, ),
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  
  finishDay = (e, t, v, i) => {
    console.log(' finishDay ： ', e, t, v, i, this.state, this.props,  )
    const {barcodeData, } = this.state
    const {userInfo, } = this.props 
    const finish_status = v.finish_status === 'T' ? 'F' : 'T'
    const params = {
      finish_status,
      barcode: v.barcode, 
      finish_user: userInfo.user_id,
      finish_date: moment(v.r_date).format(DATE_FORMAT_BAR),
    }
    console.log(' moment(v.r_date).format(DATE_FORMAT_BAR) ： ',  v.finish_status, params,  )// 
    // return  
    finishDay(params).then(res => {
      console.log('finishDay res ：', res.data);
      const {code, } = res.data
      if (code === 1) {
        const {manualNumData, } = this.state 
        const manualNumDatas = manualNumData.map((item) => {
          return item.barcode === v.barcode ? ({
            ...item,
            finish_status,
          }) : item
        })
        console.log(' manualNumData ： ', manualNumData, manualNumDatas,   )// 
        this.setState({
          manualNumData: manualNumDatas,
        })
      }
    })
  }
  


  dataFormat = (params) => {
    console.log('dataFormat res ：', params)
    const newParams = {}
    Object.keys(params).forEach((v) => {
      if (v === 'date') {
        newParams[`${v}fr`] = moment(params[v][0]).format(DATE_FORMAT_BAR)
        newParams[`${v}to`] = moment(params[v][1]).format(DATE_FORMAT_BAR)
      } else if (v === 'work_no' || v === 'factory_id') {
        newParams[v] = params[v] != undefined ? Number(params[v]) : 0
      } else {
        newParams[v] = params[v] != undefined ? params[v] : ''
      } 
    })
    console.log('dataFormat reresress ：', newParams,   )
    return newParams// 
  }
  
  getManualNum = (manualNumInit, ) => {  
    const params = this.dataFormat(manualNumInit)
    console.log('getManualNum ：', this.state, this.props, params,  )
    // Warning: you should not use `ref` on enhanced form, please use `wrappedComponentRef`
    // console.log('dataFormatdataFormat ：', this.searchForms.getFieldsValue(), params, );
    // return
    getManualNum(params).then(res => {
      console.log('getManualNum res ：', res.data);
      const {code, mes, data,  } = res.data
      if (code === 1) {
        const datas = data.map((v, index,) => ({...v, pn_no: v.pn_no.trim(), index: index + 1, }))
        const pnArr = filterDatas(datas, 'pn_no')
        const pnData = pnArr.map((v) => {
          const filterData = datas.filter(item => v === item.pn_no)
          return {data: datas.filter(item => v === item.pn_no), ...filterData[0], }
        })
        console.log(' pnData ： pnData, ', pnData,  )// 
        this.setState({
          manualNumData: datas,
          // manualNumData: pnData,
          ...this.resetPramas(),
          total: this.calcTotal(data),
        })
      }
    })
  }
  calcTotal = (data,  ) => {
    // console.log('    calcTotal ： ', data,  )
    let total = 0 
    data.forEach((v, i) => total += v.qty)
    return total 
  }
  getPnDayData = (manualNumInit, ) => {  
    const params = this.dataFormat(manualNumInit)
    console.log('getPnDayData ：', this.state, this.props, params,  )
    // Warning: you should not use `ref` on enhanced form, please use `wrappedComponentRef`
    // console.log('dataFormatdataFormat ：', this.searchForms.getFieldsValue(), params, );
    // return
    getPnDayData(params).then(res => {
      console.log('getPnDayData res ：', res.data);
      const {code, mes, data,  } = res.data
      if (code === 1) {
        const manualNumObj = {}
        const datas = data.map((v) => ({...v, pn_no: v.pn_no.trim(), 
          // factory_id: v.factory_id.toString(), 
        }))
        const pnArr = filterDatas(datas, 'pn_no')
        const pnData = pnArr.map((v) => {
          const filterData = datas.filter(item => v === item.pn_no)
          // console.log(' manualNumPnData v ： ', v, filterData, ) 
          return {data: datas.filter(item => v === item.pn_no), ...filterData[0], }
        })
        const factoryArr = filterDatas(datas, 'factory_id')
        const factoryData = factoryArr.map((v) => datas.find(item => v === item.factory_id))
        console.log(' manualNumPnData datas v ： ', datas, pnData, factoryData,  )
        this.setState({
          manualNumFormData: pnData,
          factoryData: factoryData,
        })
      }
    })
  }
  search = (v) => {
    console.log(' search ： ',  v, this.props, this.state,  )
    this.searchForms.validateFields((err, values) => {
      console.log('search  values ：', err, values);
      this.getManualNum(values)
    })
  }
  pn_noSelectCb = (v) => {
    console.log(' pn_noSelectCb ： ',  v, this.props, this.state )
    const {manualNumFormData, factoryData, } = this.state// 
    const {disabledData, } = v.props
    this.setState({
      [disabledData]: v.v != undefined ? manualNumFormData.find(item => item[v.k] === v.v && item.data).data : manualNumFormData,
    })
  }
  workSelectCb = (v) => {
    console.log(' workSelectCb ： ',  v, this.props, this.state )
    const {formWorkData, factoryData, } = this.state// 
    const {disabledData, } = v.props
    this.setState({
      [disabledData]: formWorkData.filter(item => item[v.k] == v.v),
    })
  }
  newManualNum = () => {
    console.log(' newManualNum ： ',   )
    this.setState({
      show: true,
      title: '新增人工上數',
      modalType: 'newManualNum',
    })
  }
  getProcedure = () => {
    getProcedure({}).then(res => {
      console.log('getProcedure res ：', res.data);
      const {data, } = res.data
      this.setState({
        procedureData: data,
      })
    })
  }
  componentDidMount() {
    const {manualNumInit, } = this.state//
    this.getManualNum(manualNumInit, )
    this.getPnDayData(manualNumInit, )// 
    this.getProcedure()
  }
  componentWillUnmount() {
    console.log('ManualNum 组件卸载 ：', this.props, this.state )
    this.timer && clearTimeout(this.timer)
  }

  render() {
    console.log('ManualNum 组件this.state, this.props ：', this.state, this.props);
    const { procedureData, show, work_no, canDelete, isNew, 
      title, manualNumInit, modalType, manualNumFormData, factoryData, 
      formWorkData, formFactoryData, manualNumData, total, } = this.state// 
    const {auth, } = this.props 
    const {middle,  } = this.props.route
    const commonConfig = {
      formItemLayout,
      formLayout: 'horizontal',
      ref: forms => this.searchForms = forms,
    }
    return (
      <Collapses animate={ANIMATE.fadeIn} title={'人工上數'} className="manualNum"
        middle={middle}
        extra={
          <div className="btnWrapper">
            {
              <div className="btnWrapper">
                <Button onClick={() => this.newManualNum()} type="primary" icon='plus-circle-o'>新增人工上數</Button>
              </div>
            }
          </div>
        }  
      >
         
        <Row>
          <Col span={22} >
            <ProduceForm 
              {...commonConfig}
              init={manualNumInit} 
              // config={manualNumForm.map(v => v.key === 'date' ? ({...v, haveInit: true, initVal: manualDate,  }) : ({...v, }))}
              config={[...manualNumSearchFormExtra, ...manualNumForm.map((v) => ({...v, resetKey: undefined, })), ]}
              // isLoadSelect={true}
              isRow 
              pn_no_data={manualNumFormData}
              work_no_data={procedureData}
              factory_id_data={factoryData}
              pn_noSelectCb={this.pn_noSelectCb}
              workSelectCb={this.workSelectCb}
              // loadFn={this.loadFn}
              // disableOption={'factory_id'}
              // disableKey={'work_no'} 
              middle={middle}
            />
          </Col>
          <Col span={2} >
            <Button onClick={() => this.search()} type="primary" icon='search'>搜索</Button>
          </Col>
        </Row>
        {/* <Row>
          <Col span={6} >
            <div className="dateWrapper flexCenter">
              <span className='' >起止日期: </span> {
                ['date_s', 'date_e', ].map((v, i) => {
                  return <div className='d-ib flex2 ' key={i} >
                    <DateInput className={`dateInput f1`} 
                      val={manualNumInit[v]} keys={v}
                      onChange={(v) => this.dateChange(v)}// 
                    /> 
                   {i === 0 ? <span>&nbsp;&nbsp;&nbsp;</span> : ''}
                  </div>
                })
              }
            </div>
          </Col>
          <Col span={18} >
            <ProduceForm 
              {...commonConfig}
              init={{}} 
              config={manualNumForm.map(v => ({...v, span: 8}))}
              workNoFilter={this.workNoFilter}
              factoryIdFilter={this.factoryIdFilter}
              isLoadSelect={true}
              loadFn={this.loadFn}
              isRow
            />
          </Col>
        </Row> */}

        {
          work_no !== -1 ? <ManualNumTable 
            unitFilters={unitFilters} 
            isDisable={isNew} 
            canDelete={canDelete} 
            deleteData={this.showConfirm} 
            work_no={work_no} 
            editData={this.editData} 
            procedureData={procedureData} 
            editDataAction={this.editDataAction} 
            showDialog={this.showDialog}
            data={manualNumData}
            cancelData={this.cancelData}
            middle={middle}
            total={total}
            finishDay={this.finishDay}
          ></ManualNumTable> : null
        }
        {show ? <ProductDialog className={modalType === 'editManualNum' ? 'corfirmModal' : ''} handleOk={this.handleOk} width={'45%'} show={show} close={this.close} title={title}>{this.editContent()}</ProductDialog> : null}
      </Collapses>
    );
  }
}

// export default ManualNum;
export default SecureHoc(ManualNum)
