import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
// import Logo from '@@@/LogoCom'
import './style.less'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import {hashHistory} from 'react-router'
import {login} from 'api/login'
import * as loginAction from 'actions/login.js'
import {loginForm} from 'config'
import {getItem, setItem, setItems, confirms, } from 'util'
import { Form, Icon, Input, Button, Checkbox, message } from 'antd';
const FormItem = Form.Item;

export const loginForm = [
  { unq: 'user_id', icon: 'user', rules: [{ required: true, message: '請輸入您的用戶名!' }], ph: '請輸入您的用戶名!', },
  { unq: 'password', icon: 'lock', type: 'password', rules: [{ required: true, message: '請輸入您的密碼!' }], ph: '請輸入您的密碼!', },
]

class Login extends React.Component {
  constructor(props) {
    super(props)
      this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
      this.state = {
      }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    // hashHistory.push(`/page/indexs`)
    this.props.form.validateFields((err, values) => {
      // if (!err) {
        console.log('Received values of form: ', values);
        const {user_id, password, remember} = values
        login({user_id, password}).then(res => {
          console.log('login  res ：', res.data);
          const {code, mes, data, } = res.data
          if (code === 1) {
            const {company_id} = data
            setItems("userInfo", data)
            setItems("company_id", company_id)
            setItem('user_id', user_id)
            setItem('remember', remember)
            this.props.loginAction.login(data)
            hashHistory.push(`/page/indexs`)
          } 
          // else {
          //   confirms(code, mes,  )
          // }
        }).catch(err => {
          console.log('catch(err ：', err);
          // confirms(0, mes,  )
        })
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const isRemember = getItem('remember')
    const user_id = isRemember ? getItem('user_id') : '' 
    return (
      <div className="login">
        <div className="loginWrapper">
        <div className="esTitle">PPC System</div>
          
        <Form onSubmit={this.handleSubmit} className="login-form">
          {
            loginForm.map(v => {
              console.log(' loginForm v ：', v, ) 
              return <FormItem
                hasFeedback={v.hasFeedback}
                key={v.unq} 
              >
                {getFieldDecorator('user_id', {
                  rules: v.rules != undefined ? v.rules : null, initialValue: user_id
                })(
                  <Input prefix={<Icon type={v.icon} className='icon' />} placeholder={v.ph} />
                )}
              </FormItem>
            })
          }
          <FormItem
            hasFeedback
          >
            {getFieldDecorator('user_id', {
              rules: [{ required: true, message: '請輸入您的用戶名!' }], initialValue: user_id
            })(
              <Input prefix={<Icon type="user" className='icon' />} placeholder="請輸入您的用戶名" />
            )}
          </FormItem>
            <FormItem
              hasFeedback
            >
            {getFieldDecorator('password', {
              rules: [{ required: true, message: '請輸入您的密碼!' }], 
              // initialValue: 337133
            })(
              <Input prefix={<Icon type="lock" className='icon' style={{ fontSize: 14 }} />} type="password" placeholder="請輸入您的密碼" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: isRemember,
            })(
              <Checkbox>記住密碼</Checkbox>
            )}
            <Button type="primary" htmlType="submit" className="login-form-button" size='large'>登錄</Button>
          </FormItem>
        </Form>
        </div>
      {/* <Logo></Logo> */}
      </div>
    );
  }
}


const state = state => state
const action = action => {
  return {
    loginAction: bindActionCreators(loginAction, action),
  }
}

export default connect(state, action)(Form.create()(Login));