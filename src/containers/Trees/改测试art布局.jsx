import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'

import Collapses from '@@@/Collapses'
import { Tree } from 'antd'
import ppcList from './ppcList.json'
import ClipBoard from '@@@@/ClipBoard'// 
const TreeNode = Tree.TreeNode

class Trees extends React.Component {
  state = {
    copy: 'copycopy',
  }
  onSelect = (selectedKeys, info) => {
    console.log('selected', selectedKeys, info);
  }
  showDetial = (v) => {
    console.log(' showDetial ： ',  v )
    this.setState({ 
      copy: v,
    })
  }

  onCopy = (v) => {
    console.log(' onCopy ： ', v )
    
  }
  render() {
    const data = [
      { 'name': 'zyb1', 'age': 1, 'height': 1 },
      { 'name': 'zyb2', 'age': 2, 'height': 2 },
      { 'name': 'zyb3', 'age': 3, 'height': 3 },
      { 'name': 'zyb4', 'age': 4, 'height': 4 },
    ]
    console.log('ppcTreesList 组件 this.state, this.props ：', this.state, this.props, ppcList)

    const {copy, } = this.state 
    return (
      <div>
        {/* <ClipBoard copy={copy}>
          {
            data.map((v) => {
              console.log(' datab v ： ', v, ) 
              return <div className='m-5 p-5'  onClick={this.showDetial.bind(this, v.name)} key={v.name} >{v.name}</div>
            })
          }
          
        </ClipBoard> */}
        <div>
          { 
            data.map((v, i) => <ClipBoard type={'div'} key={i} onClick={() => this.onCopy(v)} >{v.name}</ClipBoard>)
          }
          { 
            data.map((v, i) => <ClipBoard type={'button'} key={i} onClick={() => this.onCopy(v)} >{v.name}</ClipBoard>)
          }
        </div>
      </div>
    )
    
    return (
      <Collapses title={'树状'} className="trees"
      >
        <Tree
          showLine
          defaultExpandedKeys={['0-0-0']}
          onSelect={this.onSelect}
        >
          {
            ppcList.datas_barcode.map(v => {
              console.log(' ppcList v ：', v, ) 
              return (<TreeNode title="parent 1" key={v.barcode}>
                <TreeNode title="parent 1-0" key="0-0-0">
                  <TreeNode title="leaf" key="0-0-0-0" />
                  <TreeNode title="leaf" key="0-0-0-1" />
                  <TreeNode title="leaf" key="0-0-0-2" />
                </TreeNode>
              </TreeNode>)
            })
            
          }
        </Tree>
      </Collapses>
    );
  }
}

export default Trees;
