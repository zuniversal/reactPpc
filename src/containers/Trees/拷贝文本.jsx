import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'

import Collapses from '@@@/Collapses'
import { Tree } from 'antd'
import ppcList from './ppcList.json'
import ClipBoard from '@@@@/ClipBoard'// 
const TreeNode = Tree.TreeNode

class Trees extends React.Component {
  state = {
    copy: 'copycopy',
  }
  onSelect = (selectedKeys, info) => {
    console.log('selected', selectedKeys, info);
  }
  showDetial = (v) => {
    console.log(' showDetial ： ',  v )
    this.setState({ 
      copy: v,
    })
  }

  onCopy = (v) => {
    console.log(' onCopy ： ', v )
    
  }
  render() {
    
    return (
      <Collapses noLimit title={'工藝路線'} isAnimate={false}
      >
      <div className='artWrapper' >
        {/* <div className='tabWrapper'>
          {
            ppcList.data_total.map((v, i) => <div key={i} >{v.s_date}</div>)
          }
        </div> */}
        <div className='wudWrapper'>

          <div className='wud'>
        
                  {
                    ppcList.data_total.map((v, i) => <div key={i} >{v.s_date}</div>)
                  }
                  
                </div>
          
        </div>
        
      </div>

      </Collapses>
    );
  }
}

export default Trees;
