import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getEvent } from 'api/Home'
import { eventTitle, } from 'config'
import { A_SLIDE_IN_LEFT, A_SLIDE_IN_RIGHT } from 'constants'
import { addProp, mergeArr, filterArrOForm, } from 'util'

import Collapses from '@@@/Collapses'
import EventBoard from '@@@/EventBoard'
import { Row, Col, Icon, } from 'antd'
const formItemLayout = {
  labelCol: { span: 14 },
  wrapperCol: { span: 10 },
};

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      eventData: [],
    }
  }
  getEvent = () => {
    getEvent({ company_id: "3" }).then(res => {
      console.log('getEvent res ：', res.data);
      const { datas } = res.data
      const bassArr = []
      datas.forEach(v => bassArr.push(v.class_id))
      const bassHandleArr = filterArrOForm(bassArr, 'class_id', 'event')
      const mergedArr = mergeArr(datas, bassHandleArr, 'class_id', 'event')
      console.log('getEvent res ：', bassHandleArr, bassArr, mergedArr);
      const filterEvent = addProp(mergedArr, eventTitle, 'class_id', 'title')
      this.setState({
        eventData: filterEvent,
      })
    })
  }
  componentDidMount() {
    this.getEvent()
  }
  render() {
    console.log('首页 组件this.state, this.props ：', this.state, this.props);
    const { eventData } = this.state
    return (
      <section className="homes">
        <Collapses title={'首页'} isAnimate={false}>

          {eventData.length ? eventData.map((v, i) => (
            <Row gutter={16} className={`eventWrapper ${i % 2 === 0 ? 'hide' : ''}`} key={i}>
              <Col className={` ${A_SLIDE_IN_RIGHT}`} xs={24} sm={12} md={12} lg={12} xl={12}>
                <EventBoard data={v}></EventBoard>
              </Col>
            </Row>
            )
          ) : ''}

        </Collapses>
      </section>
    );
  }
}
export default Home;