import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getEvent, eventDetail, updateEvent, getUpdateEvent, } from 'api/Home'
import { getCapaticity,  } from 'api/Capaticity'
import { eventTitle, eventColmun, eventFixColmun, eventConfig, defaultNow, capcityConfig, 
  FORMAT_MONTH_ONE, legend, capcityTableConfig, DATE_FORMAT_BAR, eventSub7, dateFrNow, eventDate, unLockDate,  
  boardArr,
} from 'config'
import { ANIMATE, SELECT_TXT,  } from 'constants'
import { addProp, mergeArr, filterArrOForm, openNotification, backupFn, getMonth, confirms, getItems,  } from 'util'
import Collapses from '@@@/Collapses'
import EventBoard from '@@@/EventBoard'
import ProductDialog from '@@@/ProductDialog'
import EventTable from '@@@/EventTable'
import GridTable from '@@@@/GridTable/RichGridDeclarativeExample'
import SecureHoc from '@@@@/SecureHoc'
import { Row, Col, Button, Select,} from 'antd'
import moment from 'moment'
const Option = Select.Option;



class Home extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      eventData: [],
      show: false,
      eventDetail: [],
      title: '',
      class_id: '',
      event_id: '',
      capcityData: {},
      event_desc: '',
      isDateChange: false,
      isUnLock: false,
      eventDate, 
      unLockDate,
      searchParams: {},
      company_id: getItems('company_id'),
    }
  }
  handleOk = () => {
    console.log('handleOk ：', )
  }
  close = () => {
    // const datefr = eventDate[0].initVal
    // const dateto = eventDate[1].initVal
    // console.log('close ：', eventDate, datefr, dateto,  )
    this.setState({
      show: false,
      isDateChange: false, 
      isUnLock: false,
      eventDate, 
      unLockDate,
      searchParams: {},
    })
  }
  getCapaticity = (pDateFr = defaultNow, factory_id = 0) => {
    // const {pDateFr, factory_id} = this.state
    console.log('getCapaticity ：', this.state, pDateFr, factory_id );
    const {company_id,  } = this.state// 
    getCapaticity({p_date_fr: pDateFr, factory_id, company_id,  }).then(res => {
      console.log('getCapaticity res ：', res.data, pDateFr, defaultNow, pDateFr === defaultNow, );
      const {datas, code, } = res.data
      const capcityObj = {}
      datas.forEach((v, i) => capcityObj[v.factory_id] = [...capcityObj[v.factory_id] != undefined ? capcityObj[v.factory_id] : [], v])
      console.log(' capcityObj ： ', capcityObj )
      const data = Object.values(capcityObj).map((v) => this.filterCapaticity(v))
      console.log(' data ： ', data )
      this.setState({
        show: true,
        eventDetail: data,
      })
    })
  }
  filterCapaticity = (data) => {
    // console.log(' filterCapaticity ： ',  data )
    const datas = data[0]
    const title = data[0].factory_name.trim() + ' - ' + data[0].work_name.trim()
    datas.title = title
    datas.isCapcity = true
    data.forEach((v, i) => {
      datas[`basic_capacity${i}`] = v.basic_capacity
      datas[`done_capacity${i}`] = v.done_capacity
    })
    return datas
  }
  monthHandle = (v, m) => {
    const allStartMonth = moment(m).format(FORMAT_MONTH_ONE)
    const startIndex = legend.findIndex(v => v === allStartMonth + '月')
    // console.log('allStartMonth,11  ：',v, allStartMonth, startIndex)
    const after = legend.slice(0, startIndex)
    const before = legend.slice(startIndex, )
    v.legend = [...before, ...after]
    console.log('allStartMonth,222  ：', v,allStartMonth, [...before, ...after])
  }
  getEvent = (company_id,  ) => {
    console.log(' getEvent  company_id ： ', company_id,  )// 
    getEvent({company_id, }).then(res => {
      console.log('getEvent rs e：', res.data);
      const { datas } = res.data
      const bassArr = []
      datas.forEach(v => {
        // console.log(' v ： ', v, v.event_desc,  )// // 
        // if (v.class_id === 3 && v.event_id === 4) {
        //   v.event_desc = v.num + ' 本周内批号数量和货期变化'
        // }
        // if (v.class_id === 3 && v.event_id === 5) {
        //   console.log(' v.event_descevent_descevent_desc ： ', v.event_desc, v.event_desc.split('本周'),  )// 
        //   const [descs0, descs1, ] = v.event_desc.split('本周')
        //   v.event_desc = [descs0, '本周内', descs1, ].join('') 
        // }
        // console.log(' v ： ', v, v.event_desc.split(' ') )


        // v.num = v.event_desc.split(' ')[0]// 
        bassArr.push(v.class_id)
      })
      const bassHandleArr = filterArrOForm(bassArr, 'class_id', 'event')
      const mergedArr = mergeArr(datas, bassHandleArr, 'class_id', 'event')
      const filterEvent = addProp(mergedArr, eventTitle, 'class_id', 'title')
      console.log('getEvent res ：', bassHandleArr, bassArr, mergedArr, datas, filterEvent );
      this.setState({
        eventData: filterEvent,
        
        company_id: company_id, 
      })
    })
  }
  onChange = (e, key,  ) => {
    console.log('onChange v ：', e, e.target.value, key )
    const {date, eventDate, } = this.state// 
    this.setState({
      eventDate: eventDate.map((item) => {
        // console.log(' key === item.key ? {...item, initVal: e.target.value, } : item  ： ', key, item.key, key === item.key, key === item.key ? {...item, initVal: e.target.value, } : item, item , )// 
        return key === item.key ? {...item, initVal: e.target.value, } : item  
      })
    })
  }
  unLockDateChange = (e, key,  ) => {
    console.log('unLockDateChange v ：', e, e.target.value, key )
    const {date, unLockDate, } = this.state// 
    this.setState({
      unLockDate: unLockDate.map((item) => {
        // console.log(' key === item.key ? {...item, initVal: e.target.value, } : item  ： ', key, item.key, key === item.key, key === item.key ? {...item, initVal: e.target.value, } : item, item , )// 
        return key === item.key ? {...item, initVal: e.target.value, } : item  
      })
    })
  }
  getUpdateEvent = (v,  ) => {
    console.log('    getUpdateEvent ： ', v,   )
    const {eventDate, company_id,  } = this.state// 
    const datefr = eventDate[0].initVal
    const dateto = eventDate[1].initVal
    console.log(' datefr, dateto, ： ', datefr, dateto,  )// 
    this.setState({
      eventDetail: [],
    })
    getUpdateEvent({datefr, dateto, company_id,  }).then(res => {
      const { datas } = res.data
      const eventDetail = datas.map((v) => ({...v, isDateChange: true,  })).map((item) => this.dateHandle(item, ))
      console.log('getUpdateEvent  ：', res.data, eventDetail);
      this.setState({
        eventDetail,
        ...this.setInfo(v),
        searchParams: v,
        isDateChange: true,
      })
    })
    
  }
  updateEvent = (class_id) => {
    const { company_id,  } = this.state// 
    console.log('updateEvent ,：', class_id, this.state, )
    updateEvent({class_id, company_id, }).then(res => {
      console.log('updateEvent res ：', res.data);
      const {datas, code, mes} = res.data
      // if (code === 1) {
        const {eventData,  } = this.state// 
        confirms(1, '數據更新成功')
        datas.forEach((v, i) => v.event_desc = v.pn_count + ' ' + v.event_desc)
        this.setState({
          eventData: eventData.map((v) => {
            console.log(' class  } : v ： ', class_id === v.class_id ? {...v, event: datas,  } : v,  )// 
            // v.num = v.event_desc.split(' ')[0]// 
            return class_id === v.class_id ? {...v, event: datas,  } : v
          }),
        })
      // }
    })
  }
  getunLockEvent = (v) => {
    const {unLockDate, company_id,  } = this.state// 
    const input_date = unLockDate[0].initVal
    console.log('    getunLockEvent ： ', this.state, this.props, unLockDate, input_date  )
    this.setState({
      eventDetail: [],
    })
    eventDetail({...v, input_date, company_id,   }).then(res => {
      console.log(' eventDetail res ： ', res.data,  )// 
      const {datas, code, mes} = res.data
      this.setState({
        eventDetail: datas.map((v) => v),
        ...this.setInfo(v),
        isUnLock: v.class_id === 3 && v.event_id == 5,
        searchParams: v,
      })
    })
    
  }
  dateHandle = (item,  ) => {
    console.log(' dateHandle ： ',  item,   )
    const dateArr =  [
      'prod_date', 'update_date', 
    ]
    const newData = {...item,  }
    dateArr.forEach((v, i) => {
      // console.log(' dateArr v ： ', v, i, item[v], newData  )
      newData[v] = moment(item[v]).format(DATE_FORMAT_BAR)
    })
    console.log(' dateHandle ： ', newData   )// 
    return newData 
  }
  eventInfo = (v,    ) => {
    console.log('    eventInfo ： ', v,     )
    this.setState({
      title: `${v.title} - ${v.event_desc}`,
      class_id: v.class_id,
      event_id: v.event_id,
      event_desc: v.event_desc,
    })
  }
  setInfo = (v,    ) => {
    console.log('    setInfo ： ', v,     )
    return {
      title: `${v.title} - ${v.event_desc}`,
      class_id: v.class_id,
      event_id: v.event_id,
      event_desc: v.event_desc,
      show: true,
    }
  }
  dateFormat = (v, k,  ) => {
    console.log(' dateFormat   ： ',  v, k,   )
    return {
      [k]: v[k] ? moment(v[k]).format(DATE_FORMAT_BAR) : v[k],
    } 
  }
  eventDetail = (v) => {
    console.log('eventDetail11 ,：', v, )
    const {company_id,  } = this.state// 
    if (v.class_id === 3 && v.event_id === 5) {
      console.log(' getunLockEvent ： ',    )// 
      this.getunLockEvent(v, )
      return  
    }

    if (v.class_id === 3 && v.event_id === 4) {
      console.log(' class_idclass_id ： ',    )// 
      this.getUpdateEvent(v, )
      this.eventInfo(v)
      return  
    }
    
    if (v.class_id === 1) {
      this.getCapaticity()
      this.eventInfo(v)
    } else {
      eventDetail({...v, company_id, }).then(res => {
        console.log('eventDetail res ：', res.data);
        const {datas, code, mes} = res.data
        let detailData = v.class_id === 2 || v.class_id === 3 ? datas.map(v => ({...v, 
          kpb_status: v.kpb_status === 'Y' ? '√' : '×', 
          prod_date: v.prod_date ?  moment(v.prod_date).format(DATE_FORMAT_BAR) : v.prod_date,
          yarn_date: v.yarn_date ? moment(v.yarn_date).format(DATE_FORMAT_BAR) : v.yarn_date,
        })) : datas
        if (v.class_id === 2) {
          console.log(' class_id ： ', detailData, v, )
          detailData = datas.map(v => ({
            ...v, 
            input_date: v.input_date ?  moment(v.input_date).format(DATE_FORMAT_BAR) : v.input_date,  
            prod_date: v.prod_date ?  moment(v.prod_date).format(DATE_FORMAT_BAR) : v.prod_date,  
          }))
        }
        if (v.class_id === 4 && (v.event_id === 1 || v.event_id === 3)) {
          console.log(' v.class_id yarn_dateyarn_date ： ', v.class_id,  )// 
          detailData = datas.map(v => ({...v, yarn_date: v.yarn_date != undefined  ? v.yarn_date : '',  }))
        }
        if (v.class_id === 4 && v.event_id === 3) {
          detailData = datas.map(v => ({...v, acs_type: v.acs_type == 'F' ? '未訂購' : '已訂購', yarn_date: v.yarn_date && v.yarn_date !== '1900-01-01'  ? v.yarn_date : 'TBA' } ))
          console.log(' detailDatadetailDatadetailData ： ', detailData, v)
        }
        if (v.class_id === 3 && v.event_id === 2) {
          console.log(' detailDatadetailDatadetailData ： ', detailData, v, moment(v.lastwork_e_date),  )
          detailData = datas.map(v => {
            moment(v.lastwork_e_date).isAfter(defaultNow)
            return {
              ...v, 
              acs_type: v.acs_type == 'F' ? '未訂購' : '已訂購',  
              lastwork_e_date: v.lastwork_e_date != undefined ? moment(v.lastwork_e_date).format(DATE_FORMAT_BAR) : '',
              mc_e_date: v.mc_e_date != undefined ? moment(v.mc_e_date).format(DATE_FORMAT_BAR) : '',
              sew_e_date: v.sew_e_date != undefined ? moment(v.sew_e_date).format(DATE_FORMAT_BAR) : '',
            } 
          })
        }
        console.log(' detailData ： ', detailData, v, v.class_id, v.event_id, )
        this.setState({
          eventDetail: detailData,
          ...this.setInfo(v),
        })
      })
    }
  }
  download = (t) => {
    console.log('t  download ：', t , this.gridTable, `exportDataAs${t}`  )
    this.gridTable.api[`exportDataAs${t}`]();
    // this.api.gridTable.exportDataAsExcel();
  }
  refresh = (v,  ) => {
    console.log(' refresh ： ',  v,   )
    
  }
  companyChange = (e,  ) => {
    console.log('    companyChange ： ', e,   )
    
  }
  capcityConfig = () => {
    console.log(' , defaultNow ： ', defaultNow, getMonth, )
    return capcityTableConfig().map((v) => v)
  }
  componentDidMount() {
    // this.getCapaticity()
    // this.eventDetail()
    const {company_id,  } = this.state// 
    console.log('  首页  组件componentDidMount挂载 ： ', company_id, this.state, this.props,  )// 
    this.getEvent(company_id)
  }
  render() {
    const {eventData, show, eventDetail, title, event_id, class_id, event_desc, isDateChange, isUnLock, searchParams, } = this.state
    const {middle,  } = this.props.route
    console.log('首页 组件this.state, this.props ：', middle, eventData, eventFixColmun, isCapcity ? this.capcityConfig() : eventConfig(class_id, event_id, ), class_id === 3 && event_id === 2, event_id, class_id, this.state, this.props, );
    // 如果是1的类别单独显示报表
    const isCapcity = class_id === 1
    return (
      <section className="homes">
        <Collapses title={'首頁'} isAnimate={false}
          noLimit
          // middle={middle}
          extra={
            <div className="btnWrapper">
              {
                boardArr.length ? (
                  <Select 
                    showSearch
                    allowClear={true}
                    placeholder={SELECT_TXT + '工厂'}
                    optionFilterProp="children"
                    defaultValue={boardArr[0].name} 
                    onChange={(v) => this.getEvent(v)} 
                    // filterOption={this.filterOption}
                    className="m-r5 "
                  >
                    {boardArr.map((v, i) => <Option value={v.company_id.toString()} key={i}>{v.name}</Option>)}
                  </Select>
                ) : null
              }
            </div>
          }  
        >
          
          <Row gutter={16} className="eventWrapper">
            {eventData.length ? eventData.map((v, i) => (
              <Col className={` ${i % 2 === 0 ? ANIMATE.slideInLeft : ANIMATE.slideInRight}`} xs={24} sm={12} md={12} lg={12} xl={12} key={i}>
                <EventBoard refresh={this.updateEvent} eventDetail={this.eventDetail} data={v}></EventBoard>
              </Col>)
            ) : ''}
          </Row>
          {show ? <ProductDialog width={'90%'} handleOk={this.handleOk} close={this.close} title={title} show={show}>
            <div>

                {
                  isDateChange ? <div className='flexCenter'>
                    {
                      eventDate.map((v, i) => {
                        // console.log(' v.initVal ： ', v.initVal, v.key,  )// 
                        return <input className={`antDateInput ${i === 0 ? 'm-r5' : ''}`} type="date" key={v.key} defaultValue={v.initVal} onChange={(e) => this.onChange(e, v.key, )} />
                      })
                    }
                    <Button onClick={() => this.getUpdateEvent(searchParams)} type="primary"  >搜索</Button>
                  </div> : null
                } 
                {
                  isUnLock ? <div className='flexCenter'>
                    {
                      unLockDate.map((v, i) => {
                        return <input className={`antDateInput ${i === 0 ? 'm-r5' : ''}`} type="date" key={v.key} defaultValue={v.initVal} onChange={(e) => this.unLockDateChange(e, v.key, )} />
                      })
                    }
                    <Button onClick={() => this.getunLockEvent(searchParams)} type="primary"  >搜索</Button>
                  </div> : null
                } 

                {
                  eventDetail.length ? <div className='gridWrapper' >
                    <GridTable ref={ref => this.gridTable = ref} isDateChange={isDateChange} isCapcity={isCapcity} fixColmun={eventFixColmun} config={ isCapcity ? this.capcityConfig() : eventConfig(class_id, event_id, ) } data={eventDetail}
                        defaultExportParams={{fileName: event_desc}}
                    ></GridTable>
                    <div className="downloadWrapper">
                      <Button onClick={this.download.bind(this, 'Csv',)} type="primary" icon="download" className="m-l">導出Csv</Button>
                      <Button onClick={this.download.bind(this, 'Excel',)} type="primary" icon="download" className="m-l">導出Excel</Button>
                    </div>
                  </div> : null
                }
                
                {/* <GridTable ref={ref => this.gridTable = ref} fixColmun={eventFixColmun} config={eventConfig(event_id)} data={eventDetail.slice(0, 30)}></GridTable> */}
            </div>

            {/* <EventTable event_id={event_id} data={eventDetail}></EventTable> */}
          </ProductDialog>  : null
        }
        </Collapses>
      </section>
    );
  }
}

// export default Home;
export default SecureHoc(Home)

