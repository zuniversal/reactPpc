import React from "react"
import ReactDOM from 'react-dom';
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { getList, getBarcodeList, getPnDetailList, editBarcode, getFilter,  } from 'api/ppc'
import { getProduct,   } from 'api/productLine'
import { grid, initParams, DATE_FORMAT_BAR, MONTH_FORMAT, FORMAT_DAY, } from 'config'
import { ANIMATE, LETTER,  } from 'constants'
import { backupFn, dateSplit, newDate, toLocale, dateForm, daysLen, dateArrToLon, createRow, setItem, findDOMNode, openNotification, ajax, confirms } from 'util'
// import ppcList from './ppcList.json'
// import DragableBar from '@@@/DragableBar'
import DragableBar from '@@@/DragableSliders'
import ProductDialog from '@@@/ProductDialog'
import DragToggle from '@@@/DragToggles'
import BarcodeContent from '@@@/BarcodeContent'
import Collapses from '@@@/Collapses'
import ScheduleDay from '@@@/ScheduleDay'// 
// import FilterPane from '@@@/FilterPane'
import Loading from '@@@@/Loading'
import * as scheduleAction from 'actions/scheduleNo.js'
import moment from 'moment';
import { Dropdown, Icon, Card, Button, Row, Col, Tooltip, } from 'antd';

class ScheduleNo extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      show: false,
      dateArr: [],
      monthArr: [],
      datas_barcode: [],
      datas: [],
      data_total: [],
      barcodeData: [],
      showBarcode: false,
      showEdit: false,
      loading: false,
      isSpliting: false,
      barcode: '',
      barcodeDate: {},
      pn_no: '',
      showPnDetail: false,
      dialogType: '',
      splitData: [],
      scrollLeftPos: 0,
      scrollTopPos: 0,
      productData: [],
      visible: false,
      customerData: [], 
      styleNoData: [],
      editBarcodeData: {},
      editPnNo: '',
      pn_info: {},
      sideBarScroll: 0,
      changeBarData: [],
      isDragBack: false, 
      colorBar: '',

      colorSizeData: [],
      colorSizeOrigin: [],
      colorSizeDataObj: {},
      colorSizeQtyObj: {},

      colorSizeObjArr: [],
      calcColorSizeTotal: [],
    }
  }
  getList = (p) => {
    this.props.scheduleAction.filterInfo(p)
    this.setState({
      loading: true,
      visible: false,
    })
    getList(p).then(res => {
      // console.log('请求所有的getList ：', ppcList);
      let { datas_barcode, datas, data_total } = res.data
      const today = moment().format(DATE_FORMAT_BAR)
      console.log('data_total ：', data_total, res.data.data_total)
      // datas_barcode = datas_barcode.slice(60, 100)
      // datas_barcode = datas_barcode.slice(0, 50)
      // datas = datas.slice(0, 50)
      // data_total = data_total.slice(0, 50)

      // const { datas_barcode, datas, data_total } = ppcList
      // const data1 = ppcList.data_total;
      if (data_total.length && datas_barcode.length) {
        this.filter()
        const startDate = data_total[0].s_date
        const endtDate = data_total[0].e_date
        const dateArr = []
        const monthes = []
        let a = moment(startDate, DATE_FORMAT_BAR)
        let b = moment(endtDate, DATE_FORMAT_BAR)
        const dayLens = b.diff(a, 'days') + 1
        // console.log('dayLens ：', startDate, startDate.split('-'), endtDate, dayLens, a, b)
        // 得到排单所有天数
        for (let i = 0; i < dayLens; i++) {
          const day = moment(startDate).add(i, 'd')
          const date = day.format(FORMAT_DAY)
          const month = day.format(MONTH_FORMAT)
          const isSunday = day.format('d') === '0'
          const dateOrigin = day.format(DATE_FORMAT_BAR)
          monthes.push(month)
          // console.log('sRDates ：', month, date, isSunday)
          dateArr.push({ month, date, isSunday, dateOrigin });
        }
        // 去重 - 得到排单起止日期 - 包括月份总数组
        const set = new Set(monthes);
        const monthess = new Array(...set);
        const monthArr = monthess.map(v => ({ month: v, date: [] }))
        // console.log(' monthes, dateArr, monthArr：', monthes, monthArr, dateArr, moment(["2017", "1", "01"]));
        monthArr.forEach((v, i) => {
          for (let j = 0; j < dateArr.length; j++) {
            if (dateArr[j].month === v.month) {
              if (dateArr[j].dateOrigin === today) {
                dateArr[j].today = true
              }
              monthArr[i].date.push(dateArr[j])
            }
          }
        })
        datas_barcode.map((item, i) => {
          const shortDay = datas.filter(v => v.barcode === item.barcode)
          // 如果有红色天数数量赋给每个条码
          item.shortDay = shortDay
          // item.isOutDay = moment(item.e_date).isAfter(today)
          this.isOutDay(item, )
          item.shortDay.length && this.calcShortDay(item.shortDay, moment(item.s_date), a)
          this.calcInfo(item, a)
          
          // const width = moment(item.e_date).diff(moment(item.s_date), 'days')
          // const left = moment(item.s_date).diff(a, 'days')
          // // console.log('===222== datas_barcode item ：', left, width, a, moment(item.s_date), moment(item.e_date) )
          // item.startDay = item.s_date
          // item.width = (width + 1) * 30 + 'px'
          // item.left = (left) * 30
          return item
        })
        let level = 0
        let tableHeight = 0
        // 遍历所有的排单批号给所有的条码计算宽度、距离
        data_total.map((item, i) => {// // 
          // console.log('item ：', item, )
          const row = createRow(item.levels)
          tableHeight += row.length
          const data = datas_barcode.filter(v => {
            // if (item.factory_id === v.factory_id) {
            //   v.tops = item.levels
            // }
            return item.factory_id === v.factory_id
          })
          item.row = row
          item.data = data
          item.level = level
          level += item.levels
          return item
        })
        // console.log('1111111111111111 ：', dateArr, monthArr, datas_barcode, datas, data_total )
        this.setState({
          loading: false,
          dateArr,
          monthArr,
          datas_barcode,
          datas,
          data_total,
          // dateArr: [...backupFn(dateArr)],
          // monthArr: [...backupFn(monthArr)],
          // datas_barcode: [...backupFn(datas_barcode)],
          // datas: [...backupFn(datas)],
          // data_total: [...backupFn(data_total)],
          tableHeight: tableHeight * 30 + 75,
          tableWidth: dayLens * 30 + 130
        })
      } else {
        console.log('无数据 ：', )
        // openNotification('沒有數據，請重新選擇過濾條件!', )
        confirms(2, '沒有數據，請重新選擇過濾條件!', )
        this.setState({
          loading: false,
        })
      }
    })
  }
  // 计算每个条码的宽度、距离等数据
  calcInfo = (v, startDay) => {
    const width = moment(v.e_date).diff(moment(v.s_date), 'days')
    const left = moment(v.s_date).diff(startDay, 'days')
    v.startDay = v.s_date
    v.width = (width + 1) * 30 
    v.left = (left) * 30
  }
  // 计算缺工红色日子的距离
  calcShortDay = (v, barStart, startDay) => {
    // console.log('calcShortDay v, startDay ：', v, barStart, startDay)
    v.map(item => item.shortDate = (moment(item.p_date).diff(startDay, 'days') - barStart.diff(startDay, 'days')) * 30)
  }
  showDialog = () => this.setState({ show: true, })
  dragBack = () => {
    const { barcode, barcodeDate, editBarcodeData } = this.state
    const { left, width } = editBarcodeData
    const {s_date_new, e_date_new} = barcodeDate
    // const newWidth = moment(e_date_new.split('-')).diff(s_date_new.split('-'), 'days') * grid + 'px'
      console.log('关闭this.state11 ：', this.state, this.dragBar, dragBar, barcode, left, width)
    const dragBar = findDOMNode(ReactDOM, this[barcode])
      console.log('关闭this.state 22：', dragBar.style, dragBar.style)
    if (dragBar.style.width !== width) {
      console.log('日期变了  宽度变了：', dragBar.style.width, width)
      // dragBar.style.width = width
      // console.log('日期变了  宽度变了2：', dragBar.style.width, width)
    }
    // dragBar.style.transform = `translate(${left}px, 0px)`
    // 把后面的去掉不然批号页面的bar撤销回去会跳到最上面
    dragBar.style.transform = `translate(${left}px, )`
  }
  close = () => {
    const { barcode, showEdit, editBarcodeData } = this.state
    const resetParams = {
      show: false,
      showEdit: false,
      showBarcode: false,
      isDragBack: true, 
      cantChange: false, 
      editContent: '', 
    }
    if (showEdit) {
      this.dragBack()
    } else {
      resetParams.changeBarData = []
    }
    console.log('showEdit resetParams：', showEdit, resetParams)
    this.setState(resetParams);
  }
  // 停止拖拽设置当前操作的bar
  stopDrag = ({x, y, w, barcode, data, pn_no}) => {
    console.log('停止拖拽设置当前操作的bar ：', x, y, w, barcode, data, pn_no)
    const { dateArr } = this.state
    this.setState({
      show: true, 
      showEdit: true, 
      barcodeDate: {s_date_new: dateArr[x].dateOrigin, 
      e_date_new: dateArr[w].dateOrigin},
      editBarcodeData: data,
      editPnNo: pn_no,
      barcode,
    })
    console.log('stopDrag x, y111 ：', data, pn_no, x, y, w, barcode, dateArr, dateArr[x].dateOrigin, dateArr[w].dateOrigin);
  }
  // 点击barcode开始编辑
  showBarcodeDialog = (barcode, pn_no, work_no) => {
    console.log('showBarcodeDialog22', barcode, pn_no, work_no);
    this.setState({ barcode, editPnNo: pn_no, })
    this.getBarcodeList({barcode, work_no})
  }
  // 获取barcode信息请求
  getBarcodeList = (p) => {
    console.log('getBarcodeList ：', p);
    getBarcodeList(p).then(res => {
      console.log('getBarcodeList res ：', res);
      // this.props.scheduleAction.barcode(res.data.datas)
      console.log('[{...res.data.datas, isCopy: false}] ：', [{...res.data.datas[0], isSplitData: false}])
      const {datas, factory} = res.data
      this.setState({
        showBarcode: true, 
        barcodeData: [{...datas[0], isSplitData: false}],
        productData: factory,
        dialogType: 'showBarcode',
        show: true,
      })
      this.showDialog()
    })
  }
  colorSizeHandle = (colorBar,   ) => {
    const {colorSizeObjArr, calcColorSizeTotal,  } = this.state// 
    // console.log(' colorSizeHandle  ： ', this.state, this.props, colorSizeObjArr, colorBar,    )
    const colorSizeData = []
    colorSizeObjArr[colorBar].forEach((v, i) => {
      v.qtys = 0
      const item = colorSizeData.findIndex(item => item.combo_seq === v.combo_seq)
      item > -1 ? colorSizeData[item].data.push(v) : colorSizeData.push({...v, isCheckAll: 'T', data: [v]})
    })
    colorSizeData.forEach((v, i) => {
      v.useQty = 0 
      const matchItem = calcColorSizeTotal.find(item => item.combo_seq === v.combo_seq)
      // console.log(' matchItemsssssss ： ', matchItem,  )// 
      v.qtys = matchItem!= undefined ? matchItem.qtys : v.qtys
      v.sch_qty = matchItem!= undefined ? matchItem.sch_qty : v.qtys
    })
    console.log(' colorSizeQtyObj colorSizeQtyObj, datas, ： ', colorSizeData, )
    return colorSizeData 
  }
  isOutDay = (v) => {
    // console.log(' isOutDay  ： ',  v )
    const today = moment().format(DATE_FORMAT_BAR)
    v.isOutDay = moment(v.e_date).isAfter(today)
  }
  // 获取批号信息请求
  getPnDetailList = (pn_no) => {
    console.log('getPnDetailList ：', pn_no);
    this.setState({pn_no})
    getPnDetailList({pn_no}).then(res => {
      console.log('getPnDetailList res ：', res.data);
      // this.props.scheduleAction.barcode(res.data.datas)
      const {datas, code, pn_info, color, } = res.data
      // if (code === 1) {
        console.log('datas ：', datas)
        const colorSizeData = []
        color.map((v, i) => ({...v, barcode: v.barcode.trim()}))
        .forEach((v, i) => {
          v.qtys = 0
          const item = colorSizeData.findIndex(item => item.barcode === v.barcode)
          item > -1 ? colorSizeData[item].data.push(v) : colorSizeData.push({...v, isCheckAll: 'T', data: [v]})
        })


        const colorSizeDataObj = {}

        datas[0].qty = 0 
        datas[0].color = []
        const colorSizeQtyObj = {}
        colorSizeData.forEach((v, i) => {
          v.useQty = 0 
          v.justShow = true 
          v.data.forEach((item) => {
            v.qtys += item.qty
            v.useQty += item.status === 'T' ? item.qty :  0 
          })// 
          console.log(' ssssssssv.useQty.data ： ', v.useQty, v )
          datas[0].qty += v.useQty
          colorSizeQtyObj[v.barcode] = v.useQty
          colorSizeDataObj[v.barcode] = [v]
        })
        console.log(' ...v.qtys = 0colorSizeData ： ', colorSizeData,  )

        const colorSizeObjArr = {}
        datas.forEach((v) => colorSizeObjArr[v.barcode.trim()] = color.filter(item => v.barcode.trim() === item.barcode.trim()))
        console.log('  colorSizeObjArr ： ', colorSizeObjArr, ) 

        this.setState({
          dialogType: 'showPnCode', 
          barcodeData: datas,
          show: true,
          pn_info,
          
          colorSizeDataObj,
          
          colorSizeOrigin: [...colorSizeData],
          colorSizeQtyObj,
          colorSizeData: [...colorSizeData], 

          colorSizeObjArr,
        })
      // }
    })
  }
  handleOk = () => {
    const {barcodeData, showEdit} = this.state
    console.log('handleOk ：', this.state, barcodeData, showEdit ? '编辑条码' : '拆分')
    if (showEdit) {
      this.editBarcode()
    }
    this.close()
  }
  

  // 拖拽、改变起止日期  确认发送请求
  editBarcode = () => {
    const {barcodeDate, barcode, editBarcodeData, data_total} = this.state
    console.log('editBarcode ：', this.state, barcodeDate, barcode, editBarcodeData)
    editBarcode({...barcodeDate, barcode}).then(res => {
      console.log('editBarcode res ：', res.data, this.state);
      const {code, datas, mes} = res.data
      // openNotification(mes)
      if (code === 1) {
        data_total.map(v => {
          v.data.forEach((item, index) => {
            if (item.barcode === barcode) {
              item.s_date = barcodeDate.s_date_new
              item.e_date = barcodeDate.e_date_new
              this.calcInfo(item, data_total[0].s_date)
              console.log('等于改变起止日期 v ：', item, index, item.barcode, barcode, item.barcode === barcode)
            }
          })
          return v
        })
      }
      this.setState({
        data_total: [...data_total],
        show: false,
      })
    })
  }
  
  lock = (t, v, i) => {
    console.log('lock ：', t, v, i)
    const {barcodeData} = this.state
    barcodeData[i].lock = t === 'F' ? 'T' : 'F'
    console.log('barcodeData ：', barcodeData, barcodeData[i])
    this.setState({
      barcodeData: [...barcodeData]
    })
  }
  // 获取过滤数据
  getFilter = () => {
    getFilter({}).then(res => {
      // console.log('getFilter res ：', res.data);
      const {customer, style_no} = res.data
      this.props.scheduleAction.customerData(customer)
      this.props.scheduleAction.styleNoData(style_no)
      this.setState({
        customerData: customer, 
        styleNoData: style_no,
      })
    })
  }
  filterOption = () => {
    //  console.log('filterOption ：', )
     this.setState({ visible: true });
  }
  handleVisibleChange = (flag, e) => {
    // console.log('handleVisibleChange ：', flag, e)
    this.setState({ visible: flag });
  }
  // 固定显示的角落div
  fixBox = (isPane) => {
    const {customerData, styleNoData, visible} = this.state
    // console.log('fixBox ：',  this.state)
    return (
      <div className="headerSideBarBox">
        <div className="fixBoxItem flexEnd spaceEnd">
          {/* {
            isPane ? <Dropdown overlay={<FilterPane customerData={customerData} styleNoData={styleNoData} 
            handleVisibleChange={this.handleVisibleChange} getData={this.getList} visible={visible}></FilterPane>} 
            trigger={['click']} className='filterOption'
            // onVisibleChange={this.handleVisibleChange} 
              visible={visible}>
              <Button onClick={this.filterOption.bind(this,)} icon='filter' className='salmon' type="primary">過濾選項</Button>
            </Dropdown> : null
          } */}
          <div className='pnDay'>日期</div>
        </div>
        <div className="fixBoxItem pnInfoColumn">批號信息</div>
      </div>
    )
  }
  createDay = (isTxt, isMonth, isLasItem) => {
    const {monthArr} = this.state
    return monthArr.map((v, i) => {
      // console.log(' monthArr v ：', v, ) 
      return <ScheduleDay isTxt={isTxt} isMonth={isMonth} isLasItem={isLasItem} v={v} key={v.month + v.date.length}></ScheduleDay>
    })
    // if (!isMonth) {
    //   return monthArr.map((v, i) => {
    //     // console.log(' monthArr v ：', v, ) 
    //     return <div className="dayWrapper" key={v.month + v.date.length}>
    //       {
    //         v.date.map((item, index) => {
    //           return <div className={`${item.isSunday ? 'isSunday dateItem' : 'dateItem'} ${isLasItem ? 'boldDate' : ''}`} key={item.dateOrigin + v.date.length}>{isTxt ? item.date : null}</div>
    //         })
    //       }
    //     </div>
    //   })
    // } else {
    //   return monthArr.map((v, i) => {
    //     // console.log(' monthArr v ：', v, ) 
    //     return <div className="headerWrapper" key={v.month + v.date.length}>
    //       <div className="monthWrapper">
    //         {isMonth ? <div className={`month ${v.date.length === 1 ? 'oneDate' : ''} ${v.date.length === 2 ? 'twoDate' : ''}`}>{v.month}</div> : null}
    //         <div className="dayWrapper">
    //           {
    //             v.date.map((item, index) => {
    //               // console.log(' item.date item ：', item, ) 
    //               return <div className={`${item.isSunday ? 'isSunday dateItem' : 'dateItem'}`} key={item.dateOrigin + v.date.length}>{isTxt ? item.date : null}</div>
    //             })
    //           }
    //         </div>
    //       </div>
    //     </div>
    //   })
    // }
  }
  createBody = (justSideBar, ) => {
    // console.log('justSideBar ：', justSideBar)
    const {data_total, changeBarData, isDragBack, dateArr, showEdit, } = this.state
    return data_total.map((v, i) => {
      // console.log(' data_total v ：', v, ) 
      return (
        <div className="contentRow" style={{height: v.row.length * grid}} key={v.factory_id}>
          <Tooltip placement="top" title={
            <div className="pnInfoWrapper">
            <div className="pnDay factoryName">{v.factory_name}</div>
            </div>
          }>
            <div className="sideBarBox">
                <div className="pnDay factoryName">{v.factory_name}</div>
            </div>
          </Tooltip>
          {/* {
            v.data.map((item, index) => {
              const { left, width, levels, pn_no, barcode, shortDay, work_name, factory_name, s_date, e_date} = item
              const top = (levels - 1) * 30
              const isLasItem = index === v.row.length - 1 
              // console.log('item ：', item)
              return <div className="pnRow" key={index}>
                {this.createDay(false, false, isLasItem)}
                <DragableBar 
                  isDragBack={isDragBack} 
                  showEdit={showEdit} 
                  changeBarData={changeBarData} 
                  changeBar={this.changeBar} 
                  s_date={s_date} 
                  e_date={e_date} 
                  dateArr={dateArr} 
                  dateLen={dateArr.length} 
                  ref={dragBar => this[barcode] = dragBar} 
                  data={item}  
                  pn_no={pn_no} 
                  showBarcodeDialog={this.showBarcodeDialog} 

                  clickType='pnClick' 
                  getPnDetailList={this.getPnDetailList} 
                  key={index} 
                  isTop={true}
                  isAxis={'x'} 
                  editProduct={this.editProduct} 
                  stopDrag={this.stopDrag} 
                  config={{ left, width, top, txt: pn_no, levels, barcode, shortDay, }} 
                ></DragableBar>
                </div>
            })
          } */}
          {
            justSideBar ? null : (<div className="rowWrapper">
            {
              v.row.map((item, index) => {
                const { left, width, levels, pn_no, barcode, shortDay, work_name, factory_name} = item
                const top = (levels - 1) * 30
                const isLasItem = index === v.row.length - 1 
                // console.log('item ：', item)
                {/* const com = <DragableBar key={index} isAxis={'x'} editProduct={this.editProduct} stopDrag={this.stopDrag} config={{ left, width, top, txt: pn_no, levels, barcode, shortDay, }} key={i}></DragableBar> */}
                return <div className="pnRow" key={index}>{this.createDay(false, false, isLasItem)}</div>
              })
            }
            {
              v.data.map((item, index) => {
                const { left, width, levels, pn_no, barcode, shortDay, work_name, factory_name, s_date, e_date, } = item
                const top = (levels - 1) * 30
                const isLasItem = index === v.row.length - 1 
                // console.log('item ：', v.data, item)
                return <DragableBar 
                  isDragBack={isDragBack} 
                  showEdit={showEdit} 
                  changeBarData={changeBarData} 
                  changeBar={this.changeBar} 
                  s_date={s_date} 
                  e_date={e_date} 
                  dateArr={dateArr} 
                  dateLen={dateArr.length} 
                  ref={dragBar => this[barcode] = dragBar} 
                  data={item}  
                  pn_no={pn_no} 
                  showBarcodeDialog={this.showBarcodeDialog} 

                  clickType='pnClick' 
                  getPnDetailList={this.getPnDetailList} 
                  key={index} 
                  isTop={true}
                  isAxis={'x'} 
                  //disabled={true}
                  editProduct={this.editProduct} 
                  stopDrag={this.stopDrag} 
                  noAction={true}
                  config={{ left, width, top, txt: pn_no, levels, barcode, shortDay, }} 
                ></DragableBar>
              })
            }
          </div>)
        }
      </div>
      )
    })
  }
  editContent = () => {
    console.log('editContent  this.state ：', this.state)
    const {barcode, barcodeDate, editBarcodeData} = this.state
    const {s_date_new, e_date_new} = barcodeDate
    const {startDay, e_date} = editBarcodeData
    console.log(' moment(startDay, DATE_FORMAT_BAR ： ', moment(e_date).format(DATE_FORMAT_BAR),  )
    return <div className="editWrapper">
      <div className="editCon">從 {this.formatDay(startDay)} 到 {this.formatDay(e_date)} 變爲 {this.formatDay(s_date_new)} 到 {this.formatDay(e_date_new)}</div>
    </div>
  }
  formatDay = (d, f = DATE_FORMAT_BAR) => moment(d).format(f)

  changeBar = (data) => {
    console.log('changeBar  data ：', data)
    this.setState({
      changeBarData: data,
      isDragBack: false,
      barcode: data.barcode,
    })
  }
  filter = () => {
    this.setState({
      dateArr: [],
      monthArr: [],
      datas_barcode: [],
      datas: [],
      data_total: [],
    })
  }
  sideBarScroll = (scrollTop) => {
    const domRef = findDOMNode(ReactDOM, this.con)
    console.log('++++++++++++++++  sideBarScroll ：', scrollTop, domRef)
    domRef.scrollTop = scrollTop
    this.setState({sideBarScroll: scrollTop,})
  }
  sideScroll = (scrollTop) => {
    const domRef = findDOMNode(ReactDOM, this.dragToggle.sideBar)
    // console.log('++++++++++++++++  sideScroll ：', scrollTop, domRef, this.dragToggle.sideBar )
    domRef.scrollTop = scrollTop// 
  }
  dialogContent = () => {
    console.log('  dialogContent ：',   )
    const { 
      pn_info, productData, splitData, dialogType, isSpliting, barcodeData, showEdit, 
      colorSizeDataObj,
      colorSizeObjArr,
    } = this.state
    return showEdit ? this.editContent() : <BarcodeContent 
      pn_info={pn_info} 
      productData={productData} 
      splitData={splitData} 
      // dialogType={dialogType} 
      isShowBarcode={dialogType === 'showBarcode'}
      lock={this.lock} 
      isSpliting={isSpliting} 
      barcodeData={barcodeData}
      // colorSizeDataObj={colorSizeDataObj}
      
      colorSizeDataObj={colorSizeObjArr}
      colorSizeHandle={this.colorSizeHandle}
    ></BarcodeContent>
  }
  componentDidMount() {
    this.getList(initParams)
    this.getFilter()
    const domRef = findDOMNode(ReactDOM, this.con)
    domRef.onscroll = (e) => {
      // console.log('ScheduleNo 组件 this.state, this.props ：', this.state, this.props, domRef)
      const {scrollLeft, scrollTop} = domRef
      const {scrollLeftPos, scrollTopPos, datas_barcode, sideBarScroll, tableHeight, tableWidth} = this.state
      // domRef.scrollTop = sideBarScroll//没关滚会卡
      const time = datas_barcode.length > 500 ? null : 50
      if (scrollLeftPos !== scrollLeft) {
        // console.log('横向滚动 ：', scrollLeft)
        ajax(this.setState.bind(this), {
          scrollLeftPos: scrollLeft,
        }, time)
        // this.setState({
        //   scrollLeftPos: scrollLeft,
        // })
      } 
      else if (scrollTopPos !== scrollTop) {
        this.sideScroll(scrollTop)
      //   // console.log('纵向滚动 ：', scrollTop, tableWidth / tableHeight)
      //   ajax(this.setState.bind(this), {
      //     scrollTopPos: scrollTop,
      //     // scrollLeftPos: 1000,
      //   }, time)
      //   domRef.scrollLeft = scrollTop * tableWidth / tableHeight * 1.15
      //   // this.setState({
      //   //   scrollTopPos: scrollTop,
      //   // })
      }
      // console.log('!!!@@@@@@@222chatingWrapper Pos：', e, domRef.scrollLeft, domRef.scrollTop, domRef.scrollHeight)
    }
  }
  componentWillReceiveProps(nextProps) {
    // console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ScheduleNo componentWillReceiveProps组件获取变化 ：', nextProps.filterInfo.isSearch, this.props.filterInfo.isSearch, nextProps, this.props, this.state,  )
    if (nextProps.filterInfo.isSearch) {
      this.getList(nextProps.filterInfo)
      this.props.scheduleAction.filterInfo({...nextProps.filterInfo, isSearch: false})
    } 
  }
  
  render() {
    const {collapse,} = this.props
    const { datas_barcode,show, tableHeight, tableWidth, showEdit, data_total,
      loading, barcodeData, isSpliting, dialogType, splitData, pn_no, pn_info,
      scrollLeftPos, scrollTopPos, productData, barcode, barcodeDate, 
      colorSizeDataObj,
    } = this.state
    // const {barcodeData} = this.props.scheduleNo// 
    const title = dialogType === 'showPnCode' ? `批號 ${pn_no} 詳情` : showEdit ? `是否確認修改條碼 ${barcode} 的起止時間` : `碼詳情`
    // const content = showEdit ? this.editContent() : <BarcodeContent pn_info={pn_info} productData={productData} splitData={splitData} isShowBtn={dialogType === 'showBarcode'} dialogType={dialogType} delete={this.delete} lock={this.lock} isSpliting={isSpliting} inputing={this.inputing} splitAction={this.splitAction} barcodeData={barcodeData}></BarcodeContent>
    console.log('————————ScheduleNo 组件 this.state, this.props：', this.props, this.state, )
    const headerCom = datas_barcode.length ? (
      <div className="headerContainer">
          {this.fixBox()}
          {this.createDay(true, true)}
      </div>) : null

    return (
      // <Collapses title={'工艺路线'} className='scheduleCollapse'  isAnimate={false}>
        <div className={`noWrapper ${ANIMATE.fadeIn}`} ref={con => this.con = con}>
          {
            data_total.length ? (
            <DragToggle ref={dragToggle => this.dragToggle = dragToggle} sideBarScroll={this.sideBarScroll} scrollLeftPos={scrollLeftPos} scrollTopPos={scrollTopPos} collapse={collapse} tableHeight={tableHeight} tableWidth={tableWidth}>
              {/* 固定头0 */}
              {/* <div className="fixHeaderWrapper"> */}
                {/* <div className="fixHeader"> */}
                  {headerCom}
                {/* </div> */}
              {/* </div> */}

              {/* 固定列1 */}
              <div className="fixSideBar">
                {this.fixBox()}
                {this.createBody(true)}
              </div> 
              {/* 主体 */}
              {headerCom}
              {/* 主体2 */}
              {this.createBody(false)}

              {/* 固定显示的角落div */}
              {this.fixBox(true)}
            </DragToggle>)
          　: null
          }
          {show ? <ProductDialog handleOk={this.handleOk} width={showEdit ? '40%' : '90%'} show={show} close={this.close} title={title}>
            {this.dialogContent()}
          </ProductDialog> : null}
          <Loading loading={loading}></Loading>
        </div>
      // </Collapses>
    );
  }
}

const state = state => state
const action = action => {
  return {
    scheduleAction: bindActionCreators(scheduleAction, action),
  }
}
export default connect(state, action)(ScheduleNo);

