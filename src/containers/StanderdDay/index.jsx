import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getStanderdDay, addWorkdays, updateWorkdays, deleteWorkdays,  } from 'api/StanderdDay'
import { getProcedure, } from 'api/Procedure'
import {standerDayObj, unitFilters, } from 'config'
import { ANIMATE, SELECT_TXT, defaultWorkNo, } from 'constants'
import {openNotification, backupFn, confirms, } from 'util'
import Collapses from '@@@/Collapses'
import ProductDialog from '@@@/ProductDialog'
import StanderdDayTable from '@@@/StanderdDayTable'
import EditInfo from '@@@@/EditInfo'
import SecureHoc from '@@@@/SecureHoc'
import { Button, Icon, Select, message } from 'antd';
const Option = Select.Option;

class StanderdDay extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      standerdDayData: [],
      procedureData: [],
      editData: {},
      editOrigin: {},
      show: false,
      work_no: defaultWorkNo,
      work_name: '',
      isNew: false,
      isAdd: false,
      typeFilters: [],
      addData: {},
      // unitFilters: [],
    }
  }
  showDialog = () => this.setState({ show: true, })
  // showDialog = () => this.handleOk()
  cancelData = () => {
    console.log('cancelData ：', this.state)
    const {standerdDayData, editOrigin, isAdd, addData, } = this.state
    const {seq_no, } = editOrigin
    const indexes = standerdDayData.findIndex(v => v.seq_no === seq_no)
    console.log('indexes ：', indexes);
    console.log('修改后的数据standerdDayData ：', standerdDayData, );
    if (isAdd) {
      const addItem = standerdDayData.findIndex(v => v.seq_no === addData.seq_no)
      console.log('改变增加addItem ：', addItem);
      standerdDayData.splice(addItem, 1)
    } else {
      console.log('改变修改 ：', );
      standerdDayData[indexes] = editOrigin
    }
    console.log('standerdDayData ：', standerdDayData)
    this.setState({
      standerdDayData: [...standerdDayData],
      editData: {},
      show: false,
      isAdd: false,
      isNew: false,
    })
  }
  close = () => {
    console.log('取消 ：', )
    this.cancelData()
  }
  // close = () => {
  //   console.log('取消 ：', )
  //   this.setState({
  //     show: false,
  //   })
  // }
  selectChange = (i) => {
    console.log(`selectChange`, i);
  }
  filterOption = (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
  editContent = () => {
    const {editData, } = this.state
    console.log('editContent ：', editData, )
    return <EditInfo/>
  }
  addDataAction = (t, v, i) => {
    console.log('增加操作 ', t, v, i, this.props, this.state);
    const {standerdDayData, editData, isNew, work_name, work_no, } = this.state
    if (Object.keys(editData).length || isNew) {
      confirms(2, '請先保存上一條數據再繼續操作！', );
    } else {
      const matchItem = standerdDayData.findIndex(item => item.seq_no === v.seq_no)
      console.log('matchItem ：', matchItem)
      const addData = {
        ...standerDayObj, 
        qty_fr: matchItem !== 0 ? standerdDayData[matchItem - 1].qty_fr + 1 : 0, 
        qty_to: standerdDayData[matchItem].qty_to - 1,
        work_name, 
        work_no,
        index: matchItem - 1, 
        seq_no: -1, 
        unit: v.unit,
        newing: true,
      }
      standerdDayData.splice(matchItem, 0, addData)
      console.log('matchItem ：', matchItem, standerdDayData, )
      this.setState({
        standerdDayData: [...standerdDayData, ],
        isNew: true,
        isAdd: true,
        editData: standerdDayData[matchItem + 1],
        // addData: standerdDayData[matchItem + 1]
        addData
      })
    }
  }
  editDataAction = (v) => {
    console.log('编辑 ', v, this.props, this.state);
    const {standerdDayData, editData, isNew, } = this.state
    if (Object.keys(editData).length || isNew) {
      confirms(2, '請先保存上一條數據再繼續操作！', );
    } else {
      let editData 
      standerdDayData.forEach(item => {
        if (item.seq_no === v.seq_no)  {
          console.log('匹配的 standerdDayData item ：', item, ) 
          item.editable = true
          editData = item
        }
      })
      this.setState({
        standerdDayData: [...standerdDayData],
        editData: {...editData,},
        editOrigin: backupFn({...editData, editable: false}),
      })
    }
  }
  newStanderdDay = () => {
    console.log('newStanderdDay ：', )
    const {standerdDayData, work_no, work_name, isNew, editData, } = this.state
    // if (Object.keys(editData).length) {
      // openNotification('請先保存上一條數據再進行新增！')
      // standerdDayData.forEach(item => {
      //   if (item.seq_no === editData.seq_no)  {
      //     console.log('匹配的 standerdDayData item ：', item, ) 
      //     item.editable = false
      //   }
      // })
      // this.setState({
      //   standerdDayData: [...standerdDayData, ],
      //   editData: {},
      //   editOrigin: {},
      // })
      this.cancelData()
    // } else {
      if (isNew) {
        confirms(2, '您已新增了一個日期，請先進行保存再新增！', );
      } else {
        const lastItem = standerdDayData[standerdDayData.length - 1]
        console.log('lastItem ：', lastItem, this.state, standerDayObj, standerdDayData);
        const qty_fr = lastItem != undefined ? lastItem.qty_to : 0
        const seq_no = lastItem != undefined ? lastItem.seq_no : 0
        const editData = {
          ...standerDayObj, 
          qty_fr: qty_fr + 1, 
          qty_to: qty_fr + 2, 
          work_no,
          work_name, 
          editable: true, 
          unit: lastItem.unit,
          index: standerdDayData.length, 
          seq_no: seq_no + 1, 
        }
        this.setState({
          standerdDayData: [...standerdDayData, editData],
          editData,
          addData: editData,
          isNew: true,
        })
      }
    // }
  }
  handleOk = () => {
    const {isNew, } = this.state
    console.log('handleOk ：', isNew, this.state)
    // if (isNew) {
    //   console.log('新增 ：', )
    //   this.beforeEditData()
    // } else {
    //   this.beforeEditData()
    //   // const {editData, } = this.state
    //   // console.log('编辑 ：', editData)
    //   // this.updateWorkdays(editData)
    // }
    this.beforeEditData()
  }
  beforeSaveData = () => {
    console.log('beforeSaveData 11', this.props, this.state);
    const {editData, addData, standerdDayData} = this.state
    const {qty_fr, qty_to, work_days} = addData
    const matchItem = standerdDayData.findIndex(item => item.seq_no === addData.seq_no)
    console.log('addData ：', addData, qty_fr, qty_to, addData.seq_no, matchItem, standerdDayData[matchItem - 1], standerdDayData[matchItem]);
    if (addData.unit === 'null') {
      addData.unit = 'W'
    }
    let isIlleagal = false
    let mes = ''
    if (qty_to < qty_fr) {
      isIlleagal = true
      mes = '標准天數最大數量不能小于最小數量！'
    }
    if (work_days <= 0) {
      isIlleagal = true
      mes = '標准天數不能小于等于0！'
    } 
    console.log('isIlleagal ：',  isIlleagal);
    if (isIlleagal) {
      confirms(2, mes, );
    } else {
    //   console.log('没错可以编辑 ：', editData, )
      // this.addWorkdays({...editData, seq_no: addData.seq_no})
      // const {seq_no, ...editDatas, } = editData
      // this.addWorkdays(editDatas)
      // this.addWorkdays(editDatas)
    //   // this.addData(addData)
    }
    const {seq_no, ...editDatas, } = editData
    console.log('seq_no ：', this.state, editData, seq_no)
    this.addWorkdays({...addData, seq_no})
  }
  beforeEditData = () => {
    console.log('beforeEditData 11', this.props, this.state);
    const {standerdDayData, editData, isNew, addData, isAdd} = this.state
    const {qty_fr, qty_to, seq_no, work_days, } = isAdd ? addData : editData 
    const matchItem = standerdDayData.findIndex(item => item.seq_no === seq_no)
    console.log('editData ：', editData, qty_fr, qty_to, seq_no, work_days, isAdd, matchItem);
    let isIlleagal = false
    let mes = ''
    if (work_days <= 0) {
      isIlleagal = true
      mes = '標准天數不能小于等于0！'
    } else {
      if (qty_to < qty_fr) {
        isIlleagal = true
        mes = '標准天數最大數量不能小于最小數量！'
      } else {
        if (standerdDayData.length > 1) {
          if (seq_no !== 1 && seq_no !== standerdDayData.length) {
            isIlleagal = qty_fr < standerdDayData[matchItem - 1].qty_fr + 1
            console.log('不是第一项：也不是最后一个：', seq_no, isIlleagal, qty_fr, standerdDayData[matchItem - 1])
            if (isIlleagal) {
              mes = '標准天數最小數量不能小于上一個最小數量！'
            } else {
              isIlleagal = qty_to > standerdDayData[matchItem + 1].qty_to - 1
              console.log('最大数量isIlleagal ：', isIlleagal, qty_to, standerdDayData[matchItem + 1])
              if (isIlleagal) {
                mes = '標准天數最大數量不能大于下一個最大數量！'
              }
            }
          }
          if (seq_no === 1) {
            isIlleagal = qty_to > standerdDayData[matchItem + 1].qty_to - 1
            console.log('第一项1  ：', isIlleagal, qty_to, standerdDayData[matchItem + 1], )
            if (isIlleagal) {
              mes = '標准天數最大數量不能大于下一個最大數量！'
            } 
          } else if (seq_no === standerdDayData.length) {
            isIlleagal = qty_fr < standerdDayData[matchItem - 1].qty_fr + 1
            console.log('最后一项 ：', seq_no, isIlleagal, qty_fr, standerdDayData[matchItem - 1] ,)
            if (isIlleagal) {
              mes = '標准天數最小數量不能小于上一個最小數量！'
            } 
          }
        } 
      }
    }
    console.log('isIlleagal ：',  isIlleagal);
    if (isIlleagal) {
      confirms(2, mes, );
      this.setState({
        show: false, 
      })
    } else {
      console.log('没错可以编辑 ：', editData, )
      if (isAdd || isNew) {
        console.log('新增 ：', {...addData, seq_no: editData.seq_no})
        this.addWorkdays({...addData, seq_no: editData.seq_no})
      } else {
        this.updateWorkdays(editData)
      }
    }
  }
  addWorkdays = (v, ) => {
    console.log('addWorkdays v 111111：', v, );
    addWorkdays(v).then(res => {
      console.log('addWorkdays res ：', res.data);
      const {code, data, mes, } = res.data
      // message.success(mes, );
      if (code === 1) {
        const {standerdDayData, editData, addData} = this.state
        // const {seq_no, } = addData
        // data[1].editable = false
        // const matchItem = standerdDayData.findIndex(item => item.seq_no === seq_no)
        // console.log('addWorkdays standerdDayData ：',standerdDayData[matchItem], standerdDayData)
        // standerdDayData[matchItem] = data[1]
        // this.dataHandle(standerdDayData, matchItem, data[1])
        this.setState({
          standerdDayData: [...data, ],
          editData: {},
          addData: {},
          show: false,
          isNew: false,
          isAdd: false,
        })
      }
    })
  }
  dataHandle = (standerdDayData, matchItem, data) => {
    console.log('dataHandle standerdDayData  ：', standerdDayData, matchItem, data )
    standerdDayData[matchItem] = data
    if (data.seq_no !== 1 && data.seq_no !== standerdDayData.length) {
      console.log('不是第一项：也不是最后一个：', )
      standerdDayData[matchItem - 1].qty_to = data.qty_fr - 1
      standerdDayData[matchItem + 1].qty_fr = data.qty_to + 1
    }
    if (standerdDayData.length > 1) {
      if (data.seq_no === 1) {
        console.log('是第一项：', )
        standerdDayData[matchItem + 1].qty_fr = data.qty_to + 1
      } 
      if (data.seq_no === standerdDayData.length) {
        console.log('是最后一个：', )
        standerdDayData[matchItem - 1].qty_to = data.qty_fr - 1
      }
    }
  }
  updateWorkdays = (v, ) => {
    console.log('updateWorkdays v ', v, );
    updateWorkdays(v).then(res => {
      console.log('返回编辑结果updateWorkdays res ：', res.data, this.state);
      const {code, mes, data} = res.data
      // confirms(code, mes, )
      if (code === 1) {
        const {editOrigin, standerdDayData, } = this.state
        const {qty_fr, qty_to, unit, work_days, seq_no, } = data[0]
        const matchItem = standerdDayData.findIndex(item => item.seq_no === seq_no)
        console.log('standerdDayData[matchItem - 1],  ：', matchItem, standerdDayData[matchItem - 1], standerdDayData[matchItem + 1]);
        this.dataHandle(standerdDayData, matchItem, data[0])
        // standerdDayData[matchItem] = data[0]
        // if (seq_no !== 1 && seq_no !== standerdDayData.length) {
        //   console.log('不是第一项：也不是最后一个：', seq_no)
        //   standerdDayData[matchItem - 1].qty_to = qty_fr - 1
        //   standerdDayData[matchItem + 1].qty_fr = qty_to + 1
        // }
        // if (seq_no === 1) {
        //   console.log('是第一项：', seq_no)
        //   standerdDayData[matchItem + 1].qty_fr = qty_to + 1
        // } 
        // if (seq_no === standerdDayData.length) {
        //   console.log('是最后一个：', seq_no)
        //   standerdDayData[matchItem - 1].qty_to = qty_fr - 1
        // }
        console.log('standerdDayData ：', standerdDayData);
        this.setState({
          standerdDayData: [...standerdDayData],
          editData: {},
          show: false,
          isAdd: false,
        })
      }
    })
  }
  editData = (data) => {
    console.log('editData213 ：', data, this.state)
    const {standerdDayData, isAdd } = this.state
    const {v, keys, seq_no, } = data
    if (keys === 'unit') {
      console.log('改变单位 ：', )
      standerdDayData[keys] = v
    } else {
      console.log('改变其他 ：', )
      standerdDayData[keys] = Number(v)
    }
    let editData
    standerdDayData.forEach(item => {
      if (item.seq_no === seq_no)  {
        editData = item
        console.log('匹配的 standerdDayData item ：', item, ) 
        if (keys === 'unit') {
          console.log('改变单位 ：', )
          item[keys] = v
        } else {
          console.log('改变其他 ：', )
          item[keys] = Number(v)
        }
      }
      return item
    })
    const newState = {
      standerdDayData: [...standerdDayData],
    }
    if (isAdd) {
      newState.addData = {...editData,}
    } else {
      newState.editData = {...editData,}
    }
    console.log('standerdDayData ：', standerdDayData, this.state, newState)
    this.setState(newState)
  }
  deleteData = (item) => {
    console.log('deleteData ：', item)
    const {qty_fr, qty_to, seq_no, } = item
    const {standerdDayData,  } = this.state
    const newData = standerdDayData.filter(v => v.seq_no !== seq_no)
    console.log('删除后的数据standerdDayData ：', standerdDayData, newData);
    console.log('editData ：', qty_fr, qty_to, );
    deleteWorkdays(item).then(res => {
      console.log('deleteWorkdays res ：', res.data);
      const {code, mes, data} = res.data
      // confirms(code, mes, );
      if (code === 1) {
        const changeArr = []
        standerdDayData.map((v, i) => {
          if (v.seq_no === seq_no)  {
            console.log('匹配的 standerdDayData v ：', v, v.seq_no, seq_no) 
            v.editable = false
            // if (i !== 0) {
            //   changeArr.push({...standerdDayData[i - 1], qty_to: standerdDayData[i + 1].qty_fr - 1}, v, {...standerdDayData[i + 1], qty_fr: standerdDayData[i - 1].qty_to + 1})
            // } else {
            //   changeArr.push(v, {...standerdDayData[i + 1],})
            // }
            console.log('changeArr ：', changeArr);
          }
          return v
        })
        this.setState({
          canDelete: true,
        })
        // this.timer = setTimeout(() => this.setState({standerdDayData: [...newData], }), 500);
        this.timer = setTimeout(() => this.setState({standerdDayData: [...data], }), 500);
      }
    })
  }
  
  getProcedure = () => {
    getProcedure({}).then(res => {
      console.log('getProcedure res ：', res.data);
      const {data, } = res.data
      this.setState({
        procedureData: data,
        // work_no: data[0].work_no,
        work_name: data[0].work_name,
      })
    })
  }
  
  getStanderdDay = (work_no = defaultWorkNo) => {
    console.log('work_no ：', work_no);
    getStanderdDay({work_no,}).then(res => {
      console.log('getStanderdDay res ：', res.data);
      const {typeFilters, unitFilters, } = this.state
      const typeArr = [], unitArr = []
      res.data.data.map((v, i) => {
        v.index = i
        v.editable = false
        const {type_id, unit, } = v
        typeArr.push(type_id)
        // unitArr.push(unit)
        return v
      })
      const typeArrs = new Array(...new Set(typeArr));
      // const unitArrs = new Array(...new Set(unitArr));
      typeArrs.forEach((v, i) => typeFilters.push({text: v, value: v,}))
      // unitArrs.forEach((v, i) => unitFilters.push({text: v, value: v,}))
      this.setState({
        standerdDayData: res.data.data.filter(v => {
          // console.log(' res.data.data filter v ：', v)
          return v.seq_no !== -1 
        }),
        work_no: Number(work_no),
        work_name: res.data.data[0].work_name,
        typeFilters: [...typeFilters],
        editData: {},
        editOrigin: {},
        isNew: false,
        // unitFilters: [...unitFilters],
        // standerdDayData: res.data.data.slice(0, 12),
      })
    })
  }
  componentDidMount() {
    this.getStanderdDay()
    this.getProcedure()
  }
  componentWillUnmount() {
    console.log('StanderdDay 组件卸载 ：', this.props, this.state )
    this.timer && clearTimeout(this.timer)
  }
  render() {
    console.log('StanderdDay 组件this.state, this.props ：', this.state, this.props);
    const { standerdDayData, procedureData, show, work_no, work_name, canDelete, isNew, typeFilters,  } = this.state
    const title = '提示框'
    const {auth, } = this.props 
    const {middle,  } = this.props.route

    return (
      <Collapses noLimit={true} animate={ANIMATE.fadeIn} title={'標准天數'} className="standerdDay"
        middle={middle}
        extra={
          <div className="btnWrapper">
            {
              procedureData.length ? (
                <Select 
                  showSearch
                  allowClear={true}
                  placeholder={SELECT_TXT + '工序'}
                  optionFilterProp="children"
                  defaultValue={work_name} 
                  onChange={this.getStanderdDay} 
                  filterOption={this.filterOption}
                  className="m-r5 selectMin"
                >
                  {procedureData.map((v, i) => <Option value={v.work_no.toString()} key={i}>{v.work_name}</Option>)}
                </Select>
              ) : null
            }
            {
              auth ? <Button onClick={this.newStanderdDay.bind(this, )} type="primary" icon='plus-circle-o'>新增標准天數</Button> : null 
            }
          </div>
        }
           
      >
        {
          work_no !== -1 ? <StanderdDayTable 
            unitFilters={unitFilters} 
            typeFilters={typeFilters} 
            isDisable={isNew} 
            canDelete={canDelete} 
            deleteData={this.deleteData} 
            work_no={work_no} 
            editData={this.editData} 
            procedureData={procedureData} 
            addDataAction={this.addDataAction} 
            editDataAction={this.editDataAction} 
            showDialog={this.showDialog}
            data={standerdDayData}
            cancelData={this.cancelData} 
            middle={middle}
          ></StanderdDayTable> : null
        }
        {show ? <ProductDialog className='corfirmModal' handleOk={this.handleOk} width={'45%'} show={show} close={this.close} title={title}>{this.editContent()}</ProductDialog> : null}
      </Collapses>
    );
  }
}

// export default StanderdDay;
export default SecureHoc(StanderdDay)
