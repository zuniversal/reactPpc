import React from "react"
import ReactDOM from 'react-dom';
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { getPnList, getList, getBarcodeList, getPnDetailList } from 'api/ppc'
import { ANIMATE,  } from 'constants'
import ppcList from './ppcList.json'
import DragableBar from '@@@/DragableBar'
import ProductDialog from '@@@/ProductDialog'
import DragToggle from '@@@/DragToggles'
import BarcodeContent from '@@@/BarcodeContent'
import Loading from '@@@@/Loading'
import { grid } from 'config'
import Draggable from 'react-draggable'
import { backupFn, dateSplit, newDate, toLocale, dateForm, daysLen, dateArrToLon, ajax, findDOMNode } from 'util'
import * as scheduleAction from 'actions/scheduleNo.js'
import { Icon, Card, Button, Row, Col, notification, Tooltip, } from 'antd';
class PnNo extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      show: false,
      dateArr: [],
      monthArr: [],
      datas_barcode: [],
      datas: [],
      data_total: [],
      dragBox: {
        width: 0,
        left: '130px',
        top: '75px',
      },
      showBarcode: false,
      showEdit: false,
      loading: false,
      barcode: '',
      pn_no: '',
      showPnDetail: true,
      dialogType: '',
      scrollLeftPos: 0,
      scrollTopPos: 0,
    }
  }
  getList = () => {
    this.setState({
      loading: true,
    })
    getList().then(res => {
    console.log('请求所有的getList ：', ppcList, res);

    // const { datas_barcode, datas, data_total } = ppcList
    // const data1 = ppcList.data_total;
    const { datas_barcode, datas, data_total } = res.data
    const data1 = res.data.data_total;

    datas.map(v => {
      // console.log('11111 ：',v, dateForm(v.s_date), dateForm(v.p_date), daysLen(newDate(dateSplit(v.s_date)), newDate(dateSplit(v.p_date))));
      v.shortDate = daysLen(newDate(dateSplit(v.s_date)), newDate(dateSplit(v.p_date))) * grid
      v.pDate = dateForm(v.p_date)
      return v
    })

    //日期格数
    var sArr = dateSplit(data1[0].s_date)
    var eArr = dateSplit(data1[0].e_date)
    var sRDate = newDate(sArr)
    var eRDate = newDate(eArr)
    var days = daysLen(sRDate, eRDate)
    var dateArr = []
    var obj = []
    var month = []
    console.log('days ：', days);
    for (var i = 0; i < days; i++) {
      const sRDates = new Date(sRDate - 0 + i * 86400000)
      var date = toLocale(sRDates)
      // console.log('sArr ：', sArr, sRDate, days, sRDates, date);
      const sliceDate = date.substring(0, date.lastIndexOf('/'))
      month.push(sliceDate)
      // console.log('sliceDate ：',date, sliceDate, date.lastIndexOf('/'));
      dateArr.push({ month: sliceDate, date: date.split('/')[2], dateOrigin: date, isSunday: sRDates.getDay() === 0 });
    }
    const set = new Set(month);
    const monthes = new Array(...set);
    //
    const monthArr = monthes.map(v => ({ month: v, date: [] }))
    // console.log(' monthes, dateArr, monthArr：', monthes, monthArr);
    // console.log('date ：',sRDate, monthes, dateArr, monthArr.length, datas_barcode);
    monthArr.forEach((v, i) => {
      for (var j = 0; j < dateArr.length; j++) {
        // console.log('dateArr[j].date ：', dateArr[j].dateOrigin);
        if (dateArr[j].dateOrigin.includes(v.month)) {
          // console.log('$$$ monthes v ：', dateArr[j], v, dateArr[j].dateOrigin.includes(v.month)) 
          monthArr[i].date.push(dateArr[j])
        }
        // var month = 'M' + dateArr[j].dateOrigin.split('/')[1];
        // if(!obj[month]) {
        //   obj[month] = [];
        // }
        // obj[month].push(dateArr[j].dateOrigin);
      }
    })
    // datas_barcode.forEach((item, i) => {
    //   console.log('%%datas_barcode item ：', item, )

    // })
    const proDay = data_total[0].s_date.split('T')[0].split("-")
    const proParseDay = new Date(proDay[0], proDay[1] - 1, proDay[2]);
    const proStartDay = proParseDay.toLocaleDateString()
    // datas_barcode.map(v => v.shortDay = [])
    // console.log('ppcList.data_total[0].s_date. ：', ppcList.data_total[0].s_date, proParseDay, proStartDay);
    datas_barcode.map((item, i) => {
      const shortDay = datas.filter(v => v.barcode === item.barcode)
      if (shortDay.length !== 0) {
        // console.log('item.shortDay ：', item.shortDay, shortDay);
        item.shortDay = shortDay
      }
      const sArr = dateSplit(item.s_date)
      const eArr = dateSplit(item.e_date)
      const sRDate = newDate(sArr)
      const eRDate = newDate(eArr)
      const startDay = new Date(sRDate - 0 + i * 86400000).toLocaleDateString()
      const endDay = new Date(eRDate - 0 + i * 86400000).toLocaleDateString()
      const width = daysLen(sRDate, eRDate)
      const left = daysLen(proParseDay, sRDate)
      // console.log('======== datas_barcode item ：', sRDate, proParseDay, proStartDay,startDay, endDay,  left, width)
      item.startDay = startDay
      item.endDay = endDay
      item.width = (width - 1) * 30 + 'px'
      item.left = (left - 1) * 30
      return item
    })
    // console.table(datas)
    let level = 0
    let tableHeight = 0
    data_total.map((item, i) => {
      // console.log('##data_total item ：', item.levels, )
      // const data = datas_barcode.filter(v => {
      //   console.log('$$data_todatas_barcodetal v ：', item, item.levels, v.levels, )
      //   return item.levels === v.levels
      // })
      const row = this.createArr(item.levels)
      tableHeight += row.length
      const data = datas_barcode.filter(v => {
        if (item.factory_id === v.factory_id) {
          v.tops = item.levels
          // console.log('$$data_todatas_barcodetal v ：',v.tops )
        }
        // console.log('$$data_todatas_barcodetal v ：', v, item,v.tops,v.levels,  item.factory_id, v.factory_id, )
        return item.factory_id === v.factory_id
      })
      // console.log('levels v ：',item.levels )
      // const datas = datas_barcode.map(v => {
      //   if (item.factory_id === v.factory_id) {
      //     // v.tops = item.levels
      //     // console.log('$$data_todatas_barcodetal v ：',v.tops )
      //   console.log('$$data_todatas_barcodetal v ：', v, item,v.tops,v.levels,  item.factory_id, v.factory_id, )
      //   return v.tops = item.levels
      //   }
      // })
      // console.log('tableHeight ：', tableHeight);
      item.row = row
      item.data = data
      item.level = level
      level += item.levels
      return item
    })
    this.setState({
      loading: false,
      dateArr,
      monthArr,
      datas_barcode,
      datas,
      data_total,
      dragBox: {
        width: dateArr.length * 30 + 'px',
        left: '130px',
        top: '75px',
      },
      tableHeight: tableHeight * 30 + 75,
      tableWidth: days * 30 + 130
      // config: {
      //   width: dateArr.length * 30 + 'px',
      //   left: '130px',
      //   top: '75px',
      // }
    })
    // console.log('obj ：', obj, monthes, monthArr);
    // console.log('dateArr ：', dateArr, );
    // })
    })
  }
  createArr = (num) => {
    const arr = []
    for (let i = 0; i < num; i++) {
      // console.log('i ：', i)
      arr.push(i)
    }
    return arr
  }
  newLevel = (i) => {
    const { data_total } = this.state
    data_total[i].levels += 1
    data_total[i].row.push(data_total[i].row.length)
    console.log('newLevel ：', i, data_total, data_total[i]);
    this.setState({
      data_total: [...data_total]
    })
  }
  showDialog = () => this.setState({ show: true, })
  close = () => {
    this.setState({
      show: false,
      showEdit: false,
      showBarcode: false,
    });
  }
  stopDrag = (x, y, d, b) => {
    const { dateArr } = this.state
    // this.showModal()
    console.log('stopDrag x, y ：', x, y, d, b, dateArr, dateArr[x].dateOrigin, dateArr[d].dateOrigin);
  }
  editProduct = (barcode) => {
    console.log('editProduct', barcode);
    this.getBarcodeList(barcode)
  }
  getBarcodeList = (barcode) => {
    console.log('getBarcodeList ：', barcode);
    this.setState({barcode})
    
    getBarcodeList({barcode}).then(res => {
      console.log('getBarcodeList res ：', res);
      // this.props.scheduleAction.barcode(res.data.datas)
      console.log('[{...res.data.datas, isCopy: false}] ：', [{...res.data.datas[0], isSplitData: false}])
      
      // this.setState({
      //   showBarcode: true, 
      //   barcodeData: [{...res.data.datas[0], isSplitData: false}],
      //   dialogType: 'showBarcode',
      //   show: true,
      // })
      // this.showDialog()
    })
  }
  getPnDetailList = (pn_no) => {
    console.log('getPnDetailList ：', pn_no);
    this.setState({pn_no})
    
    // getPnDetailList({pn_no}).then(res => {
    //   console.log('getPnDetailList res ：', res.data);
    //   // this.props.scheduleAction.barcode(res.data.datas)
    //   const {datas, code} = res.data
    //   // if (code === 1) {
    //     this.setState({
    //       dialogType: 'showPnCode', 
    //       barcodeData: res.data.datas,
          
    //       show: true,
    //     })
    //     this.showDialog()
    //   // }
    // })
  }
  
  // 固定显示的角落div
  fixBox = () => {
    return (
      <div className="headerSideBarBox">
        <div className="fixBoxItem pnDay">日期</div>
        <div className="fixBoxItem pnInfoColumn">批号信息</div>
      </div>
    )
  }
  createDay = (isTxt, isMonth, isLasItem, ) => {
    const {monthArr} = this.state
    // console.log(' item.date item ：', item, ) 
    if (!isMonth) {
      return monthArr.map((v, i) => {
        // console.log(' monthArr v ：', v, ) 
        return <div className="dayWrapper" key={i}>
          {
            v.date.map((item, index) => {
              return <div className={`${item.isSunday ? 'isSunday dateItem' : 'dateItem'} ${isLasItem ? 'boldDate' : ''}`} key={index}>{isTxt ? item.date : null}</div>
            })
          }
        </div>
      })
    } else {
      return monthArr.map((v, i) => {
        // console.log(' monthArr v ：', v, ) 
        return <div className="headerWrapper" key={i}>
          <div className="monthWrapper">
            {isMonth ? <div className="month">{v.month}</div> : null}
            <div className="dayWrapper">
              {
                v.date.map((item, index) => {
                  // console.log(' item.date item ：', item, ) 
                  return <div className={`${item.isSunday ? 'isSunday dateItem' : 'dateItem'}`} key={index}>{isTxt ? item.date : null}</div>
                })
              }
            </div>
          </div>
        </div>
      })
    }
  }

  createBody = (justSideBar, ) => {
    const {data_total, } = this.state
    console.log('justSideBar ：', data_total, justSideBar)
    return data_total.map((v, i) => {
      // console.log(' data_total v ：', v, ) 
      const heights = v.levels * 30
      const topss = v.level * 30
      if (justSideBar) {
        return (
          <div className="contentRow" style={{height: v.row.length * grid}} key={i}>
            <div className="sideBarBox">
              <Tooltip placement="top" title={
                <div className="pnInfoWrapper">
                <div className="pnDay factoryName">{v.factory_name}</div>
                </div>
              }>
              <div className="pnDay factoryName">{v.factory_name}</div>
              </Tooltip>
            </div>
          </div>
        )
      } else {
        return (
          <div className="contentRow" style={{height: v.row.length * grid}} key={i}>
              <Tooltip placement="top" title={
                <div className="pnInfoWrapper">
                <div className="pnDay factoryName">{v.factory_name}</div>
                </div>
              }>
            <div className="sideBarBox">
              <div className="pnDay factoryName">{v.factory_name}</div>
            </div>
              </Tooltip>
            <div className="rowWrapper">
              {
                v.row.map((item, index) => {
                  const { left, width, tops, levels, pn_no, barcode, shortDay, work_name, factory_name} = item
                  const top = (levels - 1) * 30
                  const isLasItem = index === v.row.length - 1 
                  // console.log('item ：', item)
                  {/* const com = <DragableBar key={index} isAxis={'x'} editProduct={this.editProduct} stopDrag={this.stopDrag} config={{ left, width, top, txt: pn_no, levels, barcode, shortDay, }} key={i}></DragableBar> */}
                  return <div className="pnRow" key={index}>{this.createDay(false, false, isLasItem)}</div>
                })
              }
              {
                v.data.map((item, index) => {
                  const { left, width, tops, levels, pn_no, barcode, shortDay, work_name, factory_name} = item
                  const top = (levels - 1) * 30
                  const isLasItem = index === v.row.length - 1 
                  // console.log('item ：', item)
                  return <DragableBar key={index} isTop={true} isAxis={'x'} editProduct={this.editProduct} stopDrag={this.stopDrag} config={{ left, width, top, txt: pn_no, levels, barcode, shortDay, }} key={index}></DragableBar>
                })
              }
            </div>
          </div>
        )
      }
    })
  }
  
  componentDidMount() {
    this.getList()
    const {offsetHeight, offsetWidth} = findDOMNode(ReactDOM, this.con)
    const domRef = findDOMNode(ReactDOM, this.con)
    domRef.onscroll = (e) => {
      const {scrollLeft, scrollTop} = domRef
      const {scrollLeftPos, scrollTopPos} = this.state
      if (scrollLeftPos !== scrollLeft) {
        console.log('横向滚动 ：', )
        ajax(this.setState.bind(this), {
          scrollLeftPos: scrollLeft,
        })
      } else if (scrollTopPos !== scrollTop) {
        console.log('纵向滚动 ：', )
        ajax(this.setState.bind(this), {
          scrollTopPos: scrollTop,
        })
      }
    }
  }
  render() {
    const {scrollHeight, collapse,} = this.props
    const {offsetHeight, offsetWidth} = this.props.styles
    const { datas_barcode, dragBox, show, tableHeight, tableWidth, showEdit, loading, 
      scrollLeftPos, scrollTopPos } = this.state
    const {barcodeData} = this.props.scheduleNo
    const { width, height, left, top } = dragBox
    const title = showEdit ? "是否确认修改起止时间" : '条码详情'
    const content = showEdit ? <p>edit ProductDialog</p> : <BarcodeContent data={barcodeData}></BarcodeContent>
    console.log(' dateArrdateArr 7：', this.state, this.props)
    const headerCom = datas_barcode.length ? (
      <div className="headerContainer">
        {this.fixBox()}
        {this.createDay(true, true)}
      </div>)
      : null

    return (
      // <Collapses title={'工艺路线'} className='scheduleCollapse'  isAnimate={false}>
        <div className={`noWrapper ${ANIMATE.fadeIn}`} ref={con => this.con = con}>
          {
            datas_barcode.length ? (
            <DragToggle scrollLeftPos={scrollLeftPos} scrollTopPos={scrollTopPos} collapse={collapse} scrollHeight={scrollHeight} tableHeight={tableHeight} tableWidth={tableWidth} offsetWidth={offsetWidth} offsetHeight={offsetHeight}>
              {/* 固定头0 */}
              {/* <div className="fixHeaderWrapper"> */}
                <div className="fixHeader">
                  {headerCom}
                </div>
              {/* </div> */}

              {/* 固定列1 */}
              <div className="fixSideBar">
                {this.fixBox()}
                {this.createBody(true)}
              </div> 
              {/* 主体 */}
              {headerCom}
              {/* 主体2 */}
              {this.createBody(false)}

              {/* 固定显示的角落div */}
              {this.fixBox()}
            </DragToggle>)
          　: null
          }
          {show ? <ProductDialog handleOk={this.handleOk} width="90%" show={show} close={this.close} title={title}>{content}</ProductDialog> : null}
          <Loading loading={loading}></Loading>
        </div>
      // </Collapses>
    );
  }
}

const state = state => state
const action = action => {
  return {
    scheduleAction: bindActionCreators(scheduleAction, action),
  }
}
export default connect(state, action)(PnNo);
// export default PnNo;

