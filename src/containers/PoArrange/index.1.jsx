import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getPn, addPn, editPn, getColorSize, updateColorSize, deleteColorSize, getSidePnWork, saveSidePnWork, } from 'api/PoArrange'
import { openNotification, backupFn, infoFilter, confirms, } from 'util'
import { poInfoForm, width90, poInfoConfig, } from 'config'
import { ANIMATE, SIZE10, PAGE, } from 'constants'
import Collapses from '@@@/Collapses'
import ProductDialog from '@@@/ProductDialog'
import ProduceForm from '@@@/ProduceForm'
import ColorTable from '@@@/ColorTable'
import ColorCombineTable from '@@@/ColorCombineTable'
import FactoryTable from '@@@/FactoryTable'
import PoArrangeTable from '@@@/PoArrangeTable'
import EditInfo from '@@@@/EditInfo'
import Count from '@@@@/Count'
import {  Row, Col, Tabs, Icon, Button, message, } from 'antd'
const TabPane = Tabs.TabPane
const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
}
let index = 0
const poNoDatas = [
  {pn_no: 'red', gauge: 'gauge', sc_no: 'sc_no', date: 'date', s_date: 's_date', 
  prod_date: 'prod_date', qty: 'qty', arrange: 'arrange',  num: 'num', },
  {pn_no: 'red2', gauge: 'gauge2', sc_no: 'sc_no2', date: 'date2', s_date: 's_date2', 
  prod_date: 'prod_date2', qty: 'qty2', arrange: 'arrange2',  num: 'num2', },
]

class PoArrange extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    this.state = {
        productData: [],
        sizeOrigin: {},
        colorOrigin: {},
        show: false,
        // show: true,
        isEditing: false,
        showConfirm: false,
        poInit: {},
        editData: {},
        editOrigin: {},
        title: '',
        type: 'processDetail',
        poNoData: [],
        sizeData: [],
        colorData: [],
        processData: [],
        editIndex: -1,
        pn_no: '',
        total: 0,
        pnData: [],
        processInfoData: [],
        // editAble: false,
      }
  }
  handleOk = () => {
    console.log('handleOk ：', this.state)
    // this.beforeEditData()
    const {type, processInfoData, pn_no} = this.state
    if (type === 'addPn' || type === 'editPn') {
      console.log('addPn ：', )
      this.forms.validateFields((err, values) => {
        console.log('values ：', err, values);
        if (!err) {    
          this[type](values)
        }
      })
    } else if (type === 'processDetail') {
      console.log('processDetail ：', processInfoData)
      this.saveSidePnWork({ss: processInfoData, pn_no,})
    } else if (type === 'colorDetail') {
      console.log('colorDetail ：', )
      this.close()
    }
  }
  close = () => this.setState({show: false, type: '', poInit: {},})
  handleOkConfirm = () => {
    console.log('handleOkConfirm ：', this.state)
    // this.beforeEditData()
  }
  closeConfirm = () => this.setState({showConfirm: false, type: '',})
  showConfirmDialog = () => this.setState({showConfirm: true, type: '',})
  confirmContent = () => {
    const {editData, } = this.state
    console.log('confirmContent ：', editData, )
    return <EditInfo/>
  }
  editContent = () => {
    const {show, type, poNoData, colorData, sizeData, processData, poInit, pn_no, total, pnData, processInfoData, } = this.state
    console.log('editContent ：', this.state, total)
    const commonHeader = <div className="titleInfo">
      <div className="title">
        <span className="poNo">批号：{pn_no}</span>
        <span className="total">总数：{<Count end={total} duration={2}/>}</span>
      </div>
    </div>

    if (type === 'colorDetail') {
      console.log('colorDetail：', )
      return <div className='poDialogContent' >
          {commonHeader}
          <Row gutter={5} className='anrrangeWrapper' >
            <Col className={`${ANIMATE.bounceIn}`} span={10}>
              <Row gutter={5}>
                <Col className={`${ANIMATE.bounceIn}`} span={12}>
                  <ColorTable
                    ref={table => this.colorTable = table}
                    rowKey={'color'}
                    data={colorData}
                    inputing={this.inputing}
                    addData={this.addData}
                    saveData={this.saveData}
                    showDetail={this.showDetail}
                  ></ColorTable>
                </Col>
                  
                <Col className={`${ANIMATE.bounceIn}`} span={12}>
                  <ColorTable
                    ref={table => this.sizeTable = table}
                    rowKey={'size'}
                    data={sizeData}
                    inputing={this.inputing}
                    addData={this.addData}
                    saveData={this.saveData}
                    showDetail={this.showDetail}
                  ></ColorTable>
                </Col>
            </Row>
            </Col>
            <Col className={`t-c ${ANIMATE.bounceIn}`} span={2}>
              <Button onClick={this.compose} type="primary" size='large' icon="double-right" className=""></Button>
            </Col>
            <Col className={`${ANIMATE.bounceIn}`} span={12}>
              <ColorCombineTable
                data={pnData}
                rowKey={'qty'}
                addData={this.addData}
                newData={this.newData}
                inputing={this.qtyInputing}
                showDetail={this.showDetail}
                // editCTData={this.editCTData}
                // editAble={editAble}
                deleteCTData={this.deleteColorSize}
              ></ColorCombineTable>
            </Col>
        </Row>
      </div>
    } else if (type === 'processDetail') {
      console.log('processDetail ：', )
      return <div className='poDialogContent' >
        {commonHeader}
        <FactoryTable
          data={processInfoData}
          inputing={this.qtyInputing}
        ></FactoryTable>
      </div>
    } else if (type === 'addPn'  || type === 'editPn') {
      console.log('addPn ：', )
      const editPnInfoForms = poInfoForm.map(v => v.key === 'pn_no' ? {...v, disabled: true} : v)
      const poInfoForms = type === 'editPn' ? editPnInfoForms : poInfoForm 
      return <ProduceForm 
        init={poInit}   
        ref={forms => this.forms = forms} 
        formLayout='horizontal'
        formItemLayout={formItemLayout}
        config={poInfoForms}
      />
    }
    // return <EditInfo/>
  }
  addData = (v) => {
    console.log('v  add ：', v, this.state[`${v}Data`], index )
    const editing = this.state[`${v}Data`].some(v => v.editAble)
    console.log('editing ：', editing)
    // if (!editing) {
      const len = this.state[`${v}Data`].length + 1
      // const origin = {[infoFilter(poInfoConfig, v, 'rowKey')]: `${v}${len}`, [infoFilter(poInfoConfig, v, 'seq')]: len, status: 'N', editAble: true, }
      const origin = {[infoFilter(poInfoConfig, v, 'rowKey')]: ` `.repeat(len), [infoFilter(poInfoConfig, v, 'seq')]: len, status: 'N', editAble: true, }
      console.log('origin ：', origin, [...this.state[`${v}Data`], {color: `color${this.state[`${v}Data`].length + 1}`, size: `XL${this.state[`${v}Data`].length + 1}`, status: 'N', editAble: true, },])
      this.setState({
        [`${v}Data`]: [...this.state[`${v}Data`], {...origin}, ],
        // [`${v}Origin`]: {...origin}, 
      })
    // } else {
    //   message.warn('請先保存上一條數據再繼續操作！', );
    // }
  }
  inputing = (val) => {
    console.log('inputing变化 val：', val, this.state[`${val}Data`], index, this.state, );
    const unq = [infoFilter(poInfoConfig, val.keys, 'seq')]
    console.log('unq ：', unq, this.state[`${val.keys}Data`])
    const data = this.state[`${val.keys}Data`].map(v => v[unq] === val[unq] ? {...v, [infoFilter(poInfoConfig, val.keys, 'rowKey')]: val.v, } : v)
    console.log('unqs ：', data, this.state[`${val.keys}Data`])
    this.setState({[`${val.keys}Data`]: data,})
  }
  qtyInputing = (val) => {
    console.log('qtyInputing变化 val：', val, this.state[`${val}Data`], index, this.state, );
    const {keys, } = val
    if (keys === 'work_no') {
      this.setState({ processInfoData: this.state.processInfoData.map(v => val[val.keys] === v[val.keys] ? {...v, day_qty: Number(val.v), work_status: 'T', } : v), })
    } else {
      this.setState({ pnData: this.state.pnData.map(v => val[val.keys] === v[val.keys] ? {...v, qty: Number(val.v), status: v.status === 'O' ?  'U' : v.status} : v), })
    }
  }
  // getProduct = () => {
  //   getProduct({}).then(res => {
  //     console.log('!!!!!!!getProduct res ：', res.data)
      
  //   })
  // }
  editData = (v, o, i) => {
    console.log('v, o, i  editData ：', v, o, i  )
    this.setState({ show: true, type: 'editPn', poInit: o, title: v, })
    // const {poNoData,  isEditing, editData, editOrigin, } = this.state
    // if (Object.keys(editData).length) {
    //   message.warn('請先保存上一條數據再繼續操作！', );
    // } else {
    //   let editData 
    //   poNoData.map(v => {
    //     if (v.pn_no === o.pn_no) {
    //       console.log('小engdew相等 ：', )
    //       v.editAble = !v.editAble
    //       editData = v
    //     } 
    //     return v
    //   })
    //   this.setState({
    //     poNoData: [...poNoData],
    //     editData,
    //     editOrigin: {...editData},
    //     isEditing: true, 
    //   })
    // } 
    // console.log('poNoData ：', poNoData)
  }
  saveData = (v, o, i) => {
    console.log('v, o, i  saveData ：', v, o, i, this.state )
    // poNoData.map(v => {
    //   if (v.pn_no === o.pn_no) {
    //     console.log('小engdew相等 ：', )
    //     v.editAble = !v.editAble
    //   } 
    //   return v
    // })
    // this.setState({
    //   poNoData: [...poNoData],
    // })
    // console.log('poNoData ：', poNoData)
  }
  newData = (v) => {
    console.log('v  newData ：', v, this.state )
    const {pnData, pn_no} = this.state
    const newData = pnData.filter(v => v.status !== 'O')
    newData.length ? this.updateColorSize({ss: newData, pn_no,}) : confirms(0, '請先選擇相應的信息，，再進行新增！',  )
  }
  addPnAction = (title) => this.setState({ show: true, type: 'addPn', title, })
  showDetail = (type, o, i,) => {
    console.log('type.   showDetail ：', type, o, i  )
    this.setState({
      show: true, 
      type,
      title: infoFilter(poInfoConfig, type, 'title', )
    })
  }
  tableInput = (p) => {
    console.log('p  tableInput ：', p  )
    const {poNoData,} = this.state
    const {keys, v, pn_no} = p
    poNoData.map(item => {
      if (item.pn_no === pn_no) {
        console.log('小engdew相等 ：', keys, v)
        item[keys] = v
      } 
      return item
    })
    this.setState({
      poNoData: [...poNoData],
    })

  }
  dialogWidth = () => width90.some(v => v === this.state.type) ? '90%' : '60%'
  compose = (v) => {
    const {colorData, sizeData, pnData, pn_no,  } = this.state
    // if (editAble) {
    //   confirms(2, '請先保存要更新的數據，再進行新增！',  )
    // } else {
    const assistData = this.sizeTable.state.selectedRowKeys
    const targetData = this.colorTable.state.selectedRowKeys
    const assistDatas = sizeData.filter(v => assistData.some(item => v.size_no === item))
    const targetDatas = colorData.filter(v => targetData.some(item => v.combo_id === item))
    // const targetDatas = colorData.filter(v => {
    //   // console.log(' targetData.some(item => v.color === item) ：',  targetData.some(item => v.color === item))
    //   return  targetData.some(item => v.combo_id === item)
    // })
    console.log('colorData ：', colorData, sizeData)
    console.log('v  compose ：', v, this.sizeTable, this.colorTable, targetData, assistData, targetDatas, assistDatas)
    const newData = []
    // const newDatas = targetDatas.map(v => ({combo_id: v.combo_id, ...v})).forEach(v => newData.push(...assistDatas.map(item => ({...v, size_no: item.size_no, ...item, compose: `${v.combo_id}${item.size_no}`, qty: 1, }))))
    const newDatas = targetDatas.map(v => ({combo_id: v.combo_id.trim(), ...v})).forEach(v => newData.push(...assistDatas.map(item => ({...v, size_no: item.size_no.trim(), ...item, compose: `${v.combo_seq}${item.size_seq}`, qty: 1, }))))
    
    console.log('newData ：', newData, newDatas, '  1 1'.trim())
    const filterData = newData.filter(v => {
      console.log(' pnData filter v ：', v, pnData.some(item => item.size_no === v.size_no && item.combo_id === v.combo_id))
      return !pnData.some(item => item.size_no === v.size_no && item.combo_id === v.combo_id)
    })
    console.log('filterData ：', filterData, [...pnData, ...filterData])
    const isEmpty = filterData.some(v => {
      console.log('v.combo_ ：', v.combo_id, v.size_no, v.combo_id.trim() === '', v.size_no.trim() === '', v.combo_id.trim() === '' && v.size_no.trim() === '')
      return v.combo_id.trim() === '' && v.size_no.trim() === ''
    })
    console.log('isEmpty ：', isEmpty)
    !isEmpty ? this.setState({pnData: [...pnData, ...filterData],}) : confirms(0, '新增的色碼信息值不能爲空！',  )
    
    // }
  }
  addPn = (v) => {
    console.log('addPn ：', v);
    addPn(v).then(res => {
      console.log('addPn2：', res.data, );
      const { code, mes,  } = res.data
      confirms(code, mes,  )
      if (code === 1) {
        const {poNoData, } = this.state
        this.setState({
          poNoData: [v, ...poNoData,], 
          show: false, 
        })
      }
    })
  }
  editPn = (v) => {
    console.log('editPn ：', v);
    editPn(v).then(res => {
      console.log('editPn2：', res.data, );
      const { code, mes,  } = res.data
      confirms(code, mes,  )
      if (code === 1) {
        const {poNoData, } = this.state
        const matchItem = poNoData.findIndex(item => item.pn_no === v.pn_no)
        console.log('matchItem ：', matchItem)
        poNoData[matchItem] = v
        this.setState({
          poNoData: [...poNoData,], 
          show: false, 
        })
      }
    })
  }
  // editCTData = (v) => {
  //   console.log('v  editCTData ：', v  )
  //   this.setState({
  //     editAble: !this.state.editAble,
  //   })
  // }
  getColorSize = (type, o, i,) => {
    console.log('getColorSize ：', type, o, i);
    const {pn_no, } = o
    getColorSize({pn_no}).then(res => {
      console.log('getColorSize res：', res.data, );
      const { code, mes, data_all, color, size,  } = res.data
      const total = data_all.reduce((t, v,) => t + v.qty, 0)
      console.log('total ：', total)
      if (code === 1) {
        this.setState({
          title: infoFilter(poInfoConfig, type, 'title', ), 
          editIndex: i,
          show: true, 
          type,
          pn_no,
          total,
          // pnData: data_all.map(v => ({...v, compose: `${v.combo_id}${v.size_no}`,})),
          pnData: data_all.map(v => ({...v, compose: `${v.combo_v.combo_seq}${v.size_seq}`,})),  
          colorData: color, 
          sizeData: size, 
        })
      }
    })
  }
  getSidePnWork = (type, o, i,) => {
    console.log('getSidePnWork ：', type, o, i);
    const {pn_no, } = o
    getSidePnWork({pn_no}).then(res => {
      console.log('getSidePnWork res：', res.data, );
      const { code, data,  } = res.data
      const total = data.reduce((t, v,) => t + v.day_qty, 0)
      console.log('total ：', total)
      if (code === 1) {
        this.setState({
          title: infoFilter(poInfoConfig, type, 'title', ),
          editIndex: i,
          show: true, 
          type,
          pn_no,
          total,
          processInfoData: data,  
        })
      }
    })
  }
  csFilter = (data) => {
    const str = data.reduce((t, v,) => t += v['work_name'] + '--' + v['day_qty'] + ';', '')
    console.log('csFilterstr ：', str)
    return str
  }
  porcessFilter = (data) => {
    const str = data.reduce((t, v,) => t += v['combo_id'] + '--' + v['size_no'] + '--' + v['qty'] + ';', '')
    console.log('porcessFilterstr ：', str)
    return str
  }
  saveSidePnWork = (v) => {
    console.log('saveSidePnWork ：', v);
    saveSidePnWork(v).then(res => {
      console.log('saveSidePnWork res：', res.data);
      const { code, mes,  } = res.data
      confirms(code, mes,  )
      if (code === 1) {
        const {poNoData, processInfoData, editIndex, } = this.state
        poNoData[editIndex].work_dtl = this.porcessFilter(processInfoData)
        this.setState({
          show: false, 
          poNoData: [...poNoData ]
        })
      }
    })
  }
  updateColorSize = (v) => {
    console.log('updateColorSize ：', v);
    updateColorSize(v).then(res => {
      console.log('updateColorSize res：', res.data, this.state, );
      const { code, mes,  } = res.data
      confirms(code, mes,  )
      if (code === 1) {
        const {pnData, colorData, sizeData, } = this.state
        const {poNoData, processInfoData, editIndex, } = this.state
        pnData[editIndex].qty_dtl = this.csFilter(pnData)
        this.setState({
          pnData: pnData.map(v => ({...v, status: 'O'})), 
          colorData: colorData.map(v => ({...v, editAble: false})), 
          sizeData: sizeData.map(v => ({...v, editAble: false})), 
        })
      }
    })
  }
  deleteColorSize = (v) => {
    console.log('deleteColorSize ：', v);
    const {pn_no, combo_seq, size_no, compose} = v
    deleteColorSize({pn_no, combo_seq, size_no, }).then(res => {
      console.log('deleteColorSize res：', res.data, );
      const { code, mes, color, size,  } = res.data
      confirms(code, mes,  )
      if (code === 1) {
        const {pnData, } = this.state
        this.setState({
          pnData: pnData.filter(v => v.compose !== compose), 
          colorData: color, 
          sizeData: size, 
        })
      }
    })
  }
  getPn = (page_rows = SIZE10, page = PAGE, pn_no = '') => {
    console.log('page_rows, page, pn_no, pn_no  getPn ：', page_rows, page, pn_no,   )
    getPn({page_rows, page, pn_no, }).then(res => {
      console.log('getPn res ：', res.data);
      const {data, code, } = res.data
      if (code === 1) {
        console.log('data ：', data)
        this.setState({
          poNoData: data, 
        })
      }
    })
  }
  componentDidMount() {
    this.getPn()
    // var aa = (112233).toString()
    // var change = (aa) => {
    //   return aa.substring(0, 3) + ',' + aa.substring(3, aa.length)
    // }
    // var bb = aa.substring(0, 3)
    // const ccd =  aa.substring(3, aa.length)
    // var cc = bb + ',' + ccd
    // console.log('aa, bb ：', aa, bb, ccd, cc, change(aa))
  }

  render() {
    console.log('PoArrange 组件 this.state, this.props ：', this.state, this.props, )
    const {show, type, poNoData, colorData, sizeData, showConfirm, title } = this.state
    // const poNoData = [
    //   {pn_no: 'red', gauge: 'gauge', sc_no: 'sc_no', date: 'date', s_date: 's_date', 
    //   prod_date: 'prod_date', qty: 'qty', arrange: 'arrange',  num: 'num', },
    //   {pn_no: 'red2', gauge: 'gauge2', sc_no: 'sc_no2', date: 'date2', s_date: 's_date2', 
    //   prod_date: 'prod_date2', qty: 'qty2', arrange: 'arrange2',  num: 'num2', },
    // ]
    // const colorAData= [
    //   {color: 'color1', size: 'XL', status: 'O'},
    //   {color: 'color2', size: 'L', status: 'O'},
    //   {color: 'color3', size: 'M', status: 'O'},
    //   {color: 'color4', size: 'S', status: 'N'},
    //   {color: 'color5', size: 'XS', status: 'N'},
    // ]
    return (
      <section className="poArrange">
        <Collapses animate={ANIMATE.bounceIn} title={'批号信息'} className="poArrange"
          extra={
            <div className="btnWrapper">
              <Button onClick={this.addPnAction.bind(this, '新增批号')} type="primary" icon="plus" className="m-l">新增批号</Button>
            </div>
          }
        >
          <PoArrangeTable
            data={poNoData}
            editData={this.editData}
            getColorSize={this.getColorSize}
            getSidePnWork={this.getSidePnWork}
            tableInput={this.tableInput}
            showConfirmDialog={this.showConfirmDialog}
          ></PoArrangeTable>
        </Collapses>

        {show ? <ProductDialog handleOk={this.handleOk} width={this.dialogWidth()} show={show} close={this.close} title={title}>
          {this.editContent()}
        </ProductDialog> 
      : null}

        {/* {showConfirm ? <ProductDialog className='corfirmModal' handleOk={this.handleOkConfirm} width={'50%'} show={showConfirm} close={this.closeConfirm} title={'提示框'}>
          {this.confirmContent()}
        </ProductDialog> 
      : null} */}
      </section>
    )
  }
}

export default PoArrange
