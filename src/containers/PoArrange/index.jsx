import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getPn, addPn, editPn, getColorSize, updateColorSize, deleteColorSize, getSidePnWork, saveSidePnWork, } from 'api/PoArrange'
import { poInfoForm, width90, poInfoConfig, sewData, } from 'config'
import { ANIMATE, SIZE10, PAGE, } from 'constants'
import { openNotification, backupFn, infoFilter, confirms, filterDatas, } from 'util'
import Collapses from '@@@/Collapses'
import ProduceForm from '@@@/ProduceForm'
import ProductDialog from '@@@/ProductDialog'
import ColorTable from '@@@/ColorTable'
import ColorCombineTable from '@@@/ColorCombineTable'
import FactoryTable from '@@@/FactoryTable'
import PoArrangeTable from '@@@/PoArrangeTable'
import Count from '@@@@/Count'
import {  Row, Col, Tabs, Icon, Button, message, } from 'antd'
const TabPane = Tabs.TabPane
const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
}


class PoArrange extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    this.state = {
      show: false,
      poInit: {},
      title: '',
      type: 'processDetail',
      poNoData: [],
      sizeData: [],
      colorData: [],
      processData: [],
      editIndex: -1,
      pn_no: '',
      qtyTotal: 0,
      pnData: [],
      processInfoData: [],
      gaugeData: [],
      total: 0,
    }
  }
  close = () => this.setState({show: false, type: '', poInit: {},})
  dialogWidth = () => width90.some(v => v === this.state.type) ? '90%' : '60%'
  addPnAction = (title) => this.setState({ show: true, type: 'addPn', title, })
  editPnAction = (title, poInit, ) => this.setState({ show: true, type: 'editPn', title, poInit, })
  handleOk = () => {
    console.log('handleOk ：', this.state)
    const {type, processInfoData, pn_no} = this.state
    if (type === 'addPn' || type === 'editPn') {
      console.log('addPn ：', )
      this.forms.validateFields((err, values) => {
        console.log('values ：', err, values, values.pn_no, values.pn_no);
        if (!err) {
          values.pn_no.trim().length < 20 ? this[type](values) : confirms(2, '批號的長度不能超過10位！',  )
        }
      })
    } else if (type === 'processDetail') {
      console.log('processDetail ：', processInfoData)
      this.saveSidePnWork({ss: processInfoData, pn_no,})
    } else if (type === 'colorDetail') {
      console.log('colorDetail ：', )
      this.close()
    }
  }
  editContent = () => {
    const {show, type, poNoData, colorData, sizeData, processData, poInit, pn_no, qtyTotal, pnData, processInfoData, gaugeData, } = this.state
    console.log('editContent ：', this.state, qtyTotal)
    const commonHeader = <div className="titleInfo">
      <div className="title">
        <span className="poNo">批号：{pn_no}</span>
        <span className="qtyTotal">总数：{<Count end={qtyTotal} duration={2}/>}</span>
      </div>
    </div>

    if (type === 'colorDetail') {
      console.log('colorDetail：', )
      return <div className='poDialogContent' >
          {commonHeader}
          <Row gutter={5} className='anrrangeWrapper' >
            <Col className={`${ANIMATE.bounceIn}`} span={10}>
              <Row gutter={5}>
                {
                  ['color', 'size'].map(v => {
                    console.log(' v ：', v, ) 
                    return <Col className={`${ANIMATE.bounceIn}`} key={v} span={12}>
                      <ColorTable
                        ref={table => this[`${v}Table`] = table}
                        rowKey={v}
                        data={this.state[`${v}Data`]}
                        inputing={this.inputing}
                        addData={this.addData}
                      ></ColorTable>
                    </Col>
                  })
                }
                {/* <Col className={`${ANIMATE.bounceIn}`} span={12}>
                  <ColorTable
                    ref={table => this.colorTable = table}
                    rowKey={'color'}
                    data={colorData}
                    inputing={this.inputing}
                    addData={this.addData}
                  ></ColorTable>
                </Col>
                  
                <Col className={`${ANIMATE.bounceIn}`} span={12}>
                  <ColorTable
                    ref={table => this.sizeTable = table}
                    rowKey={'size'}
                    data={sizeData}
                    inputing={this.inputing}
                    addData={this.addData}
                  ></ColorTable>
                </Col> */}
            </Row>
            </Col>
            <Col className={`t-c ${ANIMATE.bounceIn}`} span={2}>
              <Button onClick={this.compose} type="primary" size='large' icon="double-right" className=""></Button>
            </Col>
            <Col className={`${ANIMATE.bounceIn}`} span={12}>
              <ColorCombineTable
                data={pnData}
                newData={this.newData}
                inputing={this.qtyInputing}
                deleteCTData={this.deleteColorSize}
              ></ColorCombineTable>
            </Col>
        </Row>
      </div>
    } else if (type === 'processDetail') {
      console.log('processDetail ：', )
      return <div className='poDialogContent' >
        {commonHeader}
        <FactoryTable
          data={processInfoData}
          inputing={this.qtyInputing}
        ></FactoryTable>
      </div>
    } else if (type === 'addPn' || type === 'editPn') {
      console.log('addPn ：', ) 
      const editPnInfoForms = poInfoForm.map(v => v.key === 'pn_no' ? {...v, disabled: true} : v)
      const poInfoForms = type === 'editPn' ? editPnInfoForms : poInfoForm 
      return <ProduceForm 
        init={poInit}   
        gaugeData={gaugeData}
        ref={forms => this.forms = forms} 
        formLayout='horizontal'
        formItemLayout={formItemLayout}
        config={poInfoForms}
        dateDisable={true}
        sewData={sewData}
      />
    }
  }
  newData = (v) => {
    console.log('v  newData ：', v, this.state )
    const {pnData, pn_no} = this.state
    const newData = pnData.filter(v => v.status !== 'O')
    newData.length ? this.updateColorSize({ss: newData, pn_no,}) : confirms(2, '請先選擇相應的信息，再進行新增！',  )
  }
  addData = (v) => {
    console.log('v  add ：', v, this.state[`${v}Data`],  )
    const last = this.state[`${v}Data`].length - 1
    console.log('last ：', last)
    const len = last !== -1 ? this.state[`${v}Data`][last][infoFilter(poInfoConfig, v, 'seq')] + 1 : 1 
    // const origin = {[infoFilter(poInfoConfig, v, 'rowKey')]: `${v}${len}`, [infoFilter(poInfoConfig, v, 'seq')]: len, status: 'N', editAble: true, }
    const origin = {[v]: '', [infoFilter(poInfoConfig, v, 'rowKey')]: ` `.repeat(len), [infoFilter(poInfoConfig, v, 'seq')]: len, status: 'N', editAble: true, }
    // console.log('origin ：', this.state[`${v}Data`], origin, [...this.state[`${v}Data`], {color: `color${this.state[`${v}Data`].length + 1}`, size: `XL${this.state[`${v}Data`].length + 1}`, status: 'N', editAble: true, },])
    console.log(', this.state[`${v}Data`], origin ：', this.state[`${v}Data`], origin, )
    this.setState({
      [`${v}Data`]: [...this.state[`${v}Data`], {...origin}, ],
    })
  }

  inputing = (val) => {
    console.log('inputing变化 val：', val, this.state, );
    const {keys, } = val
    const unq = infoFilter(poInfoConfig, keys, 'seq')
    const data = this.state[`${keys}Data`].map(v => v[unq] === val[unq] ? {...v, [keys]: val.v, } : v)
    // const data = this.state[`${keys}Data`].map(v => v[unq] === val[unq] ? {...v, [keys]: val.v, [infoFilter(poInfoConfig, keys, 'rowKey')]: val.v, } : v)
    console.log('unqs ：', unq, data, this.state[`${keys}Data`])
    this.setState({[`${keys}Data`]: data,})
  }
  qtyInputing = (val) => {
    console.log('qtyInputing变化 val：', val, this.state[`${val}Data`], this.state, );
    const {keys, } = val
    if (keys === 'work_no') {
      this.setState({ processInfoData: this.state.processInfoData.map(v => val[keys] === v[keys] ? {...v, day_qty: Number(val.v), work_status: 'T', } : v), })
    } else {
      this.setState({ pnData: this.state.pnData.map(v => val[keys] === v[keys] ? {...v, qty: Number(val.v), status: v.status === 'O' ?  'U' : v.status} : v), })
    }
  }
  tableInput = (p) => {
    console.log('p  tableInput ：', p  )
    const {poNoData,} = this.state
    const {keys, v, pn_no} = p
    poNoData.map(item => {
      if (item.pn_no === pn_no) {
        console.log('小engdew相等 ：', keys, v)
        item[keys] = v
      } 
      return item
    })
    this.setState({
      poNoData,
    })
  }


  compose = (v) => {
    const {colorData, sizeData, pnData, pn_no,  } = this.state
    const assistData = this.sizeTable.state.selectedRowKeys
    const targetData = this.colorTable.state.selectedRowKeys
    const assistDatas = sizeData.filter(v => assistData.some(item => v.size_no === item))
    const targetDatas = colorData.filter(v => targetData.some(item => v.combo_id === item))
    const noSameColor = filterDatas(assistDatas, 'size')
    const noSameSize = filterDatas(targetDatas, 'color')
    console.log('colorData ：', colorData, sizeData, assistDatas, targetDatas, noSameColor, noSameSize,)
    // noSameColor.forEach(v => {
    //   console.log(' noSameColor some v ： ', v, typeof v, typeof Number(v), isNaN(Number(v)), Number(v) )
    //   return typeof v
    // })
    
    if (noSameColor.length !== assistDatas.length || noSameSize.length !== targetDatas.length) {
      confirms(2, '新增的色碼信息有重複，請檢查！',  )
      return
    }   
    console.log('v  compose ：', v, this.sizeTable, this.colorTable, targetData, assistData, targetDatas, assistDatas)
    const newData = []
    // const newDatas = targetDatas.map(v => ({combo_id: v.combo_id, ...v})).forEach(v => newData.push(...assistDatas.map(item => ({...v, size_no: item.size_no, ...item, compose: `${v.combo_id}${item.size_no}`, qty: 1, }))))
    // const newDatas = targetDatas.map(v => ({...v, combo_id: v.color, })).forEach(v => newData.push(...assistDatas.map(item => ({...v, ...item, size_no: item.size, compose: `${v.combo_seq}${item.size_seq}`, qty: 1, }))))
    const newDatas = targetDatas.map(v => ({...v, combo_id: v.color, })).forEach(v => newData.push(...assistDatas.map(item => ({...v, ...item, size_no: item.size, compose: `${v.combo_id}${item.size_no}`, qty: 1, }))))
    console.log('newData ：', newData, newDatas, )
    // const filterData = newData.filter(v => {
    //   console.log(' pnData filter v ：', v, pnData.some(item => item.size_no === v.size_no && item.combo_id === v.combo_id))
    //   return !pnData.some(item => item.size_no === v.size_no && item.combo_id === v.combo_id)
    // })
    const filterData = newData.filter(v => !pnData.some(item => item.size_no === v.size_no && item.combo_id === v.combo_id))
    console.log('filterData ：', filterData, [...pnData, ...filterData])
    // const isEmpty = filterData.some(v => {
    //   console.log('v.combo_ ：', v.combo_id, v.size_no, v.combo_id.trim() === '', v.size_no.trim() === '', v.combo_id.trim() === '' && v.size_no.trim() === '')
    //   return v.combo_id.trim() === '' || v.size_no.trim() === ''
    // })
    const isEmpty = filterData.some(v => v.combo_id.trim() === '' || v.size_no.trim() === '')
    console.log('isEmpty ：', isEmpty) 
    if (isEmpty) {
      confirms(2, '新增的色碼信息值不能爲空！',  )
      return
    }
    if (!filterData.length) {
      confirms(2, '沒有匹配出可以新增的色碼信息，請檢查！',  )
      return
    }
    this.setState({
      pnData: [...pnData, ...filterData], 
      colorData: [...colorData.filter(v => v.color.trim() !== '').map(v => ({...v, combo_id: v.color, editAble: false}))], 
      sizeData: [...sizeData.filter(v => v.size.trim() !== '').map(v => ({...v, size_no: v.size, editAble: false}))], 
    }) 
  }

  addPn = (v) => {
    console.log('addPn ：', v);
    addPn(v).then(res => {
      console.log('addPn2：', res.data, );
      const { code, mes,  } = res.data
      // confirms(code, mes,  )
      if (code === 1) {
        console.log(' this.state ： ', this.state,  )
        const {poNoData, total, } = this.state
        this.setState({
          total: total + 1,
          poNoData: [{...v, qty: 0,}, ...poNoData,], 
          show: false, 
        })
      }
    })
  }
  editPn = (v) => {
    console.log('editPn ：', v);
    editPn(v).then(res => {
      console.log('editPn2：', res.data, );
      const { code, mes,  } = res.data
      // confirms(code, mes,  )
      if (code === 1) {
        const {poNoData, } = this.state
        const matchItem = poNoData.findIndex(item => item.pn_no === v.pn_no)
        console.log('matchItem ：', matchItem)
        poNoData[matchItem] = {...poNoData[matchItem], ...v}
        this.setState({
          poNoData: [...poNoData],
          show: false, 
        })
      }
    })
  }
  getColorSize = (type, o, i,) => {
    console.log('getColorSize ：', type, o, i);
    const {pn_no, } = o
    getColorSize({pn_no}).then(res => {
      console.log('getColorSize res：', res.data, );
      const { code, mes, data_all, color, size,  } = res.data
      // const qtyTotal = data_all.reduce((t, v,) => t + v.qty, 0)
      const qtyTotal = this.qtyTotal(data_all, 'qty')
      console.log('qtyTotal ：', qtyTotal)
      if (code === 1) {
        this.setState({
          title: infoFilter(poInfoConfig, type, 'title', ), 
          editIndex: i,
          show: true, 
          type,
          pn_no,
          qtyTotal,
          pnData: data_all.map(v => ({...v, combo_id: v.combo_id.trim(), size_no: v.size_no.trim(), compose: `${v.combo_id}${v.size_no}`,})),
          // pnData: data_all.map(v => ({...v, compose: `${v.combo_seq}${v.size_seq}`,})),  
          // colorData: color.map(v => ({...v, color: v.combo_id.trim(), combo_id: v.combo_id.trim(), })), 
          // sizeData: size.map(v => ({...v, size: v.size_no.trim(), size_no: v.size_no.trim(), })),
          colorData: this.infoTrim('color', color, ), 
          sizeData: this.infoTrim('size', size, ),
        })
      }
    })
  }
  infoTrim = (t, data) => {
    console.log('v  infoTrim ：', )
    const k = infoFilter(poInfoConfig, t, 'rowKey', )
    return data.map(v => ({...v, [t]: v[k].trim(), [k]: v[k].trim(), }))
  }
  getSidePnWork = (type, o, i,) => {
    console.log('getSidePnWork ：', type, o, i);
    const {pn_no, } = o
    getSidePnWork({pn_no}).then(res => {
      console.log('getSidePnWork res：', res.data, );
      const { code, data,  } = res.data
      // const qtyTotal = data.reduce((t, v,) => t + v.day_qty, 0)
      const qtyTotal = this.qtyTotal(data, 'day_qty')
      console.log('qtyTotal ：', qtyTotal)
      if (code === 1) {
        this.setState({
          title: infoFilter(poInfoConfig, type, 'title', ),
          editIndex: i,
          show: true, 
          type,
          pn_no, 
          qtyTotal,
          processInfoData: data,  
        })
      }
    })
  }
  qtyTotal = (data, k) => {
    console.log('data, k  qtyTotal ：', data, k  )
    return data.reduce((t, v,) => t + v[k], 0)
  }
  // csFilter = (data) => {
  //   const str = data.reduce((t, v,) => t += v['combo_id'] + '--' + v['size_no'] + '--' + v['qty'] + ';', '')
  //   // infoFilter(poInfoConfig, 'color', 'tag', ).reduce((tt, vv,) => {
  //   //   console.log('vv.k + vv.link ：', tt, vv, vv.k, vv.link, [tt.k] + tt.link + [vv.k] + vv.link)
  //   // })
  //   // color1--XL--O;color2--L--O;color3--M--O;color4--S--N;color5--XS--N;
  //   // const str = data.reduce((t, v,) => t += infoFilter(poInfoConfig, 'color', 'tag', ).reduce((tt, vv,) => {
  //   //   console.log('v[tt.k] + tt.link  ：', tt.k + tt.link + vv.k + vv.link, v, v[tt.k] + tt.link + v[vv.k] + vv.link)
  //   //   return v[tt.k] + tt.link + v[vv.k] + vv.link
  //   // }), '')
  //   console.log('csFilterstr ：', str)
  //   return str
  // }
  csFilter = (data) => {
    const obj = {}
    data.forEach((v, i) => {
      console.log('  对吗  v.combo_id ', v.combo_id, obj,    )
      if (obj[v.combo_id]) {
        obj[v.combo_id] += v.qty
      } else {
        obj[v.combo_id] = v.qty
      }
    })
    // const str = data.reduce((t, v,) => t += v['combo_id'] + '--' + v['qty'] + ';', '')
    const str = Object.entries(obj).reduce((t, v,) => t += v[0] + '--' + v[1] + ';', '')
    console.log('csFilterstr ：', data, str, obj,   )
    // return data
    return str
  }
  porcessFilter = (data) => {
    console.log('porcessFilterstr ：', str)
    const str = data.reduce((t, v,) => t += v['work_name'] + '--' + v['day_qty'] + ';', '')
    return str
  }
  saveSidePnWork = (v) => {
    console.log('saveSidePnWork ：', v);
    saveSidePnWork(v).then(res => {
      console.log('saveSidePnWork res：', res.data);
      const { code, mes,  } = res.data
      // confirms(code, mes,  )
      if (code === 1) {
        const {poNoData, processInfoData, editIndex, } = this.state
        poNoData[editIndex].work_dtl = this.porcessFilter(processInfoData)
        // const qtyTotal = poNoData.reduce((t, v,) => t + v.day_qty, 0)
        const qtyTotal = this.qtyTotal(poNoData, 'day_qty')
        this.setState({
          show: false, 
          qtyTotal,
          poNoData: [...poNoData],
        })
      }
    })
  }
  updateColorSize = (v) => {
    console.log('updateColorSize ：', v);
    updateColorSize(v).then(res => {
      console.log('updateColorSize res：', res.data, this.state, );
      const { code, mes,  } = res.data
      // confirms(code, mes,  )
      if (code === 1) {
        const {pnData, colorData, sizeData, } = this.state
        const {poNoData, processInfoData, editIndex, } = this.state
        poNoData[editIndex].qty_dtl = this.csFilter(pnData)
        // const qtyTotal = pnData.reduce((t, v,) => t + v.qty, 0)
        const qtyTotal = this.qtyTotal(pnData, 'qty')
        poNoData[editIndex].qty = qtyTotal
        console.log('poNoData11111 ：', poNoData)
        this.setState({
          qtyTotal,
          poNoData: [...poNoData],
          pnData: pnData.map(v => ({...v, status: 'O'})), 
          colorData: colorData.map(v => ({...v, editAble: false})), 
          sizeData: sizeData.map(v => ({...v, editAble: false})), 
          show: false, 
        })
      }
    })
  }
  deleteColorSize = (v) => {
    console.log('deleteColorSize ：', v);
    const {pn_no, combo_seq, size_no, compose, status} = v
    const {pnData, poNoData, editIndex, } = this.state
    const isNew = status === 'N'
    if (isNew) {
      console.log('isNew删除已有 ：', )
      this.setState({
        pnData: pnData.filter(v => v.compose !== compose)
      })
    } else {
      deleteColorSize({pn_no, combo_seq, size_no, }).then(res => {
        console.log('deleteColorSize res：', res.data, );
        const { code, mes, color, size,  } = res.data
        // confirms(code, mes,  )
        if (code === 1) {
          console.log('pnData ：', poNoData, pnData, editIndex, )
          const newData = pnData.filter(v => v.compose !== compose)
          poNoData[editIndex].qty_dtl = this.csFilter(newData)
          // const qtyTotal = newData.reduce((t, v,) => t + v.qty, 0)
          const qtyTotal = this.qtyTotal(newData, 'qty')
          poNoData[editIndex].qty = qtyTotal
          this.setState({
            qtyTotal,
            poNoData: [...poNoData],
            pnData: newData, 
            // colorData: color.map(v => ({...v, color: v.combo_id.trim(), combo_id: v.combo_id.trim(), })), 
            // sizeData: size.map(v => ({...v, size: v.size_no.trim(), size_no: v.size_no.trim(), })), 
            colorData: this.infoTrim('color', color, ), 
            sizeData: this.infoTrim('size', size, ),
          })
        }
      })
    }
  }
  calcColor = (data,  ) => data.map((v) => {
    console.log(' data v ： ', v, v.qty_dtl.split(';') ) 
    v.colorData = {}
    v.qty_dtl.split(';').forEach((item, i) => {
      const items = item.split('--')
      if (items[0] !== '') {
        v.colorData[items[0].trim()] = items[1]
      }
    }) 
    return v
  })
  getPn = (page_rows = SIZE10, page = PAGE, pn_no = '') => {
    console.log('page_rows, page, pn_no, pn_no  getPn ：', page_rows, page, pn_no,   )
    getPn({page_rows, page, pn_no, }).then(res => {
      console.log('getPn res ：', res.data);
      const {data, gauge, code, } = res.data
      if (code === 1) {
        console.log('data ：', data, data.length ? data[0].total : data.length)
        this.setState({
          poNoData: this.calcColor(data), 
          gaugeData: gauge,
          total: data.length ? data[0].total : data.length,
        })
      }
    })
  }
  componentDidMount() {
    this.getPn()
    // this.setState({
    //   show: true, 
    //   type: 'colorDetail', 
    // })
    // const colorAData= [
    //   {combo_id: 'color1', size_no: 'XL', qty: 'O'},
    //   {combo_id: 'color2', size_no: 'L', qty: 'O'},
    //   {combo_id: 'color3', size_no: 'M', qty: 'O'},
    //   {combo_id: 'color4', size_no: 'S', qty: 'N'},
    //   {combo_id: 'color5', size_no: 'XS', qty: 'N'},
    // ]
    // console.log('this.csFilter() ：', this.csFilter(colorAData))
  }

  render() {
    console.log('PoArrange 组件 this.state, this.props ：', this.state, this.props, )
    const {show, type, poNoData, colorData, sizeData, title, total,  } = this.state
    const {middle, } = this.props.route
    return (
      <section className="poArrange">
        <Collapses animate={ANIMATE.bounceIn} title={'批号信息'} className="poArrange"
          middle={middle}
          extra={
            <div className="btnWrapper">
              <Button onClick={this.addPnAction.bind(this, '新增批号', {})} type="primary" icon="plus" className="m-l">新增批号</Button>
            </div>
          }
        >
          <PoArrangeTable
            data={poNoData}
            total={total}
            getPn={this.getPn}
            editData={this.editPnAction}
            getColorSize={this.getColorSize}
            getSidePnWork={this.getSidePnWork}
            tableInput={this.tableInput}
            middle={middle}
          ></PoArrangeTable>
        </Collapses>

        {show ? <ProductDialog handleOk={this.handleOk} width={this.dialogWidth()} show={show} close={this.close} title={title}>
          {this.editContent()}
        </ProductDialog> 
      : null}

      </section>
    )
  }
}

export default PoArrange
