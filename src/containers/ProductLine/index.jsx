import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getProduct, addProductLine, addProcess, editProcess, editCapacity, deleteProcess,  } from 'api/productLine'
import { getProcedure,  } from 'api/Procedure'
import {productLineForm, processForm, DATE_FORMAT_BAR, processDisableArr, disableFactory, onSelfCompany, sewData, sewKey, productLineOutForm, workNoArr, } from 'config'
import { ANIMATE, defaultFactoryType, } from 'constants'
import { openNotification, backupFn, confirms, } from 'util'
import Collapses from '@@@/Collapses'
import ProduceForm from '@@@/ProduceForm'
import ProductTable from '@@@/ProductTable'
import ProductDialog from '@@@/ProductDialog'
import { Tabs, Icon, Button, } from 'antd';
import moment from 'moment';
const TabPane = Tabs.TabPane;
const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

class ProductLine extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      productData: [],
      show: false,
      title: '生産線',
      formLayout: 'vertical',
      label: '工廠名稱',
      inputVal: '',
      init: {},
      processInit: {},
      dialogType: '',
      procedureData: [],
      processParams: {},
      gaugeData: [],
      processInit: {},
      editIndex: -1,
      factory_type: defaultFactoryType,
      processFilters: [],
      facData: [],
      depData: [],
      // factory_type: 'I',
      // factory_type: 'O',
      factory_name: '',
      canAddProduct: true,
      factory_code: '',
    }
  }
  valueChange = (v) => {
    console.log('valueChange v ：', v.target.value, this.props, );
    this.setState({ inputVal: v.target.value })
  }
  showDialog = () => this.setState({ show: true, })
  closeDialog = () => this.setState({ show: false, processInit: {},})
  noSelfCompnay = v => {
    const {factory_type, } = this.state
    const isCompnay = factory_type !== 'O'
    if (!isCompnay) {
      v.dep_no = '*'
      v.fac_no = '*'
    }
  }
  handleOk = () => {
    console.log('handleOk ：', this.forms, this.state, this.state.inputVal);
    const {inputVal, productData, dialogType, processInit, procedureData, factory_type, gaugeData, } = this.state
    const {gauge,   } = processInit
    const work_nos = processInit.work_no
    const isCompnay = factory_type !== 'O'
    console.log('dialogType ：', dialogType, processInit, factory_type, )
    if (dialogType === 'addProductLine') {
      console.log('增加生产线11 ：', )
      this.forms.validateFields((err, values) => {
        console.log('values ：', err, values); //
        this.noSelfCompnay(values)
        if (!err) {
          // return //  
          this.addProductLine({...this.rateFilter({...values, work_no: Number(values.work_no), }), factory_type})
          console.log('@@@@@@@@values ：', {...this.rateFilter({...values, work_no: Number(values.work_no), }), factory_type})
        }
      })
    } else if (dialogType === 'editProcess') {
      console.log('编辑生产线 ：', )
      this.forms.validateFields((err, values) => {
        this.noSelfCompnay(values)
        console.log('values ：', err, values);
        if (!err) {
          values.factory_id = Number(processInit.factory_id)
          values.void = values.void ? 'Y' : 'N'
          const {factory_type,  } = this.state
          const isCompnay = factory_type !== 'O'
          // const work_no = !isCompnay ? workNoArr.some(v => v == values.work_no) ? values.work_no : procedureData.find(v => v.work_name == values.work_no).work_no : work_nos
          let work_no
          if (values.work_no != 0 ) {
            work_no = workNoArr.some(v => v == values.work_no) ? values.work_no : procedureData.find(v => v.work_name == values.work_no).work_no
          } else {
            work_no = values.work_no 
          } 
          
          // values.gauge = gauge
          // values.work_no = work_no
          // const {gauge_no } = gaugeData.find(v => v.gauge_txt.trim() === values.gauge.trim())
          console.log(' work_no ： ', work_no,  )// 
          console.log('values222, work_no gauge_no：', work_nos, err, work_no, isCompnay, {...values, work_no: Number(work_no), } );
          // return  
          // this.editProcess(this.rateFilter(isCompnay ? values : {...values, work_no: Number(work_no), }))
          this.editProcess(this.rateFilter({...values, work_no: Number(work_no), }))

          // console.log('values333 ：', err, values);
          // this.editProcess(this.rateFilter(values))
        }
      })
    } else if (dialogType === 'addProcess') {
      console.log('增加工序 ：', )
      this.forms.validateFields((err, values) => {
        console.log('values ：', err, values);
        if (!err) {
          values.s_date = moment(values.s_date).format(DATE_FORMAT_BAR)
          values.workers = Number(values.workers)
          values.factory_id = Number(processInit.factory_id)
          // if (!isCompnay) {
          //   console.log('不是本公司 ：', )
          //   values.workers = 0
          //   // values.gauge = '*'
          //   values.pts = 0
          // } 
          console.log('factory_type ? ：',  values, procedureData, values.work_no,  )
          const work_no = gauge != undefined ? procedureData.find(v => v.work_name == values.work_no).work_no : Number(values.work_no)
          console.log(' work_no ： ', work_no, {...values, work_no} )// 
          this.addProcess({...values, work_no})
          // this.addProcess(values)
        }
      })
    } else if (dialogType === 'editCapacity') {
      console.log('编辑工序 ：', )
      this.forms.validateFields((err, values) => {
        console.log('values ：', err, values);
        if (!err) {
          values.s_date = moment(values.s_date).format(DATE_FORMAT_BAR)
          values.workers = Number(values.workers)
          values.factory_id = processInit.factory_id
          values.seq_no = processInit.seq_no
          values.pts = Number(values.pts)
          const work_name = procedureData.filter(v => v.work_name === processInit.work_name)[0].work_no
          // values.work_no = work_name
          console.log('@@@@@@@@values ：', values)
          this.editCapacity({...values, work_no: work_name, work_nos: values.work_no})
        }
      })
    } 
  }
  rateFilter = (v) => ({...v, prod_rate: v.prod_rate / 100})
  beforeAddProductLine = (t) => {
    const {canAddProduct, } = this.state 
    console.log('beforeAddProductLine ：', t, this.state )
    if (!canAddProduct) {
      confirms(2, '所有針種已用完，無法新增産線 o(*￣︶￣*)o')
      return
    }
    this.setState({
      dialogType: t,
      show: true,
    })
  }
  valHandle = (p, filterArr, filterKey, ak) => {
    console.log(' valHandle ： ', p, p[ak], filterKey, filterArr, ak )
    return p[ak] = filterArr.find(v => {// 
      // console.log(' v ： ', v, p, p[filterKey], v[filterKey] )
      return p[filterKey].trim() == v[filterKey].trim()
    })[ak]
  }
  addProductLine = (p) => {
    console.log('addProductLineaddProductLine ：', p, this.state, )
    addProductLine(p).then(res => {
      console.log('addProductLine res ：', res.data);
      const {code, mes, recordset} = res.data
      // openNotification(mes)
      if (code === 1) {
        const {productData, factory_type, depData, facData, procedureData,  } = this.state
        const isCompnay = factory_type !== 'O'
        recordset.dep_no = p.dep_no
        recordset.fac_no = p.fac_no
        const work_name = procedureData.find(item => item.work_no === recordset.work_no).work_name
        console.log(' work_name ： ', work_name,  )// 
        recordset.work_name = work_name
        if (isCompnay) {
          recordset.dep_name = depData.find(v => v.dep_no === p.dep_no).dep_name
          recordset.fac_name = facData.find(v => v.fac_no === p.fac_no).fac_name
        }
        if (!isCompnay) {
          // const work_name = procedureData.find(item => item.work_no === recordset.work_no).work_name
          // console.log(' work_name ： ', work_name,  )// 
          // recordset.work_name = work_name
          recordset.capatitys = []
        } else {
          recordset.historys = []
        }
        this.setState({
          // productData: [recordset, ...productData, ],
          productData: [recordset, ...productData, ],
          processInit: {}, 
          show: false,
        })
      }
    })
  }
  addProcess = (v,) => {
    console.log('11111addProcess v ：', v,);
    addProcess(v).then(res => {
      console.log('addProcess res ：', res.data, this.state);
      const {code, mes, data, } = res.data
      // openNotification(mes)
      const {productData, processInit, factory_type, depData, facData, procedureData, } = this.state
      const isCompnay = factory_type !== 'O'
      const { factory_id, gauge, } = processInit
      console.log('productData, processInit,  ：', productData, processInit,  factory_id, gauge,);
      // 新增产能对应的下标
      const index = productData.findIndex(v => v.factory_id === factory_id)
      // 该行是否已有针种等信息
      const isExistInfo = productData[index].gauge != undefined
      // const changeItem = processInit.data.filter(v => v.factory_id === factory_id)
      console.log('isEqual ：', data, isExistInfo, index, productData[index], productData[index].gauge);
      if (code === 1) {
        if (isExistInfo) {
          if (isCompnay) {
            data[1].dep_no = processInit.dep_no
            data[1].fac_no = processInit.fac_no
            data[1].dep_name = processInit.dep_name
            data[1].fac_name = processInit.fac_name
            data[1].prod_rate = processInit.prod_rate
          }
          // 是否是新增同一个针种 需要插入历史记录
          console.log('已有针种 ：',  )
          // const isExist = (productData[index].gauge === data[0].gauge)
          // if (isExist) {
            console.log('新增同一个针种 需要插入历史记录 ：', )
            const {historys, factory_code, factory_name} = productData[index]
            // productData[index] = {...data[1], factory_code, factory_name, historys: [data[0], ...historys]}
            productData[index] = data.length > 1 ? {...data[1], historys: [data[0], ...historys]} : {...data[0], historys: []}
            console.log('productData ：', productData, productData[index], );
            this.setState({
              productData: [...productData],
              processInit: {}, 
              show: false, 
            })
          // }
          //  else {
          //   console.log('不需要插入历史记录 ：', )
          //   const {factory_code, factory_name} = productData[index]
          //   productData[index] = {...data[0], factory_code, factory_name, historys: []}
          //   console.log('productData ：', productData, productData[index], factory_code, factory_name);
          //   this.setState({
          //     productData: [...productData],
          //     show: false, 
          //   })
            
          // }
        } else {
          console.log('该产能为空 直接新增 ：', )
          const {factory_code, factory_name} = productData[index]
          // if (!isCompnay) {
          //   // productData.forEach((item, i) => {
          //   //   if (item.factory_id === factory_id) {
          //   //     console.log('匹配项 factory_id item ：', item, i, data[0], [...item.capatitys, data[0]])
          //   //     item.capatitys = [...item.capatitys, data[0]]
          //   //   }
          //   // })
          //   productData[index].capatitys = [...productData[index].capatitys, data[0]]
          // } else {
          //   productData[index] = {...data[0], historys: []}
          // }
          // productData[index] = {...data[0], dep_no: processInit.dep_no, fac_no: processInit.fac_no, dep_name: processInit.dep_name, fac_name: processInit.fac_name, historys: []}
          const work_name = procedureData.find(item => item.work_no === v.work_no).work_name
          data[0].prod_rate = processInit.prod_rate
          productData[index] = {...data[0], gauge: v.gauge, work_name, historys: []}
          console.log('productData ：', productData, productData[index], factory_code, factory_name);
          this.setState({
            productData: [...productData],
            processInit: {}, 
            show: false, 
          })
        }
      }
    })
  }
  editProcess = (v,) => {
    console.log('editProcess v 编辑上产线', v,);
    editProcess(v).then(res => {
      console.log('editProcess res ：', res.data);
      const {code, mes, } = res.data
      // openNotification(mes)
      if (code === 1) {
        const {productData, processInit, depData, facData, factory_type, procedureData,   } = this.state
        const {factory_id, factory_code, factory_name, dep_no, fac_no} = v
        const isCompnay = factory_type !== 'O'
        // productData.forEach(item => {
        //   if (item.factory_id === factory_id) {
        //     item.factory_code = factory_code
        //     item.factory_name = factory_name
        //     item.dep_no = dep_no
        //     item.fac_no = fac_no
        //     item.prod_rate = v.prod_rate
        //     item.void = v.void
        //     console.log('item ：', item, v)
        //   }
        //   return v
        // })
        const productDatas = productData.map(item => {
          if (item.factory_id === factory_id) {
            if (isCompnay) {
              item = {...item,...v, dep_name: depData.find(v => v.dep_no === dep_no).dep_name, fac_name: facData.find(v => v.fac_no === fac_no).fac_name}
            } else {
              console.log(' 更新不是本公司 ：', item, v, )
              item = {
                ...item,
                ...v, 
                work_name: v.work_no != 0 ? procedureData.find(item => item.work_no == v.work_no).work_name : v.work_name,
              }
            }
            console.log(' 更新item ：', item, v)
          }
          return item
          // return {...item, ...v, dep_name: depData.find(v => v.dep_no === dep_no).dep_name, fac_name: facData.find(v => v.fac_no === fac_no).fac_name}
        })
        console.log(' productData v ：', productDatas) 
        this.setState({
          productData: [...productDatas],
          processInit: {}, 
          show: false,
        })
      }
    })
  }
  deleteProcess = (v, ) => {
    console.log('deleteProcess v 111111：', v, );
    deleteProcess(v).then(res => {
      console.log('deleteProcess res ：', res.data);
      const {code, mes, } = res.data
      // openNotification(mes)
      if (code === 1) {
        const {productData, } = this.state
        const {factory_id,  } = v 
        const newData = productData.filter(item => item.factory_id !== factory_id)
        console.log(' productData v ：', newData, factory_id, ) 
        this.setState({
          productData: [...newData],
          processInit: {}, 
          show: false,
        })
      }
    })
  }
  editCapacity = (p, ) => {
    console.log('editCapacity p ', p, );
    editCapacity(p).then(res => {
      console.log('editCapacity res ：', res.data, this.state, );
      const {code, mes, data} = res.data
      const {productData, processInit, editIndex, procedureData } = this.state
      // openNotification(mes)
      if (code === 1) {
        const { factory_id, gauge, } = processInit
        console.log('修改了时间rocessInit.s_date != data[data.length].s_date：', processInit, data[data.length - 1], processInit != data[data.length - 1])
        // if (processInit.s_date != data[data.length - 1].s_date) {
        //   console.log('修改了时间 ：', )
        // } else {
          console.log('不是修改时间 ：', )
          productData.forEach(v => {
            // console.log(' productData v ：', v, ) 
            if (v.factory_id === factory_id) {
              console.log('匹配项 ：', v, v.workers, p, procedureData )
              // const {historys, ...editItem} = backupFn(v)
              // console.log('historys ：', historys, {...data[data.length - 1], historys});
              // v.historys[0].e_date = data[0].e_date
              // const {workers, s_date, pts, } = data[data.length - 1]
              v.gauge = p.gauge
              // v.work_name = procedureData.find(v => v.work_no == p.work_nos).work_name
              v.work_name = p.work_nos
              v.workers = p.workers
              v.s_date = p.s_date
              v.pts = p.pts
            } 
            // return v
          })
          console.log('productData ：', productData);
          // const matchItem = productData.findIndex(v => v.factory_id === factory_id)
          // console.log('matchItem ：', matchItem);
          this.setState({
            productData: [...productData],
            processInit: {}, 
            show: false,
          })
        // }

        // productData.map(v => {
        //   // console.log(' productData v ：', v, factory_id) 
        //   if (v.factory_id === processInit.factory_id) {
        //     console.log('v.capatitys[index] ：', v.capatitys[editIndex]);
        //     const {historys, ...editItem} = backupFn(v.capatitys[editIndex])
        //     v.capatitys[editIndex] = {...data[0], historys: [...historys, {...editItem, seq_no: editItem.seq_no + 1}, ]}
        //     console.log('相等 ：', v, editItem, v.capatitys[editIndex], historys, editItem)
        //   } 
        //   return v
        // })
        // console.log('productData ：', productData);
      }
    })
  }
  beforeProcessAction = (t, info) => {
    console.log('beforeProcessAction v ：', t, info);
    this.setState({
      dialogType: t,
      processInit: backupFn(info),
      factory_name: info.factory_name,
      show: true,
      factory_code: info.factory_code,
    })
  }
  beforeAction = (t, o, i) => {
    console.log('beforeAction v ：', t, o, i);
    // this.gaugeFilter('')// 
    this.setState({
      dialogType: t,
      processInit: backupFn(o),
      show: true,
      editIndex: i,
      factory_name: o.factory_name,
    })
  }
  tabClick = (key) => {
    console.log('key ：', key);
    this.getProduct(key)
    this.setState({
      factory_type: key,
      productData: [],
    })
  }
  workFilter = (v) => {
    const {procedureData, processInit, } = this.state
    console.log(' processInit.capatitys ： ', processInit  )
    const result = processInit.capatitys.some(item => item.work_no === v)
    // console.log('workFilter result ：', v, result);
    return result
  }
  gaugeFilter = (v) => {
    // return false
    const {productData, factory_type, } = this.state
    const isCompnay = factory_type !== 'O'
    const result = productData.some(item => item.gauge === v && item.work_no != 0)
    // console.log('gaugeFilter过滤 result ：', v, result);// 
    return isCompnay ? result : false
  }
  gaugeOutFilter = (v) => {
    // return false
    const {productData, factory_type, factory_code, procedureData, } = this.state
    const result = productData.some(item => {
      if (item.factory_code.trim() == factory_code.trim()) {
        // console.log(' 相等 factory_code ：', item.work_no, factory_code, item.factory_code, item, item.gauge, v, item.gauge.trim(), v.trim(), item.gauge.trim() === v.trim()  );// 
        return item.gauge.trim() == v.trim() && item.work_no != 0
      }// 
    })// // 
    // console.log('gaugeOutFilter过滤 result ：', v, result, factory_code, this.state,  );
    return result
  }
  inputCb = (v) => {
    console.log(' inputCb ： ',  v )
    this.setState({
      factory_code: v.v.trim(),
    })
  }
  loadFn = (p) => {
    const {procedureData, } = this.state
    // const work_no = procedureData.find(v => v.work_name == p.work_no).work_no
    const matchItem = procedureData.find(v => v.work_name == p.work_no)
    const work_no = matchItem != undefined  ? matchItem.work_no : p.work_no
    console.log(' loadFn ： ',  p, procedureData, work_no )
    return work_no
  }
  getProduct = (factory_type = defaultFactoryType) => {
  // getProduct = (factory_type = 'O') => {
    getProduct({factory_type}).then(res => {
      // console.log('getProduct res ：', res.data);
      const {gaugeData, } = this.state
      const {code, capatity, factory, gauge, history, dep_no, fac_no,} = res.data
      console.log('factory_type !== defaultFactoryType ：', factory_type,  defaultFactoryType);
      if (code === 1) {
        const isSelf = factory_type === defaultFactoryType
        // if (factory_type === defaultFactoryType) {
          // console.log(' 本公司 ：', )
          let gauges = []
          if (isSelf) {
            gauges = gauge.map((v, i) => capatity.some(item => item.gauge.trim() === v.gauge_no.trim()))
          }
          console.log(' gauges ： ', gauges,  )
          const canAddProduct = gauges.some(item => !item)
          const canAdds = gauges.some(item => !true)
          console.log(' isCanAdd ： ', canAddProduct, canAdds,  )
          gauge.forEach(v => v.gauge_txt = sewKey.includes(v.gauge_no.trim()) ? sewData.find(p => p.sew_no === v.gauge_no.trim()).sew : v.gauge_no)
          capatity.forEach(v => v.historys = factory_type === defaultFactoryType ? history.filter(item => item.gauge === v.gauge) : history.filter(item => item.factory_id === v.factory_id))
          console.log('capatitycapatitycapatity ：', capatity)
          this.setState({
            // productData: capatity.reverse(),
            productData: capatity,
            gaugeData: [...gauge, ],
            facData: fac_no,
            depData: dep_no,
            depData: dep_no,
            canAddProduct: isSelf ? canAddProduct : true,
            // canAddProduct: false,
          })
        // } else {
        //   console.log('非本公司 ：', )
        //   factory.forEach(v => {
        //     const capatitys = capatity.filter(item => item.factory_id === v.factory_id)
        //     v.capatitys = capatitys
        //   })
        //   this.setState({
        //     productData: factory,
        //   })
        // }
      }
    })
  }
  getProcedure = () => {
    getProcedure({}).then(res => {
      console.log('!!!!!!!getProcedure res ：', res.data);
      const {processFilters, } = this.state
      // const processFilters = []
      res.data.data.forEach((v, i) => {
        // console.log('  v ：', v, i, )
        processFilters.push({
          text: v.work_name,
          value: v.work_name,
        })
      })
      
      this.setState({
        procedureData: res.data.data,
        processFilters: [...processFilters]
      })
    })
  }
  componentDidMount() {
    this.getProduct()
    this.getProcedure()
  }
  render() {
    const {
       productData,
       show, 
       formLayout, 
       label, 
       init, 
       dialogType, 
       processInit, 
       procedureData, 
       gaugeData, 
       factory_type,  
       processFilters,
       facData,
       depData,
       factory_name,
    } = this.state
    const {middle,  } = this.props.route
    const isCompnay = factory_type !== 'O'
    const commonConfig = {
      formItemLayout,
      formLayout: 'horizontal',
      ref: forms => this.forms = forms,
    }
    let content, title, config, processForms, work_no, processInits, disableDay
    if (isCompnay) {
      // console.log('Object.keys(processInit),  ：', Object.keys(processInit), processInit.historys);
      disableDay = Object.keys(processInit).length && processInit.historys != undefined && processInit.historys.length ? processInit.historys[0].e_date : ''
    }// 
    // const productLineConfig = isCompnay ? productLineForm : productLineForm.filter(v => v.key !== 'fac_no' && v.key !== 'dep_no')
    const productLineConfig = isCompnay ? productLineForm : productLineOutForm
    // if (depData.length && facData.length) {
    console.log('ProductLine 组件this.state, this.props ：', this.state, this.props, processInit, );
    if (Object.keys(processInit).length && isCompnay) {
      this.valHandle(processInit, depData, 'dep_no', 'dep_no') 
      this.valHandle(processInit, facData, 'fac_no', 'fac_no')
      // console.log(' this.valHandle() ： ', this.valHandle(processInit, depData, 'dep_no', 'dep_no'), this.valHandle(processInit, facData, 'fac_no', 'fac_no') )
      // console.log(' processInit ： ', processInit,  )
      // console.log(' processInit 111111111111 ： ',  processInit, processInit.dep_no, processInit.fac_no, )
      // processInit.dep_name = depData.find(v => processInit.dep_no.trim() == v.dep_no.trim()).dep_name
      // processInit.fac_name = facData.find(v => processInit.fac_no.trim() == v.fac_no.trim()).fac_name
    }
    const selfConfig = {}
    selfConfig.disableKey = 'work_no'
    selfConfig.disableOption = 'gauge'  
    if (isCompnay) {
    }
    const {gauge} = processInit
    const haveGauge = gauge != undefined
    
    if (show) {
      switch (dialogType) {
        case 'addProductLine':
          title = '新增生産線'
          console.log('新增生産線 ：', productLineConfig)
          content = <ProduceForm 
              {...commonConfig}
              init={{}} 
              facData={facData}
              depData={depData}
              config={productLineConfig}
              
              {...selfConfig}
              gaugeData={gaugeData}
              procedureData={procedureData} 
              gaugeFilter={this.gaugeOutFilter}
              workFilter={this.workFilter}
              inputCb={this.inputCb}
            />
          break;
        case 'editProcess':
          title = `編輯 ${factory_name} 生産線`
          // const productLineConfigs = !isCompnay ? backupFn(productLineConfig).map(v => {
          //   v.disabled = processDisableArr.some(item => item === v.key)
          //   return v 
          // }) : productLineConfig
          work_no = haveGauge && processInit.work_no !== 0 ? procedureData.find(v => {
            console.log(' v.work_no == processInit.work_no ： ', v.work_no, processInit.work_no, v.work_no == processInit.work_no )// 
            return v.work_no == processInit.work_no 
          }).work_name : processInit.work_no 
          // const gauge_txt = haveGauge && processInit.work_no !== 0 ? gaugeData.find(v => v.gauge_no === processInit.gauge).gauge_txt : processInit.gauge
          processInits = {...processInit, work_no, 
            // gauge: gauge_txt, 
           }
          console.log('編輯生産線 ：', processInit, productLineConfig, isCompnay )
          content = <ProduceForm 
              {...commonConfig}
              facData={facData}
              depData={depData}

              gaugeData={gaugeData}
              procedureData={procedureData} 
              gaugeFilter={this.gaugeOutFilter}
              inputCb={this.inputCb}

              // 初次打开 表单 请求 select下拉列表的数据 修改初始值
              isLoadSelect={true}
              loadFn={this.loadFn}

              {...selfConfig}

              init={{...processInits, void: processInits.void === 'Y', prod_rate: (processInits.prod_rate * 100).toFixed(2)}} 
              // init={{...processInit, void: processInit.void === 'Y', prod_rate: (processInit.prod_rate * 100).toFixed(2)}} 
              config={[...productLineConfig, disableFactory]}
            />
          break;
        case 'addProcess':
          title = `新增 ${factory_name} 産能`
          // processForms = isCompnay ? backupFn(processForm) : backupFn(processForm).filter(v => v.key !== 'gauge')
          // processForms = isCompnay ? backupFn(processForm) : onSelfCompany
          processForms = backupFn(processForm)
          // haveGauge && processForms.map(v => v.disabled = processDisableArr.some(item => item === v.key))
          console.log(' procedureData ： ', procedureData, processInit.work_no )// 
          // work_no = haveGauge ? procedureData.find(v => v.work_no == processInit.work_no).work_name : ''
          const matchItem = procedureData.find(v => v.work_no == processInit.work_no)
          work_no = haveGauge ? matchItem != undefined ? matchItem.work_name : processInit.work_no : processInit.work_no 
          processInits = haveGauge ? {...processInit, work_no, } : {}
          // processInit.s_date = processInit.s_date ? processInit.s_date : '' 
          console.log('新增産能 111：', matchItem, processInits, processInit, work_no, disableDay, processForm, gauge, processInits, processInits.historys != undefined,)
          content = <ProduceForm 
              {...commonConfig}
              init={processInits} 
              gaugeData={gaugeData}
              procedureData={procedureData} 
              {...selfConfig}
              config={processForms}
              disableDay={disableDay}
              dateDisable={(processInits.historys != undefined && !processInits.historys.length) ||processInits.historys == undefined}
              gaugeFilter={this.gaugeFilter}
              workFilter={this.workFilter}
              isLoadSelect={true}
              loadFn={this.loadFn}
            />
          break;
        case 'editCapacity':
          title = `編輯 ${factory_name} 産能`
          console.log('編輯産能1 ：', show, processForms, processInit, procedureData, haveGauge)
          // processForms = isCompnay ? backupFn(processForm) : backupFn(processForm).filter(v => v.key !== 'gauge')
          processForms = backupFn(processForm)
          haveGauge && processForms.map(v => v.disabled = processDisableArr.some(item => item === v.key))
          // processForms.map(v => v.disabled = processDisableArr.some(item => item === v.key))
          // work_no = haveGauge ? procedureData.find(v => v.work_no == processInit.work_no).work_name : ''
          const matchItems = procedureData.find(v => v.work_no == processInit.work_no)
          work_no = haveGauge ? matchItems != undefined ? matchItems.work_name : processInit.work_no : processInit.work_no 
          // processInit.s_date = processInit.s_date ? processInit.s_date : '' 
          processInits = {...processInit, work_no, }
          console.log(' processInits ： ', processInits,  )// 
          content = <ProduceForm 
              {...commonConfig}
              init={processInits} 
              gaugeData={gaugeData}
              procedureData={procedureData} 
              {...selfConfig}
              config={processForms}
              disableDay={disableDay}
              gaugeFilter={this.gaugeFilter}
              isLoadSelect={true}
              loadFn={this.loadFn}
            />
          break;
        case 'deleteProcess':
          title = `删除 ${factory_name} 生产线`
          console.log('删除生产线  ：', )
          content = <div className='editWrapper warnDialog'>{`是否確認删除生产线 工厂代號：${processInit.factory_code} 工厂名稱：${processInit.factory_name}?`}</div>
          break;
      }
    }
    const width = (dialogType === 'deleteProcess' || dialogType === 'deleteData') ? '40%' : '50%'
    const rowContent = productData.length ? <ProductTable 
        data={productData}
        info={productData[0]}
        beforeProcessAction={this.beforeProcessAction}
        beforeAction={this.beforeAction}
        factory_type={factory_type}

        procedureData={procedureData}
        processFilters={processFilters}
        middle={middle}
      ></ProductTable> : null
    return (
      <section className="productLine">
        <Tabs 
        defaultActiveKey={defaultFactoryType} 
        // defaultActiveKey={'O'} 
        onChange={this.tabClick}>
          <TabPane tab={<span><Icon type="idcard" />本公司</span>} key="I">
            <Collapses animate={ANIMATE.fadeIn} title={'生産線'} className="productLine"
              middle={middle}
              extra={
                <div className="btnWrapper">
                  <Button onClick={this.beforeAddProductLine.bind(this, 'addProductLine')} type="primary" icon="plus-circle-o" className="m-l">新增生産線</Button>
                </div>}>
              {rowContent}
            </Collapses>
          </TabPane> 
          <TabPane className='noSelfCompany' tab={<span><Icon type="schedule" />不是本公司</span>} key="O">
            <Collapses animate={ANIMATE.fadeIn} title={'生産線'} className="productLine"
              middle={middle}
              extra={
                <div className="btnWrapper">
                  <Button onClick={this.beforeAddProductLine.bind(this, 'addProductLine')} type="primary" icon="plus-circle-o" className="m-l">新增生産線</Button>
                </div>}>
              {rowContent}
            </Collapses>
          </TabPane>
        </Tabs>

        {
          show ? <ProductDialog 
              className='productLineDialog' 
              show={show} 
              width={width}
              handleOk={this.handleOk} 
              close={this.closeDialog} 
              title={title}
            >
              {content}
          </ProductDialog> : null
        }
      </section>
    );
  }
}
export default ProductLine;
