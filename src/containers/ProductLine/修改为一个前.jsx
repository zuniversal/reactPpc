import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getProduct, addProductLine, addProcess, editProcess, editCapacity, deleteProcess,  } from 'api/productLine'
import { getProcedure,  } from 'api/Procedure'
import {productLineForm, processForm, DATE_FORMAT_BAR, processDisableArr, disableFactory, onSelfCompany, } from 'config'
import { ANIMATE, defaultFactoryType, } from 'constants'
import { openNotification, backupFn, confirms, } from 'util'
import Collapses from '@@@/Collapses'
import ProduceForm from '@@@/ProduceForm'
import ProductTable from '@@@/ProductTable'
import ProductDialog from '@@@/ProductDialog'
import { Tabs, Icon, Button, } from 'antd';
import moment from 'moment';
const TabPane = Tabs.TabPane;
const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

class ProductLine extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      productData: [],
      show: false,
      title: '生産線',
      formLayout: 'vertical',
      label: '工廠名稱',
      inputVal: '',
      init: {},
      processInit: {},
      dialogType: '',
      procedureData: [],
      processParams: {},
      gaugeData: [],
      processInit: {},
      editIndex: -1,
      factory_type: defaultFactoryType,
      processFilters: [],
      // factory_type: 'I',
      // factory_type: 'O',
    }
  }
  valueChange = (v) => {
    console.log('valueChange v ：', v.target.value, this.props, );
    this.setState({ inputVal: v.target.value })
  }
  showDialog = () => this.setState({ show: true, })
  closeDialog = () => this.setState({ show: false, processInit: {},})
  handleOk = () => {
    console.log('handleOk ：', this.forms, this.state, this.state.inputVal);
    const {inputVal, productData, dialogType, processInit, procedureData, factory_type, } = this.state
    const {gauge} = processInit
    console.log('dialogType ：', dialogType, processInit, factory_type, )
    if (dialogType === 'addProductLine') {
      console.log('增加生产线 ：', )
      this.forms.validateFields((err, values) => {
        console.log('values ：', err, values);
        if (!err) {
          this.addProductLine({...this.rateFilter(values), factory_type})
          console.log('@@@@@@@@values ：', values)
        }
      })
    } else if (dialogType === 'editProcess') {
      console.log('编辑生产线 ：', )
      this.forms.validateFields((err, values) => {
        console.log('values ：', err, values);
        if (!err) {
          values.factory_id = Number(processInit.factory_id)
          values.void = values.void ? 'Y' : 'N'
          this.editProcess(this.rateFilter(values))
        }
      })
    } else if (dialogType === 'addProcess') {
      console.log('增加工序 ：', )
      this.forms.validateFields((err, values) => {
        console.log('values ：', err, values);
        if (!err) {
          values.s_date = moment(values.s_date).format(DATE_FORMAT_BAR)
          values.workers = Number(values.workers)
          values.factory_id = Number(processInit.factory_id)
          if (gauge != undefined) {
            console.log('有针种 新增 ：', procedureData.find(v => v.work_name === values.work_no))
          } 
          const isCompnay = factory_type !== 'O'
          if (!isCompnay) {
            console.log('不是本公司 ：', )
            values.workers = 0
            values.gauge = '*'
            values.pts = 0
          } 
          console.log('factory_type ? ：',  values, moment(values.s_date).format(DATE_FORMAT_BAR))
          values.work_no = gauge != undefined ? procedureData.find(v => v.work_name === values.work_no).work_no : Number(values.work_no)
          // values.work_no = processInit.data[0].work_no
          this.addProcess(values)
        }
      })
    } else if (dialogType === 'editCapacity') {
      console.log('编辑工序 ：', )
      this.forms.validateFields((err, values) => {
        console.log('values ：', err, values);
        if (!err) {
          values.s_date = moment(values.s_date).format(DATE_FORMAT_BAR)
          values.workers = Number(values.workers)
          values.factory_id = processInit.factory_id
          values.seq_no = processInit.seq_no
          values.pts = Number(values.pts)
          const work_name = procedureData.filter(v => v.work_name === processInit.work_name)[0].work_no
          values.work_no = work_name
          console.log('@@@@@@@@values ：', values)
          this.editCapacity(values)
        }
      })
    } 
  }
  rateFilter = (v) => ({...v, prod_rate: v.prod_rate / 100})
  beforeAddProductLine = (t) => {
    console.log('beforeAddProductLine ：', t)
    this.setState({
      dialogType: t,
      show: true,
    })
  }
  addProductLine = (p) => {
    console.log('addProductLineaddProductLine ：', p, this.state, )
    addProductLine(p).then(res => {
      console.log('addProductLine res ：', res.data);
      const {code, mes, recordset} = res.data
      // openNotification(mes)
      if (code === 1) {
        const {productData, factory_type, } = this.state
        const isCompnay = factory_type !== 'O'
        if (!isCompnay) {
          recordset.capatitys = []
        } else {
          recordset.historys = []
        }
        this.setState({
          productData: [...productData, recordset, ],
          show: false,
        })
      }
    })
  }
  addProcess = (v,) => {
    console.log('11111addProcess v ：', v,);
    addProcess(v).then(res => {
      console.log('addProcess res ：', res.data, this.state);
      const {code, mes, data, } = res.data
      // openNotification(mes)
      const {productData, processInit, factory_type, } = this.state
      const { factory_id, gauge, } = processInit
      console.log('productData, processInit,  ：', productData, processInit,  factory_id, gauge,);
      // 新增产能对应的下标
      const index = productData.findIndex(v => v.factory_id === factory_id)
      // 该行是否已有针种等信息
      const isExistInfo = productData[index].gauge != undefined
      // const changeItem = processInit.data.filter(v => v.factory_id === factory_id)
      console.log('isEqual ：', isExistInfo, index, productData[index], productData[index].gauge);
      if (code === 1) {
        if (isExistInfo) {
          // 是否是新增同一个针种 需要插入历史记录
          console.log('已有针种 ：',  )
          // const isExist = (productData[index].gauge === data[0].gauge)
          // if (isExist) {
            console.log('新增同一个针种 需要插入历史记录 ：', )
            const {historys, factory_code, factory_name} = productData[index]
            // productData[index] = {...data[1], factory_code, factory_name, historys: [data[0], ...historys]}
            productData[index] = {...data[1], historys: [data[0], ...historys]}
            console.log('productData ：', productData, productData[index], );
            this.setState({
              productData: [...productData],
              show: false, 
            })
          // }
          //  else {
          //   console.log('不需要插入历史记录 ：', )
          //   const {factory_code, factory_name} = productData[index]
          //   productData[index] = {...data[0], factory_code, factory_name, historys: []}
          //   console.log('productData ：', productData, productData[index], factory_code, factory_name);
          //   this.setState({
          //     productData: [...productData],
          //     show: false, 
          //   })
            
          // }
        } else {
          console.log('该产能为空 直接新增 ：', )
          const {factory_code, factory_name} = productData[index]
          const isCompnay = factory_type !== 'O'
          if (!isCompnay) {
            // productData.forEach((item, i) => {
            //   if (item.factory_id === factory_id) {
            //     console.log('匹配项 factory_id item ：', item, i, data[0], [...item.capatitys, data[0]])
            //     item.capatitys = [...item.capatitys, data[0]]
            //   }
            // })
            productData[index].capatitys = [...productData[index].capatitys, data[0]]
          } else {
            productData[index] = {...data[0], historys: []}
          }
          console.log('productData ：', productData, productData[index], factory_code, factory_name);
          this.setState({
            productData: [...productData],
            show: false, 
          })
        }
      }
    })
  }
  editProcess = (v,) => {
    console.log('editProcess v 编辑上产线', v,);
    editProcess(v).then(res => {
      console.log('editProcess res ：', res.data);
      const {code, mes, } = res.data
      // openNotification(mes)
      if (code === 1) {
        const {productData, processInit} = this.state
        const {factory_id, factory_code, factory_name, } = v
        productData.forEach(item => {
          if (item.factory_id === factory_id) {
            item.factory_code = factory_code
            item.factory_name = factory_name
            item.prod_rate = v.prod_rate
            item.void = v.void
            console.log('item ：', item, v)
          }
          return v
        })
        console.log(' productData v ：', productData) 
        this.setState({
          productData: [...productData],
          show: false,
        })
      }
    })
  }
  deleteProcess = (v, ) => {
    console.log('deleteProcess v 111111：', v, );
    deleteProcess(v).then(res => {
      console.log('deleteProcess res ：', res.data);
      const {code, mes, } = res.data
      // openNotification(mes)
      if (code === 1) {
        const {productData, } = this.state
        const {factory_id,  } = v
        const newData =  productData.filter(item => item.factory_id !== factory_id)
        console.log(' productData v ：', newData, factory_id, ) 
        this.setState({
          productData: [...newData],
          show: false,
        })
      }
    })
  }
  editCapacity = (v, ) => {
    console.log('editCapacity v ', v, );
    editCapacity(v).then(res => {
      console.log('editCapacity res ：', res.data, this.state, );
      const {code, mes, data} = res.data
      const {productData, processInit, editIndex } = this.state
      // openNotification(mes)
      if (code === 1) {
        const { factory_id, gauge, } = processInit
        console.log('修改了时间rocessInit.s_date != data[data.length].s_date：', processInit.s_date, data[data.length - 1].s_date, processInit.s_date != data[data.length - 1].s_date)
        // if (processInit.s_date != data[data.length - 1].s_date) {
        //   console.log('修改了时间 ：', )
        // } else {
          console.log('不是修改时间 ：', )
          productData.forEach(v => {
            // console.log(' productData v ：', v, ) 
            if (v.factory_id === factory_id) {
              console.log('匹配项 ：', v)
              // const {historys, ...editItem} = backupFn(v)
              // console.log('historys ：', historys, {...data[data.length - 1], historys});
              // v.historys[0].e_date = data[0].e_date
              const {workers, s_date, pts, } = data[data.length - 1]
              v.workers = workers
              v.s_date = s_date
              v.pts = pts
            } 
            // return v
          })
          console.log('productData ：', productData);
          // const matchItem = productData.findIndex(v => v.factory_id === factory_id)
          // console.log('matchItem ：', matchItem);
          this.setState({
            productData: [...productData],
            show: false,
          })
        // }

        // productData.map(v => {
        //   // console.log(' productData v ：', v, factory_id) 
        //   if (v.factory_id === processInit.factory_id) {
        //     console.log('v.capatitys[index] ：', v.capatitys[editIndex]);
        //     const {historys, ...editItem} = backupFn(v.capatitys[editIndex])
        //     v.capatitys[editIndex] = {...data[0], historys: [...historys, {...editItem, seq_no: editItem.seq_no + 1}, ]}
        //     console.log('相等 ：', v, editItem, v.capatitys[editIndex], historys, editItem)
        //   } 
        //   return v
        // })
        // console.log('productData ：', productData);
      }
    })
  }
  beforeProcessAction = (t, info) => {
    console.log('beforeProcessAction v ：', t, info);
    this.setState({
      dialogType: t,
      processInit: backupFn(info),
      show: true,
    })
  }
  beforeAction = (t, o, i) => {
    console.log('beforeAction v ：', t, o, i);;
    this.setState({
      dialogType: t,
      processInit: backupFn(o),
      show: true,
      editIndex: i,
    })
  }
  tabClick = (key) => {
    console.log('key ：', key);
    this.getProduct(key)
    this.setState({
      factory_type: key,
      productData: [],
    })
  }
  workFilter = (v) => {
    const {procedureData, processInit, } = this.state
    const result = processInit.capatitys.some(item => item.work_no === v)
    // console.log('workFilter result ：', v, result);
    return result
  }
  gaugeFilter = (v) => {
    const {productData, } = this.state
    const result = productData.some(item => item.gauge === v)
    console.log('gaugeFilter过滤 result ：', v, result);
    return result
  }
  getProduct = (factory_type = defaultFactoryType) => {
  // getProduct = (factory_type = 'O') => {
    getProduct({factory_type}).then(res => {
      // console.log('getProduct res ：', res.data);
      const {gaugeData, } = this.state
      const {code, capatity, factory, gauge, history, } = res.data
      console.log('factory_type !== defaultFactoryType ：', factory_type,  defaultFactoryType);
      if (code === 1) {
        if (factory_type === defaultFactoryType) {
          gauge.map(v => v.gauge_txt = v.gauge_no)
          capatity.map(v => v.historys = history.filter(item => item.gauge === v.gauge))
          console.log('capatitycapatitycapatity ：', capatity)
          this.setState({
            productData: capatity,
            gaugeData: [...gauge, ],
          })
        } else {
          console.log('非本公司 ：', )
          factory.forEach(v => {
            const capatitys = capatity.filter(item => item.factory_id === v.factory_id)
            v.capatitys = capatitys
          })
          this.setState({
            productData: factory,
          })
        }
      }
    })
  }
  getProcedure = () => {
    getProcedure({}).then(res => {
      console.log('!!!!!!!getProcedure res ：', res.data);
      const {processFilters, } = this.state
      // const processFilters = []
      res.data.data.forEach((v, i) => {
        console.log('  v ：', v, i, )
        processFilters.push({
          text: v.work_name,
          value: v.work_name,
        })
      })
      
      this.setState({
        procedureData: res.data.data,
        processFilters: [...processFilters]
      })
    })
  }
  componentDidMount() {
    this.getProduct()
    this.getProcedure()
  }
  render() {
    console.log('ProductLine 组件this.state, this.props ：', this.state, this.props);
    const {
       productData,
       show, 
       formLayout, 
       label, 
       init, 
       dialogType, 
       processInit, 
       procedureData, 
       gaugeData, 
       factory_type,  
       processFilters,
    } = this.state
    const isCompnay = factory_type !== 'O'
    const commonConfig = {
      formItemLayout,
      formLayout: 'horizontal',
      ref: forms => this.forms = forms,
    }
    let content, title, config, processForms, work_no, processInits, disableDay
    if (isCompnay) {
      console.log('Object.keys(processInit),  ：', Object.keys(processInit), processInit.historys);
      disableDay = Object.keys(processInit).length && processInit.historys != undefined && processInit.historys.length ? processInit.historys[0].e_date : ''
    }
    if (show) {
      switch (dialogType) {
        case 'addProductLine':
          title = '新增生産線'
          console.log('新增生産線 ：', )
          content = <ProduceForm 
              {...commonConfig}
              init={{}} 
              config={productLineForm}
            />
          break;
        case 'editProcess':
          title = '編輯生産線'
          console.log('編輯生産線 ：', processInit, )
          content = <ProduceForm 
              {...commonConfig}
              init={{...processInit, void: processInit.void === 'Y', prod_rate: processInit.prod_rate * 100}} 
              config={[...productLineForm, disableFactory]}
            />
          break;
        case 'addProcess':
          title = '新增産能'
          const {gauge} = processInit
          const haveGauge = gauge != undefined
          // processForms = isCompnay ? backupFn(processForm) : backupFn(processForm).filter(v => v.key !== 'gauge')
          processForms = isCompnay ? backupFn(processForm) : onSelfCompany
          haveGauge && processForms.map(v => v.disabled = processDisableArr.some(item => item === v.key))
          work_no = haveGauge ? procedureData.filter(v => v.work_no === processInit.work_no)[0].work_name : ''
          processInits = haveGauge ? {...processInit, work_no, } : {}
          console.log('新增産能 111：', processInits, processInit, work_no, disableDay, processForm, gauge, processInits, processInits.historys != undefined,)
          content = <ProduceForm 
              {...commonConfig}
              init={processInits} 
              gaugeData={gaugeData}
              procedureData={procedureData} 
              disableKey={'work_no'}
              disableOption={'gauge'}
              config={processForms}
              disableDay={disableDay}
              dateDisable={(processInits.historys != undefined && !processInits.historys.length) ||processInits.historys == undefined}
              gaugeFilter={this.gaugeFilter}
              workFilter={this.workFilter}
            />
          break;
        case 'editCapacity':
          title = '編輯産能'
          console.log('編輯産能1 ：', show, processForms, processInit,)
          // processForms = isCompnay ? backupFn(processForm) : backupFn(processForm).filter(v => v.key !== 'gauge')
          processForms = isCompnay ? backupFn(processForm) : backupFn(processForm).filter(v => v.key !== 'gauge')
          processForms.map(v => v.disabled = processDisableArr.some(item => item === v.key))
          work_no = procedureData.filter(v => v.work_no === processInit.work_no)[0].work_name
          processInits = {...processInit, work_no, }
          content = <ProduceForm 
              {...commonConfig}
              init={processInits} 
              gaugeData={gaugeData}
              procedureData={procedureData} 
              disableKey={'work_no'}
              disableOption={'gauge'}
              config={processForms}
              disableDay={disableDay}
            />
          break;
        case 'deleteProcess':
          title = '删除生产线 '
          console.log('删除生产线  ：', )
          content = <div className='editWrapper warnDialog'>{`是否確認删除生产线 工厂代號：${processInit.factory_code} 工厂名稱：${processInit.factory_name}?`}</div>
          break;
      }
    }
    const width = (dialogType === 'deleteProcess' || dialogType === 'deleteData') ? '40%' : '50%'
    const rowContent = productData.length ? <ProductTable 
        data={productData}
        info={productData[0]}
        beforeProcessAction={this.beforeProcessAction}
        beforeAction={this.beforeAction}
        factory_type={factory_type}

        procedureData={procedureData}
        processFilters={processFilters}
      ></ProductTable> : null
    return (
      <section className="productLine">
        <Tabs defaultActiveKey={defaultFactoryType} onChange={this.tabClick}>
          <TabPane tab={<span><Icon type="idcard" />本公司</span>} key="I">
            <Collapses animate={ANIMATE.fadeIn} title={'生産線'} className="productLine"
              extra={
                <div className="btnWrapper">
                  <Button onClick={this.beforeAddProductLine.bind(this, 'addProductLine')} type="primary" size='large' icon="plus-circle-o" className="m-l">新增生産線</Button>
                </div>}>
              {rowContent}
            </Collapses>
          </TabPane>
          <TabPane className='noSelfCompany'  tab={<span><Icon type="schedule" />不是本公司</span>} key="O">
            <Collapses animate={ANIMATE.fadeIn} title={'生産線'} className="productLine"
              extra={
                <div className="btnWrapper">
                  <Button onClick={this.beforeAddProductLine.bind(this, 'addProductLine')} type="primary" size='large' icon="plus-circle-o" className="m-l">新增生産線</Button>
                </div>}>
              {rowContent}
            </Collapses>
          </TabPane>
        </Tabs>

        {
          show ? <ProductDialog 
              className='productLineDialog' 
              show={show} 
              width={width}
              handleOk={this.handleOk} 
              close={this.closeDialog} 
              title={title}
            >
              {content}
          </ProductDialog> : null
        }
      </section>
    );
  }
}
export default ProductLine;
