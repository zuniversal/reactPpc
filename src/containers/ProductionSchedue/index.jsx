import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getFilter,  } from 'api/ppc'
import { getProductionSchedue,  } from 'api/ProductionSchedue'
import {
  bacodeInfoWithDate, 
  pnDateArr, 
  DATE_FORMAT, 
  manualNumForm, 
  initParams, 
  productionSchedueForm, 
  filterOptionDate, 
  filterOptionForm, 
  productionSchedueConfig, 
  capcityTableConfig,
  eventFixColmun, 
  DATE_FORMAT_BAR,
} from 'config'
import { ANIMATE, SELECT_TXT, defaultWorkNo, } from 'constants'
import {openNotification, backupFn, confirms, filterArrOForm, mergeArr, filterTrimDatas,   } from 'util'
import Collapses from '@@@/Collapses'
import ProductDialog from '@@@/ProductDialog'
import ProduceForm from '@@@/ProduceForm'
import SecureHoc from '@@@@/SecureHoc'
// import PnNoTable from '@@@/PnNoTable'
import ClipBoard from '@@@@/ClipBoard'//
import Load from '@@@@/Load'
import { Button, Icon, Select, message, Row, Col, Input, } from 'antd'
import GridTable from '@@@@/GridTable/RichGridDeclarativeExample'
import moment from 'moment'

// import ppcList from './ppcList.json'
const Option = Select.Option
const { TextArea } = Input
const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
}

class ProductionSchedue extends React.Component {
  constructor(props) {
    super(props)
    // this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    this.state = {
      show: false,
      title: '提示框',
      pnBarcodeData: [],
      filterInfo: productionSchedueForm,
      filterOptionDate,
      showNo: '',
      customerData: [],
      styleNoData: [],
      pnData: {},
      barcodeData: [],
      dateForm: {
        reason_remark: '',
        
      },
      pnInfo: {}, 
      showPnIndex: 0, 
      editBarIndex: 0, 
      reasonArr: [],
      
      fileName: '',
      
      productionSchedueConfig: productionSchedueConfig(), 
      productSchedueData: [],
      // productSchedueData: ppcList.datas,
      
    }
  }
  closeDialog = (v,  ) => {
    console.log(' closeDialog ： ',  v,   )
    this.setState(this.closeInfo())
  }
  closeInfo = (v,  ) => {
    console.log(' closeInfo ： ',  v,   )
    return {
      title: '提示框',
      
    } 
  }

  onChange = (e, key,  ) => {
    console.log('onChange v ：', e, e.target.value, key )
    const {date, filterOptionDate, } = this.state// 
    this.setState({
      filterOptionDate: filterOptionDate.map((item) => {
        // console.log(' key === item.key ? {...item, initVal: e.target.value, } : item  ： ', key, item.key, key === item.key, key === item.key ? {...item, initVal: e.target.value, } : item, item , )// 
        return key === item.key ? {...item, initVal: e.target.value, } : item  
      })
    })
  }
  filterOption = (v,  ) => {
    console.log(' filterOption ： ',  v,   )
    
    this.forms.validateFields((err, values) => {
      const {filterOptionDate, } = this.state// 
      console.log('开始过滤filterOption ：', err, values, this.state, filterOptionDate, )
      const date1 = filterOptionDate[0].initVal
      const date2 = filterOptionDate[1].initVal
      console.log(' date1 ： ', date1, date2,  )// 
      const isValid = [date1, date2, ].every((v, i) => {
        // console.log('  isValid v ： ', v, i, moment(v).isValid(),  )
        return moment(v).isValid()
      })
      console.log(' isValid isValid ： ', isValid, moment(date2).isAfter(date1) ) 
      if (!moment(date2).isAfter(date1)) {
        confirms(2, '搜索起止日期不正確，請檢查 ！！！', )
        return  
      }
      if (!isValid) {
        confirms(2, '日期非法，請重新填寫 ！！！', )
        return  
      }
      values.shipment_datefr = moment(date1).format(DATE_FORMAT_BAR)
      values.shipment_dateto = moment(date2).format(DATE_FORMAT_BAR)
      values.pn_no = values.pn_no != undefined ? values.pn_no : ''
      values.customer = values.customer != undefined ? values.customer: ''
      values.style_no = values.style_no != undefined ? values.style_no : ''
      values.pn_noto = values.pn_nofr = values.pn_no 
      const params = {...initParams, ...values,  } 
      console.log('values.customer.split ：', values, params, )
      // return  
      this.getProductionSchedue(params)
    })
  }
  dataHandle = (data,   ) => {
    // console.log(' dataHandle ： ',  data  )
    const dataArr =  [
      'acs_date', 
      'prod_date', 
      'real_lastwork_e_date', 
      'real_lastwork_s_date', 
      'real_mc_e_date', 
      'real_mc_s_date', 
      'real_sew_e_date', 
      'real_sew_s_date', 
      'real_yarn_date', 
      'sample_ok_date', 
      'yarn_date', 
      'yj_lastwork_e_date', 
      'yj_lastwork_s_date', 
      'yj_mc_e_date', 
      'yj_mc_s_date', 
      'yj_sew_e_date', 
      'yj_sew_s_date', 
    ]
     
    const newData = data.map((item) => {
      // console.log(' data item ： ', item,  ) 
      const items = {
        ...item,
      }
      dataArr.forEach((v, i) => {
        // console.log(' dataArr v ： ', v, i, v, newData  )
        items[v] = items[v] != undefined ? moment(items[v]).format(DATE_FORMAT) : '' 
      })
      return items
      
    })
    const keys = Object.keys(newData[0])
    // console.log(' dataHandle ： ', newData   )// 
    return newData.map((v) => {
      keys.forEach((item, i) => {
        // console.log(' keys item ： ', item, i, v[item], v[item] != undefined  )
        v[item] =  v[item] != undefined ? typeof v[item] === 'string' ? v[item].trim() : v[item] : ''
      })
      return v 
    })
  }

  getProductionSchedue = (v,  ) => {
    // console.log(' getProductionSchedue ： ',  v,   )
    this.setState({
      isReload: true,
    })
    getProductionSchedue(v, ).then(res => {
      const {datas, code, reason,  } =  res.data
      console.log('getProductionSchedue res ：', res.data,  )  
      // if (code === 1) {
        if (datas.length) {
          this.setState({
            productSchedueData: this.dataHandle(datas),
            // productSchedueData: ppcList.datas,
            isReload: false,
            
          })
        } else {
          confirms(2, '沒有數據 o(╥﹏╥)o')
        }
      // }
    })
  }
  download = (t) => {
    console.log('t  download ：', t , this.gridTable, `exportDataAs${t}`  )
    this.gridTable.api[`exportDataAs${t}`]();
    // this.api.gridTable.exportDataAsExcel();
  }
  
  getFilter = () => {
    getFilter({}).then(res => {
      // console.log('getFilter res ：', res.data)
      const {customer, style_no} = res.data
      this.setState({
        customerData: customer, 
        styleNoData: style_no,
      })
    })
  }

  componentDidMount() {
    const {filterInfo,  } = this.state// 
    this.getProductionSchedue(filterInfo)
    this.getFilter()
  }


  render() {
    const {
      filterInfo, 
      customerData,
      styleNoData,
      fileName,
      productionSchedueConfig,
      productSchedueData,
      isReload,
    } = this.state
    console.log('ProductionSchedue 组件this.state, this.props ：', isReload, eventFixColmun, productionSchedueConfig, capcityTableConfig(), this.state, this.props, )
    return (
      <Collapses noLimit={true} animate={ANIMATE.fadeIn} title={'生産進度報表'} className="manualNum"
        extra={
          <div className="btnWrapper">
            <Button onClick={this.autoPpc} type="primary" icon='plus-circle-o'>新增生産進度報表</Button>
          </div>
        }
      >
        <div className="manulWrapper">
          <div className="filterInfoWrapper">
            <div className="dateWrapper">
              <span className='label' >起止日期: </span>
              <div className='flexCenter'>
                {
                  filterOptionDate.map((v, i) => {
                    // console.log(' v.initVal ： ', v.initVal, v.key,  )// 
                    return <input className={`antDateInput ${i === 0 ? 'm-r5' : ''}`} type="date" key={v.key} defaultValue={v.initVal} onChange={(e) => this.onChange(e, v.key, )} />
                  })
                }
              </div>
            </div>
            <div className="formWrapper">
              <ProduceForm customerData={customerData} styleNoData={styleNoData} ref={forms => this.forms = forms}
                selectData={[]} formItemLayout={formItemLayout} formLayout={'inline'}
                init={filterInfo} config={filterOptionForm} className='filterForm' 
              ></ProduceForm>
            </div>
            <div className="filterBtn">
              <Button onClick={this.filterOption} icon='smile-o' className='f-r' type="primary">过滤</Button>
            </div>
          </div>

            {
              productSchedueData.length && !isReload ? <div className="gridWrapper"><GridTable 
                  ref={ref => this.gridTable = ref} 
                  isCapcity={false} 
                  fixColmun={eventFixColmun} 
                  config={productionSchedueConfig} 
                  data={productSchedueData}
                  defaultExportParams={{fileName: fileName}}
                ></GridTable> 
                <div className="downloadWrapper">
                  <Button onClick={() => this.download('Csv',)} type="primary" icon="download" className="m-l">導出Csv</Button>
                  <Button onClick={() => this.download('Excel',)} type="primary" icon="download" className="m-l">導出Excel</Button>
                </div>
              </div>: <Load></Load>
            }

        </div>
      </Collapses>
    )
  }
}

// export default ProductionSchedue
export default SecureHoc(ProductionSchedue)
