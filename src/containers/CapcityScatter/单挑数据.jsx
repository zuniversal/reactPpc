import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getCapaticity, getFactory } from 'api/Capaticity'
import { defaultNow, capcityConfig, DATE_FORMAT_BAR } from 'config'
import { ANIMATE, SELECT_TXT, DEFAULT_FACTORY } from 'constants'
import { backupFn,  } from 'util'
import Collapses from '@@@/Collapses'
import Echarts from '@@@/Echarts'
import { Row, Col, Icon, DatePicker, Select,  } from 'antd'
import moment from 'moment'
const { MonthPicker,  } = DatePicker;
const Option = Select.Option;

class CapcityScatter extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      capcityData: {},
      factoryData: [],
      pDateFr: defaultNow,
      // pDateFr: "2018-04-01",
      factory_id: DEFAULT_FACTORY,
    }
  }
  getFactory = () => {
    getFactory({}).then(res => {
      console.log('getFactory res ：', res.data);
      const {factory, } = res.data
      this.setState({
        factoryData: factory, 
      })
    })
  }
  getCapaticity = () => {
    const {pDateFr, factory_id} = this.state
    console.log('getCapaticity ：', this.state,  );
    getCapaticity({pDateFr, factory_id}).then(res => {
      console.log('getCapaticity res ：', res.data);
      const {datas, code, } = res.data
      const set = new Set();
      // const monthess = new Array(...set);
      const title = datas[0].factory_name.trim() + ' - ' + '工序：'+ datas[0].work_name.trim()
      const capcityConfigData = backupFn(capcityConfig)
      capcityConfigData.title = title
      datas.forEach((v, i) => {
        // console.log(' datas v ：', v, i, )
        capcityConfigData.data[0].seriesData.push(v.done_capacity)
        capcityConfigData.data[1].seriesData.push(v.basic_capacity)
      })
      // console.log('factoryArr ：', title, capcityConfigData)
      // if (code) {
        this.setState({
          capcityData: capcityConfigData, 
        })
      // }
    })
  }
  selectChange = (value) => {
    console.log(`selected ${value}`);
    this.setState({
      factory_id: Number(value),
    }, () => {
      this.getCapaticity()
    })
  }
  onChange = (date, dateString)=> {
    console.log('onChange', date, dateString);
    this.setState({
      pDateFr: dateString,
    }, () => {
      this.getCapaticity()
    })
  }
  componentDidMount() {
    this.getFactory()
    this.getCapaticity()
  }
  render() {
    console.log('CapcityScatter 组件this.state, this.props ：', this.state, this.props);
    const { capcityData, factoryData, pDateFr, } = this.state
    const defaultValue = factoryData.length ? factoryData[0].factory_name : ''
    // console.log('defaultValue ：', defaultValue)
    return (
      <section className="capcitywrapper">
        {
          Object.keys(capcityData).length && capcityData.data.length ? <Collapses title={capcityData.title} animate={ANIMATE.bounceIn} extra={
            <div className="extraWrapper">
              {
                defaultValue !== ''  ? <Select defaultValue={defaultValue} onChange={this.selectChange} className="factorySelect m-r5">
                  {factoryData.length ? factoryData.map((v, i) => <Option value={v.factory_id.toString()} key={i}>{v.factory_name}</Option>) : null}
                </Select> : null
              }
              <MonthPicker defaultValue={moment(pDateFr, DATE_FORMAT_BAR)} onChange={this.onChange.bind(this,)} format={DATE_FORMAT_BAR} placeholder={SELECT_TXT + '月份'} />
            </div>
            }
          >
            <Echarts data={capcityData} type="bar"></Echarts>
          </Collapses> : null
        }
      </section>
    );
  }
}
export default CapcityScatter;