import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getCapaticity, getFactory } from 'api/Capaticity'
import { defaultNow, capcityConfig, DATE_FORMAT_BAR, FORMAT_MONTH_ONE, monthArr, legend, 
  workConfig, workOption, workAll, factoryTypeConfig, factoryTypeOption, 
} from 'config'
import { ANIMATE, SELECT_TXT, DEFAULT_FACTORY, DEFAULT_WORK_NO, capictyFactoryType, } from 'constants'
import { backupFn,  } from 'util'
import Collapses from '@@@/Collapses'
import Echarts from '@@@/Echarts'
import SelectForm from '@@@/SelectForm'
import { Row, Col, Icon, DatePicker, Select, Button, } from 'antd'
import moment from 'moment'
const { MonthPicker,  } = DatePicker;
const Option = Select.Option;

class CapcityScatter extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      capcityData: {},
      factoryData: [],
      pDateFr: defaultNow,
      // pDateFr: "2018-04-01",
      factory_id: DEFAULT_FACTORY,
      changeFactory: -1,
      work_no: DEFAULT_WORK_NO,
      workNoPh: '工序',
      factoryTypePh: '工廠',
      factory_type: capictyFactoryType,
      workConfigs: [...workConfig, workAll],
      capcityDataOrigin: [],
    }
  }
  getFactory = () => {
    getFactory({}).then(res => {
      console.log('getFactory res ：', res.data);
      const {factory, } = res.data
      this.setState({
        factoryData: factory, 
      })
    })
  }
  monthHandle = (v, m) => {
    const allStartMonth = moment(m).format(FORMAT_MONTH_ONE)
    const startIndex = legend.findIndex(v => v === allStartMonth + '月')
    // console.log('allStartMonth,11  ：',v, allStartMonth, startIndex)
    const after = legend.slice(0, startIndex)
    const before = legend.slice(startIndex, )
    v.legend = [...before, ...after]
    console.log('allStartMonth,222  ：', v,allStartMonth, [...before, ...after])
  }
  getCapaticity = (pDateFr = defaultNow, factory_id = 0) => {
    // const {pDateFr, factory_id} = this.state
    console.log('getCapaticity ：', this.state, pDateFr, factory_id );
    getCapaticity({p_date_fr: pDateFr, factory_id}).then(res => {
      console.log('getCapaticity res ：', res.data, pDateFr, defaultNow, pDateFr === defaultNow, );
      const {datas, code, } = res.data
      if (pDateFr === defaultNow && factory_id === 0) {
        const {factoryData, } = this.state 
        console.log('不变换时间 ：', )
        const factoryArr = {}
        const capcityConfigs = backupFn(capcityConfig)
        this.monthHandle(capcityConfigs, pDateFr)
        console.log('factoryArr ：', factoryArr, capcityConfigs)
        datas.forEach(v => {
          // console.log('v ：', v, factoryArr[v.factory_id])
          if (factoryArr[v.factory_id] == undefined) {
            factoryArr[v.factory_id] = []
            factoryArr[v.factory_id].push(v)
          } else {
            factoryArr[v.factory_id].push(v)
          }
        })
        // datas.forEach(v => factoryArr.add(v.factory_id))
        const capcityData = {}
        Object.values(factoryArr).forEach((item, index) => {
          const {factory_type} = factoryData.find(v => v.factory_id === item[0].factory_id)
          // console.log('item ：', factoryData, factory_type, item, factoryArr[item[0].factory_id], item[0].factory_id, )
          const title = item[0].factory_name.trim() + ' - ' + '工序：'+ item[0].work_name.trim()
          // index === 0 && capcityConfigData.legend.push(moment(item.factory_id.month_date).format(FORMAT_MONTH_ONE))
          
          const capcityConfigData = backupFn(capcityConfigs)
          capcityConfigData.factory_id = item[0].factory_id
          capcityConfigData.work_no = item[0].work_no
          capcityConfigData.factory_type = factory_type
          capcityConfigData.title = title
          capcityConfigData.pDateFr = pDateFr
          item.forEach((v, i) => {
            // console.log(' datas v ：', v, i, )
            capcityConfigData.data[0].seriesData.push(v.done_capacity)
            capcityConfigData.data[1].seriesData.push(v.basic_capacity)
          })
          capcityData[item[0].factory_id] = capcityConfigData
        })
        console.log('capcity ：', capcityData)
        this.setState({
          capcityData, 
          capcityDataOrigin: {...capcityData},
        })
      } else {
        console.log('变换时间 ：', )
        const {capcityData, } = this.state
        const matchItem = capcityData[factory_id]
        this.monthHandle(matchItem, pDateFr)
        matchItem.pDateFr = pDateFr
        matchItem.data[0].seriesData = []
        matchItem.data[1].seriesData = []
        console.log('capcityData ：', capcityData)
        datas.forEach((v, i) => {
          // console.log(' datas v ：', v, i, )
          // console.log('变换时间 moment(s_dat：', moment(v.month_date).format(FORMAT_MONTH_ONE))
          matchItem.data[0].seriesData.push(v.done_capacity)
          matchItem.data[1].seriesData.push(v.basic_capacity)
        })
        console.log('变换时间 capcityData11 ：', capcityData, matchItem)
        this.setState({
          capcityData: {...capcityData},
          capcityDataOrigin: {...capcityData},
        })
      }
    })
  }
  selectChange = (i, value) => {
    console.log(`selected ${i}`, value);
    this.setState({
      changeFactory: i,
      factory_id: Number(value),
    })
  }
  onChange = (i, date, dateString)=> {
    console.log('onChange', i, date, dateString);
    this.getCapaticity(dateString, i)
    this.setState({
      changeFactory: i,
    })
  }
  filterChange = (val, k) => {
    const {capcityDataOrigin, work_no, factory_type} = this.state 
    console.log(' filterChange ： ', val, k, capcityDataOrigin, val === work_no, val === DEFAULT_WORK_NO, work_no, capictyFactoryType )
    let capcityDatas = {}
    if (val == DEFAULT_WORK_NO || val === capictyFactoryType) {
      capcityDatas = capcityDataOrigin
    } else {
      Object.entries(capcityDataOrigin).filter(v => v[1][k] == val).forEach(v => capcityDatas[v[0]] = v[1])
    }
    console.log(' capcityData ： ', capcityDatas, Object.entries(capcityDatas).length, Object.entries(capcityDataOrigin).length )
    this.setState({
      capcityData: capcityDatas,
      [k]: val,
    })
  }
  workChange = (work_no) => {
    const {capcityDataOrigin, } = this.state 
    console.log(' workChange ： ', work_no, capcityDataOrigin,  )
    this.setState({
      work_no,
    })
  }
  dvFilter = (d, v, k, t) => d.find(item => item[k] == v)[t]
  componentDidMount() {
    this.getFactory()
    this.getCapaticity()
  }
  render() {
    console.log('CapcityScatter 组件this.state, this.props ：', this.state, this.props, defaultNow);
    const { capcityData, factoryData, pDateFr, workNoPh, factoryTypePh, work_no, workConfigs, factoryType, factory_type, } = this.state
    const defaultValue = factoryData.length ? factoryData[0].factory_name : ''
    const {middle,  } = this.props.route
    // console.log('defaultValue ：', defaultValue)
    return (
      <section className="capcitywrapper">
        <Row gutter={4}>
          {
            Object.keys(capcityData).length ? Object.values(capcityData).map((item, index) => {
              {/* console.log(' Object.values(capcityData) item ：', item, )  */}
              return <Collapses noLimit key={item.factory_id} title={`${item.title} - ${item.factory_type === 'I' ? '內廠' : '外廠'} `} animate={ANIMATE.bounceIn} 
                className={`${index % 2 === 0 ? '' : 'odd'} m-b10`} extra={
                  <div className="extraWrapper">
                    {/* {
                      defaultValue !== ''  ? <Select defaultValue={defaultValue} onChange={this.selectChange.bind(this, item.factory_id)} className="factorySelect m-r5">
                        {factoryData.length ? factoryData.map((v, i) => <Option value={v.factory_id.toString()} key={i}>{v.factory_name}</Option>) : null}
                      </Select> : null
                    } */}
                    <SelectForm data={factoryTypeConfig} onChange={v => this.filterChange(v, 'factory_type')} option={factoryTypeOption} ph={factoryTypePh} dv={this.dvFilter(factoryTypeConfig, factory_type, 'k', 't')} ></SelectForm>
                    <SelectForm data={workConfigs} onChange={v => this.filterChange(v, 'work_no')} option={workOption} ph={workNoPh} dv={this.dvFilter(workConfigs, work_no, 'work_no', 'work_name')} ></SelectForm>
                    <MonthPicker defaultValue={moment(pDateFr, DATE_FORMAT_BAR)} onChange={this.onChange.bind(this, item.factory_id)} format={DATE_FORMAT_BAR} placeholder={SELECT_TXT + '月份'} />
                  </div>
                }
              >
                <Echarts data={item} type="bar"></Echarts>
              </Collapses>
              })
            : null
          }
        </Row> 
      </section>
    );
  }
}
export default CapcityScatter;