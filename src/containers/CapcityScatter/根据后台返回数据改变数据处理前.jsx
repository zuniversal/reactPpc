import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getCapaticity, getFactory } from 'api/Capaticity'
import { defaultNow,  } from 'config'
import { ANIMATE,  } from 'constants'
import Collapses from '@@@/Collapses'
import Echarts from '@@@/Echarts'
import { Row, Col, Icon, } from 'antd'

class CapcityScatter extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      capcityData: [],
    }
  }
  getCapaticity = (pDateFr = defaultNow, factory_id = '1') => {
    console.log('getCapaticity res ：', pDateFr,  );
    getCapaticity({p_date_fr: pDateFr, factory_id}).then(res => {
      console.log('getCapaticity res ：', res.data);
      const {datas, code, } = res.data
      const factoryArr = {}
      datas.forEach(v => {
        console.log('v ：', v, factoryArr[v.factory_id])
        if (factoryArr[v.factory_id] == undefined) {
          factoryArr[v.factory_id] = []
          factoryArr[v.factory_id].push(v)
        } else {
          factoryArr[v.factory_id].push(v)
        }
      })
      // datas.forEach(v => factoryArr.add(v.factory_id))
      console.log('factoryArr ：', factoryArr)
      // const set = new Set(monthes);
      // const monthess = new Array(...set);
      // const titleArr = []
      // const basicArr = []
      // const capaticyArr = []
      // factoryArr.forEach((v, i) => {
      //   console.log(' datas v ：', v, i, )
      //   titleArr.push(v.factory_name + v.work_name)
      //   basicArr.push(v.basic_capacity)
      //   capaticyArr.push(v.done_capacity)
      // })
      
      
      // if (code) {
        // this.setState({
        //   // capcityData: datas, 
        //   capcityData: datas, 
        // })
      // }
    })
  }
  showDetial = (v) => {
    console.log('showDetial v ：', v, this.state);
  }
  componentDidMount() {
    this.getCapaticity()
  }
  render() {
    console.log('CapcityScatter 组件this.state, this.props ：', this.state, this.props);
    const { capcityData, } = this.state
    return (
      <section className="artLine">
        <Collapses title={'产能分布'} animate={ANIMATE.bounceIn}>
          {/* {capcityData.length ? 
            capcityData.map((v, i) => {
              console.log(' capcityData (v, i) ：', v, i, i % 2 === 0) 
              return <Echarts key={i} data={[]} type="bar" legend={["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sep"]}></Echarts>
            }) : null
          } */}
          {/* <Row gutter={16}>
            <Col className={`m-b20 ${ANIMATE.slideInLeft}`} xs={12} sm={12} md={12} lg={12} xl={12}>
              <Echarts data={[]} type="bar" legend={["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sep"]}></Echarts>
            </Col>
            <Col className={`m-b20 ${ANIMATE.slideInRight}`} xs={12} sm={12} md={12} lg={12} xl={12}>
              <Echarts data={[]} type="bar" legend={["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sep"]}></Echarts>
            </Col>
          </Row>    */}
          <Echarts data={capcityData} type="bar" legend={["Jan", "Feb", "Mar", "Apr", "May", "Jun",]}></Echarts>
        </Collapses>
      </section>
    );
  }
}
export default CapcityScatter;