import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getCapaticity, getFactory } from 'api/Capaticity'
import { defaultNow, capcityConfig, DATE_FORMAT_BAR, FORMAT_MONTH_ONE, monthArr, legend } from 'config'
import { ANIMATE, SELECT_TXT, DEFAULT_FACTORY } from 'constants'
import { backupFn,  } from 'util'
import Collapses from '@@@/Collapses'
import Echarts from '@@@/Echarts'
import { Row, Col, Icon, DatePicker, Select,  } from 'antd'
import moment from 'moment'
const { MonthPicker,  } = DatePicker;
const Option = Select.Option;

class CapcityScatter extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      capcityData: {},
      factoryData: [],
      pDateFr: defaultNow,
      // pDateFr: "2018-04-01",
      factory_id: DEFAULT_FACTORY,
      changeFactory: -1,
    }
  }
  getFactory = () => {
    getFactory({}).then(res => {
      console.log('getFactory res ：', res.data);
      const {factory, } = res.data
      this.setState({
        factoryData: factory, 
      })
    })
  }
  monthHandle = (v, m) => {
    const allStartMonth = moment(m).format(FORMAT_MONTH_ONE)
    const startIndex = legend.findIndex(v => v === allStartMonth + '月')
    // const startIndex = legend.findIndex(v => v === allStartMonth)
    console.log('allStartMonth,11  ：',v, allStartMonth, startIndex)
    v.legend = [...legend.slice(0, startIndex), ...legend.slice(startIndex, )]
    console.log('allStartMonth,222  ：', v, allStartMonth, )
  }
  getCapaticity = (pDateFr = defaultNow, factory_id = 0) => {
    // const {pDateFr, factory_id} = this.state
    console.log('getCapaticity ：', this.state, pDateFr, factory_id );
    getCapaticity({p_date_fr: pDateFr, factory_id}).then(res => {
      // const allStartMonth = moment(defaultNow).format(FORMAT_MONTH_ONE)
      // console.log('getCapaticity res ：', res.data, pDateFr, defaultNow, pDateFr === defaultNow, allStartMonth);
      // const startIndex = legend.findIndex(v => v === allStartMonth + '月')
      
      // // const startIndex = legend.findIndex(v => v === allStartMonth)
      // const slices = legend.slice(0, startIndex)
      // const slicess = legend.slice(startIndex, )
      // console.log('startIndex ：', startIndex, slices,slicess, [...slicess, ...slices] ,);
      const {datas, code, } = res.data
      if (pDateFr === defaultNow && factory_id === 0) {
        console.log('不变换时间 ：', )
        const factoryArr = {}
        datas.forEach(v => {
          // console.log('v ：', v, factoryArr[v.factory_id])
          if (factoryArr[v.factory_id] == undefined) {
            factoryArr[v.factory_id] = []
            factoryArr[v.factory_id].push(v)
          } else {
            factoryArr[v.factory_id].push(v)
          }
        })
        // datas.forEach(v => factoryArr.add(v.factory_id))
        const capcityData = {}
        const capcityConfigData = backupFn(capcityConfig)
        this.monthHandle(capcityConfigData, pDateFr)
        console.log('factoryArr ：', factoryArr, capcityConfigData)
        Object.values(factoryArr).forEach((item, index) => {
          // console.log('item ：', item, factoryArr[item[0].factory_id], item[0].factory_id, )
          const title = item[0].factory_name.trim() + ' - ' + '工序：'+ item[0].work_name.trim()
          capcityConfigData.factory_id = item[0].factory_id
          capcityConfigData.title = title
          capcityConfigData.pDateFr = pDateFr
          item.forEach((v, i) => {
            // console.log(' datas v ：', v, i, )
            capcityConfigData.data[0].seriesData.push(v.done_capacity)
            capcityConfigData.data[1].seriesData.push(v.basic_capacity)
          })
          capcityData[item[0].factory_id] = capcityConfigData
        })
        console.log('capcity ：', capcityData)
        this.setState({
          capcityData, 
        })
      } else {
        console.log('变换时间 ：', )
        const {capcityData, } = this.state
        const matchItem = capcityData[factory_id]
        
        matchItem.pDateFr = pDateFr
        matchItem.data[0].seriesData = []
        matchItem.data[1].seriesData = []
        console.log('capcityData ：', capcityData)
        datas.forEach((v, i) => {
          // console.log(' datas v ：', v, i, )
          console.log('变换时间 moment(s_dat：', moment(v.month_date).format(FORMAT_MONTH_ONE))
          matchItem.data[0].seriesData.push(v.done_capacity)
          matchItem.data[1].seriesData.push(v.basic_capacity)
        })
        console.log('变换时间 capcityData11 ：', capcityData, matchItem)
        this.setState({
          capcityData: {...capcityData},
        })
      }
    })
  }
  selectChange = (i, value) => {
    console.log(`selected ${i}`, value);
    this.setState({
      changeFactory: i,
      factory_id: Number(value),
    })
  }
  onChange = (i, date, dateString)=> {
    console.log('onChange', i, date, dateString);
    this.getCapaticity(dateString, i)
    this.setState({
      changeFactory: i,
    })
  }
  componentDidMount() {
    this.getFactory()
    this.getCapaticity()
  }
  render() {
    console.log('CapcityScatter 组件this.state, this.props ：', this.state, this.props, defaultNow);
    const { capcityData, factoryData, pDateFr, } = this.state
    const defaultValue = factoryData.length ? factoryData[0].factory_name : ''
    // console.log('defaultValue ：', defaultValue)
    return (
      <section className="capcitywrapper">
        <Row gutter={4}>
          {
            Object.keys(capcityData).length ? Object.values(capcityData).map((item, index) => {
              {/* console.log(' Object.values(capcityData) item ：', item, )  */}
              return <Col className={`m-b20 ${ANIMATE.bounceInLeft}`} key={item.factory_id} span={24}>
              <Collapses title={item.title} animate={ANIMATE.bounceIn} 
                className={`${index % 2 === 0 ? '' : 'odd'}`} extra={
                  <div className="extraWrapper">
                    {/* {
                      defaultValue !== ''  ? <Select defaultValue={defaultValue} onChange={this.selectChange.bind(this, item.factory_id)} className="factorySelect m-r5">
                        {factoryData.length ? factoryData.map((v, i) => <Option value={v.factory_id.toString()} key={i}>{v.factory_name}</Option>) : null}
                      </Select> : null
                    } */}
                    <MonthPicker defaultValue={moment(pDateFr, DATE_FORMAT_BAR)} onChange={this.onChange.bind(this, item.factory_id)} format={DATE_FORMAT_BAR} placeholder={SELECT_TXT + '月份'} />
                  </div>
                }
              >
                <Echarts data={item} type="bar"></Echarts>
              </Collapses></Col>
              })
            : null
          }
        </Row> 
      </section>
    );
  }
}
export default CapcityScatter;