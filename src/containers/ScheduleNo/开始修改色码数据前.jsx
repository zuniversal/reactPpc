import React from "react"
import ReactDOM from 'react-dom';
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { getAccPnInfo, } from 'api/Acc'
import { getPnList, getBarcodeList, getPnDetailList, editBarcode, getFilter, splitPnBarcode, barcodeLock, finishDay, getRealDetail, deleteBarcode, } from 'api/ppc'
import { grid, initParams, DATE_FORMAT_BAR, MONTH_FORMAT, FORMAT_DAY, realExist, equalAttr, workConfig, workOption, scrollBtnInfo, sizeConfig, } from 'config'
import { ANIMATE, LETTER, SELECT_TXT,  } from 'constants'
import { backupFn, createRow, findDOMNode, openNotification, ajax, confirms, getItems, } from 'util'
import ppcList from './ppcList.json'
// import DragableBar from '@@@/DragableBar'
// import DragableBar from '@@@/DragableSlider'
import DragableBar from '@@@/DragableSliders'
import ProductDialog from '@@@/ProductDialog'
import DragToggle from '@@@/DragToggles'
import BarcodeContent from '@@@/BarcodeContent'
import Collapses from '@@@/Collapses'
// import FilterPane from '@@@/FilterPane'
import ScheduleDay from '@@@/ScheduleDay'// 
import ScheduleBody from '@@@/ScheduleBody'
import Loading from '@@@@/Loading'
import SelectForm from '@@@/SelectForm'
import ColorSizeTable from '@@@/ColorSizeTable'
import RealQtyTable from '@@@/RealQtyTable'
import ArtPnDetailTable from '@@@/ArtPnDetailTable'
import * as scheduleAction from 'actions/scheduleNo.js'
import {copy} from 'actions/login.js'
import moment from 'moment';
// import ClipBoards from '@@@@/ClipBoards'
import ClipBoard from '@@@@/ClipBoard'
import { Dropdown, Button, Tooltip, Slider, Icon, Popover, Select, } from 'antd';
const Option = Select.Option;

class ScheduleNo extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      show: false,
      dateArr: [],
      monthArr: [],
      datas_barcode: [],
      datas: [],
      data_total: [],
      barcodeData: [],
      showBarcode: false,
      showEdit: false,
      loading: false,
      isSpliting: false,
      barcode: '',
      barcodeDate: {},
      pn_no: '',
      showPnDetail: false,
      dialogType: '',
      splitData: [],
      scrollLeftPos: 0,
      scrollTopPos: 0,
      productData: [],
      visible: false,
      customerData: [], 
      styleNoData: [],
      editBarcodeData: {},
      editPnIndex: '',
      pn_info: {},
      dayRange: [],
      changeBarData: [],
      isDragBack: false, 
      cantChange: false,
      sideBarScroll: 0,
      indexes: -1,
      disableDay: '',
      showModal: false, 
      colorData: [],
      colorOrigin: [],
      colorQtyData: {},
      colorBar: '',
      pnColorData: [],
      realDayQty: [],
      modalType: '',
      showDelete: false, 
      mergeData: [],
      splitLen: 0,
      d_barcode: '',
      scrollInfo: {},
      pnDetail: [],
    }
  }
  getPnList = (p) => {
    this.props.scheduleAction.filterInfo(p)
    console.log(' getPnList ：',  )
    this.setState({
      loading: true,
      visible: false,
    })
    getPnList(p).then(res => {
      console.log(' getPnList 22：',  )
      // console.log('请求所有的getPnList ：', ppcList);
      let { datas_barcode, datas, data_total } = res.data
      const today = moment().format('YYYY-MM-DD')
      // console.log('data_total ：', data_total, res.data.data_total, data_total.length)
      // datas_barcode = datas_barcode.slice(60, 100)
      // let { datas_barcode, datas, data_total } = ppcList
      // datas_barcode = datas_barcode.slice(10, 100)
      // datas = datas.slice(10, 100)
      // data_total = data_total.slice(10, 100)

      // const { datas_barcode, datas, data_total } = ppcList
      // const data1 = ppcList.data_total;
      if (data_total.length && datas_barcode.length) {
        this.filter()
        console.log('有数据1  HH:mm:ss：', data_total, today)
        const startDate = data_total[0].s_date
        const endtDate = data_total[0].e_date
        const dateArr = []
        const monthes = []
        let a = moment(startDate, DATE_FORMAT_BAR)
        let b = moment(endtDate, DATE_FORMAT_BAR)
        const dayLens = b.diff(a, 'days') + 1
        // console.log('dayLens ：', startDate, startDate.split('-'), endtDate, dayLens, a, b)
        // 得到排单所有天数
        for (let i = 0; i < dayLens; i++) {
          const day = moment(startDate).add(i, 'd')
          const date = day.format(FORMAT_DAY)
          const month = day.format(MONTH_FORMAT)
          const isSunday = day.format('d') === '0'
          const dateOrigin = day.format(DATE_FORMAT_BAR)
          monthes.push(month)
          // console.log('sRDates ：', month, date, isSunday)
          dateArr.push({ month, date, isSunday, dateOrigin });
        }
        // 去重 - 得到排单起止日期 - 包括月份总数组
        const set = new Set(monthes);
        const monthess = new Array(...set);
        const monthArr = monthess.map(v => ({ month: v, date: [] }))
        // console.log(' monthes, dateArr, monthArr：', monthes, monthArr, dateArr, moment(["2017", "1", "01"]));
        monthArr.forEach((v, i) => {
          for (let j = 0; j < dateArr.length; j++) {
            if (dateArr[j].month === v.month) {
              if (dateArr[j].dateOrigin === today) {
                dateArr[j].today = true
              }
              monthArr[i].date.push(dateArr[j])
              // console.log(' v ： ', dateArr[j].dateOrigin, dateArr[j].dateOrigin === today, v, today )
            }
          }
        })
        datas_barcode.map((item, i) => {
          const shortDay = datas.filter(v => v.barcode === item.barcode)
          // 如果有红色天数数量赋给每个条码
          // console.log('shortDay ：', shortDay);
          item.shortDay = shortDay
          item.shortDay.length && this.calcShortDay(item.shortDay, moment(item.s_date), a)
          this.calcInfo(item, a)
          return item
        })
        let level = 0
        let tableHeight = 0
        let startPnNo 
        const scrollHeightArr = []
        const scrollInfo = {}
        // 遍历所有的排单批号给所有的条码计算宽度、距离
        data_total.map((item, i) => {
          // console.log('item ：', item)
          const row = createRow(item.levels)
          if (i === 0) {
            // console.log('第一次 ：', scrollHeightArr, tableHeight, data_total.length)
            startPnNo = item.pn_no.substr(2, 2)
          } else if (item.pn_no.substr(2, 2) !== startPnNo && i !== 0) {
            // console.log('startPnNo = item.pn_no.substr(2, 2) ：', item, data_total[i + 1], data_total[i - 1], tableHeight, item.levels, item.pn_no, startPnNo, item.pn_no.substr(2, 2))
            startPnNo = item.pn_no.substr(2, 2)
            scrollHeightArr.push({startPnNo, tableHeight, })
            // console.log('%%%%%%%%%%scrollHeightArr ：', scrollHeightArr, tableHeight);
          } 
          tableHeight += row.length
          const factoryArr = []
          const data = datas_barcode.filter(v => {
            // if (item.pn_no === v.pn_no) {
            //   factoryArr.push(v.factory_id)
            //   v.tops = item.levels
            // }
            return item.pn_no === v.pn_no
          })
          const factoryWipe = Array.from(new Set(factoryArr))
          item.indexes = i
          item.row = row
          item.data = data
          item.level = level
          item.factoryArr = factoryWipe
          level += item.levels
          scrollInfo[item.level] = item.level * grid
          return item
        })
        
        // console.log('1111111111111111 ：', dateArr, monthArr, datas_barcode, datas, data_total )
        this.setState({
          scrollInfo,
          loading: false,
          dateArr,
          monthArr,
          datas_barcode,
          datas,
          data_total,
          // dateArr: [...backupFn(dateArr)],
          // monthArr: [...backupFn(monthArr)],
          // datas_barcode: [...backupFn(datas_barcode)],
          // datas: [...backupFn(datas)],
          // data_total: [...backupFn(data_total)],
          tableHeight: tableHeight * 30 + 75,
          tableWidth: dayLens * 30 + 130,
        }
        )
      } else {
        console.log('无数据 ：', )
        // openNotification('沒有數據，請重新選擇過濾條件！',  )
        confirms(2, '沒有數據，請重新選擇過濾條件!', )
        this.setState({
          loading: false,
        })
      }
    })
  }
  // 计算每个条码的宽度、距离等数据
  calcInfo = (v, startDay) => {
    // v.shortDay = []
    // console.log('v. open_date ： ', v, v.open_date, v.real_qty )
    const width = moment(v.e_date).diff(moment(v.s_date), 'days')
    const left = moment(v.s_date).diff(startDay, 'days')
    v.startDay = v.s_date
    v.width = (width + 1) * 30
    v.left = (left) * 30
    if (realExist.some(item => v[item] != undefined)) {
      const realStart = moment(v.open_date).diff(startDay, 'days')
      const realWidth = Math.abs(moment(v.finish_date).diff(moment(v.open_date), 'days')) + 1
      // console.log(' realWidth ： ', v, realStart, v.open_date, v.finish_date,  )
      v.realStart = realStart > 1 ? realStart * 30 : 0 
      v.realWidth = realWidth * 30
      v.zIndex = 1
    }
  }
  // 计算缺工红色日子的距离
  calcShortDay = (v, barStart, startDay) => {
    // console.log('calcShortDay v, startDay ：', v, barStart, startDay)
    v.map(item => item.shortDate = (moment(item.p_date).diff(startDay, 'days') - barStart.diff(startDay, 'days')) * 30)
  }
  showDialog = () => this.setState({ show: true, })
  dragBack = () => {
    const { barcode, barcodeDate, editBarcodeData } = this.state
    const { left, width } = editBarcodeData
    const {s_date_new, e_date_new} = barcodeDate
    // const newWidth = moment(e_date_new.split('-')).diff(s_date_new.split('-'), 'days') * grid + 'px'
      console.log('关闭this.state11 ：', this.state, this.dragBar, dragBar, barcode, left, width)
    const dragBar = findDOMNode(ReactDOM, this[barcode])
      console.log('关闭this.state 22：', dragBar.style, dragBar.style)
    if (dragBar.style.width !== width) {
      console.log('日期变了  宽度变了：', dragBar.style.width, width)
      // dragBar.style.width = width
      // console.log('日期变了  宽度变了2：', dragBar.style.width, width)
    }
    dragBar.style.transform = `translate(${left}px, 0px)`
  }
  resetColorInfo = (v) => {
    return {
      show: false,
      splitData: [],
      colorData: [],
      colorOrigin: [],
      colorQtyData: {},
      colorBar: '',
      splitLen: 0,
    }
  }
  close = () => {
    const { barcode, showEdit, editBarcodeData } = this.state
    const resetParams = {
      show: false,
      showEdit: false,
      showBarcode: false,
      splitData: [],
      isDragBack: true, 
      cantChange: false, 
      editContent: '', 
      dialogType: '',
      modalType: '',
      ...this.resetColorInfo(),
    }
    if (showEdit) {
      this.dragBack()
    } else {
      resetParams.changeBarData = []
    }
    console.log('showEdit resetParams：', showEdit, resetParams)
    this.setState(resetParams);
  }
  // 停止拖拽设置当前操作的bar
  stopDrag = ({x, y, w, barcode, data, pn_no, dragAction}) => {
    const { dateArr, data_total } = this.state
    const s_date_new = dateArr[x].dateOrigin
    const e_date_new = dateArr[w].dateOrigin
    const dragPn = data_total.find(v => v.pn_no === pn_no)
    const {factoryArr} = dragPn
    const matchIndex = factoryArr.indexOf(data.factory_id) + 1
    const matchItem = dragPn.data.find(v => v.factory_id === factoryArr[matchIndex])
    
    this.setState({
      show: true, 
      showEdit: true, 
      barcodeDate: {
        s_date_new, 
        e_date_new
      },
      editBarcodeData: data,
      editPnNo: pn_no,
      barcode,
    })
    console.log('停止拖拽设置当前操作的bar ：', dragPn, matchItem, x, y, w, barcode, data, pn_no, dragAction, s_date_new, e_date_new, this.state)
    if (dragAction !== 'dragging') {
      if (dragAction === 'dragLeft') {
        // 当不是最后一条bar的时候才去统计是否正确
        console.log('不是最后一条 ：', matchIndex, factoryArr.length, matchIndex !== factoryArr.length )
        if (matchIndex !== factoryArr.length) {
          const {s_date} = matchItem
          const diffDay = moment(s_date.split('-')).diff(moment(s_date_new.split('-')), 'days')
          console.log('dragLeft ：', diffDay, data.levels, s_date, s_date_new, this.state)
          if (diffDay < 1) {
            console.log('注意：改變後的條碼的生産開始日期必須在下一條之前！ ：', diffDay < 1)
            this.setState({
              cantChange: true, 
              editContent: '注意：改變後的條碼的生産開始日期必須在下一條之前！', 
            })
            // openNotification('注意：改變後的條碼的生産開始日期必須在下一條之前！')
          } 
        }
      } else if (dragAction === 'dragRight') {
        const diffDay = moment(data.e_date.split('-')).diff(moment(e_date_new.split('-')), 'days')
        console.log('dragRight右边 ：', data.e_date, e_date_new, diffDay, this.state)
        if (diffDay < 0) {
          console.log('注意：條碼的清期不能改變！：', diffDay < 0)
          this.setState({
            cantChange: true, 
            editContent: '注意：條碼的清期不能改變！', 
          })
          // openNotification('注意：條碼的清期不能改變！')
        }
      }
    } else {
      const {s_date} = matchItem
      const diffDay = moment(s_date.split('-')).diff(moment(s_date_new.split('-')), 'days')
      console.log('dragLeft ：', diffDay, data.levels, s_date, s_date_new, this.state)
      if (diffDay < 1) {
        console.log('注意：新条码的生产开始日期必须在下一条之前！ ：', diffDay < 1)
        this.setState({
          cantChange: true, 
          editContent: '注意：新条码的生产开始日期必须在下一条之前！', 
        })
      } 
    }
  }
  // 点击barcode开始编辑
  showBarcodeDialog = (barcode, pn_no, work_no, indexes) => {
    // console.log('showBarcodeDialog22 ', barcode, pn_no, work_no, indexes);
    // this.props.scheduleAction.showDialog(true)
    this.getBarcodeList({barcode, work_no, pn_no, indexes})
    // this.setState({ barcode, editPnNo: pn_no, })
  }
  // 获取barcode信息请求
  getBarcodeList = (p) => {
    const {barcode, pn_no, indexes} = p
    getBarcodeList(p).then(res => {
      console.log('getBarcodeList 获取批号详情 res ：', p, res);
      // this.props.scheduleAction.barcode(res.data.datas)
      let {datas, factory, color, } = res.data
      // color = [...backupFn(color), ...backupFn(color),]
      // color.forEach((v, i) => {
      //   console.log(' color v ： ', v, ) 
      //   v.combo_seq = i
      //   v.combo_id = `${v.combo_id}${i}`
      //   // v.sch_qty = `${i}00` * 1
      // })
      const colorQtyObj = {}
      color.forEach((v, i) => {
        console.log(' colorQtyObj v ： ', v, i, )
        colorQtyObj[v.combo_seq] = v.sch_qty
      })
      console.log(' colorQtyObj ： ', color, colorQtyObj,  )
      // datas[0].colorData = color
      this.setState({
        showBarcode: true, 
        barcodeData: [{...datas[0], isSplitData: false}],
        productData: factory,
        dialogType: 'showBarcode',
        show: true,
        barcode, 
        editPnNo: pn_no,
        indexes,
        colorOrigin: [...color],
        colorData: {[datas[0].barcode]: color},
        colorQtyObj,
      })
    })
  }
  // 批号辅料详情
  getAccPnInfo = () => {
    const {editPnNo, } = this.state 
    console.log(' getItems() ： ', getItems('acc_token'), this.state, editPnNo,  )
    getAccPnInfo({obj: { 
      // pn_no: '1812M2001', 
      pn_no: editPnNo,
    }})
    .then(res => {
      console.log('getAccPnInfo res ：', res.data);
      this.setState({
        pnDetail: res.data.datas,
        showPetail: true,
        show: true,
        modalType: 'getAccPnInfo',
      })
    })
  }
  // 获取bar实际日产
  getRealDetail = (barcode, pn_no, factory_id) => {
    console.log(' getRealDetail ： ', barcode, pn_no, factory_id )
    getRealDetail({pn_no, factory_id}).then(res => {
      console.log('getRealDetail res ：', res.data);
      const {datas, code, } = res.data
      this.setState({
        show: true,
        pn_no,
        barcode,
        realDayQty: datas, 
        modalType: 'getRealDetail',
      })
    })
  }
  // 获取批号信息请求
  getPnDetailList = (pn_no) => {
    console.log('getPnDetailList pn_no ：', pn_no);
    this.props.copy(pn_no)
    this.setState({pn_no})
    getPnDetailList({pn_no}).then(res => {
      // console.log('getPnDetailList res ：', res.data);
      // this.props.scheduleAction.barcode(res.data.datas)
      const {datas, code, pn_info, color, } = res.data
      // if (code === 1) {
        const colorData = {}
        color.forEach((v, i) => colorData[v.barcode.trim()] = [v])
        console.log(' colorData ： ', colorData,  )
        this.setState({
          dialogType: 'showPnCode', 
          barcodeData: datas,
          show: true,
          pn_info,
          colorData,
        })
      // }
    })
  }
  inputing = ({v, barcode, keys}) => {
    const {barcodeData, splitData} = this.state
    console.log('排单inputing e v, barcode, keys：', barcodeData, v, barcode, keys, this.props);
    let {originQty, qty} = barcodeData[0]
    
    if (keys === 'qty') {
      let total = 0
      splitData.forEach(item => {
        if (item.barcode === barcode) {
          item.qty = Number(v)
          console.log(' 新数量 splitData item ：', item, Number(v), ) 
        }
        total += item.qty
      })
      if (total === qty) {
        console.log('刚刚好 ：', )
        originQty = originQty - total
      } else {
        if (originQty - total > 0) {
          // 对应的改变父条码的数量
          console.log('输入的数合法 ：', originQty, total)
          originQty = originQty - total
        } else if (originQty - total < 0) {
          console.log('输入的数不合法 ：', originQty, total)
          // openNotification('對不起，您輸入的數量超過了父條碼的數量，請重新拆分！', 'warning')
          confirms(2, '對不起，您輸入的數量超過了父條碼的數量，請重新拆分!', )
        }
      }
      console.log('改变数量 ：', barcodeData[0], Number(v))
    } else if (keys === 'date') {
      console.log('不是改变数量 date：', v[0]._d, v[1]._d, moment(v[0]._d).format(DATE_FORMAT_BAR), )
      console.log('v[1].：', moment((moment(v[1]._d).format(DATE_FORMAT_BAR).split('-'))).diff(moment((moment(v[0]._d).format(DATE_FORMAT_BAR).split('-'))), 'days'))
      const s_date = moment(v[0]._d).format(DATE_FORMAT_BAR)
      const e_date = moment(v[1]._d).format(DATE_FORMAT_BAR)
      const work_days = moment(e_date.split('-')).diff(moment(s_date.split('-')), 'days') + 1
      splitData.map(item => {
        if (item.barcode === barcode) {
          item.s_date = moment(v[0]._d).format(DATE_FORMAT_BAR)
          item.e_date = moment(v[1]._d).format(DATE_FORMAT_BAR)
          item.work_days = work_days
        }
        return item
      })
    } else {
      console.log('改变内容', )
      if (keys === 'work_days') {
        v = Number(v)
      }
      splitData.map(item => {
        if (item.barcode === barcode) {
          console.log('改变内容', item.barcode === barcode, item.barcode, barcode, item[keys], v)
          item[keys] = v
        }
        return item
      })
    }
    console.log('matchItem ：', splitData, barcodeData)
    this.setState({
      barcodeData: [...barcodeData],
      splitData: [...splitData],
    })
  }
  showColor = (t, v, i) => {
    console.log(' showColorshowColor ： ',  t, v, i )
    const {barcode, } = v
    this.setState({
      colorBar: barcode,
      showModal: true,
    })
  }
  calColorData = (colorDatas) => {
    const {colorData, colorQtyObj, barcode, } = this.state 
    console.log(' calColorData ： ', colorDatas )
    const newColorObj = {}
    Object.keys(colorQtyObj).forEach(v => newColorObj[v] = 0)
    const withoutFa = backupFn(colorDatas)
    delete withoutFa[barcode.trim()]
    console.log(' colorQtyObj 111111  ： ', barcode, colorQtyObj, newColorObj, withoutFa )
    Object.values(withoutFa).map(item => {
      item.reduce((total, cuv, cui, arr ) => {
        if (cuv.status === 'T') {
          // console.log(' 选中 && cuv.combo_seq ===  color reduce total, cuv, cui, arr ： ', total, cuv, cui, arr,  )
          return newColorObj[cuv.combo_seq] += cuv.qty
        }
      }, newColorObj)
    })
    return newColorObj
  }
  modalOk = () => {
    console.log(' modalOk ： ', this.colorTable, this.state  )
    const {colorOrigin, colorData, colorQtyObj, colorBar, splitData, barcode,  } = this.state 
    const {state, props, } = this.colorTable
    const {data, selectedRowKeys, } = state
    colorData[colorBar] = data
    // const {barcode, } = props
    console.log(' data ： ', barcode, data, selectedRowKeys, colorData )

    // const newColorObj = {}
    // Object.keys(colorQtyObj).forEach(v => newColorObj[v] = 0)
    // const withoutFa = backupFn(colorData)
    // delete withoutFa[barcode]
    // console.log(' colorQtyObj 111111  ： ', colorQtyObj, newColorObj, withoutFa )
    // Object.values(withoutFa).map(item => {
    //   item.reduce((total, cuv, cui, arr ) => {
    //     if (cuv.status === 'T') {
    //       console.log(' 选中 && cuv.combo_seq ===  color reduce total, cuv, cui, arr ： ', total, cuv, cui, arr,  )
    //       return newColorObj[cuv.combo_seq] += cuv.qty
    //     }
    //   }, newColorObj)
    // })
    
    splitData.forEach((v, i) => v.qty = colorData[v.barcode].reduce((t, c, ) => t += c.status === 'T' ? c.qty : 0, 0))
    const newColorObj = this.calColorData(colorData)
    console.log(' newColorObj ： ', newColorObj, splitData,  )
    // splitData.forEach((v, i) => ({...v, qty: v.barcode === barcode ? v.qty : v.qty}))
    this.setState({
      splitData: [...splitData],
      colorQtyObj: newColorObj,
      colorData: {...colorData},
      showModal: false,
    })
  }
  modalClose = () => {
    console.log(' modalClose ： ', this.colorTable  )
    this.setState({
      showModal: false,
    })
  }
  // 点击一条barcode开始拆分 - 增加一条子条码
  splitAction = (data, qty) => {
    const {splitData, barcodeData, colorOrigin, colorData, splitLen, } = this.state
    // data.barcode = data.barcode.trim() + LETTER[splitData.length]
    data.barcode = data.barcode.trim() + LETTER[splitLen]
    console.log(' data.barcode.trim(), len ： ', data.barcode, LETTER[splitLen], splitLen )
    const disableDay = barcodeData[0].e_date
    const colorOrigins = backupFn(colorOrigin)
    const newColorData = Object.values(colorData).reduce((t, c, i) => {
      // console.log(' newColorData t, c ： ', t, c, i, )
      if (i !== 0) {
        c.forEach((item, index) => {
          // console.log(' item ： ', c, item, colorOrigin, colorOrigins, colorOrigin[index].qty,)
          if (item.status === 'T') {
            // console.log(' 是选中状态的的 ： ',  )
            colorOrigins[index].qty -= item.qty
          }
        })
      }
      return colorOrigins
    }, colorOrigins)
    console.log(' colorOriginscolorOriginscolorOrigins= ： ', colorOrigins,  )
    if (colorOrigins.some(v => v.qty < 0) || colorOrigins.every(v => v.qty === 0)) {
      confirms(2, '色碼數據拆分數量總數超過總數量，請檢查重新拆分 !', )
      return
    }
    const colorDatas = {...colorData, [data.barcode]: colorOrigins}
    const newColorObj = this.calColorData(colorDatas)// 
    splitData.forEach((v, i) => v.qty = colorData[v.barcode].reduce((t, c, ) => {
      // console.log(' t, c ： ', t, c  )
      return t += c.status === 'T' ? c.qty : 0
    }, 0))
    console.log('**拆分targettarget , newColorObj：', newColorObj, data, splitData, barcodeData, colorData, colorOrigin, this.state, disableDay, qty, qty != undefined ? qty : 0, splitData, data, data.barcode, [...splitData, {...data, isSplitData: true, qty: qty != undefined ? qty : 0 }])
    this.setState({
      splitData: [...splitData, {...data, isSplitData: true, 
        // qty: qty != undefined ? qty : 555 
        qty: colorOrigins.reduce((t, c,) => t += c.qty, 0)
      }],
      barcodeData: [{...barcodeData[0], originQty: barcodeData[0].qty}],
      disableDay, 
      colorData: colorDatas,// 
      colorQtyObj: newColorObj,
      splitLen: splitLen + 1,
    })
  }
  deleteBar = (t, v, i) => {
    const {splitData, colorData, } = this.state
    console.log(' deleteBarsssss ： ',  t, v, i, splitData, barcode, colorData, this.state )
    const {barcode, } = v
    delete colorData[barcode]
    console.log(' colorData ： ', colorData,  )
    this.setState({
      splitData: splitData.filter(v => v.barcode !== barcode),
      colorData,
    })
  }
  handleOk = () => {
    const {showEdit, cantChange, } = this.state
    console.log('handleOk ：', this.state, showEdit ? '编辑条码' : '拆分')
    if (showEdit) {
      this.editBarcode()
    } else {
      this.splitPnBarcode()
    }
  }
  mergeShort = (data) => {
    const effectObj = {}
    const effectData = data.reduce((t, c) => {
      t[c.pn_no.trim()] = [...t[c.pn_no.trim()] != undefined  ? t[c.pn_no.trim()] : [], c]
      return t
    }, effectObj)
    return effectObj
  }

  mergeShorts = (data) => {
    const effectObj = {}
    const effectData = data.reduce((t, c) => {
      const pn_no = c.pn_no.trim()
      const barcode = c.barcode.trim()
      // console.log(' c.barcode.trim() ： ', t,  c, pn_no, barcode )
      t[pn_no] = t[pn_no] == undefined ? {} : {
        // [barcode]: [...t[pn_no][barcode] != undefined ? [...t[pn_no][barcode], c] : [], c]
        [barcode]: [...t[pn_no][barcode] != undefined ? () => {
          console.log(' [...t[pn_no][barcode], c] ： ', [...t[pn_no][barcode], c],  )
          return [...t[pn_no][barcode], c]
        } : [], c]
      }
      return t
    }, effectObj)
    return effectObj
  }
  // 确认对批号的条码进行拆分
  splitPnBarcode = () => {
    const {barcodeData, splitData, editPnNo, data_total, datas_barcode, colorData, colorQtyObj, colorOrigin, pn_no, indexes,  } = this.state
    if (!splitData.length) {
      confirms(2, '未選擇拆分，請拆分再確認 !', )
      return
    }
    console.log('splitPnBarcode barcodeData, splitData：', colorQtyObj, colorData, barcodeData, splitData, this.state, )
    const tableStartDay = data_total[0].s_date
    const {barcode, originQty, qty, factory_id, } = barcodeData[0]
    const total = splitData.reduce((t, n) => t + n.qty, 0)
    const residueData = {}
    const residue = colorOrigin.forEach((v, i) => {
      // console.log(' colorOrigin v ： ', v, i, colorQtyObj[v.combo_seq], v.qty, colorQtyObj[v.combo_seq] - v.qty)
      // if (v.status === 'T') {
      //   residueData[v.combo_seq] = colorQtyObj[v.combo_seq] - v.qty
      // } else {
      //   residueData[v.combo_seq] = colorQtyObj[v.combo_seq] - v.qty
      // }
      //   residueData[v.combo_seq] = v.status === 'T' ? colorQtyObj[v.combo_seq] - v.qty : residueData[v.combo_seq]
      //   residueData[v.combo_seq] = colorQtyObj[v.combo_seq] - v.status === 'T' ? v.qty : 0
      residueData[v.combo_seq] = colorQtyObj[v.combo_seq] - v.qty
    })
    
    console.log('色码数量 residueData ： ', residueData, colorQtyObj, colorOrigin )
    const isResidue = Object.values(residueData).some(v => v !== 0)
    // const residueInfo = colorOrigin.forEach((v, i) => {
    //   console.log('  v ： ', v, i, )
    // })
    // console.log(' residueInfo ： ', residueInfo  )
    console.log(' isResidue， ： ', isResidue,  )
    if (isResidue) {
      confirms(2, '色碼數據沒拆分完或者超過了，請檢查重新拆分 !', )
      return
    }
    // return
    // 判断拆分数量是否还剩
    console.log(' 判断拆分数量是否还剩total, originQty ：', residue, this.state, total, originQty, qty, total < originQty, qty - total)
    // if (total < qty) {// 
    //   console.log(' 是还剩 ：', )
    //   this.splitAction(backupFn(barcodeData[0]), qty - total)
    //   splitData.push({
    //     ...barcodeData[0], 
    //     isSplitData: true, 
    //     barcode: barcode.trim() + LETTER[splitData.length], 
    //     qty: qty - total,
    //   })
    // }
    // 判断拆分的每一条bar是不是都有色码
    const withoutFa = backupFn(colorData)
    delete withoutFa[barcode]
    const newData = Object.values(withoutFa).map(item => item.some((v, i) => v.status === 'T'))
    console.log(' newData ： ', withoutFa, newData, newData.every(v => v) )
    if (!newData.every(v => v)) {
      confirms(2, '拆分的每一條bar至少要有一條色碼信息 o(*￣︶￣*)o !', )
      return
    }
    // return
    // console.log('splitPnBarcode totaltotal：', barcode, barcodeData, this.state, splitData, total, originQty, qty, total < originQty, qty - total)
    const data = []
    const isRepeat = new Set()
    const wordNum = []
    const startDay = moment(tableStartDay, DATE_FORMAT_BAR)
    // 父批号的工序
    const dragPn = data_total.find(v => v.pn_no === editPnNo)
    const {factoryArr} = dragPn
    // console.log('dragPn ：', dragPn, factoryArr)
    const isDayRightArr = []
    // const {sc_gauge, } = splitData[0]
    const parentCode = barcode.substr(0, 9)
    const parentBars = dragPn.data.find(item => {
      // console.log(' item.barcode === parentCode ： ', item.barcode, parentCode, item.barcode.indexOf(parentCode), item.barcode.indexOf(parentCode) > -1 )
      return item.barcode.indexOf(parentCode) > -1
    })
    // 重新计算要插入的条码生成日期 日产等是否正确
    splitData.map((v, i) => {
      // console.log(' splitData  v ：', barcode, v, barcode.substr(0, 9), dragPn.data )
      const matchIndex = factoryArr.indexOf(v.factory_id) + 1
      // console.log(' matchIndex ：', matchIndex, factoryArr, v.factory_id  )
      // 当不是最后一条bar的时候才去统计是否正确
      if (matchIndex > 0 && matchIndex !== factoryArr.length) {
        const matchItem = dragPn.data.find(item => item.factory_id === factoryArr[matchIndex])
        // console.log(' matchItem ：', matchItem, dragPn.data.find(item => item.factory_id === factoryArr[matchIndex]) ) 
        // 拆分的bar的开始和结束日期都要大于下一条的日期
        const splitStart = moment(v.s_date).diff(moment(matchItem.s_date), 'days')
        const splitEnd = moment(v.e_date).diff(moment(matchItem.e_date), 'days')
        const isBeforeFactory = splitStart <= 0 && splitEnd <= 0
        isDayRightArr.push(isBeforeFactory)
        // console.log(' 拆分bar的下一条找到了  matchIndex ：', isBeforeFactory, splitStart, splitEnd, matchIndex, matchItem, moment(v.s_date), moment(matchItem.s_date))
      } else {
        console.log('没找到 下一条拆分bar：', )
      }
      // console.log('isDayRightArr ：', isDayRightArr)
      wordNum.push((v.qty / v.day_qty) / moment(v.e_date).diff(moment(v.s_date), 'days') < 1)
      v.user_id = this.props.userInfo.user_id
      // getItems('userInfo')
      v.factory_id = Number(v.factory_id)
      // console.log('isRepeat ：', isRepeat, v.factory_id, isRepeat.has(v.factory_id))
      isRepeat.add(v.factory_id)
      return v
    })
    if (splitData.length === 1 && (equalAttr.every(v => splitData[0][v] === barcodeData[0][v]))) {
      confirms(2, '新拆分的日期 生產線必須不一樣!', )
      return 
    }
    
    const isWorkRight = wordNum.some(v => v)
    // console.log('isWorkRight ：', isWorkRight, wordNum)
    if (!isWorkRight) {
      const isDayRight = isDayRightArr.every(v => v)
      // console.log(' 拆分日期是否合法 isDayRight ：', dragPn, isDayRight)
      if (isDayRight) {
        const spitedFactory = {}
        isRepeat.forEach((item, index) => {
          // console.log(' isRepeat v ：', item, index, )
          splitData.forEach((v, i) => {
            // console.log(' splitData v ：', v, i, )
            if (v.factory_id === item) {
              if (spitedFactory[v.factory_id] == undefined) {
                spitedFactory[v.factory_id] = []
              } 
              spitedFactory[item].push(v)
            }
          })
        })
        // console.log('olds ：', splitData, spitedFactory, Object.entries(spitedFactory))
        Object.values(spitedFactory).forEach((v, i) => v.sort((a, b) => moment(a.s_date).isAfter(b.s_date)))
        // 同一产线的bar不能重叠
        const isOverlayArr = []
        Object.entries(spitedFactory).forEach((v, i) => {
          // console.log(' spitedFactory v ：', v[1].length, v, i, v[0], parentBars.factory_id, v[0] == parentBars.factory_id)  
          if (v[1].length !== 1) {//
          // if (v[1].length !== 1 && v[0] == parentBars.factory_id) {
            let isOverlay = false
            for (let i = 0; i < v[1].length; i++) {
              if (i !== v[1].length - 1) {
                //  console.log(' 是否重叠 111moment(a.s_date).isAfter(b.s_date moment()) ：', v, v[1][i + 1].s_date,v[1][i].e_date,v[1][i + 1].s_date, (v[1][i].e_date), moment(v[1][i + 1].s_date).isAfter(v[1][i].e_date))
                // if (moment(v[i + 1].s_date).isAfter(v[i].e_date)) {
                //   isOverlay = moment(v[i + 1].s_date).isAfter(v[i].e_date)!
                // }
                isOverlay = moment(v[1][i + 1].s_date).isAfter(v[1][i].e_date)
              }
            }
            // console.log('isOverlay ：', isOverlay)
            isOverlayArr.push(isOverlay)
          }
        })
        const illeageResult = isOverlayArr.every(v => v)
        // console.log(' isOverlayArr ：', isOverlayArr, illeageResult)
        if (!illeageResult) {
          // openNotification('拆分日期重疊了，請重新填寫日期！', )
          confirms(2, '拆分日期重疊了，請重新填寫日期!', )
        } else {
          splitData.forEach((v, i) => v.color = colorData[v.barcode].filter(v => v.status === 'T' && v.qty !== 0))
          const params = {p_barcode: barcode, data: splitData, pn_no: editPnNo, }
          console.log(' 生产线没有重复拆分合法可以拆分 ：', params)
          // return
          splitPnBarcode(params).then(res => {
            // console.log('splitPnBarcode res ：', res.data);
            const {code, datas, mes, newBarcode} = res.data
            // openNotification(mes)

            if (code === 1) {
              // 插入拆分的数据
              newBarcode.map((v, i) => {
                // console.log('插入拆分的数据 barcode v ：', v, i, )
                v.shortDay = []
                v.lock = 'F'
                this.calcInfo(v, startDay)
                return v
              })
              console.log('newBarcode ：', newBarcode)
              data_total.map(v => {
                if (v.pn_no === editPnNo) {
                  // 改变父批号的工序数组
                  const shortArr = []
                  Array.from(isRepeat).forEach((item, i) => {
                    const indexs = v.factoryArr.findIndex(items => items === item)
                    indexs < 0 && shortArr.push(item)
                  })
                  v.factoryArr = [...v.factoryArr, ...shortArr].sort((a, b) => a - b)
                  // console.log(' mathcs1111 ： ', v.data )
                  const mathcs = v.data.findIndex(item => item.barcode.trim() === barcode.trim())
                  // console.log(' mathcs2222222 ： ', mathcs, v.data )
                  v.data.splice(mathcs, 1, ...newBarcode)
                }
                return v
              })
              // console.log('data_total ：', data_total)
              if (datas.length) {
                this.mergeData(data_total, datas)
                // data_total.forEach((item, i) => {
                //   const shortDay = datas.filter(v => v.barcode === item.barcode)
                //   // 如果有红色天数数量赋给每个条码
                //   item.shortDay = shortDay
                //   item.shortDay.length && this.calcShortDay(item.shortDay, moment(item.s_date), tableStartDay)
                // })
              }
              this.setState({
                data_total: [...data_total],
                ...this.resetColorInfo(),
              })
            }
          })
        }
      } else {
        // openNotification('拆分工序的開始、结束日期不能在下一個工序的後面！', )
        confirms(2, '拆分工序的開始、结束日期不能在下一個工序的後面!', )
      }
    } else {
      // openNotification('拆分日産數量不正確，請重新填寫！', )
      confirms(2, '拆分日産數量不正確，請重新填寫!', )
    }
  }
  mergeData = (target, datas) => {
    console.log(' mergeData ： ', target, datas, this.mergeShort(datas) )
    const {data_total} = this.state
    const tableStartDay = data_total[0].s_date
    const mergeShort = this.mergeShort(datas)
    target.forEach(v => {
      // console.log(' 拆分拆分v ： ', v, v.pn_no, mergeShort, mergeShort[v.pn_no.trim()])
      if (mergeShort[v.pn_no.trim()] != undefined) {
        // console.log(' undefinedundefinedundefinedundefinedv ： ', v, v.pn_no, mergeShort, mergeShort[v.pn_no.trim()])
        mergeShort[v.pn_no.trim()].forEach(item => { 
          console.log(' item[mergeShort item ： ', item, mergeShort, item.barcode,)
          if (mergeShort[item.barcode.trim()] != item.barcode.trim()) {
            v.data.forEach((items, i) => {
              if (items.barcode.trim() === item.barcode.trim()) {
                //console.log('相等相等相等 items ： ', items, items.shortDay, item, moment(items.s_date), tableStartDay)
                items.shortDay.push(item)
                this.calcShortDay(items.shortDay, moment(items.s_date), tableStartDay)
              }
            })
          }
        })
      }
    })
    console.log(' target ： ', target,  )
    return target
  }

  // 拖拽、改变起止日期  确认发送请求
  editBarcode = () => {
    const {barcodeDate, barcode, editBarcodeData, data_total, datas_barcode} = this.state
    const tableStartDay = data_total[0].s_date// 
    console.log('editBarcode ：', this.state, barcodeDate, barcode, editBarcodeData)
    editBarcode({...barcodeDate, barcode}).then(res => {
      // console.log('editBarcode res ：', res.data, this.state);
      const {code, datas, mes} = res.data
      // openNotification(mes)
      if (code === 1) {
        data_total.map(v => {
          v.data.forEach((item, index) => {
            if (item.barcode === barcode) {
              item.s_date = barcodeDate.s_date_new
              item.e_date = barcodeDate.e_date_new
              this.calcInfo(item, data_total[0].s_date)
              console.log('等于改变起止日期 v ：', item, index, item.barcode, barcode, item.barcode === barcode)
            }
          })
          return v
        })
        console.log(' datas ： ', datas,  )
        datas.forEach(v => v.barcode = v.s_barcode)
        if (datas.length) {// 
          this.mergeData(data_total, datas)
        }
      }
      this.setState({
        data_total: [...data_total],
        show: false,
        showEdit: false,
        changeBarData: [],
        data: [],
        ...this.resetColorInfo(),
      })
    })
  }

  finishAction = (v, finish_date) => {
    const {barcodeData, barcode, } = this.state
    const {userInfo, } = this.props 
    const params = {
      finish_status: v,
      barcode,
      finish_user: userInfo.user_id,
      // finish_date: moment().format(DATE_FORMAT_BAR),
      finish_date,
    }
    console.log(' finishAction ： ', params, v, barcodeData, Date.now(), {...moment}, moment().format(DATE_FORMAT_BAR) )
    this.finishDay(params)
  }
  finishDay = (v) => {
    console.log(' finishDay ： ',  v )
    finishDay(v).then(res => {
      console.log('finishDay res ：', res.data);
      const {code, } = res.data
      if (code === 1) {
        const {data_total, editPnNo, } = this.state 
        const matchPn = data_total.findIndex(v => v.pn_no === editPnNo)
        data_total[matchPn].data.forEach((item, i) => {
          if (item.barcode === v.barcode) {
            // item = {...item, ...v}
            item.finish_status = v.finish_status
            item.finish_user = v.finish_user
            item.finish_date = v.finish_date
            console.log('======  ： ', v.barcode, item )
          }
        })
        console.log(' matchPn ： ', matchPn,  data_total[matchPn] )
        this.setState({
          data_total: [...data_total],
        })
      }
    })
  }
  
  lock = (barcode, lock) => {
    const {data_total, indexes, show, } = this.state
    console.log('lock ：', barcode, lock, indexes, this.state, data_total, data_total[indexes])
    barcodeLock({barcode, lock}).then(res => {
      console.log('barcodeLock res ：', res.data);
      const {code, customer, style_no, } = res.data
      if (code === 1) {
        const newData = data_total[indexes].data.map(v => ({...v, lock: v.barcode === barcode ? lock : v.lock}))
        data_total[indexes].data = [...newData]
        console.log('newData ：', newData)
        this.setState({
          data_total: [...data_total],
          show: lock === 'T' ? false : show,
        })
      }
    })
  }
  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('##########shouldComponentUpdate  nextProps ：', nextProps, nextState, )
  //     return false
  // //   // const {splitData} = this.state
  // //   // console.log('@@@@@@@@shouldComponentUpdate  show ：', splitData)
  // //   if (nextState.shou) {
  // //     return false
  // //   } else {
  // //     return true
  // //   }
  // }
    
  delete = (v) => {// 
    const {barcode, data_total, barcodeData, indexes, } = this.state
    const {work_no} = barcodeData[0]
    const mergeData = data_total[indexes].data.filter(v => v.work_no === work_no)
    if (mergeData.length < 2) {
      confirms(2, '只有一條同工序生産線無法進行刪除  o(╥﹏╥)o!')
      return 
    }
    console.log('productNoWrapper delete ：', data_total[indexes].data, mergeData, work_no, v, indexes, barcode, this.state)
    this.setState({
      showDelete: true,
      d_barcode: mergeData.filter(v => v.barcode !== barcode)[0].barcode,
      mergeData: mergeData.filter(v => v.barcode !== barcode),
    })
  }
  deleteBarcode = (v) => {
    console.log(' deleteBarcode ： ',  v )
    deleteBarcode(v).then(res => {
      console.log('deleteBarcode res ：', res.data);
      const {code, data, } = res.data
      const {barcode, data_total, indexes, } = this.state 
      data_total[indexes].data = data_total[indexes].data.filter(v => v.barcode !== barcode)
      console.log(' data_total[indexes].datas ： ', data_total[indexes].data,)
      if (code === 1) {
        if (data.length) {
          this.mergeData(data_total, data)
        }
      }
      this.setState({
        data_total: [...data_total],
        showDelete: false,
        ...this.resetColorInfo(),
      })
    })
  }
  // 获取过滤数据
  getFilter = () => {
    getFilter({}).then(res => {
      // console.log('getFilter res ：', res.data);
      const {customer, style_no} = res.data
      this.props.scheduleAction.customerData(customer)
      this.props.scheduleAction.styleNoData(style_no)
      this.setState({
        customerData: customer, 
        styleNoData: style_no,
      })
    })
  }
  filterOption = () => {
    //  console.log('filterOption ：', )
    this.setState({ visible: true });
  } 
  handleVisibleChange = (flag, e) => {
    // console.log('handleVisibleChange ：', flag, e)
    this.setState({ visible: flag });
  }
  zIndex = (v, i, index) => {
    // console.log(' zIndex ： ', v, i, index )
    const {data_total, barcode, } = this.state 
    const {pn_no, } = v
    // console.log(' data_total ： ', data_total[i], data_total[i].data[index] )
    data_total[i].data[index].zIndex++
    this.setState({
      data_total: [...data_total],
    })
  }
  workChange = (v) => {
    console.log(' workChange ： ',  v )
  }
  pnScroll = (v) => {
    console.log(' pnScroll ： ',  v, this.props  )
    const {sideBarScroll, data_total, scrollInfo } = this.state 
    const isUp = v === 'up'
    const domRef = findDOMNode(ReactDOM, this.con)
    if (['down', 'up'].some(item => item === v)) {
      const scrollIndex = Math[isUp ? 'floor' : 'ceil'](sideBarScroll / grid)   
      // const matchItem = Object.keys(scrollInfo).find(v => v > scrollIndex)
      const matchIndex = Object.keys(scrollInfo).findIndex(v => v > scrollIndex)
      this.dragToggle.sideBar.scrollTop = scrollInfo[Object.keys(scrollInfo)[matchIndex - (isUp ? 2 : 0)]] 
      console.log(' 垂直滚 ', scrollInfo[Object.keys(scrollInfo)[matchIndex - (isUp ? 2 : 0)]]  ,Object.keys(scrollInfo)[matchIndex - (isUp ? 2 : 0)], matchIndex )
    } else {
      console.log(' 横向滚 ： ',  domRef.scrollLeft)
      domRef.scrollLeft = v === 'left' ? domRef.scrollLeft - 210 : domRef.scrollLeft + 210 
    }
  }
    
  // 固定显示的角落div
  fixBox = (isPane) => {
    const {customerData, styleNoData, visible} = this.state
    // console.log('fixBox ：',  this.state)
    return (
      <div className="headerSideBarBox">
        <div className="fixBoxItem flexEnd">
            {/* <SelectForm data={workConfig} onChange={this.workChange} option={workOption}></SelectForm> */}
            <div className='scrollBtn'>
              {
                scrollBtnInfo.map((v) => <Tooltip key={v.dire} placement="top" title={<div className="pnInfoWrapper">向{v.txt}滾動批號</div>}>
                  <Button onClick={() => this.pnScroll(v.dire)} shape="circle" type="primary" icon={`arrow-${v.dire}`} ></Button>
                </Tooltip>
                )
              }
            </div>
          <div className='pnDay'>日期</div>
        </div>
        <div className="fixBoxItem pnInfoColumn">批号信息</div>
      </div>
    )
  }
  createDay = (isTxt, isMonth, isLasItem) => {
    // console.log('createDay ：', )
    const {monthArr} = this.state
    return monthArr.map((v, i) => {
      // console.log(' monthArr v ：', v, ) 
      return <ScheduleDay isTxt={isTxt} isMonth={isMonth} isLasItem={isLasItem} v={v} key={v.month + v.date.length}></ScheduleDay>
    })
    // if (!isMonth) {
    //   return monthArr.map((v, i) => {
    //     // console.log(' monthArr v ：', v, ) 
    //     return <div className="dayWrapper" key={v.month + v.date.length}>
    //       {
    //         v.date.map((item, index) => {
    //           return <div className={`${item.isSunday ? 'isSunday dateItem' : 'dateItem'} ${isLasItem ? 'boldDate' : ''}`} key={item.dateOrigin + v.date.length}>{isTxt ? item.date : null}</div>
    //         })
    //       }
    //     </div>
    //   })
    // } else {
    //   return monthArr.map((v, i) => {
    //     // console.log(' monthArr v ：', v, ) 
    //     return <div className="headerWrapper" key={v.month + v.date.length}>
    //       <div className="monthWrapper">
    //         {isMonth ? <div className={`month ${v.date.length === 1 ? 'oneDate' : ''} ${v.date.length === 2 ? 'twoDate' : ''}`}>{v.month}</div> : null}
    //         <div className="dayWrapper">
    //           {
    //             v.date.map((item, index) => {
    //               // console.log(' item.date item ：', item, ) 
    //               return <div className={`${item.isSunday ? 'isSunday dateItem' : 'dateItem'}`} key={item.dateOrigin + v.date.length}>{isTxt ? item.date : null}</div>
    //             })
    //           }
    //         </div>
    //       </div>
    //     </div>
    //   })
    // }
  }
  createBody = (justSideBar, ) => {
    // console.log('justSideBar ：', justSideBar)
    const {data_total, dateArr, changeBarData, showEdit, isDragBack, monthArr, } = this.state
    // return data_total.map((v, i) => {
    //   return <ScheduleBody v={v} key={v.pn_no} {...{data_total, dateArr, changeBarData, showEdit, isDragBack, monthArr, getPnDetailList}}></ScheduleBody>
    // })
    const props = {
      justSideBar, data_total, dateArr, changeBarData, showEdit, isDragBack, monthArr, 
    }
    
    // return <ScheduleBody {...props} zIndex={() => this.zIndex()} getRealDetail={() => this.getRealDetail()}
    //  changeBar={() => this.changeBar()} 
    //  showBarcodeDialog={() => this.changeBar()} 
    //  stopDrag={() => this.stopDrag()} 
    //  getPnDetailList={() => this.getPnDetailList()} createDay={() => this.createDay()}></ScheduleBody>
      
    return data_total.map((v, i) => {
      // console.log(' data_total v ：', v, v.data.length) 
        return (
          <div className="contentRow" style={{height: v.data.length * grid}} key={v.pn_no}>
            <div className="sideBarBox">
              <Tooltip placement="top" title={
                <div className="pnInfoWrapper">
                  {/* <div className="pnInfo pnNos">{v.pn_no}</div>
                  <div className="pnInfo pnNos">{v.style_no}</div>
                  <div className="pnInfo">{v.cloth_style}</div> */}
                  <ClipBoard type={'div'} className='pnInfo pnNos'>{v.pn_no}</ClipBoard>
                  <ClipBoard type={'div'} className='pnInfo pnNos'>{v.style_no}</ClipBoard>
                  <ClipBoard type={'div'} className='pnInfo '>{v.cloth_style}</ClipBoard>
                </div>
              }>
                <div className="pnDay">
                  {
                    // v.data.length ? <Button onClick={this.getPnDetailList.bind(this, v.pn_no)} type="primary">{v.pn_no}</Button> : null
                    v.data.length ? <ClipBoard onClick={() => this.getPnDetailList(v.pn_no)} type="primary">{v.pn_no}</ClipBoard> : null
                  }
                </div>
              </Tooltip>
              {v.data.length > 2 ? <div className="pnInfo pnNos">{v.style_no}</div>: null}
              {v.data.length > 2 ? <div className="pnInfo">{v.cloth_style}</div>: null}
            </div>
            {
              justSideBar ? null : (<div className="rowWrapper">
                {
                  v.data.map((item, index) => {
                    const { left, width, levels, pn_no, barcode, shortDay, work_name, factory_name, s_date, e_date, real_qty, open_date, finish_date, rate, realStart, realWidth, factory_id, zIndex, } = item
                    const top = (levels - 1) * 30
                    const isLasItem = index === v.data.length - 1 
                    {/* const widths = dateArr.length * grid
                    const lefts = Math.abs(left) */}
                    {/* index < 50 && console.log('item ：', v, item, index, left, width, barcode, shortDay) */}
                    const realStyle = {zIndex, left: realStart, width: realWidth, position: 'absolute'}
                    {/* const rates = rate * 100 > 100 ? 100 : rate * 100 */}
                    const rates = (rate * 100).toFixed(2)
                    return <div className="pnRow" key={barcode + work_name + factory_name}>
                      {this.createDay(false, false, isLasItem)} 
                      {/* <Slider className='sliders' range 
                      tipFormatter={this.formatter}
                      //step={1} 
                      step={grid / widths * 100}
                      defaultValue={[(lefts) /( widths) * 100, (lefts + width) / (widths) * 100]}  
                      onChange={this.onChange.bind(this, left, width, )} onAfterChange={this.onAfterChange.bind(this, left, width, )} /> */}
                      
                      {/* {realStart != undefined ? <Tooltip placement="top" title={`${barcode + work_name} / ${factory_name} - 實際生産：${real_qty}
                      ${<Button type="primary" onClick={this.getRealDetail} className='detailbtn' size='small'>详情</Button>}`}>
                        <div className="real" style={realStyle}>{real_qty}</div></Tooltip> : null} */}
                        
                      {realStart != undefined ? <Popover content={<span className="barPopover">
                        {barcode + work_name} / {factory_name} - 實際總生産數：{real_qty} - 百分比 {rates}%  <Button type="primary" onClick={() => this.getRealDetail(barcode, pn_no, factory_id)} size='small'>详情</Button></span>}
                      ><div className="real" style={realStyle} onClick={() => this.zIndex(item, i, index)}>{rates}%</div>
                      </Popover> : null}
                        
                      {/* {realStart != undefined ? <Tooltip placement="top" title={`${barcode + work_name} / ${factory_name} - 實際生産：${real_qty}`}>
                      <div className="real" style={realStyle}>{real_qty}</div></Tooltip> : null} */}
                      
                      <DragableBar 
                        isDragBack={isDragBack} 
                        showEdit={showEdit} 
                        changeBarData={changeBarData} 
                        changeBar={this.changeBar} 
                        s_date={s_date} 
                        e_date={e_date} 
                        dateArr={dateArr} 
                        dateLen={dateArr.length} 
                        ref={dragBar => this[barcode] = dragBar} 
                        clickType='barClick' 
                        key={barcode + work_name + factory_name} 
                        pn_no={pn_no} 
                        data={item} 
                        isAxis={'x'} 
                        showBarcodeDialog={this.showBarcodeDialog} 
                        stopDrag={this.stopDrag} 
                        indexes={v.indexes}
                        config={{ left, width, top, txt: `${work_name} / ${factory_name}`, levels, barcode, shortDay, }}
                        //config={{ left, width, top, txt: `${barcode + work_name} / ${factory_name}`, levels, barcode, shortDay, }}
                      ></DragableBar>
                    </div>
                  })
                }
              </div>)
            }
          </div>
        )
    })
  }
  editContent = () => {
    console.log('editContent  this.state ：', this.state)
    const {barcode, barcodeDate, editBarcodeData, cantChange, editContent,  } = this.state
    const {s_date_new, e_date_new} = barcodeDate
    const {startDay, e_date} = editBarcodeData
    if (cantChange) {
      return <div className={`editWrapper ${cantChange ? 'warnDialog' : ''}`}>
        <Icon type="exclamation-circle-o" />{editContent}
      </div>
    } else {
      return <div className="editWrapper">
        <div className="editCon">从 {startDay} 到 {e_date} 变为 {s_date_new} 到 {e_date_new}</div>
      </div>
    }
  }
  headerCom = () => {
    const {datas_barcode, } = this.state
    return datas_barcode.length ? (
      <div className="headerContainer">
          {this.fixBox()}
          {this.createDay(true, true)}
      </div>) : null
  }
  
  changeBar = (data) => {
    console.log('changeBar  data ：', data)
    this.setState({
      changeBarData: data,
      isDragBack: false,
      barcode: data.barcode,
    })
  }
  filter = () => {
    this.setState({
      dateArr: [],
      monthArr: [],
      datas_barcode: [],
      datas: [],
      data_total: [],
    })
  }
  sideBarScroll = (scrollTop) => {
    const domRef = findDOMNode(ReactDOM, this.con)
    // console.log('++++++++++++++++  sideBarScroll ：', scrollTop, domRef)
    domRef.scrollTop = scrollTop// 
    this.setState({sideBarScroll: scrollTop,})
  }
  sideScroll = (scrollTop) => {
    const domRef = findDOMNode(ReactDOM, this.dragToggle.sideBar)
    // console.log('++++++++++++++++  sideScroll ：', scrollTop, domRef, this.dragToggle.sideBar )
    domRef.scrollTop = scrollTop// 
  }
  dialogContent = () => {
    console.log('  dialogContent ：',   )
    const { 
      pn_info, 
      productData, 
      splitData, 
      dialogType, 
      isSpliting, 
      barcodeData, 
      showEdit, 
      disableDay, 
      colorData, 
      colorOrigin, 
      colorQtyObj,
      modalType, 
      realDayQty, 
      pnDetail,
    } = this.state
    if (modalType === 'getAccPnInfo') {
      console.log(' getAccPnInfogetAccPnInfo ： ',  )
      return <ArtPnDetailTable data={pnDetail}/>
    }
    
    if (modalType === 'getRealDetail') {
      return <RealQtyTable data={realDayQty}></RealQtyTable>
    }
    return showEdit ? this.editContent() : <BarcodeContent 
      pn_info={pn_info} 
      productData={productData} 
      splitData={splitData} 
      // dialogType={dialogType} 
      isShowBarcode={dialogType === 'showBarcode'}
      delete={this.delete} 
      lock={this.lock} 
      isSpliting={isSpliting} 
      inputing={this.inputing} 
      splitAction={this.splitAction} 
      showColor={this.showColor}
      barcodeData={barcodeData}
      disableDay={disableDay}
      finishAction={this.finishAction}
      deleteBar={this.deleteBar} 
      colorData={colorData}
      colorOrigin={colorOrigin}
      getAccPnInfo={this.getAccPnInfo}
    ></BarcodeContent>
  }
  onMergeChange = (d_barcode) => {
    console.log(' onMergeChange ： ',  d_barcode )
    this.setState({
      d_barcode,
    })
  }
  sureDelete = () => {
    const {barcode, d_barcode, } = this.state 
    console.log(' sureDelete ： ', barcode, d_barcode  )
    if (d_barcode == undefined) {
      confirms(2, '必選選擇一個條碼進行合並  o(*￣︶￣*)o！')
      return 
    } 
    this.deleteBarcode({barcode, d_barcode,})
  }
  cancleDelete = () => {
    console.log(' cancleDelete ： ',   )
    this.setState({showDelete: false,})
  }
  filterOption = (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
  deleteContent = (v) => {
    const {showDelete, mergeData, } = this.state 
    console.log(' deleteContent ： ',  v, mergeData)
    // if (showDelete) {
    //   return 'showDelete'
    // }
    return <div>
      是否確認將生産線數量全部合並到 &nbsp;&nbsp;&nbsp;
      {
        mergeData.length ? (
          <Select 
            showSearch
            allowClear={true}
            placeholder={SELECT_TXT + '生産線'}
            optionFilterProp="children"
            defaultValue={mergeData[0].factory_name} 
            onChange={this.onMergeChange} 
            filterOption={this.filterOption}
            className="width40"
          >
            {mergeData.map((v, i) => <Option value={v.barcode.toString()} key={i}>{v.barcode} - {v.factory_name} - {v.s_date}</Option>)}
          </Select>
        ) : null
      }
      &nbsp;&nbsp;&nbsp;生産線?
    </div>
  }
  componentDidMount() {
    
    const datas =  [
      {
          "barcode": "100439087B",
          "p_date": "2018-08-11",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087B",
          "p_date": "2018-08-12",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087B",
          "p_date": "2018-08-13",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087B",
          "p_date": "2018-08-14",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087B",
          "p_date": "2018-08-15",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087B",
          "p_date": "2018-08-16",
          "owe_workers": 2,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087A",
          "p_date": "2018-08-10",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087A",
          "p_date": "2018-08-11",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087A",
          "p_date": "2018-08-12",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087A",
          "p_date": "2018-08-13",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087A",
          "p_date": "2018-08-14",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439087A",
          "p_date": "2018-08-15",
          "owe_workers": 1,
          "pn_no": "1808M2031 "
      }, {
          "barcode": "100439092",
          "p_date": "2018-08-16",
          "owe_workers": 0,
          "pn_no": "1808P2024 "
      }
    ]
    
    this.mergeShorts(datas)
    console.log(' this.mergeShorts(datas) ： ', this.mergeShorts(datas), this.props, this.props.userInfo.user_id )
    
    this.getPnList(initParams)
    this.getFilter()
    const domRef = findDOMNode(ReactDOM, this.con)
    domRef.onscroll = (e) => {
      // console.log('ScheduleNo 组件 this.state, this.props ：', this.state, this.props, domRef)
      const {scrollLeft, scrollTop} = domRef
      const {scrollLeftPos, scrollTopPos, datas_barcode, sideBarScroll, tableHeight, tableWidth} = this.state
      // domRef.scrollTop = sideBarScroll//没关滚会卡
      const time = datas_barcode.length > 500 ? null : 50
      // 滚动纵轴同步横轴滚动
      if (scrollLeftPos !== scrollLeft) {// 
        // console.log('横向滚动 ：', scrollLeft)
        ajax(this.setState.bind(this), {
          scrollLeftPos: scrollLeft,
        }, time)
        // this.setState({
        //   scrollLeftPos: scrollLeft,
        // })
      } 
      else if (scrollTopPos !== scrollTop) {
        this.sideScroll(scrollTop)
        // console.log('纵向滚动 ：', e, scrollTopPos, scrollTop, tableWidth / tableHeight, domRef)
        // ajax(this.setState.bind(this), {
        //   scrollTopPos: scrollTop,
        //   // scrollLeftPos: 1000,
        // }, time)
        // domRef.scrollTop = scrollTop + 100 
        // domRef.scrollLeft = scrollTop * tableWidth / tableHeight * 1.15
        // this.setState({
        //   scrollTopPos: scrollTop,
        // })
      }
      // console.log('纵向滚动 ：', e, scrollTopPos, scrollTop, tableWidth / tableHeight, domRef)
      // console.log('!!!@@@@@@@222chatingWrapper Pos：', e, domRef.scrollLeft, domRef.scrollTop, domRef.scrollHeight)
    }
  }
  componentWillReceiveProps(nextProps) {
    // console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ScheduleNo componentWillReceiveProps组件获取变化 ：', nextProps.filterInfo.isSearch, this.props.filterInfo.isSearch, nextProps, this.props, this.state,  )
    if (nextProps.filterInfo.isSearch) {
      this.getPnList(nextProps.filterInfo)
      this.props.scheduleAction.filterInfo({...nextProps.filterInfo, isSearch: false})
    } 
  }
  
  
  render() {
    const {collapse,} = this.props
    const { datas_barcode,show, tableHeight, tableWidth, showEdit, data_total,
      loading, barcodeData, isSpliting, dialogType, splitData, pn_no, pn_info,
      scrollLeftPos, scrollTopPos, productData, barcode, barcodeDate, cantChange, 
      showModal, editPnNo, colorData, colorBar, colorOrigin, colorQtyObj, modalType,
      showDelete, 
    } = this.state
    const title = dialogType === 'showPnCode' ? `批号 ${pn_no} 详情` : 
      showEdit ? `是否确认修改条码 ${barcode} 的起止时间` : modalType === 'getRealDetail' ? 
      `批号 ${pn_no} 實際日産詳情` : modalType === 'getAccPnInfo' ? `批号 ${editPnNo} 详情` : `条码 ${barcode} 詳情` 
    const modalTitle = `批号 ${colorBar} 色碼詳情`
    const deleteTitle = `批号 ${editPnNo} 条码 ${barcode} 刪除操作`
    // const content = showEdit ? this.editContent() : <BarcodeContent pn_info={pn_info} productData={productData} splitData={splitData} isShowBtn={dialogType === 'showBarcode'} dialogType={dialogType} delete={this.delete} lock={this.lock} isSpliting={isSpliting} inputing={this.inputing} splitAction={this.splitAction} barcodeData={barcodeData}></BarcodeContent>
    console.log('————————ScheduleNo 组件 this.state, this.props：', this.props, this.state, )
    // 
    const fixBox = this.fixBox(true)
    const headerCom = datas_barcode.length ? (
      <div className="headerContainer">
          {fixBox}
          {this.createDay(true, true)}
      </div>) : null

    return (
      // <Collapses title={'工艺路线'} className='scheduleCollapse'  isAnimate={false}>
        <div className={`noWrapper ${ANIMATE.fadeIn}`} ref={con => this.con = con}>
          {
            data_total.length ? (
            <DragToggle ref={dragToggle => this.dragToggle = dragToggle} sideBarScroll={this.sideBarScroll} scrollLeftPos={scrollLeftPos} scrollTopPos={scrollTopPos} collapse={collapse} tableHeight={tableHeight} tableWidth={tableWidth}>
              {/* 固定头0 */}
              {/* <div className="fixHeaderWrapper"> */}
                {/* <div className="fixHeader"> */}
                  {headerCom}
                {/* </div> */}
              {/* </div> */}

              {/* 固定列1 */}
              <div className="fixSideBar">
                {fixBox}
                {this.createBody(true)}
              </div> 
              {/* 主体 */}
              {headerCom}
              {/* 主体2 */}
              {this.createBody(false)}

              {/* 固定显示的角落div */}
              {fixBox}

              <div className="corverBar"></div>

            </DragToggle>)
          　: null
          }
          {show ? <ProductDialog isHideOk={cantChange} handleOk={this.handleOk} width={showEdit ? '40%' : '90%'} show={show} close={this.close} title={title}
          okTxt='確認拆單'>
            {this.dialogContent()}
          </ProductDialog> : null}
          {showDelete ? <ProductDialog isHideOk={cantChange} handleOk={this.sureDelete} width={'50%'} show={show} close={this.cancleDelete} title={deleteTitle}>
            {this.deleteContent()}
          </ProductDialog> : null}
          {showModal ? <ProductDialog isHideOk={cantChange} handleOk={this.modalOk} width={'80%'} show={showModal} close={this.modalClose} title={modalTitle}>
            <ColorSizeTable ref={colorTable => this.colorTable = colorTable} 
            colorQtyObj={colorQtyObj} 
            data={colorData[colorBar]}  
            inputing={this.inputing} barcode={colorBar}  
            sizeConfig={sizeConfig}
            ></ColorSizeTable>
          </ProductDialog> : null}
          <Loading loading={loading}></Loading>
        </div>
      // </Collapses>
    );
  }
}

const state = state => state
const action = action => ({ 
  scheduleAction: bindActionCreators(scheduleAction, action), 
  copy: bindActionCreators(copy, action), 
})
export default connect(state, action)(ScheduleNo);

