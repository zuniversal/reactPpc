import React from "react"
import ReactDOM from 'react-dom';
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { getAccPnInfo, } from 'api/Acc'
import { autoPpc,   } from 'api/ArtLine'
import { getPnList, getBarcodeList, getPnDetailList, editBarcode, getFilter, splitPnBarcode, barcodeLock, finishDay, getRealDetail, deleteBarcode, } from 'api/ppc'
import { 
  grid, initParams, DATE_FORMAT_BAR, MONTH_FORMAT, FORMAT_DAY, 
  realExist, equalAttr, workConfig, workOption, scrollBtnInfo, sizeConfig, 
  pnDateArr, splitMap, 
} from 'config'
import { ANIMATE, LETTER, SELECT_TXT, ALPN_ENABLED,  } from 'constants'
import { backupFn, createRow, findDOMNode, openNotification, ajax, confirms, getItems, filterDatas, dateSplits, dateFormat,} from 'util'
import ppcList from './ppcList.json'
// import DragableBar from '@@@/DragableBar'
// import DragableBar from '@@@/DragableSlider'
import DragableBar from '@@@/DragableSliders'
import ProductDialog from '@@@/ProductDialog'
import DragToggle from '@@@/DragToggles'
import BarcodeContent from '@@@/BarcodeContent'
import Collapses from '@@@/Collapses'
// import FilterPane from '@@@/FilterPane'
import ScheduleDay from '@@@/ScheduleDay'// 
import ScheduleDayRow from '@@@/ScheduleDayRow'// 
import ScheduleBody from '@@@/ScheduleBody'
import Loading from '@@@@/Loading'
import SelectForm from '@@@/SelectForm'
import ColorSizeTable from '@@@/ColorSizeTable'
import RealQtyTable from '@@@/RealQtyTable'
import ArtPnDetailTable from '@@@/ArtPnDetailTable'
import {haveAuth } from '@@@@/SecureHoc'
import * as scheduleAction from 'actions/scheduleNo.js'
import {copy} from 'actions/login.js'
import moment from 'moment';
// import ClipBoards from '@@@@/ClipBoards'
import ClipBoard from '@@@@/ClipBoard'
import { Dropdown, Button, Tooltip, Slider, Icon, Popover, Select, Modal, } from 'antd';
const Option = Select.Option;
const confirm = Modal.confirm;


class ScheduleNo extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      show: false,
      dateArr: [],
      monthArr: [],
      datas_barcode: [],
      datas: [],
      data_total: [],
      barcodeData: [],
      showBarcode: false,
      showEdit: false,
      loading: false,
      isSpliting: false,
      barcode: '',
      barcodeDate: {},
      pn_no: '',
      dialogType: '',
      splitData: [],
      scrollLeftPos: 0,
      scrollTopPos: 0,
      productData: [],
      visible: false,
      customerData: [], 
      styleNoData: [],
      editBarcodeData: {},
      editPnIndex: '',
      pn_info: {},
      dayRange: [],
      changeBarData: [],
      isDragBack: false, 
      cantChange: false,
      sideBarScroll: 0,
      indexes: -1,
      disableDay: '',
      showModal: false, 
      colorBar: '',
      pnColorData: [],
      realDayQty: [],
      modalType: '',
      showDelete: false, 
      mergeData: [],
      splitLen: 0,
      d_barcode: '',
      scrollInfo: {},
      pnDetail: [],
      
      colorSizeData: [],
      colorSizeOrigin: [],
      colorSizeDataObj: {},
      colorSizeQtyObj: {},
      isSplit: false, 
      new_s_date: '', 
      new_e_date: '',
      new_factory_id: '', 
      new_factory_name: '',
      colorArr: [],
      pnItem: -1,
      isEditting: false, 
      residueColorData: [],
      
      colorSizeObjArr: [],
      calcColorSizeTotal: [],
    }
  }
  getPnList = (p) => {
    this.props.scheduleAction.filterInfo(p)
    console.log(' renderHeader  getPnList ：', this.headerCom, this.state, this.props,   )
    this.setState({
      loading: true,
      visible: false,
    })
    getPnList(p).then(res => {
      console.log(' getPnList 22：',  )
      // console.log('请求所有的getPnList ：', ppcList);
      let { datas_barcode, datas, data_total } = res.data

    
      this.headerCom = null// 
      const today = moment().format(DATE_FORMAT_BAR)
      // console.log('data_total ：', data_total, res.data.data_total, data_total.length)
      // datas_barcode = datas_barcode.slice(60, 100)
      // let { datas_barcode, datas, data_total } = ppcList
      // datas_barcode = datas_barcode.slice(0, 15)
      // datas = datas.slice(0, 15)
      // data_total = data_total.slice(0, 15)

      // datas_barcode = datas_barcode.slice(5, 55)
      // datas = datas.slice(5, 55)
      // data_total = data_total.slice(5, 55)

      // const { datas_barcode, datas, data_total } = ppcList
      // const data1 = ppcList.data_total;
      if (data_total.length && datas_barcode.length) {
        this.filter()
        console.log('有数据1  HH:mm:ss：', data_total, today)

        const {filterInfo: {
          pn_no,
          style_no,
          customer,
        },  } = this.props
  
        const isFilter = pn_no !== '' || style_no !== '' || customer !== ''
        const filterDay = isFilter ? 30 : 0
        console.log(' pn_nopn_nopn_no ： ', isFilter, filterDay, pn_no, style_no, customer, pn_no === '', style_no === '', customer === '', )// 

        // const startDate = data_total[0].s_date
        // const endtDate = data_total[0].e_date
        const startDate = moment(data_total[0].s_date).subtract(filterDay, 'days').format(DATE_FORMAT_BAR)
        const endtDate = moment(data_total[0].e_date).add(filterDay, 'days').format(DATE_FORMAT_BAR)
        data_total[0].s_date = startDate
        data_total[0].e_date = endtDate

        const dateArr = []
        const monthes = []
        let a = moment(startDate, DATE_FORMAT_BAR)
        let b = moment(endtDate, DATE_FORMAT_BAR)
        // console.log(' a, b ： ', a, b,  )// 
        const dayLens = b.diff(a, 'days') + 1
        console.log('dayLens ：', startDate, startDate.split('-'), endtDate, dayLens, a, b)
        // 得到排单所有天数
        for (let i = 0; i < dayLens; i++) {
          const day = moment(startDate).add(i, 'd')
          const date = day.format(FORMAT_DAY)
          const month = day.format(MONTH_FORMAT)
          const isSunday = day.format('d') === '0'
          const dateOrigin = day.format(DATE_FORMAT_BAR)
          monthes.push(month)
          // console.log('sRDates ：', month, date, isSunday)
          dateArr.push({ month, date, isSunday, dateOrigin });
        }
        // 去重 - 得到排单起止日期 - 包括月份总数组
        const set = new Set(monthes);
        const monthess = new Array(...set);
        const monthArr = monthess.map(v => ({ month: v, date: [] }))
        // console.log(' monthes, dateArr, monthArr：', monthes, monthArr, dateArr, moment(["2017", "1", "01"]));
        monthArr.forEach((v, i) => {
          for (let j = 0; j < dateArr.length; j++) {
            if (dateArr[j].month === v.month) {
              if (dateArr[j].dateOrigin === today) {
                dateArr[j].today = true
              }
              monthArr[i].date.push(dateArr[j])
              // console.log(' v ： ', dateArr[j].dateOrigin, dateArr[j].dateOrigin === today, v, today )
            }
          }
        })
        datas_barcode.map((item, i) => {
          const shortDay = datas.filter(v => v.barcode === item.barcode)
          // 如果有红色天数数量赋给每个条码
          item.shortDay = shortDay
          // item.isOutDay = moment(item.e_date).isAfter(today)
          this.isOutDay(item, )
          // console.log('shortDay ：', shortDay, item.isOutDay);// 
          item.shortDay.length && this.calcShortDay(item.shortDay, moment(item.s_date), a)
          this.calcInfo(item, a)
          return item
        })
        let level = 0
        let tableHeight = 0
        let startPnNo 
        const scrollHeightArr = []
        const scrollInfo = {}
        // 遍历所有的排单批号给所有的条码计算宽度、距离
        data_total.forEach((item, i) => {
          // console.log('item ：', item)
          const row = createRow(item.levels)
          if (i === 0) {
            // console.log('第一次 ：', scrollHeightArr, tableHeight, data_total.length)
            startPnNo = item.pn_no.substr(2, 2)
          } else if (item.pn_no.substr(2, 2) !== startPnNo && i !== 0) {
            // console.log('startPnNo = item.pn_no.substr(2, 2) ：', item, data_total[i + 1], data_total[i - 1], tableHeight, item.levels, item.pn_no, startPnNo, item.pn_no.substr(2, 2))
            startPnNo = item.pn_no.substr(2, 2)
            scrollHeightArr.push({startPnNo, tableHeight, })
            // console.log('%%%%%%%%%%scrollHeightArr ：', scrollHeightArr, tableHeight);
          } 
          tableHeight += row.length
          const factoryArr = []
          const workArr = []
          // const data = datas_barcode.filter(v => {
          //   if (item.pn_no === v.pn_no) {
          //     factoryArr.push(v.factory_id)
          //     workArr.push(v.work_no)
          //     // v.tops = item.levels
          //   }
          //   return item.pn_no === v.pn_no
          // })

          // const datas = datas_barcode.slice(level, level + item.levels)
          // console.log(' datas ： ', data, datas,  )// 


          const data = datas_barcode.slice(level, level + item.levels)
          data.forEach((v, i) => {
            // console.log('  v ： ', v, i,  )
            factoryArr.push(v.factory_id)
            workArr.push(v.work_no)
          })


          // const datas = []
          // for (let i = 0, len = datas_barcode.length; i < len; i++) {
          //   const itemss = datas_barcode[i]
          //   if (itemss.pn_no === item.pn_no && item.levels === data.length) {
          //     // console.log(' 相等批号的 i ： ', i, itemss, itemss.pn_no, item.pn_no, item,  )
          //     break
          //   }
          //   console.log('  datas ：', datas, item.levels, datas.length )
          //   datas.push(itemss)
            
          //   // console.log(' 不是同一个批号的 ： ', itemss, itemss.pn_no, item.pn_no,    )// 
          // }
          

          const factoryWipe = Array.from(new Set(factoryArr))
          const workWipe = Array.from(new Set(workArr))
          item.indexes = i
          item.row = row
          item.data = data
          item.level = level
          item.factoryArr = factoryWipe
          item.workArr = workWipe
          level += item.levels
          scrollInfo[item.level] = item.level * grid
          // return item
        })
        
        
        // console.log('1111111111111111 ：', dateArr, monthArr, datas_barcode, datas, data_total )
        this.setState({
          scrollInfo,
          loading: false,
          dateArr,
          monthArr,
          datas_barcode,
          datas,
          data_total,
          // dateArr: [...backupFn(dateArr)],
          // monthArr: [...backupFn(monthArr)],
          // datas_barcode: [...backupFn(datas_barcode)],
          // datas: [...backupFn(datas)],
          // data_total: [...backupFn(data_total)],
          tableHeight: tableHeight * 30 + 75,
          tableWidth: dayLens * 30 + 130,
          // scrollLeftPos: 0, 
        })
      } else {
        console.log('无数据 ：', )
        // openNotification('沒有數據，請重新選擇過濾條件！',  )
        confirms(2, '沒有數據，請重新選擇過濾條件!', )
        this.setState({
          loading: false,
        })
      }
    })// 
  }
  // 计算每个条码的宽度、距离等数据
  calcInfo = (v, startDay) => {
    // v.shortDay = []
    // console.log('v. open_date ： ', v, v.open_date, v.real_qty )
    const width = moment(v.e_date).diff(moment(v.s_date), 'days')
    const left = moment(v.s_date).diff(startDay, 'days')
    v.startDay = v.s_date
    v.width = (width + 1) * 30
    v.left = (left) * 30
    if (realExist.some(item => v[item] != undefined)) {
      const realStart = moment(v.open_date).diff(startDay, 'days')
      const realWidth = Math.abs(moment(v.finish_date).diff(moment(v.open_date), 'days')) + 1
      // console.log(' realWidth ： ', v, realStart, v.open_date, v.finish_date, realWidth )
      v.realStart = realStart > 1 ? realStart * 30 : 0 
      v.realWidth = realWidth * 30
      v.zIndex = 1
    }
  }
  // 计算缺工红色日子的距离
  calcShortDay = (v, barStart, startDay) => {
    // console.log('calcShortDay v, startDay ：', v, barStart, startDay)
    v.map(item => item.shortDate = (moment(item.p_date).diff(startDay, 'days') - barStart.diff(startDay, 'days')) * 30)
  }
  showDialog = () => this.setState({ show: true, })
  dragBack = () => {
    const { barcode, barcodeDate, editBarcodeData } = this.state
    const { left, width } = editBarcodeData
    const {s_date_new, e_date_new} = barcodeDate
    // const newWidth = moment(e_date_new.split('-')).diff(s_date_new.split('-'), 'days') * grid + 'px'
    console.log('关闭this.state11 ：', this.state, this.dragBar, dragBar, barcode, left, width)
    const dragBar = findDOMNode(ReactDOM, this[barcode])
    // console.log('关闭this.state 22：', dragBar.style, dragBar.style)
    if (dragBar.style.width !== width) {
      console.log('日期变了  宽度变了：', dragBar.style.width, width)
      // dragBar.style.width = width
      // console.log('日期变了  宽度变了2：', dragBar.style.width, width)
    }
    dragBar.style.transform = `translate(${left}px, 0px)`
  }
  resetColorInfo = (v) => {
    return {
      show: false,
      splitData: [],
      colorBar: '',
      splitLen: 0,
      colorSizeData: [],
      colorSizeOrigin: [],
      colorSizeDataObj: {},
      colorSizeQtyObj: {},
      colorArr: [],
      pnItem: -1,
      isSplit: false,
      isEditting: false, 
      calcColorSizeTotal: [],
      colorSizeObjArr: {},
    }
  }
  close = () => {
    const { barcode, showEdit, editBarcodeData } = this.state
    const resetParams = {
      show: false,
      showEdit: false,
      showBarcode: false,
      splitData: [],
      isDragBack: true, 
      cantChange: false, 
      editContent: '', 
      dialogType: '',
      modalType: '',
      isSplit: false,
      colorArr: [],
      pnItem: -1,
      isEditting: false, 
      // new_s_date: '', 
      // new_e_date: '',
      // new_factory_id: '', 
      // new_factory_name: '',
      ...this.resetColorInfo(),
    }
    if (showEdit) {
      this.dragBack()
    } else {
      resetParams.changeBarData = []
    }
    console.log('showEdit resetParams：', showEdit, resetParams)
    this.setState(resetParams);
  }
  // 点击barcode开始编辑
  showBarcodeDialog = (barcode, pn_no, work_no, indexes) => {
    console.log('showBarcodeDialog22 ', barcode, pn_no, work_no, indexes);
    // this.props.scheduleAction.showDialog(true)
    this.getBarcodeList({barcode, work_no, pn_no, indexes})
    // this.setState({ barcode, editPnNo: pn_no, })
  }
  calcColorSizeTotal = (data, ) => {
    // console.log(' calcColorSizeTotal ： ', data,  )
    const colorSizeData = []
    data.forEach((v, i) => {
      v.qtys = 0
      const item = colorSizeData.findIndex(item => item.combo_seq === v.combo_seq)
      item > -1 ? colorSizeData[item].data.push(v) : colorSizeData.push({...v, isCheckAll: 'T', data: [v]})
    })
    colorSizeData.forEach((v, i) => {
      v.useQty = 0 
      v.data.forEach((item) => {
        v.qtys += item.qty
        v.useQty += item.status === 'T' ? item.qty :  0 
      })
      v.sch_qty = v.qtys
    })
    console.log(' 计算 colorSizeQtyObj colorSizeQtyObj, datas, ： ', colorSizeData, )
    return colorSizeData 
  }
  // 获取barcode信息请求
  getBarcodeList = (p) => {
    const {barcode, pn_no, indexes} = p
    getBarcodeList(p).then(res => {
      console.log('getBarcodeList 获取批号详情 res... ：', p, res.data);
      // this.props.scheduleAction.barcode(res.data.datas)
      let {datas, factory, color, } = res.data
      
      // 使用已有数组重新组装一个新数组
      const colorSizeData = []
      color.forEach((v, i) => {
        v.qtys = 0
        const item = colorSizeData.findIndex(item => item.combo_seq === v.combo_seq)
        item > -1 ? colorSizeData[item].data.push(v) : colorSizeData.push({...v, isCheckAll: 'T', data: [v]})
      })

      const new_s_date = datas[0].s_date
      const new_e_date = datas[0].e_date
      datas[0].new_s_date = new_s_date
      datas[0].new_e_date = new_e_date

      // const new_s_date1 = datas[0].s_date.slice(0, 4)
      // const new_s_date2 = datas[0].s_date.slice(5, 10).replace('-', '')
      // const new_e_date1 = datas[0].e_date.slice(0, 4)
      // const new_e_date2 = datas[0].e_date.slice(5, 10).replace('-', '')

      // datas[0].new_s_date1 = new_s_date1
      // datas[0].new_s_date2 = new_s_date2
      // datas[0].new_e_date1 = new_e_date1
      // datas[0].new_e_date2 = new_e_date2

      datas[0].new_factory_id = datas[0].factory_id
      datas[0].new_factory_name = datas[0].factory_name
      datas[0].qty = 0 
      datas[0].color = []
      const colorSizeQtyObj = {}
      colorSizeData.forEach((v, i) => {
        v.useQty = 0 
        v.data.forEach((item) => {
          v.qtys += item.qty
          v.useQty += item.status === 'T' ? item.qty :  0 
        })
        // console.log(' ssssssssv.useQty ： ', v.useQty, v )
        datas[0].qty += v.useQty
        colorSizeQtyObj[v.combo_seq] = v.useQty
      })
      console.log(' colorSizeQtyObj111 ： ', colorSizeQtyObj, datas, colorSizeData,  )
      // return 
      this.setState({
        showBarcode: true, 
        barcodeData: [{...datas[0], isSplitData: false}],
        productData: factory,
        dialogType: 'showBarcode',
        show: true,
        barcode, 
        colorArr: color,
        editPnNo: pn_no,
        colorSizeDataObj: {[datas[0].barcode]: [...colorSizeData]},
        colorSizeOrigin: [...colorSizeData],
        colorSizeQtyObj,
        colorSizeData: [...colorSizeData],
        // new_s_date: dateSplits(datas[0].s_date), 
        // new_e_date: dateSplits(datas[0].e_date), 
        // new_s_date1,
        // new_s_date2,
        // new_e_date1,
        // new_e_date2,
        new_s_date, 
        new_e_date, 
        new_factory_id: datas[0].factory_id, 
        new_factory_name: datas[0].factory_name,
        // new_s_date: datas[0].s_date, 
        // new_e_date: datas[0].e_date, 
        pnItem: indexes, 
        indexes,

        colorSizeObjArr: {[datas[0].barcode]: [...color]},
        calcColorSizeTotal: this.calcColorSizeTotal(backupFn(color)),
      })
    })
  }
  colorSizeHandle = (colorBar,   ) => {
    const {colorSizeObjArr, calcColorSizeTotal,  } = this.state// 
    console.log(' colorSizeHandle  ： ', this.state, this.props, colorSizeObjArr, colorBar,    )
    const colorSizeData = []
    colorSizeObjArr[colorBar].forEach((v, i) => {
      v.qtys = 0
      v.isUseOut = v.qty === 0 
      const item = colorSizeData.findIndex(item => item.combo_seq === v.combo_seq)
      item > -1 ? colorSizeData[item].data.push(v) : colorSizeData.push({...v, isCheckAll: 'T', data: [v]})
    })
    colorSizeData.forEach((v, i) => {
      v.useQty = 0 
      const matchItem = calcColorSizeTotal.find(item => item.combo_seq === v.combo_seq)
      // console.log(' matchItemsssssss ： ', matchItem,  )// 
      v.qtys = matchItem!= undefined ? matchItem.qtys : v.qtys
      v.sch_qty = matchItem!= undefined ? matchItem.sch_qty : v.qtys
      
    })
    // console.log(' colorSizeQtyObj colorSizeQtyObj, datas, ： ', colorSizeObjArr, colorSizeObjArr[colorBar], colorSizeData, this.state, )
    return colorSizeData 
  }
  isOutDay = (v) => {
    // console.log(' isOutDay  ： ',  v )
    const today = moment().format(DATE_FORMAT_BAR)
    v.isOutDay = moment(v.e_date).isAfter(today)
  }
  // 批号辅料详情
  getAccPnInfo = () => {
    const {editPnNo, } = this.state 
    console.log(' getItems() ： ', this.state, editPnNo,  )
    getAccPnInfo({obj: { 
      // pn_no: '1812M2001', 
      pn_no: editPnNo,
    }})
    .then(res => {
      console.log('getAccPnInfo res ：', res.data);
      this.setState({
        pnDetail: res.data.datas,
        showDelete: true,
        modalType: 'getAccPnInfo',
      })
    })
  }
  // 获取bar实际日产
  getRealDetail = (barcode, pn_no, factory_id) => {
    console.log(' getRealDetail ： ', barcode, pn_no, factory_id )
    getRealDetail({pn_no, factory_id}).then(res => {
      console.log('getRealDetail res ：', res.data);
      const {datas, code, } = res.data
      const vals = datas.reduce((total, cuv, cui, arr ) => {
        return total + cuv.real_qty
      }, 0)
      this.setState({
        show: true,
        pn_no,
        barcode,
        realDayQty: [...datas, {sf_date: '總數', real_qty: vals, }], 
        modalType: 'getRealDetail',
      })
    })
  }
  // 获取批号信息请求
  
  
  getPnDetailList = (pn_no) => {
    console.log('getPnDetailList pn_no ：', pn_no);
    this.props.copy(pn_no)
    this.setState({pn_no})
    getPnDetailList({pn_no}).then(res => {
      console.log('getPnDetailList res ：', res.data);
      // this.props.scheduleAction.barcode(res.data.datas)
      const {datas, code, pn_info, color, } = res.data
      // if (code === 1) {
        const colorSizeData = []
        color.map((v, i) => ({...v, barcode: v.barcode.trim()}))
        .forEach((v, i) => {
          v.qtys = 0
          const item = colorSizeData.findIndex(item => item.barcode === v.barcode)
          item > -1 ? colorSizeData[item].data.push(v) : colorSizeData.push({...v, isCheckAll: 'T', data: [v]})
        })
        const colorSizeDataObj = {}
        // datas[0].qty = 0 
        datas[0].color = []
        const colorSizeQtyObj = {}
        colorSizeData.forEach((v, i) => {
          v.useQty = 0 
          v.justShow = true 
          v.data.forEach((item) => {
            v.qtys += item.qty
            v.useQty += item.status === 'T' ? item.qty :  0 
          })// 
          // console.log(' ssssssssv.useQty.data ： ', v.useQty, v )
          // datas[0].qty += v.useQty
          colorSizeQtyObj[v.barcode] = v.useQty
          colorSizeDataObj[v.barcode] = [v]
        })
        console.log(' ...v.qtys = 0colorSizeData ： ', datas,  colorSizeData,  )
        const colorSizeObjArr = {}
        datas.forEach((v) => colorSizeObjArr[v.barcode.trim()] = color.filter(item => v.barcode.trim() === item.barcode.trim()))
        console.log('  colorSizeObjArr ： ', colorSizeObjArr, ) 
        // return  
        this.setState({
          dialogType: 'showPnCode', 
          barcodeData: datas,
          show: true,
          pn_info,

          colorSizeDataObj,
          
          colorSizeOrigin: [...colorSizeData],
          colorSizeQtyObj,
          colorSizeData: [...colorSizeData],       
          colorSizeObjArr,
        })
      // }
    })
  }

  startEditDay = (t, v, i) => {
    console.log(' startEditDay ： ', t, v, i )
    this.setState({ isEditting: true, })
  }
  inputing = ({v, barcode, keys}) => {
    const {barcodeData, splitData, pnItem, } = this.state
    console.log('排单inputing e v, barcode, keys：', pnDateArr.some(v => v === keys), barcodeData, v, barcode, keys, this.props);
    let {originQty, qty} = barcodeData[0]
    
    if (keys === 'qty') {
      let total = 0
      splitData.forEach(item => {
        if (item.barcode === barcode) {
          item.qty = Number(v)
          console.log(' 新数量 splitData item ：', item, Number(v), ) 
        }
        total += item.qty
      })
      if (total === qty) {
        console.log('刚刚好 ：', )
        originQty = originQty - total
      } else {
        if (originQty - total > 0) {
          // 对应的改变父条码的数量
          console.log('输入的数合法 ：', originQty, total)
          originQty = originQty - total
        } else if (originQty - total < 0) {
          console.log('输入的数不合法 ：', originQty, total)
          // openNotification('對不起，您輸入的數量超過了父條碼的數量，請重新拆分！', 'warning')
          confirms(2, '對不起，您輸入的數量超過了父條碼的數量，請重新拆分!', )
        }
      }
      console.log('改变数量 ：', barcodeData[0], Number(v))
    // } else if (pnDateArr.some(v => v === keys)) {
    } else if (pnDateArr.some(v => v.key === keys)) {
      console.log(' 改变日期 date：', v, barcode, keys, )
      // console.log('不是改变数量 date：', v[0]._d, v[1]._d, moment(v[0]._d).format(DATE_FORMAT_BAR), )
      // console.log('v[1].：', moment((moment(v[1]._d).format(DATE_FORMAT_BAR).split('-'))).diff(moment((moment(v[0]._d).format(DATE_FORMAT_BAR).split('-'))), 'days'))
      // const s_date = moment(v[0]._d).format(DATE_FORMAT_BAR)
      // const e_date = moment(v[1]._d).format(DATE_FORMAT_BAR)
      // console.log(' s_date  e_date ： ', s_date, e_date,  )
      // const validTxt = this.dateRange(s_date, e_date, barcodeData[0], pnItem, )// 
      // console.log(' s_date, e_date,： ', s_date, e_date, validTxt )// 
      // if (validTxt !== '' ) {
      //   confirms(2, validTxt, )
      //   return  
      // }
      // const work_days = moment(e_date.split('-')).diff(moment(s_date.split('-')), 'days') + 1
      // const work_days2 = moment(e_date).diff(moment(s_date), 'days') + 1
      // console.log(' s_date  e_date ： ', s_date, e_date, work_days2 )
      splitData.forEach(item => {
        if (item.barcode === barcode) {
          // // item.s_date = moment(v[0]._d).format(DATE_FORMAT_BAR)
          // // item.e_date = moment(v[1]._d).format(DATE_FORMAT_BAR)
          // item[keys] = dateFormat(v)
          // // item[`new_${keys}`] = dateFormat(v)
          // item[`new_${keys}`] = v
          // // console.log(' s_date, e_date222222,： ', item.s_date, item.e_date,  )// 
          // // const validTxt = this.dateRange(dateSplits(item.s_date), dateSplits(item.e_date), barcodeData[0], pnItem, )// 
          
          item[`new_${keys}`] = v
          const new_s_dates = dateFormat(item.new_s_date1 + item.new_s_date2.replace('-', ''))
          const new_e_dates = dateFormat(item.new_e_date1 + item.new_e_date2.replace('-', ''))
          item.s_date = new_s_dates
          item.e_date = new_e_dates

          console.log(' s_date, e_date22222211,： ', item, item.s_date, item.e_date,  )// 
          const validTxt = this.dateRange(item.s_date, item.e_date, barcodeData[0], pnItem, )// 
          console.log(' s_date, e_date222222,： ', validTxt )// 
          if (validTxt !== '' ) {
            // confirms(2, validTxt, )
            return  
          }
          const work_days2 = moment(item.e_date).diff(moment(item.s_date), 'days') + 1
          console.log(' splitData.forEac ： ', dateFormat(v), v, barcode, keys, work_days2,  )
          item.work_days = work_days2
        }
      })
    } else {
      console.log('改变内容', )
      if (keys === 'work_days') {
        v = Number(v)
      }
      splitData.map(item => {
        if (item.barcode === barcode) {
          console.log('改变内容', item.barcode === barcode, item.barcode, barcode, item[keys], v)
          item[keys] = v
        }
        return item
      })
    }
    console.log('matchItem ：', splitData, barcodeData)
    this.setState({
      barcodeData: [...barcodeData],
      splitData: [...splitData],
    })
  }
  showColor = (t, v, i) => {
    console.log(' showColorshowColor ： ',  t, v, i )
    const {barcode, } = v
    this.setState({
      colorBar: barcode,
      showModal: true,
    })
  }
  modalOk = () => {
    console.log(' modalOk ： ', this.colorTable, this.state  )
    const {colorBar, splitData, barcode, colorSizeDataObj, colorArr, colorSizeData, colorSizeObjArr, calcColorSizeTotal,   } = this.state 
    const {state, props, } = this.colorTable
    const {data, selectedRowKeys, } = state
    // const {barcode, } = props
    // const haveQty = data.map((v, i) => v.data.some(item => item.status === 'T'))
    const haveQty = data.map((v, i) => v.data.some(item => item.qty !== 0))
    console.log(' data ： ', barcode, data, selectedRowKeys, haveQty, data.map((v, i) => v.data.some(item => item.status === 'T')) )
    if (haveQty.length &&!haveQty.some(v => v)) {
      confirms(2, '拆分的每一條bar至少要有一組色碼數據 !', )
      return
    }
    let qty = 0
    data.forEach((v) => {
      // console.log('  v ： ', v, )
      v.useQty = 0
      v.data.forEach(item => {
        v.useQty += item.status === 'T' ? item.qty : 0
        item.qty = item.status === 'T' ? item.qty : 0
        item.status = item.qty !== 0 ? item.status : 'F'
      })
      qty += v.useQty
      // 如果是选中 
      
    })
    colorSizeDataObj[colorBar] = data
    let newColorData = colorSizeData
    const colorArrs = backupFn(colorArr)
    const colorSizeDataObjs = backupFn(colorSizeDataObj)


    const newData = []
    data.map((v) => newData.push(...v.data))
    console.log(' qty ： ', qty, data, colorSizeDataObj, colorArrs, newData  )

    colorSizeObjArr[colorBar] = newData

    // const residue = []
    const residueColorData = colorArrs
    // const residueColorData = colorArrs.map((v, i) => {
    //   // console.log(' colorArr v ： ', v, ) 
    //   Object.keys(colorSizeDataObjs).forEach((items, indexs, ) => {
    //     // console.log(' colorSizeDataObjs items 循环 总拆分色码 ： ', items, colorSizeDataObjs[items],    )
    //     if (items !== barcode) {
    //       const colorTotalItem = colorSizeDataObjs[items]
    //       colorTotalItem.forEach((colorItem) => {
    //         colorItem.data.forEach((item) => {
    //           // console.log(' colorItemcolorItem  ： ', item, ) 
    //             if (v.combo_seq === item.combo_seq && v.size_id === item.size_id && item.status === 'T') {
    //               // console.log(' cuvSizeDataObjs vcuvtotal 总子色码 的得到 ： ', item.qty, v.qty, item, item.combo_seq, v.combo_seq, item.size_id, v.size_id  )//  
    //               v.qty -= item.qty
    //               // console.log(' newItem ： ', v.qty, item.qty, v.status,  )// 
    //               v.status = v.qty === 0 ? 'T' : 'F'
    //               // console.log(' newItem22 ： ', v.qty !== 0, v.qty, item.qty, v.status,  )// 
    //             }
    //           //   return newColorData 
    //           // return 
    //         })
    //       })
    //     }
    //   })
    //   return v
    // })
    console.log(' residueColorData ： ', colorSizeObjArr, residueColorData, colorArrs, colorArrs.filter(v => v.qty !== 0),   )// 
    // const newColorArr = []
    // const newColorDatas = Object.keys(colorSizeDataObjs).filter((items, indexs, ) => {
    //   console.log(' colorSizeDataObjs items 循环 总拆分色码 ： ', items, colorSizeDataObjs[items],    )
    //   if (items !== barcode) {
    //     newColorData = colorSizeDataObjs[items].map((v, i) => {
    //       console.log(' colorSizeDataObjs vitems 循环 子拆分色码 ： ', v  )
    //       return v.data
    //     })
    //     console.log(' newColorData ： ', newColorData,  )//
    //     newColorArr.push(...newColorData)
    //     console.log('  newColorArr ：', newColorArr, )
    //     return newColorData 
    //   }
    // })
    // console.log(' newColorArr ： ', newColorArr,  )// 

    const remainColorDatas = this.calcColorSize(barcode, colorSizeObjArr)


    splitData.forEach((v, i) => {
      if (v.barcode === colorBar) {
        console.log(' 相等 ： ',  )
        v.qty = qty
      }
    })
    console.log(' newColorObj ： ', data,  splitData, colorSizeDataObj, residueColorData, remainColorDatas )

    const residue = remainColorDatas.filter(v => v.qty !== 0)
    console.log(' residue ： ', residue )

    calcColorSizeTotal.forEach((v, i) => {
      // const sch_qty = v.qtys
      v.sch_qty = v.qtys
      residue.forEach(item => {
        // console.log(' itemitemitemitem ： ', item,  )// 
        if (item.combo_seq === v.combo_seq) {
          // console.log(' 相等相等 ： ', v.sch_qty, item, v,   )
          v.sch_qty -= item.qty
        }
      })
    })

    this.setState({
      splitData: [...splitData],
      colorSizeDataObj: {...colorSizeDataObj},
      showModal: false, 
      // residueColorData: colorArrs.filter(v => v.qty !== 0), 
      residueColorData: residue,
      // residueColorData: residueColorData.filter(v => v.qty > 0),
      colorSizeObjArr: {...colorSizeObjArr, [colorBar]: colorSizeObjArr[colorBar].map((v) => {
        // console.log('  v s： ', v, v.status,     ) 
        return {...v, qty: v.status === 'T' ? v.qty: 0,  }
      })},
      calcColorSizeTotal,
    })
  }
  modalClose = () => {
    console.log(' modalClose ： ', this.colorTable  )
    const {colorSizeObjArr, } = this.state// 
    this.setState({
      showModal: false,
    })
  }

  calcColor = (parentBarcode, data, ) => {
    const {colorSizeData, splitData, colorSizeDataObj, colorArr, } = this.state// 
    console.log(' calcColor ： ', colorSizeData, splitData, colorSizeDataObj, parentBarcode, data, this.state,    )
    let newColorData = colorSizeData
    const colorArrs = backupFn(colorArr)
    const colorSizeDataObjs = backupFn(colorSizeDataObj)
    // let newColorData = []
    Object.keys(colorSizeDataObjs).forEach((items, indexs, ) => {
      // console.log(' colorSizeDataObjs items 循环 总拆分色码 ： ', items, colorSizeDataObjs[items],    )
      if (items !== parentBarcode) {
        colorArrs.map((colorItem) => {
          // let newColorItem
          const newColorItem = colorItem
          // newColorItem.qty = colorItem.status == 'T' ? item.qty : 0
          // newColorItem.status = newColorItem.qty !== 0 ? 'T' : 'F'
          newColorData = colorSizeDataObjs[items].map((v, i) => {
            // console.log(' colorSizeDataObjs vitems 循环 子拆分色码 ： ', v  )
            return {...v, data: v.data.map((item) => {
              // console.log(' colorSizeDataObjs vitems 色码新建 v ： ', item  )// 
              // const newColorItem = item
              // newColorItem.qty = item.status == 'T' ? item.qty : 0
              // newColorItem.status = newColorItem.qty !== 0 ? 'T' : 'F'
              // console.log(' newColorItem 111： ', newColorItem, newColorItem.status, item.status, item.status === 'T' )// 
              // return {...item, qty: newColorItem, status: newColorItem !== 0 ? 'T' : 'F',  }
              // console.log(' newColorItem ： ', newColorItem, newColorItem.status,   )// 
              // return {...newColorItem, 
              //   status: newColorItem.qty !== 0 ? 'T' : 'F',  // 
              // }
              if (colorItem.combo_seq === item.combo_seq && colorItem.size_id === item.size_id && item.status === 'T') {
                // console.log(' colorSizeDataObjs vcolorItems 总子色码 的得到 ： ', newColorItem, colorItem.qty, item.qty, colorItem, colorItem.combo_seq, item.combo_seq, colorItem.size_id, item.size_id  )//  
                colorItem.qty -= item.qty
                // console.log(' newColorItem222 ： ', newColorItem, newColorItem.qty, colorItem.qty, item.qty,  )// 
                newColorItem.qty = colorItem.qty
                newColorItem.status = newColorItem.qty !== 0 ? 'T' : 'F'
                // console.log(' newColorItem333 ： ', newColorItem, newColorItem.qty, colorItem.qty, item.qty,  )// 
              }
            return newColorItem
            // return item
            })}
          })
        })
      }
    })
    console.log(' newColorData ： ', newColorData,  )// 
    return newColorData 
  }
  calcColorSize = (parentBarcode, colorSizeObjArr) => {
    const {colorSizeData, splitData, colorArr, } = this.state// 
    const colorArrs = backupFn(colorArr)
    console.log(' calcColorSize ： ', colorSizeObjArr,  parentBarcode, colorArr, colorArrs, colorSizeObjArr, this.state, )
    const newColorData = colorArrs.map((v, i) => {
      // console.log(' colorArr v ： ', v, i, )
      Object.keys(colorSizeObjArr).forEach((item, ) => {
        // console.log(' colorArr itemiitem  ,： ', parentBarcode, item !== parentBarcode[item],   )
        if (item !== parentBarcode) {
          colorSizeObjArr[item].forEach((items) => {// 
            // console.log(' items v ： ', item, colorSizeObjArr, items, v.combo_seq === items.combo_seq, v.size_id === items.size_id, items.status === 'T')
            if (v.combo_seq === items.combo_seq && v.size_id === items.size_id && items.status === 'T') {
              v.qty -= items.qty
              v.status = v.qty !== 0 ? 'T' : 'F'
              // console.log(' v333 ： ', items, v, v.qty, items.qty,  )// 
            }
          })
        }
      })
      // console.log(' v222 ： ', v  )// 
      return v
    })
    // console.log(' newColorDatatem ： ', newColorData,  )// 
    return newColorData 
  }  

  // 点击一条barcode开始拆分 - 增加一条子条码
  splitAction = (data, qty) => {
    const {splitData, barcodeData, splitLen, colorSizeData, colorSizeDataObj, colorSizeOrigin, colorArr, colorSizeObjArr, calcColorSizeTotal,   } = this.state
    // data.barcode = data.barcode.trim() + LETTER[splitData.length]
    const haveQty = colorArr.every(v => v.qty == 0)
    console.log(' haveQty ： ', haveQty, colorArr )// 
    if (haveQty) {
      confirms(2, '該條碼色碼數爲0，請到主界面對該批號重排')
      return  
    }
    const parentBarcode = data.barcode.trim()
    const newBarcode = parentBarcode + LETTER[splitLen]
    data.barcode = newBarcode
    const disableDay = barcodeData[0].e_date
    console.log(' splitAction 点击一条barcode开始拆分 data, qty ： ', colorSizeDataObj, parentBarcode, newBarcode, splitData, data, qty, disableDay,  )

    const newSplitData = backupFn(data)
    // console.log(' newSplitData ： ', newSplitData,  )
    
    newSplitData.qty = 0 
    newSplitData.isSplitData = true
    // colorSizeData.forEach((v, i) => {
    //   v.useQty = 0 
    //   v.data.forEach((item) => {
    //     v.qtys += item.qty
    //     v.useQty += item.status === 'T' ? item.qty :  0 
    //   })
    //   // console.log(' v.v.useQty ： ', v.useQty,  )
    //   newSplitData.qty += v.useQty
    // })
    // const remainColorData = this.calcColor(parentBarcode, data)
    const remainColorDatas = this.calcColorSize(parentBarcode, colorSizeObjArr)
    const residue = remainColorDatas.filter(v => v.qty > 0)
    console.log(' data.barcode.trim(), len, remainColorData ： ', residue, remainColorDatas, colorSizeData, newSplitData, colorSizeDataObj, newBarcode, LETTER[splitLen], splitLen, disableDay, this.state, data, )
    if (!residue.length) {
      confirms(2, '色碼數據拆分剛好，不能再拆分喽，請檢查  o(*￣︶￣*)o！')
      return 
    }

    const res = remainColorDatas.reduce((total, cuv, cui, arr ) => {
      // console.log(' remainColorDatas reduce total, cuv, cui, arr ： ', total, cuv, cui, arr )
      total += cuv.status === 'T' ? cuv.qty :  0 
      return total 
    }, 0)
    newSplitData.qty = res
    console.log(' res ： ', res, newSplitData )//

    // // 再次计算 已用生产数
    const residueColorData = []
    calcColorSizeTotal.forEach((v, i) => {
      // const sch_qty = v.qtys
      v.sch_qty = v.qtys
      residueColorData.forEach(item => {
        // console.log(' itemitemitemitem ： ', item,  )// 
        if (item.combo_seq === v.combo_seq) {
          // console.log(' 相等相等 ： ', v.sch_qty, item, v,   )
          v.sch_qty -= item.qty
        }
      })
    })
 
    
    this.setState({
      splitData: [...splitData, newSplitData],
      disableDay, 
      splitLen: splitLen + 1,
      isSplit: true,
      // colorSizeDataObj: {...colorSizeDataObj, [newBarcode]: backupFn(colorSizeData)},
      colorSizeDataObj: {...colorSizeDataObj, [newBarcode]: backupFn(remainColorDatas)},
      // remainColorData,

      residueColorData: [],
      colorSizeObjArr: {...colorSizeObjArr, [newBarcode]: remainColorDatas, },
      calcColorSizeTotal,
    })
  }
  deleteBar = (t, v, i) => {
    const {splitData, colorSizeDataObj, colorArr, colorSizeObjArr,   } = this.state
    console.log(' deleteBarsssss ： ',  t, v, i, splitData, barcode, this.state )
    const {barcode, } = v
    delete colorSizeDataObj[barcode]
    delete colorSizeObjArr[barcode]
    const remainColorDatas = this.calcColorSize(this.state.barcode, colorSizeObjArr)
    console.log(' colorSizeDataObj ： ', colorSizeDataObj )
    this.setState({
      splitData: splitData.filter(v => v.barcode !== barcode),
      colorSizeDataObj,
      colorSizeObjArr,
      residueColorData: remainColorDatas.filter(v => v.qty > 0),
      // residueColorData: {[this.state.barcode]: [...colorArr]},: []
    })
  }
  handleOk = (isLock, ) => {
    const {showEdit, cantChange, modalType, isSplit, isEditting, } = this.state
    console.log('handleOk ：', this.state, modalType, showEdit ? '编辑条码' : '拆分', isLock, )
    if (showEdit) {
      console.log(' showEditshowEdit ： ', )
      this.editBarcode()
    } else if (modalType === 'getAccPnInfo') {
      console.log(' closeclose ： ', )
      this.close()
    } else {
      console.log(' isSplitisSplitisSplit ： ', )
      isSplit ? this.splitPnBarcode(isLock) : isEditting ? this.editPnBarcode(isLock) : this.close() 
    }
  }
  mergeShort = (data) => {
    const effectObj = {}
    const effectData = data.reduce((t, c) => {
      t[c.pn_no.trim()] = [...t[c.pn_no.trim()] != undefined  ? t[c.pn_no.trim()] : [], c]
      return t
    }, effectObj)
    return effectObj
  }

  mergeShorts = (data) => {
    const effectObj = {}
    const effectData = data.reduce((t, c) => {
      const pn_no = c.pn_no.trim()
      const barcode = c.barcode.trim()
      // console.log(' c.barcode.trim() ： ', t,  c, pn_no, barcode )
      t[pn_no] = t[pn_no] == undefined ? {} : {
        // [barcode]: [...t[pn_no][barcode] != undefined ? [...t[pn_no][barcode], c] : [], c]
        [barcode]: [...t[pn_no][barcode] != undefined ? () => {
          console.log(' [...t[pn_no][barcode], c] ： ', [...t[pn_no][barcode], c],  )
          return [...t[pn_no][barcode], c]
        } : [], c]
      }
      return t
    }, effectObj)
    return effectObj
  }
  editDay = (p) => {
    let {v, keys, type, factory_name, } = p
    const {barcodeData, } = this.state// 
    // const barcodeDatas = [{...barcodeData[0], [`new_${keys}`]: dateFormat(v),}, ]
    // console.log(' editDayeditDay props ： ', v, keys, dateFormat(v), p,  )
    let barcodeDatas
    console.log(' editDayeditDay props ： ', v, keys, p,  )
    if (type === 'productLine') {
      barcodeDatas = [{...barcodeData[0], [`new_${keys}`]: v, [`new_factory_name`]: factory_name,}, ]
      console.log(' 修改生产线ss ： ', v, keys, p, barcodeDatas,   )
      this.setState({
        barcodeData: barcodeDatas,// 
      })
      this.setState({
        [`new_${keys}`]: v,// 
        new_factory_name: factory_name,
        barcodeData: barcodeDatas,// 
      })
    } 
    else {
      barcodeDatas = [{...barcodeData[0], [`new_${keys}`]: v,}, ]
      this.setState({
        [`new_${keys}`]: v,// 
        barcodeData: barcodeDatas,// 
      })
    }
  }
  subDay = (day) => {
    return moment(day).subtract(1, 'day').format(DATE_FORMAT_BAR) 
  }
  // 停止拖拽设置当前操作的bar
  stopDrag = ({x, y, w, barcode, data, pn_no, dragAction, barcodeData, indexes, }) => {
    const { dateArr, data_total } = this.state
    const s_date_new = dateArr[x].dateOrigin
    const e_date_new = dateArr[w].dateOrigin
    const dragPn = data_total.find(v => v.pn_no === pn_no)
    const {factoryArr} = dragPn
    const matchIndex = factoryArr.indexOf(data.factory_id) + 1
    const matchItem = dragPn.data.find(v => v.factory_id === factoryArr[matchIndex])
    const newBarcode = {
      ...barcodeData, 
      new_s_date: s_date_new, 
      new_e_date: e_date_new,
    }

    console.log(' 起止日期 ： ', s_date_new, e_date_new, x, y, moment(e_date_new).isAfter(s_date_new),  )// 
    if (!moment(e_date_new).isAfter(s_date_new)) {
      confirms(0, '起止日期不正確，請檢查 ！！',  )
      return  
    } 
     
    const {work_no,  } = data// 
    const {middle,  } = this.props.route
    const {userInfo,  } = this.props// 
    const haveAuths = haveAuth(work_no, userInfo, )// 
    console.log(' haveAuths ： ', middle, userInfo, haveAuths, this.state,  )//
    if (!haveAuths) {
      confirms(2, '對不起，你沒有操作該工序條碼的權限')
      return  
    }

    console.log(' 停止拖拽 stopDrag ： ', dateArr, barcodeData, dateArr[x], x, y, w, barcode, data, pn_no, dragAction, indexes, newBarcode, )// 
    this.setState({
      show: true, 
      showEdit: true, 
      barcodeDate: {
        s_date_new, 
        e_date_new
      },
      barcodeData: [{
        ...this.state.barcodeData[0], 
        new_s_date: s_date_new, 
        new_e_date: e_date_new,
      }],
      editBarcodeData: data,
      editPnNo: pn_no,
      barcode,
    }, () => {
      console.log(' 完成 ： ',    )// 
      const validTxt = this.dateRange(s_date_new, e_date_new, newBarcode, indexes, )// 
      // console.log(' validTxt ： ', validTxt,  )// 
      if (validTxt !== '' ) {
        this.setState({
          cantChange: true, 
          editContent: validTxt, 
        })
        return  
      }
    })// 
    
    return  
    // console.log('停止拖拽设置当前操作的bar ：', s_date_new, e_date_new, dragPn, matchItem, x, y, w, barcode, data, pn_no, dragAction, this.state)
    // if (dragAction !== 'dragging') {
    //   // 点击往左拖长
    //   if (dragAction === 'dragLeft') {
    //     // 当不是最后一条bar的时候才去统计是否正确
    //     console.log('不是最后一条 ：', matchIndex, factoryArr.length, matchIndex !== factoryArr.length )
    //     if (matchIndex !== factoryArr.length) {
    //       const {s_date} = matchItem
    //       const diffDay = moment(s_date.split('-')).diff(moment(s_date_new.split('-')), 'days')
    //       console.log('dragLeft ：', diffDay, data.levels, s_date, s_date_new, this.state)
    //       if (diffDay < 1) {
    //         console.log('注意：改變後的條碼的生産開始日期必須在下一條之前！ ：', diffDay < 1)
    //         this.setState({
    //           cantChange: true, 
    //           editContent: '注意：改變後的條碼的生産開始日期必須在下一條之前！', 
    //         })
    //         // openNotification('注意：改變後的條碼的生産開始日期必須在下一條之前！')
    //       } 
    //     }
    //   } else if (dragAction === 'dragRight') {
    //     // 点击往右拖长  直接拖右
    //     // const diffDay = moment(data.e_date.split('-')).diff(moment(e_date_new.split('-')), 'days')
    //     const diffDay =  moment(endDay).add(1, 'day').isAfter(e_date_new)
    //     console.log('dragRight右边 ：', data.e_date, moment(endDay).add(1, 'day').format(DATE_FORMAT_BAR), e_date_new, diffDay, this.state)
    //     // if (diffDay < 0) {
    //     if (!diffDay) {
    //       console.log('注意：條碼的清期不能改變！：', diffDay)
    //       this.setState({
    //         cantChange: true, 
    //         editContent: '注意：條碼的清期不能改變！', 
    //       })
    //       // openNotification('注意：條碼的清期不能改變！')
    //     }
    //   }
    // } else {
    //   const {s_date} = matchItem
    //   const diffDay = moment(s_date.split('-')).diff(moment(s_date_new.split('-')), 'days')
    //   console.log('dragLeft ：', diffDay, data.levels, s_date, s_date_new, this.state)
    //   if (diffDay < 1) {
    //     console.log('注意：新条码的生产开始日期必须在下一条之前！ ：', diffDay < 1)
    //     this.setState({
    //       cantChange: true, 
    //       editContent: '注意：新条码的生产开始日期必须在下一条之前！', 
    //     })
    //   } 
    // }
  }
  mxDate = (data, k, x, ) => {
    console.log(' mxDate  ： ', data, k, x,  )
    return moment[x](data.map((v) => moment(v[k]))).format(DATE_FORMAT_BAR)
  }
  workFilter = (data, k, item, ) => {
    console.log(' workFilter  ： ', data, k, item,  )
    return data.filter(v => v[k] === item)
  }
  mxDateFilter = (data, k, x,  ) => {
    // console.log(' mxDateFilter  ： ', data, k, x,   )// 
    return moment[x](data.map(v => moment(v[k]))).format(DATE_FORMAT_BAR)
  }
  dateRange = (new_s_dates, new_e_dates, barcodeData, pnItem, isDelete, ) => {
    // return ''
    const { data_total, splitData,editPnNo,   } = this.state
    console.log(' dateRange ： ',  this.state, isDelete, pnItem, data_total, data_total[pnItem], new_s_dates, new_e_dates, '**', barcodeData, this.state.barcodeData, )
    const {barcode, shipment_date, s_date, e_date,  } = barcodeData
    const {workArr, } = data_total[pnItem]
    const pnData = backupFn(data_total[pnItem].data)
    // const pnData = data_total[pnItem].data.slice(0, 1)
    // const pnData = data_total[pnItem].data.slice(0, 2)
    const editIndex = pnData.findIndex((v, i) => v.barcode.trim() === barcode.trim())
    const {work_no, } = pnData[editIndex]
    pnData[editIndex].s_date  = this.state.barcodeData[0].new_s_date 
    pnData[editIndex].e_date = this.state.barcodeData[0].new_e_date
    const sameWorkData = pnData.filter((v, i) => v.work_no === work_no)
    console.log(' pnData ： ', pnData, this.state.barcodeData, sameWorkData, barcode)// 
    const workIndex = workArr.findIndex((v, i) => v === work_no)
    // const sameWorkDate = splitData.length ? [...splitData, ...sameWorkData.filter((v, i) => v.barcode.trim() !== barcode.trim()), ] : sameWorkData
    const sameWorkDate = isDelete ? sameWorkData.filter((v, i) => v.barcode.trim() !== barcode.trim()) : splitData.length ? [...splitData, ...sameWorkData.filter((v, i) => v.barcode.trim() !== barcode.trim()), ] : sameWorkData
    console.log(' editIndex,  ： ', editIndex, sameWorkData, sameWorkDate,   )// 
    const new_s_date = this.mxDateFilter(sameWorkDate, 's_date', 'min', )
    const new_e_date = this.mxDateFilter(sameWorkDate, 'e_date', 'max', )
    console.log(' 條碼的 new_s_date new_e_date ： ', new_s_date, new_e_date, workArr, pnData, )// 
    const workLastIndex = workArr.length - 1
    const preIndex = editIndex - 1
    const nextIndex = editIndex + 1
    const preWIndex = workIndex - 1
    const nextWIndex = workIndex + 1
    const workPIndex = workArr[preWIndex]
    const workNIndex = workArr[nextWIndex]
    // const preBarArr= pnData.filter(v => v.work_no === workArr[preWIndex])
    // const nextBarArr= pnData.filter(v => v.work_no === workArr[nextWIndex])
    const preBarArr = this.workFilter(pnData, 'work_no', workPIndex,  )
    const nextBarArr = this.workFilter(pnData, 'work_no', workNIndex,  )
    const preBarSdate = this.mxDateFilter(preBarArr, 's_date', 'min', )
    const preBarEdate = this.mxDateFilter(preBarArr, 'e_date', 'max', )
    const nextBarSdate = this.mxDateFilter(nextBarArr, 's_date', 'min', )
    const nextBarEdate = this.mxDateFilter(nextBarArr, 'e_date', 'max', )
    const preBar = pnData[preIndex]
    const nextBar = pnData[nextIndex]
    const realEnd = moment(shipment_date).isAfter(e_date)
    const end = realEnd ? shipment_date : e_date
    // console.log(' pnData editIndex ： ', workArr, work_no, workIndex, workLastIndex, workArr[workLastIndex], work_no === workArr[0], work_no === workArr[workLastIndex],  )
    // console.log(' preWIndex, nextWIndex, preBarArr, nextBarArr,  ： ', preWIndex, nextWIndex, preBarArr, nextBarArr,   )// 
    // console.log(' preBarSdate, preBarEdate, nextBarSdate, nextBarEdate,  ： ', preBarSdate, preBarEdate, nextBarSdate, nextBarEdate,   )// 
    console.log(' preBar, nextBar, pnData, editIndex, shipment_date, e_date, realEnd, end ： ', preBar, nextBar, pnData, editIndex, shipment_date, e_date, realEnd, end, preIndex, nextIndex,  )// 
    let txt 
    // 只有1条的情况
    if (pnData.length === 1) {
    // if (workArr.length === 1) {
      const isUp =moment(end).isAfter(new_e_date) || new_e_date === end
      txt = !isUp && '注意：條碼的结束日期不能小于货期！'
      console.log(' 只有1条的情况 ：txt ',  txt, shipment_date, e_date, new_e_date, end, moment(end).isAfter(new_e_date), isUp)// 
    }
    if (workArr.length === 1) {
      console.log(' workArrworkArr ： ',    )// 
      return ''
    }
    
    // 多条 
    // if (pnData.length > 1) {
    if (workArr.length > 1) {
      // // 2条 第一条 
      // if (editIndex === 0) {
      if (work_no === workArr[0]) {
        console.log(' 2条 第一条 ： ', new_s_date, nextBarSdate, new_e_date, nextBarEdate )// 
        const isNextStartAfterStart = moment(nextBarSdate).isAfter(new_s_date) || nextBarSdate === new_s_date
        const isNextEndAfterEnd = moment(nextBarEdate).isAfter(new_e_date) || nextBarEdate === new_e_date
        txt = !(isNextStartAfterStart && isNextEndAfterEnd) && `注意：改變條碼的起止日期不正確，沒有在下一個工序的日期之前！ \n 改變條碼的起止日期 【${new_s_date} ： ${new_e_date}】 相鄰工序起止日期 【${nextBarSdate} ： ${nextBarEdate}】`
        console.log(' 2条 第一条 txt： ',  txt, isNextStartAfterStart, isNextEndAfterEnd, (isNextStartAfterStart && isNextEndAfterEnd) )// 
      }
      // // 2条 最后一条 
      // else if (editIndex === (pnData.length  ：  1)) {
      else if (work_no === workArr[workLastIndex]) {
        console.log(' 2条 最后一条 ： ', new_s_date, preBarSdate, new_e_date, end, moment(end).isAfter(new_e_date), new_e_date === end, preBarEdate )// 
        const isUp = moment(end).isAfter(new_e_date) || new_e_date === end
        const isStartAfterPreStart = moment(new_s_date).isAfter(preBarSdate) || preBarSdate === new_s_date
        const isEndAfterPreEnd = moment(new_e_date).isAfter(preBarEdate) || preBarEdate === new_e_date
        txt = !(isUp && isStartAfterPreStart && isEndAfterPreEnd) && `注意：改變條碼的起止日期不正確，沒有在上一個工序的日期之前！ \n 改變條碼的起止日期 【${new_s_date} ： ${new_e_date}】 相鄰工序起止日期 【${preBarSdate} ： ${preBarEdate}】`
        console.log(' 2条 最后一条 txt： ',   txt, 'txttxt', isUp, isStartAfterPreStart, isEndAfterPreEnd, (isUp && isStartAfterPreStart && isEndAfterPreEnd) )// 
      }
      // 中间
      // else if (pnData.length > 2) {
      else if (workArr.length > 2) {
        console.log(' 中间中间 ： ', preBarEdate, preBarSdate, new_e_date, new_s_date, nextBarSdate, nextBarEdate )// 
        const isStartAfterPreStart = moment(new_s_date).isAfter(preBarSdate) || preBarSdate === new_s_date
        const isNextStartAfterStart = moment(nextBarSdate).isAfter(new_s_date) || nextBarSdate === new_s_date
        const isEndAfterPreEnd = moment(new_e_date).isAfter(preBarEdate) || preBarEdate === new_e_date
        const isNextEndAfterEnd = moment(nextBarEdate).isAfter(new_e_date) || nextBarEdate === new_e_date
        txt = !((isStartAfterPreStart && isNextStartAfterStart) && (isEndAfterPreEnd && isNextEndAfterEnd)) && `注意：改變條碼的日期範圍不正確，沒有夾在相鄰工序的起止日期之間！ 改變條碼的起止日期 【${new_s_date} ： ${new_e_date}】 \n 拆分的條碼的範圍應爲： 開始【${preBarSdate} ： ${nextBarSdate}】 結束【${preBarEdate} ： ${nextBarEdate}】`
        console.log(' 中间中间： ', isStartAfterPreStart,  isNextStartAfterStart, isEndAfterPreEnd,  isNextEndAfterEnd, txt, ((isStartAfterPreStart && isNextStartAfterStart) && (isEndAfterPreEnd && isNextEndAfterEnd)) )// 
      }
    }// 
    console.log(' txttxttxttxttxt ： ', txt, txt !== '' && txt !== false, (txt !== '' && txt !== false) && ' falsefalse  ' );// 
    // (txt !== '' && txt !== false) && confirms(2, txt, )
    return txt !== '' && txt !== false ? txt : ''
  }

  isQtyEmpty = (params,   ) => {
    console.log('    isQtyEmpty ： ', params,    )
    const isInvalid = params.data.some(v => v.qty < 1);
    if (isInvalid) confirms(2, '拆分的條碼數量不能爲0',   )
    return isInvalid 
  }


  splitBarcode = (p) => {// 
    console.log(' splitBarcode ： ',  p, this.state,  )

    const isQtyEmpty = this.isQtyEmpty(p)
    console.log('  对吗  isQtyEmpty ', isQtyEmpty,    )
    if (isQtyEmpty) return
    console.log('  对吗  isQtyEmpty2222 ', isQtyEmpty,    )
    
    // return  
    
    this.setState({
      loading: true,
    })
    splitPnBarcode(p).then(res => {
      console.log(' splitPnBarcode  res.data ： ', res.data,  )// 
      const {code, datas, mes, newBarcode} = res.data
      // openNotification(mes)
      const {data_total, pnItem, colorSizeDataObj, } = this.state// 
      const tableStartDay = data_total[0].s_date
      const startDay = moment(tableStartDay, DATE_FORMAT_BAR)
      if (code === 1) {
        // 插入拆分的数据
        newBarcode.forEach((v, i) => {
          // console.log('插入拆分的数据 barcode v ：', v, i, )
          v.barcode = v.barcode.trim()
          v.shortDay = []
          // v.lock = 'F'
          this.calcInfo(v, startDay)
          this.isOutDay(v)
        })
        data_total[pnItem].data = data_total[pnItem].data.map(item => {
          console.log(' item ： ', item, item.barcode.trim(), p.p_barcode.trim(), item.barcode.trim() === p.p_barcode.trim() )// 
          return item.barcode.trim() === p.p_barcode.trim() ? newBarcode[0] : item 
        })
        console.log('newBarcode ：', newBarcode, data_total[pnItem].data)
        if (datas.length) {
          this.mergeData(data_total, datas)
        }
        this.setState({
          data_total: [...data_total],
          ...this.resetColorInfo(),
          loading: false,
        })
      }
    
    })
  } 
  editPnBarcode = (isLock, ) => {
    const { barcodeData, colorArr, pnItem, data_total, new_s_date, new_e_date, new_factory_name, new_factory_id, colorSizeDataObj, 
      // new_s_date1,
      // new_s_date2,
      // new_e_date1,
      // new_e_date2,
      colorSizeObjArr,
      editPnNo,
    } = this.state
    const {barcode, s_date, e_date,  } = barcodeData[0]
    console.log(' editPnBarcode ： ', isLock, new_s_date, new_e_date, pnItem, barcode, `${barcode}A`, this.state,   )
    // const new_s_dates = dateFormat(new_s_date)
    // const new_e_dates = dateFormat(new_e_date)

    // const new_s_dates1 = dateFormat(new_s_date1)
    // const new_e_dates2 = dateFormat(new_e_date2)
    // const new_s_dates1 = dateFormat(new_s_date1)
    // const new_e_dates2 = dateFormat(new_e_date2)
    // const new_s_dates = dateFormat(new_s_date1 + new_s_date2.replace('-', ''))
    // const new_e_dates = dateFormat(new_e_date1 + new_e_date2.replace('-', ''))
    // console.log(' new_s_date1 + new_e_date1 ： ', new_s_date1, new_e_date1, new_s_date2, new_e_date2, new_s_dates, new_e_dates )// 
    // return  
    // const isNoRepeat = pnDateArr.some((v, i) => {
    //   // console.log(' pnDateArr pnDateArrpnDateArr ： ', new_s_dates,  new_e_dates,  data_total[pnItem], this.state[`new_${v}`], barcodeData[0][v], dateFormat(this.state[`new_${v}`]) !== barcodeData[0][v], )
    //   return dateFormat(this.state[`new_${v}`]) !== barcodeData[0][v] 
    // })
    // 是否是修改过起止日期
    // if (!isNoRepeat) {
    //   confirms(2, '沒有修改起止日期，請檢查 o(*￣︶￣*)o !', )
    //   return  // 
    // // }
    // const isRight = pnDateArr.every((v, i) => {
    //   // this.dateValidate(v)
    //   // const isValid = moment(this.state[v]).isValid()
    //   const isValid = moment(this.state[v]).isValid()
    //   const isNoEmpty = this.state[v] !== ''
    //   // console.log(' pnDateArr v ： ', v, i, isValid, isNoEmpty, )
    //   return isValid && isNoEmpty
    // })
    // console.log(' isRight ： ', moment("not a real date").isValid(), isRight,    )//
    // }
    const isRight = [new_s_date, new_e_date, ].every((v, i) => {
      const isValid = moment(v).isValid()
      const isNoEmpty = v !== ''
      // console.log(' pnDateArr v ： ', v, i, isValid, isNoEmpty, )
      return isValid && isNoEmpty
    })
    console.log(' isRight ： ', moment("not a real date").isValid(), isRight,    )//
    // 修改的起止日期是否正确
    if (!moment(new_e_date).isAfter(new_s_date) && new_e_date !== new_s_date) {
      confirms(2, '修改的起止日期不正確，請檢查 !', )
      return  
    }
    const validTxt = this.dateRange(new_s_date, new_e_date, barcodeData[0], pnItem, )// 
    console.log(' new_s_date, new_e_date, ： ', new_s_date, new_e_date, validTxt )// 
    if (validTxt !== '' ) {
      confirms(2, validTxt, )
      return  
    }
    // this.dateRange('2019-02-04', '2019-02-16', )//
    // this.dateRange('2019-02-03', '2019-02-16', )//
    // this.dateRange('2019-02-03', '2019-02-22', )//
    // this.dateRange('2019-02-03', '2019-02-23', )
    // return  
    const barcodeItem = backupFn(barcodeData[0])
    barcodeItem.barcode = `${barcode}A`
    barcodeItem.color = colorArr
    barcodeItem.s_date = new_s_date
    barcodeItem.e_date = new_e_date
    barcodeItem.factory_name = new_factory_name
    barcodeItem.factory_id = new_factory_id
    barcodeItem.user_id = this.props.userInfo.user_id
    barcodeItem.reason_id = 1
    barcodeItem.reason_remark = ''
    // barcodeItem.color = []
    // colorSizeDataObj[barcode].forEach(item => barcodeItem.color.push(...item.data.filter(v => v.status === 'T')))
    barcodeItem.color = colorSizeObjArr[barcode]
    // splitData.forEach((v, i) => colorSizeDataObj[v.barcode].forEach(item => v.color = [...v.color, ...item.data.filter(v => v.status === 'T')]))
    console.log(' 正確正確 ： ', colorSizeDataObj[barcode], barcodeItem, barcodeItem.color, barcodeData[0], { p_barcode: editPnNo, data: barcodeItem, } )// 
    const params = { pn_no: editPnNo, p_barcode: barcode, data: [barcodeItem], lock: isLock ? 'T' : 'F',  }
    console.log(' { pn_no: barcode, data: barcodeData,  } ： ', params, editPnNo, { pn_no: editPnNo, p_barcode: barcode, data: barcodeItem,  },  )// 
    // return  
    // this.splitBarcode({ pn_no: editPnNo, p_barcode: barcode, data: barcodeItem,  })
    this.showConfirm({f: 'splitBarcode', p: params, item: barcode, action: 'edit',})
  }
  // autoAdd = (v) => {
  //   console.log(' autoAdd ： ',  v )
  //   const {barcodeData, } = this.props// 
  //   const newSplitData = barcodeData[0]
  //   newSplitData.qty = 0 
  //   newSplitData.isSplitData = true
  //   colorSizeData.forEach((v, i) => {
  //     v.useQty = 0 
  //     v.data.forEach((item) => {
  //       v.qtys += item.qty
  //       v.useQty += item.status === 'T' ? item.qty :  0 
  //     })
  //     newSplitData.qty += v.useQty
  //   })
  //   console.log(' newSplitData ： ', newSplitData,  )// 
  //   return newSplitData 
  // }
  showConfirm = ({f, p, item = '', action = 'delete', }) => {
    console.log('showConfirm ：', f, p, item, action, );
    confirm({
      title: `提示框`,
      content: `你確信要 ${splitMap[action]} ${item} ？`,
      width: 480,
      onOk: () => this[f](p, ),
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  confirmSplit = ({params, startDay, isRepeat, }) => {
    console.log(' confirmSplit ： ', params, startDay, isRepeat, )
    const {data_total, barcodeData, editPnNo, } = this.state// 
    const {barcode,  } = barcodeData[0]

    const isQtyEmpty = this.isQtyEmpty(params)
    console.log('  对吗  isQtyEmpty ', isQtyEmpty,    )
    if (isQtyEmpty) return
    console.log('  对吗  isQtyEmpty2222 ', isQtyEmpty,    )

    // return  

    splitPnBarcode(params).then(res => {
      console.log('splitPnBarcode res ：', res.data);
      const {code, datas, mes, newBarcode} = res.data
      if (code === 1) {
        // 插入拆分的数据
        newBarcode.forEach((v, i) => {
          // console.log('插入拆分的数据 barcode v ：', v, i, )
          v.barcode = v.barcode.trim()
          v.shortDay = []
          // v.lock = 'F'
          this.isOutDay(v)
          this.calcInfo(v, startDay)
        })
        console.log('newBarcode ：', newBarcode)
        data_total.forEach(v => {
          if (v.pn_no === editPnNo) {
            // 改变父批号的工序数组
            const shortArr = []
            Array.from(isRepeat).forEach((item, i) => {
              const indexs = v.factoryArr.findIndex(items => items === item)
              indexs < 0 && shortArr.push(item)
            })
            v.factoryArr = [...v.factoryArr, ...shortArr].sort((a, b) => a - b)
            // console.log(' mathcs1111 ： ', v.data )
            const mathcs = v.data.findIndex(item => item.barcode.trim() === barcode.trim())
            // console.log(' mathcs2222222 ： ', mathcs, v.data )
            v.data.splice(mathcs, 1, ...newBarcode)
          }
        })
        // console.log('data_total ：', data_total)
        if (datas.length) {
          datas.forEach((v, i) => v.lock = params.isLock ? 'T' : 'F')
          this.mergeData(data_total, datas)
        }
        this.setState({
          data_total: [...data_total],
          ...this.resetColorInfo(),
        })
      }
    })
  }
  // 确认对批号的条码进行拆分
  splitPnBarcode = (isLock, ) => {
  
    const {
      barcodeData, splitData, editPnNo, data_total, datas_barcode, 
      pn_no, indexes, colorSizeDataObj, colorSizeOrigin, colorSizeQtyObj, isSplit, residueColorData,
      pnItem, colorSizeObjArr, 
    } = this.state

    console.log('splitPnBarcode barcodeData, splitData：', isLock, colorSizeQtyObj, colorSizeDataObj, barcodeData, splitData, this.state, )
    if (!splitData.length) {
      confirms(2, '您還未進行拆分，請拆分再確認保存 !', )
      return
    }
   
    const tableStartDay = data_total[0].s_date
    const {barcode, originQty, qty, factory_id, work_no,  } = barcodeData[0]

    const isDayAllAfter = splitData.every(item => moment(item.e_date).isAfter(item.s_date) || item.e_date === item.s_date)
    console.log(' isDayAllAfter ： ', isDayAllAfter,  )// 
    if (!isDayAllAfter) {
      confirms(2, '拆分的條碼中有条码拆分的起止日期不正確，請檢查 ！ ', )
      return  
    }

    // 检查拆分的每一条条码是否夹在产线里
    let isValid = ''
    splitData.forEach((item, i) => {
      const validTxt = this.dateRange(item.s_date, item.e_date, {...item, barcode, }, pnItem,  )// 
      // console.log(' s_date, e_date222222,： ', item.s_date, item.e_date, validTxt   )// 
      if (validTxt !== '' ) {
        isValid = `${validTxt} 第${i + 1}條條碼`
      }
    })
    console.log(' isValid ： ', isValid,  )// 
    if (isValid !== '' ) {
      confirms(2, isValid, )
      return  
    }
    
    // const total = splitData.reduce((t, n) => t + n.qty, 0)
    // const residueData = backupFn(colorSizeQtyObj)
    // const isResidues = Object.values(colorSizeDataObj).forEach((v, i) => {
    //   // console.log(' isResidues v, i ： ', v, i, i, )
    //   if (i !== 0) {
    //     v.forEach((item, index) => residueData[item.combo_seq] -= item.useQty)
    //   }
    // })
    // // const isResidue = Object.values(residueData).some(v => v !== 0)
    // // const isResidue = Object.values(residueData).some(v => v > 0)
    // const isResidue = Object.values(residueData).some(v => v < 0)
    // console.log(' isResidue， ： ', isResidues, residueData, isResidue,  )
    // if (isResidue) {
    //   // confirms(2, `色碼數據沒拆分完或者超過了，請檢查重新拆分 !`, )
    //   confirms(2, `色碼數據拆分超過了，請檢查重新拆分 !`, )
    //   return
    // }
    // const isEqual = Object.values(residueData).some(v => v !== 0)
    // if (isEqual) {
    //   confirms(2, `色碼數據拆分超過了，請檢查重新拆分 !`, )
    // }
    // console.log(' 判断拆分数量是否还剩total, originQtyresidue,  ：', this.state, total, originQty, qty, total < originQty, qty - total)
    // return
    // console.log('splitPnBarcode totaltotal：', barcode, barcodeData, this.state, splitData, total, originQty, qty, total < originQty, qty - total)
    

    // if (residueColorData.length) {
    //   this.splitAction(backupFn({...barcodeData[0], new_s_date: barcodeData[0].s_date, new_e_date: barcodeData[0].e_date}))
    // }
    console.log('residueColorData ：', residueColorData, )
    if (residueColorData.length) {
      confirms(2, `色碼數據未拆分完或拆分超過，請檢查或者點擊繼續拆分 !`, )
      return 
    }
    
    const data = []
    const isRepeat = new Set()
    const wordNum = []
    const startDay = moment(tableStartDay, DATE_FORMAT_BAR)
    // 父批号的工序
    console.log(' editPnNo ： ', data_total, editPnNo,  )// 
    const dragPn = data_total.find(v => v.pn_no.trim() === editPnNo.trim())
    // const dragPnFactory = [...dragPn.data.filter(v => v.work_no == work_no), ...splitData, ]
    // const newDragPnFactory = filterDatas(dragPnFactory, 'factory_id', )
    // console.log(' 要拆分的批号 dragPn ： ', newDragPnFactory, dragPnFactory, splitData, dragPn, dragPn.data.filter(v => v.work_no == work_no), filterDatas(dragPnFactory, 'factory_id', ) )// 
    // if (dragPnFactory.length - 1 > newDragPnFactory.length ) {
    //   console.log(' 小于小于小于 ： ',    )// 
    //   confirms(2, `拆分的條碼生產線不能有重複的，或與原來的生產線相同 !`, )
    //   return 
    // }

    console.log('dragPndragPndragPn ：', dragPn, factoryArr)
    const {factoryArr} = dragPn
    console.log('dragPn ：', dragPn, factoryArr)
    const isDayRightArr = []
    // const {sc_gauge, } = splitData[0]
    const parentCode = barcode.substr(0, 9)
    const parentBars = dragPn.data.find(item => {
      // console.log(' item.barcode === parentCode ： ', item.barcode, parentCode, item.barcode.indexOf(parentCode), item.barcode.indexOf(parentCode) > -1 )
      return item.barcode.indexOf(parentCode) > -1
    })
    // 重新计算要插入的条码生成日期 日产等是否正确
    splitData.map((v, i) => {
      // // console.log(' splitData  v ：', barcode, v, barcode.substr(0, 9), dragPn.data )
      // const matchIndex = factoryArr.indexOf(v.factory_id) + 1
      // // console.log(' matchIndex ：', matchIndex, factoryArr, v.factory_id  )
      // // 当不是最后一条bar的时候才去统计是否正确
      // if (matchIndex > 0 && matchIndex !== factoryArr.length) {
      //   const matchItem = dragPn.data.find(item => item.factory_id === factoryArr[matchIndex])
      //   // console.log(' matchItem ：', matchItem, dragPn.data.find(item => item.factory_id === factoryArr[matchIndex]) ) 
      //   // 拆分的bar的开始和结束日期都要大于下一条的日期
      //   const splitStart = moment(v.s_date).diff(moment(matchItem.s_date), 'days')
      //   const splitEnd = moment(v.e_date).diff(moment(matchItem.e_date), 'days')
      //   const isBeforeFactory = splitStart <= 0 && splitEnd <= 0
      //   isDayRightArr.push(isBeforeFactory)
      //   // console.log(' 拆分bar的下一条找到了  matchIndex ：', isBeforeFactory, splitStart, splitEnd, matchIndex, matchItem, moment(v.s_date), moment(matchItem.s_date))
      // } else {
      //   console.log('没找到 下一条拆分bar：', )
      // }
      // console.log('isDayRightArr ：', isDayRightArr)
      console.log(' v.qty / v.day_qty) ： ', v.qty, v.day_qty,  v.qty / v.day_qty, moment(v.e_date).diff(moment(v.s_date), 'days'), )// 
      wordNum.push((v.qty / v.day_qty) / moment(v.e_date).diff(moment(v.s_date), 'days') < 1)
      v.user_id = this.props.userInfo.user_id
      // getItems('userInfo')
      v.factory_id = Number(v.factory_id)
      // console.log('isRepeat ：', isRepeat, v.factory_id, isRepeat.has(v.factory_id))
      isRepeat.add(v.factory_id)
      return v
    })
    if (splitData.length === 1 && (equalAttr.every(v => splitData[0][v] === barcodeData[0][v]))) {
      confirms(2, '新拆分的條碼 日期 或 生產線 必須與原條碼不一樣!', )
      return 
    }
    
    const isWorkRight = wordNum.some(v => v)
    console.log('isWorkRight ：', isWorkRight, wordNum)
    if (isWorkRight) {
      // confirms(2, '拆分日産數量不正確，請重新填寫!', )
      confirms(2, '生産周期過長，無法滿足一人的日産，請縮短周期！', )
      return 
    }
    
    // const isDayRight = isDayRightArr.every(v => v)
    // // console.log(' 拆分日期是否合法 isDayRight ：', dragPn, isDayRight)
    // if (!isDayRight) {
    //   confirms(2, '拆分工序的開始、结束日期不能在下一個工序的後面!', )
    //   return 
    // }
    
    // const spitedFactory = {}
    // isRepeat.forEach((item, index) => {
    //   // console.log(' isRepeat v ：', item, index, )
    //   splitData.forEach((v, i) => {
    //     // console.log(' splitData v ：', v, i, )
    //     if (v.factory_id === item) {
    //       if (spitedFactory[v.factory_id] == undefined) {
    //         spitedFactory[v.factory_id] = []
    //       } 
    //       spitedFactory[item].push(v)
    //     }
    //   })
    // })

    // console.log('olds ：', splitData, spitedFactory, Object.entries(spitedFactory))
    // Object.values(spitedFactory).forEach((v, i) => v.sort((a, b) => moment(a.s_date).isAfter(b.s_date)))
    // // 同一产线的bar不能重叠
    // const isOverlayArr = []
    // Object.entries(spitedFactory).forEach((v, i) => {
    //   // console.log(' spitedFactory v ：', v[1].length, v, i, v[0], parentBars.factory_id, v[0] == parentBars.factory_id)  
    //   if (v[1].length !== 1) {//
    //   // if (v[1].length !== 1 && v[0] == parentBars.factory_id) {
    //     let isOverlay = false
    //     for (let i = 0; i < v[1].length; i++) {
    //       if (i !== v[1].length - 1) {
    //         isOverlay = moment(v[1][i + 1].s_date).isAfter(v[1][i].e_date)
    //       }
    //     }
    //     // console.log('isOverlay ：', isOverlay)
    //     isOverlayArr.push(isOverlay)
    //   }
    // })
    // const illeageResult = isOverlayArr.every(v => v)
    // // console.log(' isOverlayArr ：', isOverlayArr, illeageResult)
    // if (!illeageResult) {
    //   // openNotification('拆分日期重疊了，請重新填寫日期！', )
    //   confirms(2, '拆分日期重疊了，請重新填寫日期!', )
    //   return 
    // } 


    // 只发送 选中的色码数据
    // splitData.forEach((v, i) => colorSizeDataObj[v.barcode].forEach(item => v.color = [...v.color, ...item.data.filter(v => v.status === 'T')]))
    splitData.forEach((v, i) => {
      v.color = colorSizeObjArr[v.barcode].filter(v => v.qty > 0)
      v.reason_id = 1
      v.reason_remark = ''
    })
    const params = {p_barcode: barcode, data: splitData, pn_no: editPnNo, lock: isLock ? 'T' : 'F', }
    console.log(' 生产线没有重复拆分合法可以拆分 ：', params, splitData, this.state, isRepeat, colorSizeObjArr )
    console.log(' splitPnBarcodesplitPnBarcode ： ',    )// 
    // return
    this.showConfirm({f: 'confirmSplit', p: {params, startDay, isRepeat, }, item: barcode, action: 'split',})
    return// // 
    splitPnBarcode(params).then(res => {
      // console.log('splitPnBarcode res ：', res.data);
      const {code, datas, mes, newBarcode} = res.data
      // openNotification(mes)

      if (code === 1) {
        // 插入拆分的数据
        newBarcode.forEach((v, i) => {
          // console.log('插入拆分的数据 barcode v ：', v, i, )
          v.barcode = v.barcode.trim()
          v.shortDay = []
          v.lock = 'F'
          this.isOutDay(v)
          this.calcInfo(v, startDay)
        })
        console.log('newBarcode ：', newBarcode)
        data_total.forEach(v => {
          if (v.pn_no === editPnNo) {
            // 改变父批号的工序数组
            const shortArr = []
            Array.from(isRepeat).forEach((item, i) => {
              const indexs = v.factoryArr.findIndex(items => items === item)
              indexs < 0 && shortArr.push(item)
            })
            v.factoryArr = [...v.factoryArr, ...shortArr].sort((a, b) => a - b)
            // console.log(' mathcs1111 ： ', v.data )
            const mathcs = v.data.findIndex(item => item.barcode.trim() === barcode.trim())
            // console.log(' mathcs2222222 ： ', mathcs, v.data )
            v.data.splice(mathcs, 1, ...newBarcode)
          }
        })
        // console.log('data_total ：', data_total)
        if (datas.length) {
          this.mergeData(data_total, datas)
        }
        this.setState({
          data_total: [...data_total],
          ...this.resetColorInfo(),
        })
      }
    })
    
  }

  mergeData = (target, datas) => {
    console.log(' mergeData ： ', target, datas, this.mergeShort(datas) )
    const {data_total} = this.state
    const tableStartDay = data_total[0].s_date
    const mergeShort = this.mergeShort(datas)
    target.forEach(v => {
      // console.log(' 拆分拆分v ： ', v, v.pn_no, mergeShort, mergeShort[v.pn_no.trim()])
      if (mergeShort[v.pn_no.trim()] != undefined) {
        // console.log(' undefinedundefinedundefinedundefinedv ： ', v, v.pn_no, mergeShort, mergeShort[v.pn_no.trim()])
        mergeShort[v.pn_no.trim()].forEach(item => { 
          // console.log(' item[mergeShort item ： ', item, mergeShort, item.barcode,)
          if (mergeShort[item.barcode.trim()] != item.barcode.trim()) {
            v.data.forEach((items, i) => {
              if (items.barcode.trim() === item.barcode.trim()) {
                //console.log('相等相等相等 items ： ', items, items.shortDay, item, moment(items.s_date), tableStartDay)
                items.shortDay.push(item)
                this.calcShortDay(items.shortDay, moment(items.s_date), tableStartDay)
              }
            })
          }
        })
      }
    })
    console.log(' target ： ', target,  )
    return target
  }

  // 拖拽、改变起止日期  确认发送请求
  editBarcode = () => {
    const {barcodeDate, barcode, editBarcodeData, data_total, datas_barcode} = this.state
    const tableStartDay = data_total[0].s_date// 
    console.log('editBarcode ：', this.state, barcodeDate, barcode, editBarcodeData)
    editBarcode({...barcodeDate, barcode}).then(res => {
      // console.log('editBarcode res ：', res.data, this.state);
      const {code, datas, mes} = res.data
      // openNotification(mes)
      if (code === 1) {
        data_total.map(v => {
          v.data.forEach((item, index) => {
            if (item.barcode === barcode) {
              item.s_date = barcodeDate.s_date_new
              item.e_date = barcodeDate.e_date_new
              this.calcInfo(item, data_total[0].s_date)
              console.log('等于改变起止日期 v ：', item, index, item.barcode, barcode, item.barcode === barcode)
            }
          })
          return v
        })
        console.log(' datas ： ', datas,  )
        datas.forEach(v => v.barcode = v.s_barcode)
        if (datas.length) {// 
          this.mergeData(data_total, datas)
        }
      }
      this.setState({
        data_total: [...data_total],
        show: false,
        showEdit: false,
        changeBarData: [],
        data: [],
        ...this.resetColorInfo(),
      })
    })
  }

  finishAction = (v, finish_date) => {
    const {barcodeData, barcode, } = this.state
    const {userInfo, } = this.props 
    const params = {
      finish_status: v,
      barcode,
      finish_user: userInfo.user_id,
      // finish_date: moment().format(DATE_FORMAT_BAR),
      finish_date,
    }
    console.log(' finishAction ： ', params, v, barcodeData, Date.now(), {...moment}, moment().format(DATE_FORMAT_BAR) )
    this.finishDay(params)
  }
  finishDay = (v) => {
    console.log(' finishDay ： ',  v )
    finishDay(v).then(res => {
      console.log('finishDay res ：', res.data);
      const {code, } = res.data
      if (code === 1) {
        const {data_total, editPnNo, pnItem, barcodeData, } = this.state 
        const matchPn = data_total.findIndex(v => v.pn_no === editPnNo)
        // data_total[matchPn].data.forEach((item, i) => {
        data_total[pnItem].data.forEach((item, i) => {
          if (item.barcode === v.barcode) {
            // item = {...item, ...v}
            item.finish_status = v.finish_status
            item.finish_user = v.finish_user
            item.finish_date = v.finish_date
            console.log('======  ： ', v.barcode, item )
          }
        })
        console.log(' matchPn ： ', matchPn, data_total[pnItem], data_total[matchPn] )
        this.setState({
          data_total: [...data_total],
          barcodeData: barcodeData.map((item) => ({
            ...item,
            finish_status: v.finish_status,
            finish_user: v.finish_user,
            finish_date: v.finish_date,
          })),
        })
      }
    })
  }
  
  lock = (barcode, lock) => {
    const {data_total, indexes, show, barcodeData, } = this.state
    console.log('lock ：', barcode, lock, indexes, this.state, data_total, data_total[indexes])
    barcodeLock({barcode, lock}).then(res => {
      console.log('barcodeLock res ：', res.data);
      const {code, customer, style_no, } = res.data
      if (code === 1) {
        const newData = data_total[indexes].data.map(v => ({...v, lock: v.barcode === barcode ? lock : v.lock}))
        data_total[indexes].data = [...newData]
        console.log('newData ：', newData,  data_total[indexes],   )
        this.setState({
          data_total: [...data_total],
          show: lock === 'T' ? false : show,
          barcodeData: [{...barcodeData[0], lock, }],
        })
      }
    })
  }
  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('##########shouldComponentUpdate  nextProps ：', nextProps, nextState, )
  //     return false
  // //   // const {splitData} = this.state
  // //   // console.log('@@@@@@@@shouldComponentUpdate  show ：', splitData)
  // //   if (nextState.shou) {
  // //     return false
  // //   } else {
  // //     return true
  // //   }
  // }
    
  delete = (v) => {// 
    const {barcode, data_total, barcodeData, indexes, } = this.state
    const {work_no} = barcodeData[0]
    const mergeData = data_total[indexes].data.filter(v => v.work_no === work_no)
    if (mergeData.length < 2) {
      confirms(2, '只有一條同工序生産線無法進行刪除  o(╥﹏╥)o!')
      return 
    }
    console.log('productNoWrapper delete ：', data_total[indexes].data, mergeData, work_no, v, indexes, barcode, this.state)
    this.setState({
      showDelete: true,
      d_barcode: mergeData.filter(v => v.barcode !== barcode)[0].barcode,
      mergeData: mergeData.filter(v => v.barcode !== barcode),
    })
  }
  deleteBarcode = (v) => {
    console.log(' deleteBarcode ： ',  v )
    deleteBarcode(v).then(res => {
      console.log('deleteBarcode res ：', res.data);
      const {code, data, } = res.data
      const {barcode, data_total, indexes, } = this.state 
      data_total[indexes].data = data_total[indexes].data.filter(v => v.barcode !== barcode)
      console.log(' data_total[indexes].datas ： ', data_total[indexes].data,)
      if (code === 1) {
        if (data.length) {
          this.mergeData(data_total, data)
        }
      }
      this.setState({
        data_total: [...data_total],
        showDelete: false,
        ...this.resetColorInfo(),
      })
    })
  }
  autoPpc = (pn_no) => {
    const {data_total,  } = this.state// 
    console.log('  autoPpc ：', pn_no, data_total, this.state )
    const res = data_total.find(v => v.pn_no === pn_no )
    const {data,  } = res
    const noLock = data.every(v => v.lock !== 'T' && v.finish_status !== 'T')
    if (!noLock) {
      confirms(2, '前先將該批號所有工序解鎖及解除清貨狀態，再重排')
      return 
    }
    console.log(' res ： ', res, noLock, data,  )// 
    autoPpc({pn_no, }).then(res => {
      console.log('autoPpc res ：', res.data);
      const {code,  } = res.data
      console.log('  对吗  code === 1 ', code === 1,   )
      if (code === 1) {
        const {filterInfo,  } = this.props// 
        console.log(' filterInfo ： ', filterInfo,  )// 
        this.getPnList(filterInfo)
      }
    })
  }
  // 获取过滤数据
  getFilter = () => {
    getFilter({}).then(res => {
      // console.log('getFilter res ：', res.data);
      const {customer, style_no} = res.data
      this.props.scheduleAction.customerData(customer)
      this.props.scheduleAction.styleNoData(style_no)
      this.setState({
        customerData: customer, 
        styleNoData: style_no,
      })
    })
  }
  filterOption = () => {
    //  console.log('filterOption ：', )
    this.setState({ visible: true });
  } 
  handleVisibleChange = (flag, e) => {
    // console.log('handleVisibleChange ：', flag, e)
    this.setState({ visible: flag });
  }
  zIndex = (v, i, index) => {
    // console.log(' zIndex ： ', v, i, index )
    const {data_total, barcode, } = this.state 
    const {pn_no, } = v
    // console.log(' data_total ： ', data_total[i], data_total[i].data[index] )
    data_total[i].data[index].zIndex++
    this.setState({
      data_total: [...data_total],
    })
  }
  pnScroll = (v) => {
    console.log(' pnScroll ： ',  v, this.props  )
    const {sideBarScroll, data_total, scrollInfo } = this.state 
    const isUp = v === 'up'
    const domRef = findDOMNode(ReactDOM, this.con)
    if (['down', 'up'].some(item => item === v)) {
      const scrollIndex = Math[isUp ? 'floor' : 'ceil'](sideBarScroll / grid)   
      // const matchItem = Object.keys(scrollInfo).find(v => v > scrollIndex)
      const matchIndex = Object.keys(scrollInfo).findIndex(v => v > scrollIndex)
      this.dragToggle.sideBar.scrollTop = scrollInfo[Object.keys(scrollInfo)[matchIndex - (isUp ? 2 : 0)]] 
      // console.log(' 垂直滚 ', scrollInfo[Object.keys(scrollInfo)[matchIndex - (isUp ? 2 : 0)]]  ,Object.keys(scrollInfo)[matchIndex - (isUp ? 2 : 0)], matchIndex )
    } else {
      // console.log(' 横向滚 ： ',  domRef.scrollLeft)
      domRef.scrollLeft = v === 'left' ? domRef.scrollLeft - 210 : domRef.scrollLeft + 210 
    }
  }
    
  // 固定显示的角落div
  fixBox = (isPane) => {
    const {customerData, styleNoData, visible} = this.state
    // console.log('fixBox ：',  this.state)
    if (!this.fixBoxCom) {
      console.log(' fixBoxCom  为空 ： ',  )// 
      this.fixBoxCom = <div className="headerSideBarBox">
        <div className="fixBoxItem flexEnd">
          <div className='scrollBtn'>
            {
              scrollBtnInfo.map((v) => <Tooltip key={v.dire} placement="top" title={<div className="pnInfoWrapper">向{v.txt}滾動批號</div>}>
                <Button onClick={() => this.pnScroll(v.dire)} shape="circle" type="primary" icon={`arrow-${v.dire}`} ></Button>
              </Tooltip>
              )
            }
          </div>
          <div className='pnDay'>日期</div>
        </div>
        <div className="fixBoxItem pnInfoColumn">批号信息</div>
      </div>
    }
    return this.fixBoxCom 
  }

  createDay = (isTxt, isMonth, isLasItem) => {
    // console.log('createDay ：', )
    const {monthArr, dateArr, }  = this.state
    return monthArr.map((v, i) => {
      // console.log(' monthArr v shouldComponentUpdate ：', this.state, ) // 
      return <ScheduleDay dateLen={dateArr.length} isTxt={isTxt} isMonth={isMonth} isLasItem={isLasItem} v={v} key={v.month + v.date.length}></ScheduleDay>
    })
  }
  // createDay = (isTxt, isMonth, isLasItem) => {
  //   // console.log('createDay ：', )
  //   const {monthArr, dateArr, }  = this.state
  //   return <ScheduleDayRow monthArr={monthArr} dateLen={dateArr.length} isTxt={isTxt} isMonth={isMonth} isLasItem={isLasItem} ></ScheduleDayRow>
  //   return monthArr.map((v, i) => {
  //     // console.log(' monthArr v shouldComponentUpdate ：', this.state, ) // 
  //     return <ScheduleDay dateLen={dateArr.length} isTxt={isTxt} isMonth={isMonth} isLasItem={isLasItem} v={v} key={v.month + v.date.length}></ScheduleDay>
  //   })
  // }
  createBody = (justSideBar, ) => {
    // console.log('justSideBar ：', justSideBar)
    const {data_total, dateArr, changeBarData, showEdit, isDragBack, monthArr, } = this.state
    // return data_total.map((v, i) => {
    //   return <ScheduleBody v={v} key={v.pn_no} {...{data_total, dateArr, changeBarData, showEdit, isDragBack, monthArr, getPnDetailList}}></ScheduleBody>
    // })
    const props = {
      justSideBar, data_total, dateArr, changeBarData, showEdit, isDragBack, monthArr, 
    }
    
    // return <ScheduleBody {...props} zIndex={() => this.zIndex()} getRealDetail={() => this.getRealDetail()}
    //  changeBar={() => this.changeBar()} 
    //  showBarcodeDialog={() => this.changeBar()} 
    //  stopDrag={() => this.stopDrag()} 
    //  getPnDetailList={() => this.getPnDetailList()} createDay={() => this.createDay()}></ScheduleBody>
      
    return data_total.map((v, i) => {
      // console.log(' data_total v ：', v, v.data.length) 
        return (
          <div className="contentRow" style={{height: v.data.length * grid}} key={v.pn_no}>
            <div className="sideBarBox">
              <Tooltip placement="top" title={
                <div className="pnInfoWrapper">
                  {/* <div className="pnInfo pnNos">{v.pn_no}</div>
                  <div className="pnInfo pnNos">{v.style_no}</div>
                  <div className="pnInfo">{v.cloth_style}</div> */}
                  <ClipBoard type={'div'} className='pnInfo pnNos'>{v.pn_no}</ClipBoard>
                  <ClipBoard type={'div'} className='pnInfo pnNos'>{v.style_no}</ClipBoard>
                  <ClipBoard type={'div'} className='pnInfo '>{v.cloth_style}</ClipBoard>
                </div>
              }>
                <div className="pnDay">
                  <Button onClick={() => this.autoPpc(v.pn_no)} type="primary" className='succ' >重排</Button>
                  {
                    // v.data.length ? <Button onClick={this.getPnDetailList.bind(this, v.pn_no)} type="primary">{v.pn_no}</Button> : null
                    v.data.length ? <ClipBoard onClick={() => this.getPnDetailList(v.pn_no)} type="primary">{v.pn_no}</ClipBoard> : null
                  }
                </div>
              </Tooltip>
              {v.data.length > 2 ? <div className="pnInfo pnNos">{v.style_no}</div>: null}
              {v.data.length > 1 ? <div className="pnInfo">{v.cloth_style}</div>: null}
            </div>
            {
              justSideBar ? null : (<div className="rowWrapper">
                {
                  v.data.map((item, index) => {
                    const { 
                      left, width, levels, 
                      pn_no, barcode, shortDay, 
                      work_name, factory_name, 
                      s_date, e_date, real_qty, 
                      open_date, finish_date, 
                      rate, realStart, realWidth, 
                      factory_id, zIndex, shipment_date, 
                    } = item
                    
                    const top = (levels - 1) * 30
                    const isLasItem = index === v.data.length - 1 
                    {/* const widths = dateArr.length * grid
                    const lefts = Math.abs(left) */}
                    {/* index < 50 && console.log('item ：', v, item, index, left, width, barcode, shortDay) */}
                    const realStyle = {zIndex, left: realStart, width: realWidth, position: 'absolute'}
                    {/* const rates = rate * 100 > 100 ? 100 : rate * 100 */}
                    const rates = (rate * 100).toFixed(2)// 
                    // console.log(' e_date ： ', e_date, v.indexes, dateArr, v.data, '==== ', v.data.length, v.data[v.indexes + 1], index,  )// 
                    // return <div className="pnRow" key={barcode + work_name + factory_name}>
                    return <div className="pnRow" key={barcode}>
                      {this.createDay(false, false, isLasItem)} 
                      {/* <Slider className='sliders' range 
                      tipFormatter={this.formatter}
                      //step={1} 
                      step={grid / widths * 100}
                      defaultValue={[(lefts) /( widths) * 100, (lefts + width) / (widths) * 100]}  
                      onChange={this.onChange.bind(this, left, width, )} onAfterChange={this.onAfterChange.bind(this, left, width, )} /> */}
                      
                      {/* {realStart != undefined ? <Tooltip placement="top" title={`${barcode + work_name} / ${factory_name} - 實際生産：${real_qty}
                      ${<Button type="primary" onClick={this.getRealDetail} className='detailbtn' size='small'>详情</Button>}`}>
                        <div className="real" style={realStyle}>{real_qty}</div></Tooltip> : null} */}
                        
                      {realWidth ? <Popover content={<span className="barPopover">
                        {barcode + work_name} / {factory_name} - 實際總生産數：{real_qty} - 百分比 {rates}%  <Button type="primary" onClick={() => this.getRealDetail(barcode, pn_no, factory_id)} size='small'>详情</Button></span>}
                      ><div className="real" style={realStyle} onClick={() => this.zIndex(item, i, index)}>{rates}%</div>
                      </Popover> : null}
                        
                      {/* {realStart != undefined ? <Tooltip placement="top" title={`${barcode + work_name} / ${factory_name} - 實際生産：${real_qty}`}>
                      <div className="real" style={realStyle}>{real_qty}</div></Tooltip> : null} */}
                      
                      <DragableBar 
                        isDragBack={isDragBack} 
                        showEdit={showEdit} 
                        changeBarData={changeBarData} 
                        changeBar={this.changeBar} 
                        s_date={s_date} 
                        e_date={e_date} 
                        dateLen={dateArr.length} 
                        ref={dragBar => this[barcode] = dragBar} 
                        clickType='barClick' 
                        // key={barcode + work_name + factory_name} 
                        key={barcode} 
                        pn_no={pn_no} 
                        data={item} 
                        isAxis={'x'} 
                        showBarcodeDialog={this.showBarcodeDialog} 
                        stopDrag={this.stopDrag} 
                        indexes={v.indexes}// 
                        index={index}// 
                        config={{ left, width, top, txt: `${work_name} / ${factory_name}`, levels, barcode, shortDay, }}
                        barcodeData={{barcode, shipment_date, }}
                        //config={{ left, width, top, txt: `${barcode + work_name} / ${factory_name}`, levels, barcode, shortDay, }}
                      ></DragableBar>
                    </div>
                  })
                }
              </div>)
            }
          </div>
        )
    })
  }
  editContent = () => {
    console.log('editContent  this.state ：', this.state)
    const {barcode, barcodeDate, editBarcodeData, cantChange, editContent,  } = this.state
    const {s_date_new, e_date_new} = barcodeDate
    const {startDay, e_date} = editBarcodeData
    if (cantChange) {
      return <div className={`editWrapper ${cantChange ? 'warnDialog' : ''}`}>
        <Icon type="exclamation-circle-o" />{editContent}
      </div>
    } else {
      return <div className="editWrapper">
        <div className="editCon">从 {startDay} 到 {e_date} 变为 {s_date_new} 到 {e_date_new}</div>
      </div>
    }
  }
  // headerCom = () => {
  //   const {datas_barcode, } = this.state
  //   return datas_barcode.length ? (
  //     <div className="headerContainer">
  //         {this.fixBox()}
  //         {this.createDay(true, true)}
  //     </div>) : null
  // }
  
  changeBar = (data) => {
    console.log('changeBar  data ：', data)
    this.setState({
      changeBarData: data,
      isDragBack: false,
      barcode: data.barcode,
      barcodeData: [data],
    })
  }
  filter = () => {
    this.setState({
      dateArr: [],
      monthArr: [],
      datas_barcode: [],
      datas: [],
      data_total: [],
    })
  }
  sideBarScroll = (scrollTop) => {
    const domRef = findDOMNode(ReactDOM, this.con)
    // console.log('++++++++++++++++  sideBarScroll ：', scrollTop, domRef)
    domRef.scrollTop = scrollTop// 
    this.setState({sideBarScroll: scrollTop,})
  }
  sideScroll = (scrollTop) => {
    const domRef = findDOMNode(ReactDOM, this.dragToggle.sideBar)
    // console.log('++++++++++++++++  sideScroll ：', scrollTop, domRef, this.dragToggle.sideBar )
    domRef.scrollTop = scrollTop// 
  }
  dialogContent = () => {
    // console.log('  dialogContent ：',   )
    const { 
      pn_info, 
      productData, 
      splitData, 
      dialogType, 
      isSpliting, 
      barcodeData, 
      showEdit, 
      disableDay, 
      modalType, 
      realDayQty, 

      pnDetail,
      colorSizeDataObj,
      isSplit, 
      colorSizeObjArr,
    } = this.state
    
    if (modalType === 'getRealDetail') {
      return <RealQtyTable data={realDayQty}></RealQtyTable>
    }
    return showEdit ? this.editContent() : <BarcodeContent 
      pn_info={pn_info} 
      productData={productData} 
      splitData={splitData} 
      // dialogType={dialogType} 
      isShowBarcode={dialogType === 'showBarcode'}
      delete={this.delete} 
      lock={this.lock} 
      isSpliting={isSpliting} 
      editDay={this.editDay} 
      inputing={this.inputing} 
      splitAction={this.splitAction} 
      showColor={this.showColor}
      barcodeData={barcodeData}
      disableDay={disableDay}
      finishAction={this.finishAction}
      deleteBar={this.deleteBar} 
      getAccPnInfo={this.getAccPnInfo}
      // colorSizeDataObj={colorSizeDataObj}
      colorSizeDataObj={colorSizeObjArr}
      isSplit={isSplit} 
      startEditDay={this.startEditDay}
      colorSizeHandle={this.colorSizeHandle}
    ></BarcodeContent>
  }
  onMergeChange = (d_barcode) => {
    console.log(' onMergeChange ： ',  d_barcode )
    this.setState({
      d_barcode,
    })
  }
  sureDelete = () => {
    const {barcode, d_barcode, barcodeData, new_s_date, new_e_date, pnItem,  } = this.state 
    console.log(' sureDelete ： ', barcode, d_barcode  )
    if (d_barcode == undefined) {
      confirms(2, '必選選擇一個條碼進行合並  o(*￣︶￣*)o！')
      return 
    } 
    const validTxt = this.dateRange(new_s_date, new_e_date, barcodeData[0], pnItem, true, )// 
    console.log(' new_s_date, new_e_date, ： ', new_s_date, new_e_date, validTxt )// 
    if (validTxt !== '') {
      confirms(2, '無法刪除該工序，刪除後同工序條碼的日期範圍不對，請先進行調整日期範圍，再進行刪除操作 ！！！', )
      return  
    }
    // return
    this.deleteBarcode({barcode, d_barcode,})
  }
  cancleDelete = () => {
    console.log(' cancleDelete ： ',   )
    this.setState({showDelete: false,})
  }
  filterOption = (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
  renderHeader = (v,  ) => {
    const {datas_barcode,  } = this.state// 
    console.log(' renderHeader ： ',  v, this.state, this.props, datas_barcode, this.headerCom,   )
    if (!this.headerCom) {
      console.log(' renderHeader  为空 ： ',  )// 
      const fixBox = this.fixBox(true)
      this.headerCom = datas_barcode.length ? (
        <div className="headerContainer">
            {fixBox}
            {this.createDay(true, true)}
        </div>) : null
    }
    return this.headerCom 
  }
  deleteContent = (v) => {
    const {showDelete, mergeData, modalType, pnDetail, } = this.state 
    console.log(' deleteContent ： ',  v, mergeData, modalType)
    // if (showDelete) {
    //   return 'showDelete'
    // }
    if (modalType === 'getAccPnInfo') {
      console.log(' getAccPnInfogetAccPnInfo ： ',  )
      return <ArtPnDetailTable data={pnDetail}/>
    }
    return <div>
      是否確認將生産線數量全部合並到 &nbsp;&nbsp;&nbsp;
      {
        mergeData.length ? (
          <Select 
            showSearch
            allowClear={true}
            placeholder={SELECT_TXT + '生産線'}
            optionFilterProp="children"
            defaultValue={mergeData[0].factory_name} 
            onChange={this.onMergeChange} 
            filterOption={this.filterOption}
            className="width40"
          >
            {mergeData.map((v, i) => <Option value={v.barcode.toString()} key={i}>{v.barcode} - {v.factory_name} - {v.s_date}</Option>)}
          </Select>
        ) : null
      }
      &nbsp;&nbsp;&nbsp;生産線?
    </div>
  }
  // clipCom = (v) => <ClipBoard type={'div'} >{v}</ClipBoard>
  componentDidMount() {
    // confirms(2, '注意：如果月份範圍選擇過大可能會導致頁面操作卡頓，可較大範圍查看排單，推薦小範圍進行排單操作!', )
    this.getPnList(initParams)
    this.getFilter()
    const domRef = findDOMNode(ReactDOM, this.con)
    domRef.onscroll = (e) => {
      // console.log('ScheduleNo 组件 this.state, this.props ：', this.state, this.props, domRef)
      const {scrollLeft, scrollTop} = domRef
      const {scrollLeftPos, scrollTopPos, datas_barcode, sideBarScroll, tableHeight, tableWidth} = this.state
      // domRef.scrollTop = sideBarScroll//没关滚会卡
      const time = datas_barcode.length > 500 ? null : 50
      // 滚动纵轴同步横轴滚动
      if (scrollLeftPos !== scrollLeft) {// 
        // console.log('横向滚动 ：', scrollLeft)
        ajax(this.setState.bind(this), {
          scrollLeftPos: scrollLeft,
        }, time)
        // this.setState({
        //   scrollLeftPos: scrollLeft,
        // })
      } 
      else if (scrollTopPos !== scrollTop) {
        this.sideScroll(scrollTop)
        // console.log('纵向滚动 ：', e, scrollTopPos, scrollTop, tableWidth / tableHeight, domRef)
        // ajax(this.setState.bind(this), {
        //   scrollTopPos: scrollTop,
        //   // scrollLeftPos: 1000,
        // }, time)
        // domRef.scrollTop = scrollTop + 100 
        // domRef.scrollLeft = scrollTop * tableWidth / tableHeight * 1.15
        // this.setState({
        //   scrollTopPos: scrollTop,
        // })
      }
      // console.log('纵向滚动 ：', e, scrollTopPos, scrollTop, tableWidth / tableHeight, domRef)
      // console.log('!!!@@@@@@@222chatingWrapper Pos：', e, domRef.scrollLeft, domRef.scrollTop, domRef.scrollHeight)
    }
  }
  componentWillReceiveProps(nextProps) {
    // console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ScheduleNo componentWillReceiveProps组件获取变化 ：', nextProps.filterInfo.isSearch, this.props.filterInfo.isSearch, nextProps, this.props, this.state,  )
    if (nextProps.filterInfo.isSearch) {
      this.getPnList(nextProps.filterInfo)
      this.props.scheduleAction.filterInfo({...nextProps.filterInfo, isSearch: false})
    } 
  }
  
  
  render() {
    const {collapse,} = this.props
    const { datas_barcode,show, tableHeight, tableWidth, showEdit, data_total,
      loading, barcodeData, isSpliting, dialogType, splitData, pn_no, pn_info,
      scrollLeftPos, scrollTopPos, productData, barcode, barcodeDate, cantChange, 
      showModal, editPnNo, colorBar, modalType,
      showDelete, colorSizeDataObj, residueColorData,
    } = this.state
    // const pn_nos = this.clipCom(pn_no)
    // const barcodes = this.clipCom(barcode)
    // const colorBars = this.clipCom(colorBar)
    // const editPnNos = this.clipCom(editPnNo)
    
    const title = dialogType === 'showPnCode' ? `批号 ${pn_no} 详情` : 
      showEdit ? `是否确认修改条码 ${barcode} 的起止时间` : modalType === 'getRealDetail' ? 
      `批号 ${pn_no} 實際日産詳情` : `条码 ${barcode} 詳情` 
    const modalTitle = `批号 ${colorBar} 色碼詳情`
    const deleteTitle = modalType === 'getAccPnInfo' ? `批号 ${editPnNo} 详情` : `批号 ${editPnNo} 条码 ${barcode} 刪除操作`
    // const content = showEdit ? this.editContent() : <BarcodeContent pn_info={pn_info} productData={productData} splitData={splitData} isShowBtn={dialogType === 'showBarcode'} dialogType={dialogType} delete={this.delete} lock={this.lock} isSpliting={isSpliting} inputing={this.inputing} splitAction={this.splitAction} barcodeData={barcodeData}></BarcodeContent>
    console.log('————————ScheduleNo 组件 this.state, this.props：', this.props, this.state, )
    // 
    const fixBox = this.fixBox(true)
    // const headerCom = datas_barcode.length ? (
    //   <div className="headerContainer">
    //       {fixBox}
    //       {this.createDay(true, true)}
    //   </div>) : null
      
    const headerCom = this.renderHeader()

    return (
      // <Collapses title={'工艺路线'} className='scheduleCollapse'  isAnimate={false}>
        <div className={`noWrapper ${ANIMATE.fadeIn}`} ref={con => this.con = con}>
          {
            data_total.length ? (
            <DragToggle ref={dragToggle => this.dragToggle = dragToggle} sideBarScroll={this.sideBarScroll} scrollLeftPos={scrollLeftPos} scrollTopPos={scrollTopPos} collapse={collapse} tableHeight={tableHeight} tableWidth={tableWidth}>
              {/* 固定头0 */}
              {/* <div className="fixHeaderWrapper"> */}
                {/* <div className="fixHeader"> */}
                  {headerCom}
                {/* </div> */}
              {/* </div> */}

              {/* 固定列1 */}
              <div className="fixSideBar">
                {fixBox}
                {this.createBody(true)}
              </div> 
              {/* 主体 */}
              {headerCom}
              {/* 主体2 */}
              {this.createBody(false)}

              {/* 固定显示的角落div */}
              {fixBox}

              <div className="corverBar"></div>

            </DragToggle>)
          　: null
          }
          {show ? <ProductDialog maskClosable={false} isHideOk={cantChange} 
          handleOk={this.handleOk} width={showEdit ? '40%' : '95%'} 
          show={show} close={this.close} title={title}
          extra={
            !showEdit && <Button  key="sureLock" onClick={() => this.handleOk(true, )} type="primary" icon="smile-o" className='acc' >確認並鎖定</Button>
          }
          cancelTxt='取消关闭' 
          okTxt='確認'>
            {this.dialogContent()}
          </ProductDialog> : null}
          {showDelete ? <ProductDialog isHideOk={modalType === 'getAccPnInfo' || cantChange} handleOk={this.sureDelete} width={showEdit ? '50%' : '90%'} show={show} close={this.cancleDelete} title={deleteTitle}>
            {this.deleteContent()}
          </ProductDialog> : null}
          {showModal ? <ProductDialog isHideOk={cantChange} handleOk={this.modalOk} width={'80%'} 
          show={showModal} close={this.modalClose} title={modalTitle} maskClosable={false}>
            <ColorSizeTable ref={colorTable => this.colorTable = colorTable} 
            // data={colorSizeDataObj[colorBar]}  
            data={this.colorSizeHandle(colorBar, )}  
            inputing={this.inputing} barcode={colorBar}  
            sizeConfig={sizeConfig}
            residueColorData={residueColorData}
            ></ColorSizeTable>
          </ProductDialog> : null}
          <Loading loading={loading}></Loading>
        </div>
      // </Collapses>
    );
  }
}

const state = state => state
const action = action => ({ 
  scheduleAction: bindActionCreators(scheduleAction, action), 
  copy: bindActionCreators(copy, action), 
})
// export default connect(state, action)(SecureHoc(ScheduleNo))
export default connect(state, action)(ScheduleNo)

