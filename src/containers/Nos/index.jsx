import React from "react"
import ReactDOM from 'react-dom';
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { getPnList, getBarcodeList, getPnDetailList, editBarcode, getFilter, splitPnBarcode,  } from 'api/ppc'
import { getProduct,   } from 'api/productLine'
import { grid, initParams, DATE_FORMAT_BAR, MONTH_FORMAT, FORMAT_DAY, } from 'config'
import { ANIMATE, LETTER,  } from 'constants'
// import ppcList from './ppcList.json'
// import DragableBar from '@@@/DragableBar'
import DragableBar from '@@@/DragableBar'
import ProductDialog from '@@@/ProductDialog'
import DragToggle from '@@@/DragToggles'
import BarcodeContent from '@@@/BarcodeContent'
import Collapses from '@@@/Collapses'
import FilterPane from '@@@/FilterPane'
import Loading from '@@@@/Loading'
import Draggable from 'react-draggable'
import { backupFn, dateSplit, newDate, toLocale, dateForm, daysLen, dateArrToLon, createRow, setItem, findDOMNode, openNotification, ajax } from 'util'
import * as scheduleAction from 'actions/scheduleNo.js'
import moment from 'moment';
import { Dropdown, Icon, Card, Button, Row, Col, Tooltip, } from 'antd';

class ScheduleNo extends React.PureComponent {
  constructor(props) {
    super(props)
    // this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      show: false,
      dateArr: [],
      monthArr: [],
      datas_barcode: [],
      datas: [],
      data_total: [],
      barcodeData: [],
      showBarcode: false,
      showEdit: false,
      loading: false,
      isSpliting: false,
      barcode: '',
      barcodeDate: {},
      pn_no: '',
      showPnDetail: false,
      dialogType: '',
      splitData: [],
      scrollLeftPos: 0,
      scrollTopPos: 0,
      productData: [],
      visible: false,
      customerData: [], 
      styleNoData: [],
      editBarcodeData: {},
      editPnNo: '',
      isFilter: false,
      pn_info: {},
    }
  }
  getPnList = (p) => {
    this.props.scheduleAction.filterInfo(p)
    
    this.setState({
      loading: true,
      visible: false,
    })
    getPnList(p).then(res => {
      // console.log('请求所有的getPnList ：', ppcList);
      let { datas_barcode, datas, data_total } = res.data
      // console.log('data_total ：', data_total, res.data.data_total, data_total.length)
      // datas_barcode = datas_barcode.slice(60, 100)
      // datas_barcode = datas_barcode.slice(0, 100)
      // datas = datas.slice(0, 100)
      // data_total = data_total.slice(0, 100)

      // const { datas_barcode, datas, data_total } = ppcList
      // const data1 = ppcList.data_total;
      if (data_total.length && datas_barcode.length) {
        this.filter()
        // console.log('有数据1 ：', data_total)
        const startDate = data_total[0].s_date
        const endtDate = data_total[0].e_date
        const dateArr = []
        const monthes = []
        let a = moment(startDate, DATE_FORMAT_BAR)
        let b = moment(endtDate, DATE_FORMAT_BAR)
        const dayLens = b.diff(a, 'days') + 1
        // console.log('dayLens ：', startDate, startDate.split('-'), endtDate, dayLens, a, b)
        // 得到排单所有天数
        for (let i = 0; i < dayLens; i++) {
          const day = moment(startDate).add(i, 'd')
          const date = day.format(FORMAT_DAY)
          const month = day.format(MONTH_FORMAT)
          const isSunday = day.format('d') === '0'
          const dateOrigin = day.format(DATE_FORMAT_BAR)
          monthes.push(month)
          // console.log('sRDates ：', month, date, isSunday)
          dateArr.push({ month, date, isSunday, dateOrigin });
        }
        // 去重 - 得到排单起止日期 - 包括月份总数组
        const set = new Set(monthes);
        const monthess = new Array(...set);
        const monthArr = monthess.map(v => ({ month: v, date: [] }))
        // console.log(' monthes, dateArr, monthArr：', monthes, monthArr, dateArr, moment(["2017", "1", "01"]));
        monthArr.forEach((v, i) => {
          for (let j = 0; j < dateArr.length; j++) {
            if (dateArr[j].month === v.month) {
              monthArr[i].date.push(dateArr[j])
            }
          }
        })
        datas_barcode.map((item, i) => {
          const shortDay = datas.filter(v => v.barcode === item.barcode)
          // 如果有红色天数数量赋给每个条码
          item.shortDay = shortDay
          item.shortDay.length && this.calcShortDay(item.shortDay, moment(item.s_date), a)
          this.calcInfo(item, a)
          
          // const width = moment(item.e_date).diff(moment(item.s_date), 'days')
          // const left = moment(item.s_date).diff(a, 'days')
          // // console.log('===222== datas_barcode item ：', left, width, a, moment(item.s_date), moment(item.e_date) )
          // item.startDay = item.s_date
          // item.endDay = item.e_date
          // item.width = (width + 1) * 30 + 'px'
          // item.left = (left) * 30
          return item
        })
        let level = 0
        let tableHeight = 0
        // 遍历所有的排单批号给所有的条码计算宽度、距离
        data_total.map((item, i) => {
          // console.log('item ：', item)
          const row = createRow(item.levels)
          tableHeight += row.length
          const data = datas_barcode.filter(v => {
            if (item.pn_no === v.pn_no) {
              v.tops = item.levels
            }
            return item.pn_no === v.pn_no
          })
          item.row = row
          item.data = data
          item.level = level
          level += item.levels
          return item
        })
        // console.log('1111111111111111 ：', dateArr, monthArr, datas_barcode, datas, data_total )
        this.setState({
          loading: false,
          // dateArr,
          // monthArr,
          // datas_barcode,
          // datas,
          // data_total,
          dateArr: [...backupFn(dateArr)],
          monthArr: [...backupFn(monthArr)],
          datas_barcode: [...backupFn(datas_barcode)],
          datas: [...backupFn(datas)],
          data_total: [...backupFn(data_total)],
          tableHeight: tableHeight * 30 + 75,
          tableWidth: dayLens * 30 + 130
        })
      } else {
        console.log('无数据 ：', )
        openNotification('沒有數據，請重新選擇過濾條件!',  )
        this.setState({
          loading: false,
        })
      }
    })
  }
  // 计算每个条码的宽度、距离等数据
  calcInfo = (v, startDay) => {
    const width = moment(v.e_date).diff(moment(v.s_date), 'days')
    const left = moment(v.s_date).diff(startDay, 'days')
    v.startDay = v.s_date
    v.endDay = v.e_date
    v.width = (width + 1) * 30
    v.left = (left) * 30
  }
  // 计算缺工红色日子的距离
  calcShortDay = (v, barStart, startDay) => {
    // console.log('calcShortDay v, startDay ：', v, barStart, startDay)
    v.map(item => item.shortDate = (moment(item.p_date).diff(startDay, 'days') - barStart.diff(startDay, 'days')) * 30)
  }
  showDialog = () => this.setState({ show: true, })
  dragBack = () => {
    const { barcode, barcodeDate, editBarcodeData } = this.state
    const { left, width } = editBarcodeData
    const {s_date_new, e_date_new} = barcodeDate
    // const newWidth = moment(e_date_new.split('-')).diff(s_date_new.split('-'), 'days') * grid + 'px'
    const dragBar = findDOMNode(ReactDOM, this[barcode])
      console.log('关闭this.state ：', this.state, this.dragBar, dragBar, barcode, dragBar.style.width, dragBar.style.transform, left, width)
    if (dragBar.style.width !== width) {
      console.log('日期变了  宽度变了：', dragBar.style.width, width)
      dragBar.style.width = width
    }
    dragBar.style.transform = `translate(${left}px, 0px)`
  }
  close = () => {
    const { barcode, showEdit, editBarcodeData } = this.state
    if (showEdit) {
      // this.dragBack()
    }
    this.setState({
      show: false,
      showEdit: false,
      showBarcode: false,
      splitData: [],
    });
  }
  // 停止拖拽设置当前操作的bar
  stopDrag = ({x, y, w, barcode, data, pn_no}) => {
    const { dateArr } = this.state
    this.setState({
      show: true, 
      showEdit: true, 
      barcodeDate: {s_date_new: dateArr[x].dateOrigin, 
      e_date_new: dateArr[w].dateOrigin},
      editBarcodeData: data,
      editPnNo: pn_no,
      barcode,
    })
    // console.log('停止拖拽设置当前操作的bar ：', x, y, w, barcode, data, pn_no), dateArr[x].dateOrigin, dateArr[w].dateOrigin
  }
  // 点击barcode开始编辑
  showBarcodeDialog = (barcode, pn_no, work_no) => {
    console.log('showBarcodeDialog22', barcode, pn_no, work_no);
    // this.props.scheduleAction.showDialog(true)
    this.getBarcodeList({barcode, work_no, pn_no})
    // this.setState({ barcode, editPnNo: pn_no, })
  }
  // 获取barcode信息请求
  getBarcodeList = (p) => {
    const {barcode, pn_no} = p
    getBarcodeList(p).then(res => {
      // console.log('getBarcodeList res ：', res);
      // this.props.scheduleAction.barcode(res.data.datas)
      const {datas, factory} = res.data
      this.setState({
        showBarcode: true, 
        barcodeData: [{...datas[0], isSplitData: false}],
        productData: factory,
        dialogType: 'showBarcode',
        show: true,
        barcode, editPnNo: pn_no,
      })
    })
  }
  // 获取批号信息请求
  getPnDetailList = (pn_no) => {
    this.setState({pn_no})
    getPnDetailList({pn_no}).then(res => {
      // console.log('getPnDetailList res ：', res.data);
      // this.props.scheduleAction.barcode(res.data.datas)
      const {datas, code, pn_info} = res.data
      // if (code === 1) {
        this.setState({
          dialogType: 'showPnCode', 
          barcodeData: datas,
          show: true,
          pn_info,
        })
      // }
    })
  }
  inputing = ({v, barcode, keys}) => {
    const {barcodeData, splitData} = this.state
    console.log('排单inputing e v, barcode, keys：', v, barcode, keys, this.props);
    
    if (keys === 'qty') {
      let total = 0
      splitData.map(item => {
        if (item.barcode === barcode) {
          item.qty = Number(v)
          console.log(' splitData item ：', item, ) 
        }
        total += item.qty
        return item
      })
      if (barcodeData[0].originQty - total >= 0) {
        // 对应的改变父条码的数量
        barcodeData[0].originQty = barcodeData[0].originQty - total
        console.log('输入的数合法 ：', )
      } else {
        console.log('输入的数不合法 ：', )
        openNotification('对不起，您输入的数量超过了父条码的数量，请重新拆分o(╥﹏╥)o', 'warning')
      }
      console.log('改变数量 ：', barcodeData[0], Number(v))
    } else if (keys === 'date') {
      console.log('不是改变数量 date：', v[0]._d, v[1]._d, moment(v[0]._d).format(DATE_FORMAT_BAR), )
      console.log('v[1].：', moment((moment(v[1]._d).format(DATE_FORMAT_BAR).split('-'))).diff(moment((moment(v[0]._d).format(DATE_FORMAT_BAR).split('-'))), 'days'))
      const s_date = moment(v[0]._d).format(DATE_FORMAT_BAR)
      const e_date = moment(v[1]._d).format(DATE_FORMAT_BAR)
      const work_days = moment(e_date.split('-')).diff(moment(s_date.split('-')), 'days') + 1
      splitData.map(item => {
        if (item.barcode === barcode) {
          item.s_date = moment(v[0]._d).format(DATE_FORMAT_BAR)
          item.e_date = moment(v[1]._d).format(DATE_FORMAT_BAR)
          item.work_days = work_days
        }
        return item
      })
    } else {
      console.log('改变内容', )
      if (keys === 'work_days') {
        v = Number(v)
      }
      splitData.map(item => {
        if (item.barcode === barcode) {
          console.log('改变内容', item.barcode === barcode, item.barcode, barcode, item[keys], v)
          item[keys] = v
        }
        return item
      })
    }
    this.setState({
      barcodeData: [...barcodeData],
      splitData: [...splitData],
    })
    console.log('matchItem ：', splitData, barcodeData)
  }
  // 点击一条barcode开始拆分 - 增加一条子条码
  splitAction = (data, qty) => {
    const {splitData, barcodeData} = this.state
    data.barcode = data.barcode.trim() + LETTER[splitData.length]
    console.log('**拆分targettarget ：', qty, qty != undefined ? qty : 0, splitData, data, data.barcode, [...splitData, {...data, isSplitData: true, qty: qty != undefined ? qty : 0 }])
    this.setState({
      splitData: [...splitData, {...data, isSplitData: true, qty: qty != undefined ? qty : 0 }],
      barcodeData: [{...barcodeData[0], originQty: barcodeData[0].qty}],
    })
  }
  handleOk = () => {
    const {barcodeData, dialogType, showEdit} = this.state
    console.log('handleOk ：', dialogType, this.state, barcodeData, showEdit ? '编辑条码' : '拆分')
    // if (dialogType === 'showBarcode') {
    if (showEdit) {
      this.editBarcode()
    } else {
      this.splitPnBarcode()
    }
    // this.close()
  }
  
  // 确认对批号的条码进行拆分
  splitPnBarcode = () => {
    const {barcodeData, splitData, editPnNo, data_total, datas_barcode, } = this.state
    console.log('splitPnBarcode barcodeData, splitData：', barcodeData, splitData, this.state, )
    const tableStartDay = data_total[0].s_date
    const {barcode, originQty, qty} = barcodeData[0]
    const total = splitData.reduce((t, n) => t + n.qty, 0)
    // 判断拆分数量是否还剩
    // console.log('total, originQty ：', total, originQty, qty, total < originQty, qty - total)
    if (total < qty) {
      // console.log('判断拆分数量是否还剩 是 ：', )
      this.splitAction(barcodeData[0], qty - total)
      splitData.push({
        ...barcodeData[0], 
        isSplitData: true, 
        barcode: barcode.trim() + LETTER[splitData.length], 
        qty: qty - total,
      })
    }
    // console.log('splitPnBarcode totaltotal：', barcode, barcodeData, this.state, splitData, total, originQty, qty, total < originQty, qty - total)
    const data = []
    const isRepeat = new Set()
    const wordNum = []
    const startDay = moment(tableStartDay, DATE_FORMAT_BAR)
    // 重新计算要插入的数
    splitData.map((v, i) => {
      // this.calcInfo(v, startDay)
      // const aa = moment(v.s_date);
      // const bb = moment(v.e_date);
      // const width = bb.diff(aa, 'days')
      // const left = aa.diff(a, 'days')
      // v.startDay = v.s_date
      // v.endDay = v.e_date
      // v.width = (width + 1) * 30 + 'px'
      // v.left = (left) * 30

      // v.factory_id = Number(v.factory_id)
      // v.qty = Number(v.qty)
      // console.log('工作天数是否正确v.qty  ：', splitData, v.qty, v.day_qty, v.qty / v.day_qty, (v.qty / v.day_qty) / moment(v.e_date).diff(moment(v.s_date), 'days'), moment(v.e_date).diff(moment(v.s_date), 'days') )
      wordNum.push((v.qty / v.day_qty) / moment(v.e_date).diff(moment(v.s_date), 'days') < 1)
      v.user_id = '3'
      // console.log('isRepeat ：', isRepeat, v.factory_id, isRepeat.has(v.factory_id))
      isRepeat.add(v.factory_id)
      return v
    })
    const isWorkRight = wordNum.some(v => v)
    // console.log('isWorkRight ：', isWorkRight, wordNum)
    if (!isWorkRight) {
      const newObj = {}
      isRepeat.forEach((item, index) => {
        // console.log(' isRepeat v ：', item, index, )
        splitData.forEach((v, i) => {
          // console.log(' splitData v ：', v, i, )
          if (v.factory_id === item) {
            if (newObj[v.factory_id] == undefined) {
              console.log('undefined ：', )
              newObj[v.factory_id] = []
            } 
            newObj[item].push(v)
          }
        })
      })
      Object.values(newObj).forEach((v, i) => v.sort((a, b) => moment(a.s_date).isAfter(b.s_date)))
      // console.log('olds ：', splitData, newObj)
      const isIllegalArr = []
      Object.values(newObj).forEach((v, i) => {
        if (v.length !== 1) {
          // console.log(' newObj v ：', v, i, )
          let isIllegal = false
          for (let i =0;i<v.length;i++) {
            if (i !== v.length-1) {
              // console.log('isAfterisAfter 111moment(a.s_date).isAfter(b.s_date) ：', v, v[i + 1].s_date,v[i].e_date, moment(v[i + 1].s_date).isAfter(v[i].e_date))
              if (moment(v[i + 1].s_date).isAfter(v[i].e_date)) {
                isIllegal = true
              }
            }
          }
          // console.log('isIllegal ：', isIllegal)
          isIllegalArr.push(isIllegal)
        }
      })
      const illeageResult = isIllegalArr.every(v => v)
      if (!illeageResult) {
        openNotification('拆分日期重疊了，請重新填寫日期!', )
      } else {
        console.log('拆分合法 ：', )
        // 插入拆分的数据
        data_total.forEach(v => {
          if (v.pn_no === editPnNo) {
            const mathcs = v.data.findIndex(item => item.barcode === barcode.trim())
            // console.log('mathcs ：', mathcs, v.data, barcode)
            v.data.splice(mathcs, 1, ...splitData)
          }
          return v
        })
        // if (splitData.length !== isRepeat.size) {
        //   console.log('！！！！！！！生产线重复了 ：', )
        // } else {
          const params = {p_barcode: barcode, data: splitData}
          // console.log('生产线没有重复splitData ：', splitData, params, data_total)
          splitPnBarcode(params).then(res => {
            // console.log('splitPnBarcode res ：', res.data);
            const {code, datas, mes, newBarcode} = res.data
            openNotification(mes)
            if (code === 1) {
              // 插入拆分的数据
              newBarcode.map((v, i) => {
                // console.log('插入拆分的数据 barcode v ：', v, i, )
                this.calcInfo(v, startDay)
                return v
              })
              data_total.forEach(v => {
                if (v.pn_no === editPnNo) {
                  const mathcs = v.data.findIndex(item => item.barcode === barcode.trim())
                  // console.log('mathcs ：', mathcs, v.data, barcode, newBarcode)
                  v.data.splice(mathcs, 1, ...newBarcode)
                }
                return v
              })
              this.setState({
                data_total: [...data_total],
                show: false,
                splitData: [],
              })
              if (datas.length) {
                datas_barcode.map((item, i) => {
                  const shortDay = datas.filter(v => v.barcode === item.barcode)
                  // 如果有红色天数数量赋给每个条码
                  item.shortDay = shortDay
                  item.shortDay.length && this.calcShortDay(item.shortDay, moment(item.s_date), tableStartDay)
                  return item
                })
              }
            }
          })
        // }
      }
      // console.log('拆分数组isIllegalArr ：', isIllegalArr, illeageResult)
    } else {
      openNotification('拆分數量日産不正確，請重新填寫!', )
    }
  }

  // 拖拽、改变起止日期  确认发送请求
  editBarcode = () => {
    const {barcodeDate, barcode, editBarcodeData, data_total, datas_barcode} = this.state
    const tableStartDay = data_total[0].s_date
    // console.log('editBarcode ：', this.state, barcodeDate, barcode, editBarcodeData)
    editBarcode({...barcodeDate, barcode}).then(res => {
      // console.log('editBarcode res ：', res.data, this.state);
      const {code, datas, mes} = res.data
      openNotification(mes)
      if (code === 1) {
        data_total.map(v => {
          v.data.forEach((item, index) => {
            if (item.barcode === barcode) {
              item.s_date = barcodeDate.s_date_new
              item.e_date = barcodeDate.e_date_new
              this.calcInfo(item, data_total[0].s_date)
              console.log('等于改变起止日期 v ：', item, index, item.barcode, barcode, item.barcode === barcode)
            }
          })
          return v
        })
        if (datas.length) {
          datas_barcode.map((item, i) => {
            const shortDay = datas.filter(v => v.barcode === item.barcode)
            // 如果有红色天数数量赋给每个条码
            item.shortDay = shortDay
            item.shortDay.length && this.calcShortDay(item.shortDay, moment(item.s_date), tableStartDay)
            return item
          })
        }
      }
      this.setState({
        data_total: [...data_total],
        show: false,
      })
    })
  }
  
  lock = (t, v, i) => {
    console.log('lock ：', t, v, i)
    const {barcodeData} = this.state
    barcodeData[i].lock = t === 'F' ? 'T' : 'F'
    console.log('barcodeData ：', barcodeData, barcodeData[i])
    this.setState({
      barcodeData: [...barcodeData]
    })
  }
  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('##########shouldComponentUpdate  nextProps ：', nextProps, nextState, )
  //   // const {splitData} = this.state
  //   // console.log('@@@@@@@@shouldComponentUpdate  show ：', splitData)
  //   if (nextState.shou) {
  //     return false
  //   } else {
  //     return true
  //   }
    
  // }
  delete = (v) => {
    const {barcode} = this.state
    console.log('productNoWrapper delete ：', v, barcode)
  }
  // 获取过滤数据
  getFilter = () => {
    getFilter({}).then(res => {
      console.log('getFilter res ：', res.data);
      const {customer, style_no} = res.data
      this.setState({
        customerData: customer, 
        styleNoData: style_no,
      })
    })
  }
  filterOption = () => {
     console.log('filterOption ：', )
     this.setState({ visible: true });
  }
  handleVisibleChange = (flag, e) => {
    // console.log('handleVisibleChange ：', flag, e)
    this.setState({ visible: flag });
  }
  // 固定显示的角落div
  fixBox = (isPane) => {
    const {customerData, styleNoData, visible} = this.state
    // console.log('fixBox ：',  this.state)
    return (
      <div className="headerSideBarBox">
        <div className="fixBoxItem ">
          {
            isPane ? <Dropdown overlay={<FilterPane customerData={customerData} styleNoData={styleNoData} 
            handleVisibleChange={this.handleVisibleChange} getData={this.getPnList} visible={visible}></FilterPane>} 
            trigger={['click']} className='filterOption'
            // onVisibleChange={this.handleVisibleChange} 
              visible={visible}>
              <Button onClick={this.filterOption.bind(this,)} icon='filter' className='salmon' type="primary">過濾選項</Button>
            </Dropdown> : null
          }
          <div className='pnDay'>日期</div>
        </div>
        <div className="fixBoxItem pnInfoColumn">批號信息</div>
      </div>
    )
  }
  createDay = (isTxt, isMonth, isLasItem) => {
    const {monthArr} = this.state
    if (!isMonth) {
      return monthArr.map((v, i) => {
        // console.log(' monthArr v ：', v, ) 
        return <div className="dayWrapper" key={v.month + v.date.length}>
          {
            v.date.map((item, index) => {
              return <div className={`${item.isSunday ? 'isSunday dateItem' : 'dateItem'} ${isLasItem ? 'boldDate' : ''}`} key={item.dateOrigin + v.date.length}>{isTxt ? item.date : null}</div>
            })
          }
        </div>
      })
    } else {
      return monthArr.map((v, i) => {
        // console.log(' monthArr v ：', v, ) 
        return <div className="headerWrapper" key={v.month + v.date.length}>
          <div className="monthWrapper">
            {isMonth ? <div className={`month ${v.date.length === 1 ? 'oneDate' : ''} ${v.date.length === 2 ? 'twoDate' : ''}`}>{v.month}</div> : null}
            <div className="dayWrapper">
              {
                v.date.map((item, index) => {
                  // console.log(' item.date item ：', item, ) 
                  return <div className={`${item.isSunday ? 'isSunday dateItem' : 'dateItem'}`} key={item.dateOrigin + v.date.length}>{isTxt ? item.date : null}</div>
                })
              }
            </div>
          </div>
        </div>
      })
    }
  }
  createBody = (justSideBar, ) => {
    // console.log('justSideBar ：', justSideBar)
    const {data_total, dateArr} = this.state
    
    return data_total.map((v, i) => {
      // console.log(' data_total v ：', v, ) 
      const heights = v.levels * 30
      const topss = v.level * 30
      if (justSideBar) {
        return (
          <div className="contentRow" style={{height: v.data.length * grid}} key={v.pn_no}>
            <div className="sideBarBox">
              <Tooltip placement="top" title={
                <div className="pnInfoWrapper">
                  <div className="pnInfo pnNos">{v.style_no}</div>
                  <div className="pnInfo">{v.cloth_style}</div>
                </div>
              }>
                <div className="pnDay">
                  <Button onClick={this.getPnDetailList.bind(this, v.pn_no)} type="primary">{v.pn_no}</Button>
                </div>
              </Tooltip>
              {v.data.length > 1 ? <div className="pnInfo pnNos">{v.style_no}</div>: null}
              {v.data.length > 1 ? <div className="pnInfo">{v.cloth_style}</div>: null}
            </div>
          </div>
        )
      } else {
        return (
          <div className="contentRow" style={{height: v.data.length * grid}} key={v.pn_no}>
            <div className="sideBarBox">
              <Tooltip placement="top" title={
                <div className="pnInfoWrapper">
                  <div className="pnTip pnNos">{v.style_no}</div>
                  <div className="pnTip">{v.cloth_style}</div>
                </div>
              }>
                <div className="pnDay">
                  <Button onClick={this.getPnDetailList.bind(this, v.pn_no)} type="primary">{v.pn_no}</Button>
                </div>
              </Tooltip>
              {v.data.length > 1 ? <div className="pnInfo pnNos">{v.style_no}</div>: null}
              {v.data.length > 1 ? <div className="pnInfo">{v.cloth_style}</div>: null}
            </div>
            <div className="rowWrapper">
              {
                v.data.map((item, index) => {
                  const { left, width, tops, levels, pn_no, barcode, shortDay, work_name, factory_name} = item
                  const top = (levels - 1) * 30
                  const isLasItem = index === v.data.length - 1 
                  {/* console.log('item ：', v, item, index, left, width, barcode) */}
                  return <div className="pnRow" key={barcode + work_name + factory_name}>
                    {this.createDay(false, false, isLasItem)} 
                    <DragableBar dateLen={dateArr.length} ref={dragBar => this[barcode] = dragBar} clickType='barClick' key={barcode + work_name + factory_name} pn_no={pn_no} data={item} isAxis={'x'} showBarcodeDialog={this.showBarcodeDialog} stopDrag={this.stopDrag} config={{ left, width, top, txt: `${work_name} / ${factory_name}`, levels, barcode, shortDay, }}></DragableBar>
                  </div>
                })
              }
            </div>
          </div>
        )
      }
    })
  }
  editContent = () => {
    console.log('editContent  this.state ：', this.state)
    const {barcode, barcodeDate, editBarcodeData} = this.state
    const {s_date_new, e_date_new} = barcodeDate
    const {startDay, endDay} = editBarcodeData
    return <div className="editWrapper">
      {/* <div className="editInfo">确认将条码 {barcode} 的起止日期修改为： </div> */}
      <div className="editCon">從 {startDay} 到 {endDay} 變爲 {s_date_new} 到 {e_date_new}</div>
      {/* <div className="editCon">为 {s_date_new} 到 {e_date_new}</div> */}
    </div>
  }
  filter = () => {
    this.setState({
      isFilter: true,
      dateArr: [],
      monthArr: [],
      datas_barcode: [],
      datas: [],
      data_total: [],
    })
  }
  componentDidMount() {
    this.getPnList(initParams)
    this.getFilter()
    
    const {offsetHeight, offsetWidth} = findDOMNode(ReactDOM, this.con)
    const domRef = findDOMNode(ReactDOM, this.con)
    domRef.onscroll = (e) => {
      const {scrollLeft, scrollTop} = domRef
      const {scrollLeftPos, scrollTopPos} = this.state
      if (scrollLeftPos !== scrollLeft) {
        // console.log('横向滚动 ：', )
        ajax(this.setState.bind(this), {
          scrollLeftPos: scrollLeft,
        })
        // this.setState({
        //   scrollLeftPos: scrollLeft,
        // })
      } else if (scrollTopPos !== scrollTop) {
        // console.log('纵向滚动 ：', )
        ajax(this.setState.bind(this), {
          scrollTopPos: scrollTop,
        })
        // this.setState({
        //   scrollTopPos: scrollTop,
        // })
      }
      // console.log('!!!@@@@@@@222chatingWrapper Pos：', e, domRef.scrollLeft, domRef.scrollTop, domRef.scrollHeight)
    }
  }
  
  render() {
    const {scrollHeight, collapse,} = this.props
    const {offsetHeight, offsetWidth} = this.props.styles
    const { datas_barcode,show, tableHeight, tableWidth, showEdit, data_total,
      loading, barcodeData, isSpliting, dialogType, splitData, pn_no, pn_info,
      scrollLeftPos, scrollTopPos, productData, barcode, barcodeDate, isFilter} = this.state
    // const {barcodeData} = this.props.scheduleNo
    const title = dialogType === 'showPnCode' ? `批號 ${pn_no} 詳情` : showEdit ? `是否確認修改條碼 ${barcode} 的起止時間` : `條碼詳情`
    const content = showEdit ? this.editContent() : <BarcodeContent pn_info={pn_info} productData={productData} splitData={splitData} isShowBtn={dialogType === 'showBarcode'} dialogType={dialogType} delete={this.delete} lock={this.lock} isSpliting={isSpliting} inputing={this.inputing} splitAction={this.splitAction} barcodeData={barcodeData}></BarcodeContent>
    console.log('————————ScheduleNo 组件 this.state, this.props：', this.props, this.state, )
    const headerCom = datas_barcode.length ? (
      <div className="headerContainer">
        {this.fixBox()}
        {this.createDay(true, true)}
      </div>)
      : null

    return (
      // <Collapses title={'工艺路线'} className='scheduleCollapse'  isAnimate={false}>
        <div className={`noWrapper ${ANIMATE.fadeIn}`} ref={con => this.con = con}>
          {
            data_total.length ? (
            <DragToggle scrollLeftPos={scrollLeftPos} scrollTopPos={scrollTopPos} collapse={collapse} scrollHeight={scrollHeight} tableHeight={tableHeight} tableWidth={tableWidth} offsetWidth={offsetWidth} offsetHeight={offsetHeight}>
              {/* 固定头0 */}
              {/* <div className="fixHeaderWrapper"> */}
                <div className="fixHeader">
                  {headerCom}
                </div>
              {/* </div> */}

              {/* 固定列1 */}
              <div className="fixSideBar">
                {this.fixBox()}
                {this.createBody(true)}
              </div> 
              {/* 主体 */}
              {headerCom}
              {/* 主体2 */}
              {this.createBody(false)}

              {/* 固定显示的角落div */}
              {this.fixBox(true)}

            </DragToggle>)
          　: null
          }
          {show ? <ProductDialog handleOk={this.handleOk} width={showEdit ? '40%' : '90%'} show={show} close={this.close} title={title}>{content}</ProductDialog> : null}
          <Loading loading={loading}></Loading>
        </div>
      // </Collapses>
    );
  }
}

const state = state => state
const action = action => {
  return {
    scheduleAction: bindActionCreators(scheduleAction, action),
  }
}
export default connect(state, action)(ScheduleNo);

