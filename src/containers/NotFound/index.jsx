import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import fly from 'static/img/fly.jpg'
import noFound from 'static/img/404.jpg'

class NotFound extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }

  render() {
    return (
      <div className="errorPage">
        <div className="fly"><img src={fly} alt="" /></div>
        <div className="noFound">
          <div className="errorImg errorItem">
            <img src={noFound} alt="" />
          </div>
          <div className="errorTxt errorItem">
            Sorry, The Page Is No Found,Please Check The URL!
          </div>
        </div>
      </div>
    );
  }
}

export default NotFound;
