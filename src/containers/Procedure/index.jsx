import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getProcedure, addWork, updateWork, deleteWork,  } from 'api/Procedure'
import {produceForm} from 'config'
import Collapses from '@@@/Collapses'
import ProduceForm from '@@@/ProduceForm'
import ProductDialog from '@@@/ProductDialog'
import ProcedureTable from '@@@/ProcedureTable'
import { Button, message, } from 'antd';
const formItemLayout = {};

class Procedure extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      procedureData: [],
      show: false,
      title: '工序信息',
      init: {},
      editIndex: -1,
    }
  }
  showDialog = () => this.setState({ show: true, })
  closeDialog = () => this.setState({ show: false, init: {},})
  handleOk = () => {
    console.log('handleOk ：', this.state, this.forms);
    const {init} = this.state
    this.forms.validateFields((err, values) => {
      console.log('values ：', err, values);
      if (!err) {
        Object.keys(init).length ? this.updateWork(values) : this.addWork(values)
      }
    })
  }
  getProcedure = () => {
    getProcedure({}).then(res => {
      console.log('getProcedure res ：', res.data);
      res.data.data.map(v => v.editCol = '')
      this.setState({
        procedureData: res.data.data
      })
    })
  }
  addWork = (v) => {
    console.log('{data: [v]} ：', {data: [v]})
    addWork({data: [v]}).then(res => {
      console.log('addWork res ：', res.data);
      const {code, mes} = res.data
      // message.warn(mes, )
      // confirms(code, mes, )
      if (code === 1) {
        const { procedureData,  } = this.state
        this.setState({procedureData: [v, ...procedureData]})
      }
      this.closeDialog()
    })
  }
  updateWork = (v) => {
    updateWork({data: [v]}).then(res => {
      console.log('updateWork res ：', res.data);
      const {code, mes} = res.data
      // message.warn(mes, )
      // confirms(code, mes, );
      if (code === 1) {
        const { procedureData, editIndex } = this.state
        console.log('editIndex ：', editIndex)
        procedureData[editIndex] = v
        this.setState({procedureData: [...procedureData]})
      }
      this.closeDialog()
    })
  }
  deleteWork = (o, index) => {
    deleteWork(o).then(res => {
      console.log('deleteWork res ：', o, index, res.data);
      const {code, mes} = res.data
      // message.warn(mes, )
      // confirms(code, mes, );
      if (code === 1) {
        this.setState({
          canDelete: true,
        })
        
        const { procedureData,  } = this.state
        const data = procedureData.filter((v, i) => i !== index)
        setTimeout(() => this.setState({procedureData: data, canDelete: false}), 500);
      }
    })
  }
  editData = (o, i) => {
    console.log('editData o ：', o, i);
    this.setState({init: o, editIndex: i,})
    this.showDialog()
  }
  deleteData = (index) => {
    console.log('deleteData v, o, i：', index, this.props);
    const { procedureData,  } = this.state
    const data = procedureData.filter((v, i) => i !== index)
    this.timer = setTimeout(() => this.setState({procedureData: data}), 500);
  }
  componentDidMount() {
    this.getProcedure()
  }
  componentWillUnmount() {
    console.log('Procedure 组件卸载 ：', this.props, this.state )
    this.timer && clearTimeout(this.timer)
  }
  
  render() {
    console.log('Procedure 组件this.state, this.props ：', this.state, this.props, this);
    const { procedureData, show, title, init, canDelete } = this.state
    const {middle,  } = this.props.route
    return (
      <section className="procedure">
        <Collapses title={'工序'} className="procedure" 
          middle={middle}
          extra={
          <div className="btnWrapper">
            <Button onClick={this.showDialog} type="primary" size='large' icon="plus-circle-o" className="m-l">新增工序</Button>
          </div>}
        >
          <ProcedureTable middle={middle} canDelete={canDelete} deleteWork={this.deleteWork} editData={this.editData} data={procedureData}></ProcedureTable>
        </Collapses>
        {
          show ? (<ProductDialog noJustify={true} show={show} handleOk={this.handleOk} close={this.closeDialog} title={title}>
          <ProduceForm formItemLayout={formItemLayout} init={init} ref={forms => this.forms = forms} config={produceForm}></ProduceForm>
        </ProductDialog> ): null
        }
        
      </section>
    );
  }
}

export default Procedure;