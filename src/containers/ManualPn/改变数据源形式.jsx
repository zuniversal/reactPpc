import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getManualPn, getPnBarcode, } from 'api/ManualPn'
import { getPnList, getFilter, splitPnBarcode, barcodeLock,  } from 'api/ppc'
import {manulPnDateArr, DATE_FORMAT_BAR, manualNumForm, initParams, manaulPnForm, manualNumInit, filterOptionDate, filterOptionForm, } from 'config'
import { ANIMATE, SELECT_TXT, defaultWorkNo, } from 'constants'
import {openNotification, backupFn, confirms, filterArrOForm, mergeArr, filterTrimDatas,   } from 'util'
import Collapses from '@@@/Collapses'
import ProductDialog from '@@@/ProductDialog'
import ProduceForm from '@@@/ProduceForm'
import EditInfo from '@@@@/EditInfo'
import SecureHoc from '@@@@/SecureHoc'
import BarcodeInfo from '@@@@/BarcodeInfo'
import BarcodeTable from '@@@@/BarcodeTable'
import PnNoTable from '@@@/PnNoTable'
import ManualBarcodeTable from '@@@/ManualBarcodeTable'
import ClipBoard from '@@@@/ClipBoard'//
import { Button, Icon, Select, message, Row, Col, Input, } from 'antd'
import moment from 'moment'
// import ppcList from './ppcList.json'
const Option = Select.Option
const { TextArea } = Input
const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
}

class ManualPn extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    this.state = {
      procedureData: [],
      editData: {},
      editOrigin: {},
      show: false,
      work_no: defaultWorkNo,
      work_name: '',
      isNew: false,
      isAdd: false,
      typeFilters: [],
      addData: {},
      modalType: '',
      title: '提示框',
      manualNumInit,
      // pnData: ppcList.data_total,
      // pnBarcodeData: ppcList.datas_barcode.slice(0, 20),
      pnBarcodeData: [],
      filterInfo: manaulPnForm,
      showPnInfo: false,
      filterOptionDate,
      showNo: '',
      customerData: [],
      styleNoData: [],
      isEditting: false, 
      showPnNoInfo: {},
      pnData: {},
      barcodeData: [],
      dateForm: {
        reason: '',
        
      },
      
      
    }
  }
  showPnInfo = (t, v, i, ) => {
    console.log(' showPnInfo ： ',  t, v, i,  )
    this.setState({
      // showPnInfo: true,
      pnInfo: v, 
      showNo: t, 
      showPnNoInfo: v[0], 
      pnBarcodeData: v, 
      // const showPnNoInfo = pnNpDataObj[showNo][0]
    })
  }
  closeDialog = (v,  ) => {
    console.log(' closeDialog ： ',  v,   )
    this.setState(this.close())
  }
  close = (v,  ) => {
    console.log(' close ： ',  v,   )
    return {
      showPnInfo: false,
      isEditting: false,
      pnInfo: {}, 
      
    } 
  }
  startEdit = (v,  ) => {
    console.log(' startEdit ： ',  v,   )
    
  }
  startEditDay = (t, v, i ) => {
    const {pnBarcodeData, isEditting, pnData,  } = this.state// 
    console.log(' startEditDay ： ',  t, v, i, pnBarcodeData, isEditting,  )
    if (isEditting) {
      confirms(2, '請先保存上一條數據再繼續操作！', )
      return  
    }
    this.setState({
      // pnBarcodeData: pnBarcodeData.map((item) => item.barcode === v.barcode ? {...item, isEdit: true, } : item),
      showPnInfo: true,
      isEditting: true,
      pnInfo: v,
      dateForm: {
        reason: '11',
        
      },
      
    })
  }
  saveEdit = (v,  ) => {
    console.log(' saveEdit ： ',  v,   )
    
  }
  editDay = (p) => {
    let {v, keys, type, factory_name, } = p
    const {barcodeData, } = this.state// 
    // const barcodeDatas = [{...barcodeData[0], [`new_${keys}`]: dateFormat(v),}, ]
    // console.log(' editDayeditDay props ： ', v, keys, dateFormat(v), p,  )
    let barcodeDatas
    console.log(' editDayeditDay props ： ', v, keys, p,  )
    if (type === 'productLine') {
      barcodeDatas = [{...barcodeData[0], [`new_${keys}`]: v, [`new_factory_name`]: factory_name,}, ]
      console.log(' 修改生产线ss ： ', v, keys, p, barcodeDatas,   )
      this.setState({
        barcodeData: barcodeDatas,// 
      })
      this.setState({
        pnBarcodeData: pnBarcodeData.map((item) => item.barcode === v.barcode ? {...item, isEdit: true, } : item),
      })
    } 
    else {
      barcodeDatas = [{...barcodeData[0], [`new_${keys}`]: v,}, ]
      this.setState({
        [`new_${keys}`]: v,// 
        barcodeData: barcodeDatas,// 
      })
    }
  }
  toggleLock = (t, v, i) => {
    console.log(' toggleLock ： ', t, v, i,    )// 
    barcodeLock({barcode: v.barcode, lock: v.lock !== 'T' ? 'T' : 'F',}).then(res => {
      // console.log('barcodeLock res ：', res.data);
      const {code, customer, style_no, } = res.data
      if (code === 1) {
        // const newData = data_total[indexes].data.map(v => ({...v, lock: v.barcode === barcode ? lock : v.lock}))
        // data_total[indexes].data = [...newData]
        // console.log('newData ：', newData,  data_total[indexes],   )
        // this.setState({
        //   data_total: [...data_total],
        //   show: lock === 'T' ? false : show,
        //   barcodeData: [{...barcodeData[0], lock, }],
        // })
      }
    })
  }
  handleOk = (v,  ) => {
    const {pnInfo, dateForm,  } = this.state// 
    console.log(' handleOk ： ',  v, dateForm, pnInfo,   )
    if (Object.keys(pnInfo).length) {
      this.editPnBarcode()
      this.closeDialog()
    }
    
  }
  editPnBarcode = (isLock, ) => {
    const { 
      barcodeData, colorArr, pnItem, data_total, new_s_date, new_e_date, new_factory_name, new_factory_id, colorSizeDataObj, 
      colorSizeObjArr,
      editPnNo,
      dateForm,
    } = this.state
    const {barcode, s_date, e_date,  } = barcodeData[0]
    console.log(' editPnBarcode ： ', isLock, new_s_date, new_e_date, pnItem, barcode, `${barcode}A`, this.state,   )
    const isRight = [new_s_date, new_e_date, ].every((v, i) => {
      const isValid = moment(v).isValid()
      const isNoEmpty = v !== ''
      // console.log(' pnDateArr v ： ', v, i, isValid, isNoEmpty, )
      return isValid && isNoEmpty
    })
    console.log(' isRight ： ', moment("not a real date").isValid(), isRight,    )//
    // 修改的起止日期是否正确
    if (!moment(new_e_date).isAfter(new_s_date)) {
      confirms(2, '修改的起止日期不正確，請檢查 !', )
      return  
    }
    const validTxt = this.dateRange(new_s_date, new_e_date, barcodeData[0], pnItem, )// 
    console.log(' new_s_date, new_e_date, ： ', new_s_date, new_e_date, validTxt )// 
    if (validTxt !== '' ) {
      confirms(2, validTxt, )
      return  
    }
    // this.dateRange('2019-02-04', '2019-02-16', )//
    // this.dateRange('2019-02-03', '2019-02-16', )//
    // this.dateRange('2019-02-03', '2019-02-22', )//
    // this.dateRange('2019-02-03', '2019-02-23', )
    // return  
    const barcodeItem = backupFn(barcodeData[0])
    barcodeItem.barcode = `${barcode}A`
    barcodeItem.user_id = this.props.userInfo.user_id
    barcodeItem.color = colorSizeObjArr[barcode]
    // splitData.forEach((v, i) => colorSizeDataObj[v.barcode].forEach(item => v.color = [...v.color, ...item.data.filter(v => v.status === 'T')]))
    console.log(' 正確正確 ： ', colorSizeDataObj[barcode], barcodeItem, barcodeItem.color, barcodeData[0], { p_barcode: editPnNo, data: barcodeItem, } )// 
    const params = { pn_no: editPnNo, p_barcode: barcode, data: [barcodeItem], lock: isLock ? 'T' : 'F',  }
    console.log(' { pn_no: barcode, data: barcodeData,  } ： ', params, editPnNo, { pn_no: editPnNo, p_barcode: barcode, data: barcodeItem,  },  )// 
    // return  
    // this.splitBarcode({ pn_no: editPnNo, p_barcode: barcode, data: barcodeItem,  })
  }
  splitBarcode = (p) => {// 
    console.log(' splitBarcode ： ',  p, this.state,  )
    // return  
    splitPnBarcode(p).then(res => {
      console.log(' splitPnBarcode  res.data ： ', res.data,  )// 
      const {code, datas, mes, newBarcode} = res.data
      const {data_total, pnItem, colorSizeDataObj, } = this.state// 
      const tableStartDay = data_total[0].s_date
      const startDay = moment(tableStartDay, DATE_FORMAT_BAR)
      // if (code === 1) {
      //   this.setState({
      //     data_total: [...data_total],
      //     ...this.resetColorInfo(),
      //   })
      // }
    
    })
  } 
  mxDateFilter = (data, k, x,  ) => {
    // console.log(' mxDateFilter  ： ', data, k, x,   )// 
    return moment[x](data.map(v => moment(v[k]))).format(DATE_FORMAT_BAR)
  }
  workFilter = (data, k, item, ) => {
    console.log(' workFilter  ： ', data, k, item,  )
    return data.filter(v => v[k] === item)
  }
  dateRange = (new_s_dates, new_e_dates, barcodeData, pnItem, isDelete, ) => {
    const { data_total, splitData,   } = this.state
    // console.log(' dateRange ： ',  this.state, isDelete, pnItem, data_total, data_total[pnItem], new_s_dates, new_e_dates, '**', barcodeData, this.state.barcodeData, )
    const {barcode, shipment_date, s_date, e_date,  } = barcodeData
    const {workArr, } = data_total[pnItem]
    const pnData = backupFn(data_total[pnItem].data)
    const editIndex = pnData.findIndex((v, i) => v.barcode.trim() === barcode.trim())
    const {work_no, } = pnData[editIndex]
    pnData[editIndex].s_date = this.state.barcodeData[0].new_s_date 
    pnData[editIndex].e_date = this.state.barcodeData[0].new_e_date
    const sameWorkData = pnData.filter((v, i) => v.work_no === work_no)
    console.log(' pnData ： ', pnData, this.state.barcodeData, sameWorkData, barcode)// 
    const workIndex = workArr.findIndex((v, i) => v === work_no)
    const sameWorkDate = isDelete ? sameWorkData.filter((v, i) => v.barcode.trim() !== barcode.trim()) : splitData.length ? [...splitData, ...sameWorkData.filter((v, i) => v.barcode.trim() !== barcode.trim()), ] : sameWorkData
    console.log(' editIndex,  ： ', editIndex, sameWorkData, sameWorkDate,   )// 
    const new_s_date = this.mxDateFilter(sameWorkDate, 's_date', 'min', )
    const new_e_date = this.mxDateFilter(sameWorkDate, 'e_date', 'max', )
    console.log(' 條碼的 new_s_date new_e_date ： ', new_s_date, new_e_date, )// 
    const workLastIndex = workArr.length - 1
    const preIndex = editIndex - 1
    const nextIndex = editIndex + 1
    const preWIndex = workIndex - 1
    const nextWIndex = workIndex + 1
    const workPIndex = workArr[preWIndex]
    const workNIndex = workArr[nextWIndex]
    const preBarArr = this.workFilter(pnData, 'work_no', workPIndex,  )
    const nextBarArr = this.workFilter(pnData, 'work_no', workNIndex,  )
    const preBarSdate = this.mxDateFilter(preBarArr, 's_date', 'min', )
    const preBarEdate = this.mxDateFilter(preBarArr, 'e_date', 'max', )
    const nextBarSdate = this.mxDateFilter(nextBarArr, 's_date', 'min', )
    const nextBarEdate = this.mxDateFilter(nextBarArr, 'e_date', 'max', )
    const preBar = pnData[preIndex]
    const nextBar = pnData[nextIndex]
    const realEnd = moment(shipment_date).isAfter(e_date)
    const end = realEnd ? shipment_date : e_date
    console.log(' preBar, nextBar, pnData, editIndex, shipment_date, e_date, realEnd, end ： ', preBar, nextBar, pnData, editIndex, shipment_date, e_date, realEnd, end, preIndex, nextIndex,  )// 
    let txt 
    // 只有1条的情况
    if (pnData.length === 1) {
    // if (workArr.length === 1) {
      const isUp =moment(end).isAfter(new_e_date) || new_e_date === end
      txt = !isUp && '注意：條碼的结束日期不能小于货期！'
      console.log(' 只有1条的情况 ：txt ',  txt, shipment_date, e_date, new_e_date, end, moment(end).isAfter(new_e_date), isUp)// 
    }
    if (workArr.length === 1) {
      console.log(' workArrworkArr ： ',    )// 
      return ''
    }
    
    // 多条 
    // if (pnData.length > 1) {
    if (workArr.length > 1) {
      // // 2条 第一条 
      // if (editIndex === 0) {
      if (work_no === workArr[0]) {
        console.log(' 2条 第一条 ： ', new_s_date, nextBarSdate, new_e_date, nextBarEdate )// 
        const isNextStartAfterStart = moment(nextBarSdate).isAfter(new_s_date) || nextBarSdate === new_s_date
        const isNextEndAfterEnd = moment(nextBarEdate).isAfter(new_e_date) || nextBarEdate === new_e_date
        txt = !(isNextStartAfterStart && isNextEndAfterEnd) && `注意：改變條碼的起止日期不正確，沒有在下一個工序的日期之前！ \n 改變條碼的起止日期 【${new_s_date} ： ${new_e_date}】 相鄰工序起止日期 【${nextBarSdate} ： ${nextBarEdate}】`
        console.log(' 2条 第一条 txt： ',  txt, isNextStartAfterStart, isNextEndAfterEnd, (isNextStartAfterStart && isNextEndAfterEnd) )// 
      }
      // // 2条 最后一条 
      // else if (editIndex === (pnData.length  ：  1)) {
      else if (work_no === workArr[workLastIndex]) {
        console.log(' 2条 最后一条 ： ', new_s_date, preBarSdate, new_e_date, end, moment(end).isAfter(new_e_date), new_e_date === end, preBarEdate )// 
        const isUp = moment(end).isAfter(new_e_date) || new_e_date === end
        const isStartAfterPreStart = moment(new_s_date).isAfter(preBarSdate) || preBarSdate === new_s_date
        const isEndAfterPreEnd = moment(new_e_date).isAfter(preBarEdate) || preBarEdate === new_e_date
        txt = !(isUp && isStartAfterPreStart && isEndAfterPreEnd) && `注意：改變條碼的起止日期不正確，沒有在上一個工序的日期之前！ \n 改變條碼的起止日期 【${new_s_date} ： ${new_e_date}】 相鄰工序起止日期 【${preBarSdate} ： ${preBarEdate}】`
        console.log(' 2条 最后一条 txt： ',   txt, 'txttxt', isUp, isStartAfterPreStart, isEndAfterPreEnd, (isUp && isStartAfterPreStart && isEndAfterPreEnd) )// 
      }
      // 中间
      // else if (pnData.length > 2) {
      else if (workArr.length > 2) {
        console.log(' 中间中间 ： ', preBarEdate, preBarSdate, new_e_date, new_s_date, nextBarSdate, nextBarEdate )// 
        const isStartAfterPreStart = moment(new_s_date).isAfter(preBarSdate) || preBarSdate === new_s_date
        const isNextStartAfterStart = moment(nextBarSdate).isAfter(new_s_date) || nextBarSdate === new_s_date
        const isEndAfterPreEnd = moment(new_e_date).isAfter(preBarEdate) || preBarEdate === new_e_date
        const isNextEndAfterEnd = moment(nextBarEdate).isAfter(new_e_date) || nextBarEdate === new_e_date
        txt = !((isStartAfterPreStart && isNextStartAfterStart) && (isEndAfterPreEnd && isNextEndAfterEnd)) && `注意：改變條碼的日期範圍不正確，沒有夾在相鄰工序的起止日期之間！ 改變條碼的起止日期 【${new_s_date} ： ${new_e_date}】 \n 拆分的條碼的範圍應爲： 開始【${preBarSdate} ： ${nextBarSdate}】 結束【${preBarEdate} ： ${nextBarEdate}】`
        console.log(' 中间中间： ', isStartAfterPreStart,  isNextStartAfterStart, isEndAfterPreEnd,  isNextEndAfterEnd, txt, ((isStartAfterPreStart && isNextStartAfterStart) && (isEndAfterPreEnd && isNextEndAfterEnd)) )// 
      }
    }// 
    console.log(' txttxttxttxttxt ： ', txt, txt !== '' && txt !== false, (txt !== '' && txt !== false) && ' falsefalse  ' );// 
    // (txt !== '' && txt !== false) && confirms(2, txt, )
    return txt !== '' && txt !== false ? txt : ''
  }



  onChange = (e, key,  ) => {
    console.log('onChange v ：', e, e.target.value, key )
    const {date, filterOptionDate, } = this.state// 
    this.setState({
      filterOptionDate: filterOptionDate.map((item) => {
        // console.log(' key === item.key ? {...item, initVal: e.target.value, } : item  ： ', key, item.key, key === item.key, key === item.key ? {...item, initVal: e.target.value, } : item, item , )// 
        return key === item.key ? {...item, initVal: e.target.value, } : item  
      })
    })
  }
  filterOption = (v,  ) => {
    console.log(' filterOption ： ',  v,   )
    
    this.forms.validateFields((err, values) => {
      const {filterOptionDate, } = this.state// 
      console.log('开始过滤filterOption ：', err, values, this.state, filterOptionDate, )
      const date1 = filterOptionDate[0].initVal
      const date2 = filterOptionDate[1].initVal
      console.log(' date1 ： ', date1, date2,  )// 
      const isValid = [date1, date2, ].every((v, i) => {
        // console.log('  isValid v ： ', v, i, moment(v).isValid(),  )
        return moment(v).isValid()
      })
      console.log(' isValid isValid ： ', isValid, moment(date2).isAfter(date1) ) 
      if (!moment(date2).isAfter(date1)) {
        confirms(2, '搜索起止日期不正確，請檢查 ！！！', )
        return  
      }
      if (!isValid) {
        confirms(2, '日期非法，請重新填寫 ！！！', )
        return  
      }
      values.datefr = moment(date1).format(DATE_FORMAT_BAR)
      values.dateto = moment(date2).format(DATE_FORMAT_BAR)
      values.pn_no = values.pn_no != undefined ? values.pn_no : ''
      values.customer = values.customer != undefined ? values.customer: ''
      values.style_no = values.style_no != undefined ? values.style_no : ''
      const params = {...initParams, ...values,  } 
      console.log('values.customer.split ：', values, params, )
      // return  
      this.getManualPn(params)
    })
  }
  reasonChange = (reason,  ) => {
    const {dateForm,  } = this.state// 
    console.log(' reasonChange ： ',  reason, dateForm,   )
    this.setState({
      dateForm: {
        ...dateForm,
        reason,
      },
    })
  }
  dateFormChange = ({v, k, } ) => {
    const {dateForm,  } = this.state// 
    console.log(' dateFormChange ： ',  v, k, dateForm,   )
    this.setState({
      dateForm: {
        ...dateForm,
        [k]: v,
      },
    })
  }
  dialogContent = (v,  ) => {
    console.log(' dialogContent ： ',  v, this.state,   )
    const {pnInfo,  } = this.state// 
    // return <BarcodeInfo data={pnInfo} hideAction ></BarcodeInfo>
    return <div className="dateForm detailBox ">
      {
        manulPnDateArr[pnInfo.work_no].map((item, i) => {
          console.log(' item ： ', v, item, pnInfo, pnInfo[item], manulPnDateArr[pnInfo.work_no] )// 
          return <Row gutter={16} className='m10' key={item}  >
            <Col className={`m-b20 formLabel ${ANIMATE.slideIn}`} span={2} >
              <div className="label">
                { i === 0 ? '開始日期' : '結束日期' }
              </div>
            </Col>
            <Col span={22} >
              <div className={`form ${ANIMATE.slideIn}`}>
                <input className={`antDateInput ${i === 0 ? 'm-r5' : ''}`} 
                  type="date" defaultValue={pnInfo[item]}
                  onChange={(e) => this.dateFormChange({i, k: item, v: e.target.value,  } )} />
              </div>
            </Col>
          </Row>
        })
      }
      <Row gutter={16} className='m10'  >
        <Col className={`m-b20 formLabel ${ANIMATE.slideIn}`} span={2} >
          <div className="label">理由：
          </div>
        </Col>
        <Col span={22} >
          <div className="form">
            <TextArea className={`${ANIMATE.slideIn}`} defaultValue={''} className={`${ANIMATE.bounceIn}`} onChange={(e) => this.dateFormChange({ k: 'reason', v: e.target.value,   } )} rows={3} />
          </div>
        </Col>
      </Row>
    </div>
  }
  dateHandle = (item,  ) => {
    // console.log(' dateHandle ： ',  item,   )
    const dateArr =  [
      's_date', 'e_date', 'lastwork_e_date', 'lastwork_s_date', 'mc_e_date', 'mc_s_date', 'sew_e_date', 'sew_s_date', 'shipment_date',
    ]
    const data = {...item, }
    dateArr.forEach((v, i) => {
      // console.log(' dateArr v ： ', v, i, item[v], data  )
      data[v] = moment(item[v]).format(DATE_FORMAT_BAR)
    })
    return data 
  }
  getManualPn = (v,  ) => {
    // console.log(' getManualPn ： ',  v,   )
    getManualPn(v, ).then(res => {
      console.log('getManualPn res ：', res.data)
      const {data, } = res.data
      const noArr = []
      const pnNpData = filterTrimDatas(data, 'pn_no', )
      console.log(' getManualPn  pnNpData ： ', pnNpData,  )//
      const pnNpDataObj = {}
      const pnData = data.forEach((v) => { 
        const pn_no = v.pn_no.trim()
        // console.log('  getManualPn v ： ', v, pnNpDataObj, pn_no ) 
        // pnNpDataObj[v] != undefined ? data[v]  :
        const item = this.dateHandle(v)
        // console.log('  getManualPn v ： ', v, pnNpDataObj, item ) 
        pnNpDataObj[pn_no] = pnNpDataObj[pn_no] == undefined ? item : [...pnNpDataObj[pn_no], item,   ]
      })
      const showNo = Object.keys(pnNpDataObj)[0]
      const pnBarcodeData = pnNpDataObj[showNo]
      const showPnNoInfo = pnBarcodeData[0]
      console.log(' getManualPn data v ： ', data, pnNpDataObj, showNo, pnBarcodeData, showPnNoInfo,  ) 
      // const pnNoArr = filterArrOForm(noArr, 'pn_no')
      // const tableData = mergeArr(data, pnNoArr, 'pn_no')
      
      // const pn_no_data = new Array(...new Set(data.map((v) => v.pn_no))).map((v) => ({k: v, t: v,}));
      // const style_no_data = new Array(...new Set(data.map((v) => v.style_no))).map((v) => ({k: v, t: v,}));
      // console.log('  getManualPn pn_no_data, style_no_datas ： ',  pn_no_data,   )
      // console.log(' getManualPn  style_no_datas, ： ', style_no_data,  )

      this.setState({
        pnData: pnNpDataObj,
        showNo, 
        showPnNoInfo, 
        pnBarcodeData,
        
        // showPnNoInfo: data[0], 
        // showPnInfo: data[0], 
      })
    })
  }
  getFilter = () => {
    getFilter({}).then(res => {
      // console.log('getFilter res ：', res.data)
      const {customer, style_no} = res.data
      this.setState({
        customerData: customer, 
        styleNoData: style_no,
      })
    })
  }
  getPnList = (p) => {
    getPnList(p).then(res => {
      const {data_total,  } = res.data
      console.log(' getPnList res.data  ： ', res.data, data_total )
      this.setState({
        pnData: data_total,
      })
    })
  }
  componentDidMount() {
    const {filterInfo,  } = this.state// 
    this.getManualPn(filterInfo)
    this.getFilter()
    // this.getPnList(initParams)
  }


  render() {
    const {
      isDragPage, 
      filterInfo, 
      customerData,
      styleNoData,
      pnData,
      showPnInfo,
      title,
      pnBarcodeData,
      showNo,
      pnInfo, 
      isEditting,
      showPnNoInfo,
      barcodeData,
    } = this.state
    console.log('ManualPn 组件this.state, this.props ：', pnBarcodeData, this.state, this.props)
    return (
      <Collapses noLimit={true} animate={ANIMATE.fadeIn} title={'手動排單'} className="manualNum"
        extra={
          <div className="btnWrapper">
            <Button onClick={() => console.log('  ： ',  )} type="primary" icon='plus-circle-o'>新增手動排單</Button>
          </div>
        }
           
      >
        <div className="manulWrapper">
          <div className="filterInfoWrapper">
            <div className="dateWrapper">
              <span className='label' >起止日期: </span>
              <div className='flexCenter'>
                {
                  filterOptionDate.map((v, i) => {
                    console.log(' v.initVal ： ', v.initVal, v.key,  )// 
                    return <input className={`antDateInput ${i === 0 ? 'm-r5' : ''}`} type="date" key={v.key} defaultValue={v.initVal} onChange={(e) => this.onChange(e, v.key, )} />
                  })
                }
              </div>
            </div>
            <div className="formWrapper">
              <ProduceForm customerData={customerData} styleNoData={styleNoData} ref={forms => this.forms = forms}
                selectData={[]} formItemLayout={formItemLayout} formLayout={'inline'}
                init={filterInfo} config={filterOptionForm} className='filterForm' 
              ></ProduceForm>
            </div>
            <div className="filterBtn">
              <Button onClick={this.filterOption} icon='smile-o' className='f-r' type="primary">过滤</Button>
            </div>

          </div>


          <Row gutter={16} className='m10'  >
            <Col className={`m-b20 pnTab ${ANIMATE.slideInDown}`} xs={24} sm={4} md={4} lg={4} xl={4}>
              <div className="pnNoWrapper">
                {Object.keys(pnData).length ? Object.keys(pnData).map((v, i) => {
                  // console.log(' v, i ： ', pnData, v, i,  )// 
                  return <ClipBoard 
                  type={'div'} 
                  onClick={() => this.showPnInfo(v, pnData[v], )} 
                  key={v} 
                  className={`pnNoItem ${v === showNo ? 'activePnNo' : ''}`} 
                  >{v}</ClipBoard> 
                }) : ''}
              </div>  
            </Col>
            <Col xs={24} sm={20} md={20} lg={20} xl={20}>
              <div className={`m-b20 artlineWrapper barcodeInfoWrapper ${ANIMATE.slideInDown}`}>
                <BarcodeInfo data={showPnNoInfo} hideAction ></BarcodeInfo>
              </div>
            </Col>
            <Col xs={24} sm={20} md={20} lg={20} xl={20}>
              <div className={`m-b20 ${ANIMATE.slideInUp}`}>
                <ManualBarcodeTable
                  data={pnBarcodeData}
                  saveEdit={this.saveEdit}
                  startEditDay={this.startEditDay}
                  isEditting={isEditting}
                  toggleLock={this.toggleLock}
                >
                </ManualBarcodeTable>
              </div>

            </Col>
          </Row>

          <div className="pnWrapper">
          
            {/* <div className="pnNoWrapper pnItem">
              <PnNoTable
                data={pnData}
                startEdit={this.startEdit}
                showPnInfo={this.showPnInfo}
              >
              </PnNoTable>
            </div> */}
            {/* <div className="barcodeWrapper pnItem">
              <ManualBarcodeTable
                data={pnBarcodeData}
                saveEdit={this.saveEdit}
                startEditDay={this.startEditDay}
              >
              </ManualBarcodeTable>
            </div> */}
          </div>
                
          {
            showPnInfo ? <ProductDialog handleOk={this.handleOk} 
            //  width={'90%'} 
             width={'60%'} 
             show={showPnInfo} 
             close={this.closeDialog} 
             title={title}>
             {this.dialogContent()}
           </ProductDialog> : null
          }
          
        </div>
      </Collapses>
    )
  }
}

// export default ManualPn
export default SecureHoc(ManualPn)
