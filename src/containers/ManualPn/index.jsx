import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getManualPn, getPnBarcode, } from 'api/ManualPn'
import { getPnList, getFilter, splitPnBarcode, barcodeLock,  } from 'api/ppc'
import { autoPpc,   } from 'api/ArtLine'
import {bacodeInfoWithDate, pnDateArr, DATE_FORMAT_BAR, manualNumForm, initParams, manaulPnForm, filterOptionDate, filterOptionForm, } from 'config'
import { ANIMATE, SELECT_TXT, defaultWorkNo, } from 'constants'
import {openNotification, backupFn, confirms, filterArrOForm, mergeArr, filterTrimDatas,   } from 'util'
import Collapses from '@@@/Collapses'
import ProductDialog from '@@@/ProductDialog'
import ProduceForm from '@@@/ProduceForm'
import EditInfo from '@@@@/EditInfo'
import SecureHoc from '@@@@/SecureHoc'
import BarcodeInfo from '@@@@/BarcodeInfo'
import BarcodeTable from '@@@@/BarcodeTable'
// import PnNoTable from '@@@/PnNoTable'
import ManualBarcodeTable from '@@@/ManualBarcodeTable'
import ClipBoard from '@@@@/ClipBoard'//
import { Button, Icon, Select, message, Row, Col, Input, } from 'antd'
import moment from 'moment'
// import ppcList from './ppcList.json'
const Option = Select.Option
const { TextArea } = Input
const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
}

class ManualPn extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    this.state = {
      show: false,
      title: '提示框',
      // pnData: ppcList.data_total,
      // pnBarcodeData: ppcList.datas_barcode.slice(0, 20),
      pnBarcodeData: [],
      filterInfo: manaulPnForm,
      showPnInfo: false,
      filterOptionDate,
      showNo: '',
      customerData: [],
      styleNoData: [],
      isEditting: false, 
      pnData: {},
      barcodeData: [],
      dateForm: {
        reason_remark: '',
        
      },
      pnInfo: {}, 
      showPnIndex: 0, 
      editBarIndex: 0, 
      reasonArr: [],
      
      
      
    }
  }
  showPnInfo = (t, v, i, ) => {
    console.log(' showPnInfo ： ',  t, v, i,  )
    const {pnData,  } = this.state// 
    this.setState({
      // pnInfo: v, 
      pnInfo: this.infoDateHandle(v),
      showNo: t, 
      pnBarcodeData: v.data.map((item) => this.dateHandle(item, pnData[i], )).sort((a, b) => a.work_no - b.work_no), 
      showPnIndex: i, 
    })
  }
  closeDialog = (v,  ) => {
    console.log(' closeDialog ： ',  v,   )
    this.setState(this.closeInfo())
  }
  closeInfo = (v,  ) => {
    console.log(' closeInfo ： ',  v,   )
    return {
      showPnInfo: false,
      isEditting: false,
      // pnInfo: {}, 
      title: '提示框',
      
    } 
  }
  startEditDay = (t, v, i, index, ) => {
    const {pnBarcodeData, isEditting, pnData, reasonArr,  } = this.state// 
    console.log(' startEditDay ： ',  t, v, i, index, pnBarcodeData, isEditting, this.state )
    if (v.lock === 'T') {
      confirms(2, '請先解鎖再繼續操作！', )
      return  
    }
    if (isEditting) {
      confirms(2, '請先保存上一條數據再繼續操作！', )
      return  
    }
    const pnInfo = this.infoDateHandle(this.dateHandle(v, pnData[index], ))
    const {work_no,  } = pnInfo 
    
    console.log(' pnInfo ： ', pnInfo,  )// 
    this.setState({
      showPnInfo: true,
      isEditting: true,
      // pnInfo: v,
      pnInfo,
      dateForm: {
        ...pnInfo,
        reason_id: reasonArr.filter(v => v.work_no === work_no)[0].reason_id,
        reason_remark: '',
      },
      editBarIndex: i, 
      title: `批號：${v.pn_no} - 條碼：${v.barcode}`,
    })
  }
  toggleLock = (t, v, i) => {
    // return  
    const lock = v.lock !== 'T' ? 'T' : 'F'
    console.log(' toggleLock ： ', t, v, i, lock,   )// 
    barcodeLock({barcode: v.barcode, lock,}).then(res => {
      console.log(' barcodeLock  res.data ： ', res.data,  )// 
      const {code,  } = res.data
      if (code === 1) {
        const {pnBarcodeData, pnData, showPnIndex,  } = this.state// 
        const pnDatas = [...pnData]
        const pnBarcodeDatas = [...pnBarcodeData]
        pnDatas[showPnIndex].data[i].lock = lock
        pnBarcodeDatas[i].lock = lock
        console.log(' pnDatas ： ', pnDatas,  )// 
        this.setState({
          pnData: pnDatas,
          pnBarcodeData: pnBarcodeDatas,
        })
      }
    })
  }
  handleOk = (v,  ) => {
    const {pnInfo, dateForm,  } = this.state// 
    console.log(' handleOk ： ',  v, dateForm, pnInfo, this.state, this.props,   )
    if (Object.keys(pnInfo).length) {
      this.editPnBarcode()
    }
  }
    
  editPnBarcode = (isLock, ) => {
    const { 
      dateForm,
      pnInfo,
      showPnIndex,
    } = this.state
    const {barcode, s_date, e_date,  } = dateForm

    console.log(' editPnBarcode ： ', isLock, pnInfo, dateForm, barcode, this.state,   )
    const isRight = [s_date, e_date, ].every((v, i) => {
      const isValid = moment(v).isValid()
      const isNoEmpty = v !== ''
      // console.log(' pnDateArr v ： ', v, i, isValid, isNoEmpty, )
      return isValid && isNoEmpty
    })
    console.log(' isRight ： ', moment("not a real date").isValid(), isRight,    )//
    // 修改的起止日期是否正确
    if (!moment(e_date).isAfter(s_date) &&  e_date !== s_date) {
      confirms(2, '修改的起止日期不正確，請檢查 !', )
      return  
    }
    const validTxt = this.dateRange({barcodeData: pnInfo, pnItem: showPnIndex, })// 
    console.log(' s_date, e_date, ： ', s_date, e_date, validTxt )// 
    if (validTxt !== '' ) {
      confirms(2, validTxt, )
      return  
    }
    // this.dateRange('2019-02-04', '2019-02-16', )//
    const barcodeItem = {...backupFn(pnInfo), ...dateForm, }
    barcodeItem.barcode = `${barcode.trim()}A`
    barcodeItem.color = []
    console.log(' 正確正確 ： ', barcodeItem, pnInfo, { p_barcode: barcode, data: barcodeItem, } )// 
    const params = { pn_no: pnInfo.pn_no, p_barcode: barcode, data: [barcodeItem], lock: isLock ? 'T' : 'F', batch_status: true,  }
    console.log(' { pn_no: barcode, data: barcodeData,  } ： ', params, barcode,  )// 
    // return  
    this.splitBarcode(params)
  }
  isQtyEmpty = (params,   ) => {
    console.log('    isQtyEmpty ： ', params,    )
    const isInvalid = params.data.some(v => v.qty < 1);
    if (isInvalid) confirms(2, '拆分的條碼數量不能爲0',   )
    return isInvalid 
  }
  splitBarcode = (p) => {// 
    console.log(' splitBarcode ： ',  p, this.state,  )

    const isQtyEmpty = this.isQtyEmpty(p)
    console.log('  对吗  isQtyEmpty ', isQtyEmpty,    )
    if (isQtyEmpty) return

    // return  
    splitPnBarcode(p).then(res => {
      console.log(' splitPnBarcode  res.data ： ', res.data,  )// 
      const {code, datas, mes, newBarcode} = res.data
      const {pnData, pnBarcodeData, showPnIndex, editBarIndex,  } = this.state// 
      const pnDatas = backupFn(pnData)
      pnDatas[showPnIndex].data[editBarIndex] = p.data[0]
      if (code === 1) {
        this.setState({
          pnData: pnDatas,
          pnBarcodeData: pnBarcodeData.map((v) => v.barcode.trim() == p.p_barcode.trim() ? p.data[0] : v),
          ...this.closeInfo(),
        })
      }
    })
  } 
    
  mxDateFilter = (data, k, x,  ) => {
    // console.log(' mxDateFilter  ： ', data, k, x,   )// 
    return moment[x](data.map(v => moment(v[k]))).format(DATE_FORMAT_BAR)
  }
  workFilter = (data, k, item, ) => {
    console.log(' workFilter  ： ', data, k, item,  )
    return data.filter(v => v[k] === item)
  }
  dateRange = ({barcodeData, pnItem, isDelete, }) => {
    const { pnData, pnBarcodeData, pnInfo, dateForm, editBarIndex, } = this.state
    console.log(' dateRange ： ', barcodeData, pnItem, isDelete, this.state, this.props,   )// 
    const {barcode, shipment_date, s_date, e_date, work_no,  } = barcodeData
    // const {workArr, } = pnInfo
    const workArr = pnInfo.workArr.sort((a, b) => a - b)
    const pnDatas = backupFn(pnBarcodeData)
    pnDatas[editBarIndex].s_date = dateForm.s_date 
    pnDatas[editBarIndex].e_date = dateForm.e_date
    const sameWorkData = pnDatas.filter((v, i) => v.work_no === work_no)
    console.log(' pnDatas ： ', pnDatas, editBarIndex, sameWorkData, barcode)// 
    const workIndex = workArr.findIndex((v, i) => v === work_no)
    const sameWorkDate = isDelete ? sameWorkData.filter((v, i) => v.barcode.trim() !== barcode.trim()) : sameWorkData
    console.log(' editBarIndex,  ： ', workIndex, editBarIndex, sameWorkData, sameWorkDate,   )//  
    const new_s_date = this.mxDateFilter(sameWorkDate, 's_date', 'min', )
    const new_e_date = this.mxDateFilter(sameWorkDate, 'e_date', 'max', )
    console.log(' 條碼的 new_s_date new_e_date ： ', new_s_date, new_e_date, )// 
    const workLastIndex = workArr.length - 1
    const preIndex = editBarIndex - 1
    const nextIndex = editBarIndex + 1
    const preWIndex = workIndex - 1
    const nextWIndex = workIndex + 1
    const workPIndex = workArr[preWIndex]
    const workNIndex = workArr[nextWIndex]
    const preBarArr = this.workFilter(pnDatas, 'work_no', workPIndex,  )
    const nextBarArr = this.workFilter(pnDatas, 'work_no', workNIndex,  )
    const preBarSdate = this.mxDateFilter(preBarArr, 's_date', 'min', )
    const preBarEdate = this.mxDateFilter(preBarArr, 'e_date', 'max', )
    const nextBarSdate = this.mxDateFilter(nextBarArr, 's_date', 'min', )
    const nextBarEdate = this.mxDateFilter(nextBarArr, 'e_date', 'max', )
    const preBar = pnDatas[preIndex]
    const nextBar = pnDatas[nextIndex]
    const realEnd = moment(shipment_date).isAfter(e_date)
    const end = realEnd ? shipment_date : e_date
    console.log(' preBar, nextBar, pnDatas, editBarIndex, shipment_date, e_date, realEnd, end ： ', pnInfo, workArr, work_no, workPIndex, workNIndex, preBar, nextBar, pnDatas, editBarIndex, shipment_date, e_date, realEnd, end, preIndex, nextIndex,  )// 
    let txt 
    // 只有1条的情况
    if (pnDatas.length === 1) {
    // if (workArr.length === 1) {
      const isUp =moment(end).isAfter(new_e_date) || new_e_date === end
      txt = !isUp && '注意：條碼的结束日期不能小于货期！'
      console.log(' 只有1条的情况 ：txt ',  txt, shipment_date, e_date, new_e_date, end, moment(end).isAfter(new_e_date), isUp)// 
    }
    if (workArr.length === 1) {
      console.log(' workArrworkArr ： ',    )// 
      return ''
    }
    
    // 多条 
    // if (pnDatas.length > 1) {
    if (workArr.length > 1) {
      // // 2条 第一条 
      // if (editBarIndex === 0) {
      if (work_no === workArr[0]) {
        console.log(' 2条 第一条 ： ', new_s_date, nextBarSdate, new_e_date, nextBarEdate )// 
        const isNextStartAfterStart = moment(nextBarSdate).isAfter(new_s_date) || nextBarSdate === new_s_date
        const isNextEndAfterEnd = moment(nextBarEdate).isAfter(new_e_date) || nextBarEdate === new_e_date
        txt = !(isNextStartAfterStart && isNextEndAfterEnd) && `注意：改變條碼的起止日期不正確，沒有在下一個工序的日期之前！ \n 改變條碼的起止日期 【${new_s_date} ： ${new_e_date}】 相鄰工序起止日期 【${nextBarSdate} ： ${nextBarEdate}】`
        console.log(' 2条 第一条 txt： ',  txt, isNextStartAfterStart, isNextEndAfterEnd, (isNextStartAfterStart && isNextEndAfterEnd) )// 
      }
      // // 2条 最后一条 
      // else if (editBarIndex === (pnDatas.length  ：  1)) {
      else if (work_no === workArr[workLastIndex]) {
        console.log(' 2条 最后一条 ： ', new_s_date, preBarSdate, new_e_date, end, moment(end).isAfter(new_e_date), new_e_date === end, preBarEdate )// 
        const isUp = moment(end).isAfter(new_e_date) || new_e_date === end
        const isStartAfterPreStart = moment(new_s_date).isAfter(preBarSdate) || preBarSdate === new_s_date
        const isEndAfterPreEnd = moment(new_e_date).isAfter(preBarEdate) || preBarEdate === new_e_date
        txt = !(isUp && isStartAfterPreStart && isEndAfterPreEnd) && `注意：改變條碼的起止日期不正確，沒有在上一個工序的日期之前！ \n 改變條碼的起止日期 【${new_s_date} ： ${new_e_date}】 相鄰工序起止日期 【${preBarSdate} ： ${preBarEdate}】`
        console.log(' 2条 最后一条 txt： ',   txt, 'txttxt', isUp, isStartAfterPreStart, isEndAfterPreEnd, (isUp && isStartAfterPreStart && isEndAfterPreEnd) )// 
      }
      // 中间
      // else if (pnDatas.length > 2) {
      else if (workArr.length > 2) {
        console.log(' 中间中间 ： ', preBarEdate, preBarSdate, new_e_date, new_s_date, nextBarSdate, nextBarEdate )// 
        const isStartAfterPreStart = moment(new_s_date).isAfter(preBarSdate) || preBarSdate === new_s_date
        const isNextStartAfterStart = moment(nextBarSdate).isAfter(new_s_date) || nextBarSdate === new_s_date
        const isEndAfterPreEnd = moment(new_e_date).isAfter(preBarEdate) || preBarEdate === new_e_date
        const isNextEndAfterEnd = moment(nextBarEdate).isAfter(new_e_date) || nextBarEdate === new_e_date
        txt = !((isStartAfterPreStart && isNextStartAfterStart) && (isEndAfterPreEnd && isNextEndAfterEnd)) && `注意：改變條碼的日期範圍不正確，沒有夾在相鄰工序的起止日期之間！ 改變條碼的起止日期 【${new_s_date} ： ${new_e_date}】 \n 拆分的條碼的範圍應爲： 開始【${preBarSdate} ： ${nextBarSdate}】 結束【${preBarEdate} ： ${nextBarEdate}】`
        console.log(' 中间中间： ', isStartAfterPreStart,  isNextStartAfterStart, isEndAfterPreEnd,  isNextEndAfterEnd, 'txttxt', txt, ((isStartAfterPreStart && isNextStartAfterStart) && (isEndAfterPreEnd && isNextEndAfterEnd)) )// 
      }
    }// 
    console.log(' txttxttxttxttxt ： ', txt, txt !== '' && txt !== false, (txt !== '' && txt !== false) && ' falsefalse  ' );// 
    // (txt !== '' && txt !== false) && confirms(2, txt, )
    return txt !== '' && txt !== false ? txt : ''
  }


  autoPpc = () => {
    const {showNo, } = this.state
    console.log('  autoPpc ：', showNo, this.state )
    autoPpc({pn_no: showNo}).then(res => {
      console.log('autoPpc res ：', res.data);
    })
  }

  onChange = (e, key,  ) => {
    console.log('onChange v ：', e, e.target.value, key )
    const {date, filterOptionDate, } = this.state// 
    this.setState({
      filterOptionDate: filterOptionDate.map((item) => {
        // console.log(' key === item.key ? {...item, initVal: e.target.value, } : item  ： ', key, item.key, key === item.key, key === item.key ? {...item, initVal: e.target.value, } : item, item , )// 
        return key === item.key ? {...item, initVal: e.target.value, } : item  
      })
    })
  }
  filterOption = (v,  ) => {
    console.log(' filterOption ： ',  v,   )
    
    this.forms.validateFields((err, values) => {
      const {filterOptionDate, } = this.state// 
      console.log('开始过滤filterOption ：', err, values, this.state, filterOptionDate, )
      const date1 = filterOptionDate[0].initVal
      const date2 = filterOptionDate[1].initVal
      console.log(' date1 ： ', date1, date2,  )// 
      const isValid = [date1, date2, ].every((v, i) => {
        // console.log('  isValid v ： ', v, i, moment(v).isValid(),  )
        return moment(v).isValid()
      })
      console.log(' isValid isValid ： ', isValid, moment(date2).isAfter(date1) ) 
      if (!moment(date2).isAfter(date1)) {
        confirms(2, '搜索起止日期不正確，請檢查 ！！！', )
        return  
      }
      if (!isValid) {
        confirms(2, '日期非法，請重新填寫 ！！！', )
        return  
      }
      values.shipment_datefr = moment(date1).format(DATE_FORMAT_BAR)
      values.shipment_dateto = moment(date2).format(DATE_FORMAT_BAR)
      values.pn_no = values.pn_no != undefined ? values.pn_no : ''
      values.customer = values.customer != undefined ? values.customer: ''
      values.style_no = values.style_no != undefined ? values.style_no : ''
      const params = {...initParams, ...values,  } 
      console.log('values.customer.split ：', values, params, )
      // return  
      this.getManualPn(params)
    })
  }
  dateFormChange = ({v, k, } ) => {
    const {dateForm,  } = this.state// 
    console.log(' dateFormChange ： ',  v, k, dateForm,   )
    this.setState({
      dateForm: {
        ...dateForm,
        [k]: k === 'reason_id' ? Number(v) : v,
      },
    })
  }
  dialogContent = (v,  ) => {
    const {pnInfo, reasonArr,  } = this.state// 
    const {work_no,  } = pnInfo
    const reasons = reasonArr.filter(v => v.work_no === work_no)
    console.log(' dialogContent ： ',  v, this.state, pnInfo, work_no,   )
    // return <BarcodeInfo data={pnInfo} hideAction ></BarcodeInfo>
    return <div className="dateForm detailBox ">
      {
        pnDateArr.map((item, i) => {
          console.log(' item ： ', v, item, pnInfo, pnInfo[item], pnDateArr )// 
          return <Row gutter={16} className='m10' key={item}  >
            <Col className={`m-b20 formLabel ${ANIMATE.slideIn}`} span={4} >
              <div className="label">
                { i === 0 ? '開始日期' : '結束日期' }
              </div>
            </Col>
            <Col span={20} >
              <div className={`form ${ANIMATE.bounceIn}`}>
                <input className={`antDateInput ${i === 0 ? 'm-r5' : ''}`} 
                  type="date" defaultValue={pnInfo[item]}
                  onChange={(e) => this.dateFormChange({i, k: item, v: e.target.value,  } )} />
              </div>
            </Col>
          </Row>
        })
      }
      <Row gutter={16} className='m10'  >
        <Col className={`m-b20 formLabel ${ANIMATE.bounceIn}`} span={4} >
          <div className="label">理由：
          </div>
        </Col>
        <Col span={20} >
          <div className="form">
            <Select 
              //size='small' 
              // mode="combobox"work_no
              showSearch
              allowClear={true}
              optionFilterProp="children"
              defaultValue={reasons[0].reason_name}
              onChange={
                (reason_id) => this.dateFormChange({ k: 'reason_id', v: reason_id,   } )
              }
              filterOption={this.filterOption}
            > 
              {reasonArr.length ? reasons.map((v, i) => {
                return <Option key={v.reason_id} title={`${v.reason_id}`}>{v.reason_name}</Option>
              }) : null}
            </Select>
          </div>
        </Col>
      </Row>
      <Row gutter={16} className='m10'  >
        <Col className={`m-b20 formLabel ${ANIMATE.bounceIn}`} span={4} >
          <div className="label">理由：
          </div>
        </Col>
        <Col span={20} >
          <div className="form">
            <TextArea defaultValue={''} 
              className={`${ANIMATE.bounceIn}`} 
              onChange={(e) => this.dateFormChange({ k: 'reason_remark', v: e.target.value,   } )} rows={3} 
            />
          </div>
        </Col>
      </Row>
    </div>
  }
  dateHandle = (item, info,  ) => {
    const {data, ...extra } = info
    console.log(' dateHandle ： ',  item, info, extra  )
    // const dateArr =  [
    //   's_date', 'e_date', 'lastwork_e_date', 'lastwork_s_date', 'mc_e_date', 'mc_s_date', 'sew_e_date', 'sew_s_date', 'shipment_date',
    // ]
    const dateArr =  [
      's_date', 'e_date', 
    ]
    const newData = {...item, ...extra, }
    dateArr.forEach((v, i) => {
      // console.log(' dateArr v ： ', v, i, item[v], newData  )
      newData[v] = moment(item[v]).format(DATE_FORMAT_BAR)
    })
    console.log(' dateHandle ： ', newData   )// 
    return newData 
  }
  infoDateHandle = (item, info = {},  ) => {
    const {data, ...extra } = info
    console.log(' infoDateHandle ： ',  item, info, extra  )
    const dateArr =  [
      's_date', 'e_date', 'lastwork_e_date', 'lastwork_s_date', 'mc_e_date', 'mc_s_date', 'sew_e_date', 'sew_s_date', 'shipment_date',
    ]
    const newData = {...item, ...extra, }
    dateArr.forEach((v, i) => {
      // console.log(' dateArr v ： ', v, i, item[v], newData  )
      newData[v] = moment(item[v]).format(DATE_FORMAT_BAR)
    })
    console.log(' infoDateHandle ： ', newData   )// 
    return newData 
  }

  getManualPn = (v,  ) => {
    // console.log(' getManualPn ： ',  v,   )
    getManualPn(v, ).then(res => {
      console.log('getManualPn res ：', res.data, )  
      const {hdr, dtl, code, reason,  } =  res.data
      if (code === 1) {
        if (dtl.length) {
          hdr.forEach((item, i) => {
            // console.log('item ：', item)
            const factoryArr = []
            const workArr = []
            const data = dtl.filter(v => {
              if (item.pn_no === v.pn_no) {
                factoryArr.push(v.factory_id)
                workArr.push(v.work_no)
              }
              return item.pn_no === v.pn_no
            }).sort((a, b) => a.work_no - b.work_no)
            const factoryWipe = Array.from(new Set(factoryArr))
            const workWipe = Array.from(new Set(workArr))
            item.indexes = i
            item.data = data
            item.factoryArr = factoryWipe
            item.workArr = workWipe
          })
          console.log(' hdrhdrhdr ： ', hdr,  )// 
          const pnInfo = this.infoDateHandle(hdr[0])
          const showNo = hdr[0].pn_no
          const pnBarcodeData = hdr[0].data.map((item) => this.dateHandle(item, hdr[0], )).sort((a, b) => a.work_no - b.work_no)// 
          console.log(' getManualPn data v ： ', showNo, pnBarcodeData, pnInfo,  ) 
          this.setState({
            pnData: hdr,
            showNo, 
            pnInfo, 
            pnBarcodeData,
            reasonArr: reason, 

          })
        } else {
          confirms(2, '沒有數據 o(╥﹏╥)o')
        }
      }
    })
  }

  getFilter = () => {
    getFilter({}).then(res => {
      // console.log('getFilter res ：', res.data)
      const {customer, style_no} = res.data
      this.setState({
        customerData: customer, 
        styleNoData: style_no,
      })
    })
  }
  componentDidMount() {
    const {filterInfo,  } = this.state// 
    this.getManualPn(filterInfo)
    this.getFilter()
  }


  render() {
    const {
      isDragPage, 
      filterInfo, 
      customerData,
      styleNoData,
      pnData,
      showPnInfo,
      title,
      pnBarcodeData,
      showNo,
      pnInfo, 
      isEditting,
      barcodeData,
      showPnIndex,
    } = this.state
    console.log('ManualPn 组件this.state, this.props ：', pnBarcodeData, this.state, this.props)
    return (
      <Collapses noLimit={true} animate={ANIMATE.fadeIn} title={'批量復期'} className="manualNum"
        extra={
          <div className="btnWrapper">
            <Button onClick={this.autoPpc} type="primary" icon='plus-circle-o'>重新自動排單</Button>
          </div>
        }
      >
        <div className="manulWrapper">
          <div className="filterInfoWrapper">
            <div className="dateWrapper">
              <span className='label' >起止日期: </span>
              <div className='flexCenter'>
                {
                  filterOptionDate.map((v, i) => {
                    // console.log(' v.initVal ： ', v.initVal, v.key,  )// 
                    return <input className={`antDateInput ${i === 0 ? 'm-r5' : ''}`} type="date" key={v.key} defaultValue={v.initVal} onChange={(e) => this.onChange(e, v.key, )} />
                  })
                }
              </div>
            </div>
            <div className="formWrapper">
              <ProduceForm customerData={customerData} styleNoData={styleNoData} ref={forms => this.forms = forms}
                selectData={[]} formItemLayout={formItemLayout} formLayout={'inline'}
                init={filterInfo} config={filterOptionForm} className='filterForm' 
              ></ProduceForm>
            </div>
            <div className="filterBtn">
              <Button onClick={this.filterOption} icon='smile-o' className='f-r' type="primary">过滤</Button>
            </div>
          </div>

          <Row gutter={16} className='m10'  >
            <Col className={`m-b20 pnTab ${ANIMATE.slideInDown}`} xs={24} sm={4} md={4} lg={4} xl={4}>
              <div className="pnNoWrapper">
                {pnData.length ? pnData.map((v, i) => {
                  // console.log(' v, i ： ', pnData, v, i,  )// 
                  return <ClipBoard 
                  type={'div'} 
                  onClick={() => this.showPnInfo(v.pn_no, pnData[i], i, )} 
                  key={v.pn_no} 
                  className={`pnNoItem ${v.pn_no === showNo ? 'activePnNo' : ''}`} 
                  >{v.pn_no}</ClipBoard> 
                }) : null}
              </div>  
            </Col>
            <Col xs={24} sm={20} md={20} lg={20} xl={20}>
              <div className={`m-b20 artlineWrapper barcodeInfoWrapper ${ANIMATE.slideInDown}`}>
                <BarcodeInfo bacodeInfoConfig={bacodeInfoWithDate} data={pnInfo} hideAction ></BarcodeInfo>
              </div>
            </Col>
            <Col xs={24} sm={20} md={20} lg={20} xl={20}>
              <div className={`m-b20 ${ANIMATE.slideInUp}`}>
              {
                pnData.length ? 
                <ManualBarcodeTable
                  // data={pnData[showPnIndex].data}
                  data={pnBarcodeData}
                  saveEdit={this.saveEdit}
                  startEditDay={this.startEditDay}
                  isEditting={isEditting}
                  toggleLock={this.toggleLock}
                  showPnIndex={showPnIndex}
                >
                </ManualBarcodeTable>
                : null
              }
              </div>

            </Col>
          </Row>
                
          {
            showPnInfo ? <ProductDialog handleOk={this.handleOk} 
            //  width={'90%'} 
             width={'60%'} 
             show={showPnInfo} 
             close={this.closeDialog} 
             title={title}>
             {this.dialogContent()}
           </ProductDialog> : null
          }
          
        </div>
      </Collapses>
    )
  }
}

// export default ManualPn
export default SecureHoc(ManualPn)
