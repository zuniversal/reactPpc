import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"

class Root extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
    }
  }

  render() {
    return (
      <div className="root">
        {this.props.children}
      </div>
    );
  }
}

export default Root;