import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import {copy} from 'actions/login.js'
import { getArtLine, editArtLine, autoPpc,  } from 'api/ArtLine'
import { ANIMATE, IMAGE_PREFIX,  } from 'constants'
import { stampToLocale, mergeArr, filterArrOForm, backupFn, confirms, imgFilter, } from 'util'
import Collapses from '@@@/Collapses'
// import ProduceForm from '@@@/ProduceForm'
import ArtLineTable from '@@@/ArtLineTable'
import ProductDialog from '@@@/ProductDialog'
import Count from '@@@@/Count'
import EditInfo from '@@@@/EditInfo'
import ClipBoard from '@@@@/ClipBoard'// 
import ClipBoards from '@@@@/ClipBoards'
import { Row, Col, Icon, Select, Button, message, } from 'antd';
const Option = Select.Option;


class ArtLine extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      // 无用的数据
      pnNoArr: [],
      showNoData: [],
      workArr: [],
      tableData: [],
      editOrigin: {},
      show: false,
      isFirst: true,
    }
  }
  showDialog = () => this.setState({ show: true, })
  close = () => {
    console.log('取消 ：', )
    this.cancelData()
  }
  cancelData = () => {
    const {showNoData, editOrigin, } = this.state
    const {work_no, } = editOrigin
    const indexes = showNoData.findIndex(v => v.work_no === work_no)
    showNoData[indexes] = editOrigin
    console.log('取消修改 ：', this.state, indexes, showNoData, );
    this.setState({
      showNoData: [...showNoData],
      editOrigin: {},
      show: false,
    })
  }
  editContent = () => {
    const {editOrigin, } = this.state
    console.log('editContent ：', editOrigin, )
    return <EditInfo/>
  }
  handleOk = () => {
    console.log('handleOk ：', this.state)
    this.editArtLine()
  }
  autoPpc = () => {
    console.log('  autoPpc ：', this.state )
    const {showNo, } = this.state
    this.editArtLine()
    autoPpc({pn_no: showNo}).then(res => {
      console.log('editArtLine res ：', res.data);
      const {code, mes, } = res.data
      // message.success(mes, )
    })
  }
  editArtLine = () => {
    console.log('editArtLine ：', )
    const {showNoData, editOrigin, workArr, } = this.state
    const {work_no, pn_no, } = editOrigin
    const indexes = showNoData.findIndex(v => v.work_no === work_no)
    console.log('showNoData[indexes] ：', indexes, showNoData[indexes]);
    const {dev_days, remark, } = showNoData[indexes]
    editArtLine({work_no, pn_no, dev_days: Number(dev_days), remark,}).then(res => {
      console.log('editArtLine res ：', res.data);
      const {code, mes, } = res.data
      // message.success(mes, )
      if (code === 1) {
        // const {showNoData, workArr, } = this.state
        showNoData[indexes].editable = false
        workArr[indexes].dev_days = dev_days
        this.setState({
          showNoData: [...showNoData], 
          workArr: [...workArr], 
          editOrigin: {},
          show: false,
        })
      }
    })
  }
  selectChange = (o, k, ) => {
    console.log(`selectChange`, o, k, );
  }
  onSelect = (o, k, ) => {
    console.log(`onSelect`, o, k, );
    this.showDetial(o)
  }
  saveData = (t, v, i) => {
    console.log('Artline saveData t, v, i：', t, v, i,);
  }
  editData = (value) => {
    console.log('Artline接收数据变化 value：', value, this.state, );
    const {showNoData, editOrigin, } = this.state
    const {keys, v, } = value
    const {work_no, } = editOrigin
    showNoData.map((item, i) => {
      if (item.work_no === work_no) {
        console.log(' showNoData item ：', keys, v, item, i, work_no)
        item[keys] = v
      } 
      return v
    })
    this.setState({
      showNoData: [...showNoData], 
    })
  }
  editDataAction = (v, o, i) => {
    console.log('editDataAction v, o, i：', v, o, i, this.state, );
    const {showNoData, editOrigin, } = this.state
    const item = backupFn(o)
    if (Object.keys(editOrigin).length) {
      confirms(2, '請先保存上一條數據再繼續編輯！', )
    } else {
      showNoData.forEach((v, index) => {
        console.log(' showNoData v ：', v, i, index)
        if (index === i) {
          v.editable = true
        } 
      })
      this.setState({
        showNoData: [...showNoData], 
        editOrigin: item,
      })
    }
  }
  showDetial = (v) => {
    console.log('showDetial v ：', v, this.state);
    const {data, pnNoArr, showNo, tableData, isFirst, } = this.state
    console.log('showDetial data, pnNoArr, showNo 1：', data, pnNoArr, showNo, tableData);
    const showNoData = pnNoArr.filter(item => item.pn_no === v)[0].data
    const workArr = []
    showNoData.forEach(v => {
      console.log(' `file://${v.file_name1}` ： ', `file://${v.file_name1}`,  )
      // const ss = `file://${v.file_name1}`
      // const src = window.URL.createObjectURL(ss);
  
      // console.log('v ：', v);
      // workArr.push({ work_no: v.work_no, work_days: v.work_days, work_name: v.work_name, dev_days: v.dev_days, file_name1: v.dev_days })
      workArr.push({...v
        // , src
      })
    })
    !isFirst && this.props.copy(v)
    console.log('showDetial data, pnNoArr, showNo 2：', data, pnNoArr, showNo, this.props );
    this.setState({ 
      showNo: v,
      workArr, 
      showNoData,
      isFirst: false,
      copy: v,
    })
  }
  getArtLine = () => {
    getArtLine({}).then(res => {
      console.log('getArtLine res ：', res.data);
      const { data } = res.data
      const noArr = []
      data.map((v, i) => {
        v.editable = false
        noArr.push(v.pn_no)
        return v
      })
      const pnNoArr = filterArrOForm(noArr, 'pn_no')
      const tableData = mergeArr(data, pnNoArr, 'pn_no')
      this.setState({
        data,
        pnNoArr,
        tableData,
        showNo: noArr[0],
      })
      this.showDetial(noArr[0])
    })
  }
  componentDidMount() {
    this.getArtLine()
  }
  render() {
    console.log('ArtLine 组件this.state, this.props ：', this.state, this.props);
    const { pnNoArr, showNoData, workArr, showNo, tableData, show, company_id, copy, } = this.state
    console.log('showNoData[0] ：', showNoData[0]);
    const title = '提示框'

    return (
      <section className="artLine">
        {/* <ClipBoard></ClipBoard> */}
        
        {/* <ClipBoards copy={copy}> */}
        <Collapses noLimit title={'工藝路線'} isAnimate={false}
          extra={
            <div className="btnWrapper">
              {
                tableData.length ? (
                  <Select 
                    showSearch
                    allowClear={true}
                    showArrow={true}
                    //defaultValue={tableData[0].pn_no} 
                    value={showNo} 
                    onChange={this.selectChange.bind(this, )} 
                    onSelect={this.onSelect.bind(this, )}
                    className="pnSelect"
                  >
                    {tableData.map((v, i) => <Option value={v.pn_no} key={i}>{v.pn_no}</Option>)}
                  </Select>
                ) : null
              }
            </div>
          }
        >
          <Row gutter={16}>
            <Col className={`m-b20 pnTab ${ANIMATE.slideInDown}`} xs={24} sm={4} md={4} lg={4} xl={4}>
              <div className="pnNoWrapper">
              {tableData.length ? tableData.map((v, i) => <ClipBoard type={'div'} onClick={this.showDetial.bind(this, v.pn_no)} key={v.pn_no} className={`pnNoItem ${v.pn_no === showNo ? 'activePnNo' : ''}`} >{v.pn_no}</ClipBoard>) : ''}
              
              
              {/* <ClipBoard text={v.pn_no}>{v.pn_no}</ClipBoard> */}
                {/* {tableData.length ? tableData.map((v, i) => <div onClick={this.showDetial.bind(this, v.pn_no)} key={v.pn_no} className={`pnNoItem ${v.pn_no === showNo ? 'activePnNo' : ''}`}>
                  
                  {v.pn_no}
                  </div>) : ''} */}
              </div>
            </Col>
            <Col xs={24} sm={20} md={20} lg={20} xl={20}>
              <div className={`m-b20 artlineWrapper ${ANIMATE.slideInDown}`}>
                {showNoData[0] != undefined ? (<div className="detailBox">
                    
                  <div className="detailTop">
                    <div className="pnNoBox flex borderRight">
                      <span className='label'>批號：</span>
                      {/* <span>{showNoData[0].pn_no}</span> */}
                      <ClipBoard type={'btn'} className='d-ib'>{showNoData[0].pn_no}</ClipBoard>
                    </div>
                    <div className="goodsDay flex borderRight">
                      <span className='label'>貨期：</span>
                      {/* <span>{stampToLocale(showNoData[0].shipment_date)}</span> */}
                      <ClipBoard type={'btn'} className='d-ib'>{stampToLocale(showNoData[0].shipment_date)}</ClipBoard>
                    </div>
                    <div className="num flex">
                      <span className='label'>數量：</span>
                      <Count end={showNoData[0].qty} duration={1.5}/>
                      <ClipBoard type={'btn'} className='d-ib'>{showNoData[0].style_no}</ClipBoard>
                    </div>
                  </div>
                  <div className="detailBottom">
                    <div className="styleNo flex borderRight">
                      <span className='label'>款號：</span>
                      {/* <span>{showNoData[0].style_no}</span> */}
                      <ClipBoard type={'btn'} className='d-ib'>{showNoData[0].style_no}</ClipBoard>
                    </div>
                    <div className="style flex">
                      <span className='label'>款式：</span>
                      {/* <span>{showNoData[0].cloth_style}</span> */}
                      <ClipBoard type={'btn'} className='d-ib'>{showNoData[0].cloth_style}</ClipBoard>
                    </div>
                  </div>
                </div>) : ''}

                {workArr.length ? <div className="m-b20 stepWrapper">
                  <div className='stepItems'>
                    {workArr.map((v, i) => {
                      //console.log(' v ： ', v, v.file_name1.replace('\\'), workArr[0].addwork, imgFilter(v.file_name1)  )
                      {/* console.log(' imgFilter(v.file_name1) ： ', imgFilter(v.file_name1),  ) */}
                      return (<div className="stepContent" key={v.work_name}>
                        <div className="stepItem">
                          {v.work_name}&nbsp;&nbsp;{v.work_days}(天)
                        </div>
                        {
                          workArr.length - 1 !== i ? <div className='indicatorWrapper' >
                          <Icon type="arrow-down" />
                          <span className='devDays' >偏移天数:{v.dev_days}</span>
                          </div> : ''
                        }
                      </div>)
                    })}
                  </div>
                  <div className="stepItems">
                    <div className="imgWrapper">
                      { workArr[0].file_name1 != undefined ? <img src={`${imgFilter(workArr[0].file_name1)}`} className="artImg" alt=""/> : <div className='noImg'>對不起沒有圖片 o(╥﹏╥)o</div> }
                    </div>
                  </div>
                  <div className="stepItems">
                    <div className="accTitle">附加工序</div>
                    <div className="border accProcess">
                      {workArr[0].addwork.split(',').map((v, i) => <div className="stepItem" key={i}>{v}</div>)}
                    </div>
                  </div>
                </div> : null}
              </div>
              
              <div className={`m-b20 ${ANIMATE.slideInUp}`}>
                <ArtLineTable 
                    saveData={this.saveData} 
                    editData={this.editData} 
                    editDataAction={this.editDataAction}  
                    showDialog={this.showDialog}
                    data={showNoData}
                ></ArtLineTable>
              </div>
            </Col>
            {/* <Col className={`m-b20 ${ANIMATE.slideInUp}`} xs={24} sm={24} md={24} lg={24} xl={24}>
            </Col> */}
            {/* <Col className={`artLineForm ${ANIMATE.slideInRight}`} xs={24} sm={12} md={10} lg={10} xl={10}>
              <Row gutter={16}>
                {showNoData.length ? showNoData.map((v, i) => (<Col xs={12} sm={12} md={12} lg={8} xl={8} key={i}>
                  <ProduceForm formItemLayout={formItemLayout} init={v} formLayout={'horizontal'} ref={forms => this.forms = forms} config={airlineForm}></ProduceForm>
                </Col>)) : ''}
              </Row>
            </Col> */}
          </Row>
        </Collapses>
        {/* </ClipBoards> */}

        {show ? <ProductDialog 
          handleOk={this.handleOk} 
          width={'45%'} show={show} 
          close={this.close} title={title}
          extra={
            <Button key='sub' onClick={this.autoPpc} type="primary" type="primary" icon="smile-o" className={`m-r10 sub`}>自動排單</Button>
          }
        >{this.editContent()}</ProductDialog> : null}
      </section>
    );
  }
}

// export default ArtLine
const state = state => state
const action = action => {
  return {
    copy: bindActionCreators(copy, action),
  }
}
export default connect(state, action)(ArtLine)
