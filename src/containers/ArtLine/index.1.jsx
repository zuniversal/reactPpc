import React from "react"
import PureRenderMixin from "react-addons-pure-render-mixin"
import './style.less'
import { getArtLine } from 'api/ArtLine'
import { airlineForm } from 'config'
import { stampToLocale, filterArr, mergeArr, filterArrOForm, } from 'util'

import Collapses from '@@@/Collapses'
import ArtLineStep from '@@@/ArtLineStep'
import ProduceForm from '@@@/ProduceForm'
import { Row, Col, Icon, } from 'antd'
const formItemLayout = {
  labelCol: { span: 14 },
  wrapperCol: { span: 10 },
};

class ArtLine extends React.Component {
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    this.state = {
      artLineData: [],
      pnNo: [],
      showNo: '',
      showNoData: [],
      workArr: [],
    }
  }
  getArtLine = () => {
    getArtLine({ company_id: "3" }).then(res => {
      console.log('getArtLine res ：', res.data);
      console.log('res.data ：', res.data);
      const { data } = res.data
      const pnNo = []
      data.forEach(v => pnNo.push(v.pn_no))
      const filterArrs = filterArrOForm(pnNo, 'pn_no')
      const handleData = mergeArr(data, filterArrs, 'pn_no')
      console.log('filterArr ：', filterArrs, handleData, filterArrOForm(pnNo, 'pn_no'));
      this.setState({
        // artLineData: handleData,
        pnNo: filterArrs,
        showNo: pnNo[0],
        showNoData: this.showDetial(handleData, pnNo[0]),
      })
    })
  }
  showDetial = (data, v) => {
    console.log('showDetial v ：', data, v);
    const detailData = data.filter(item => item.pn_no === v)[0].data
    const workArr = []
    detailData.forEach(v => workArr.push({ work_days: v.work_days, work_name: v.work_name }))
    this.setState({ 
      workArr, 
      // showNoData: this.showDetial(handleData, pnNo[0]), 
    })
    return detailData
  }
  componentDidMount() {
    this.getArtLine()
  }
  render() {
    console.log('ArtLine 组件this.state, this.props ：', this.state, this.props);
    const { artLineData, pnNo, showNo, showNoData, workArr, } = this.state
    console.log('showNoData[0] ：', showNoData[0]);
    return (
      <section className="artLine">
        <Collapses title={'ArtLine'}>
          <Row gutter={16}>
            <Col className="m-b20" xs={24} sm={12} md={4} lg={4} xl={4}>
              <div className="pnNoWrapper">
                {pnNo.length ? pnNo.map((v, i) => <div onClick={this.showDetial.bind(this, pnNo, v.pn_no)}key={v.pn_no} className="pnNoItem ">{v.pn_no}</div>) : ''}
              </div>
            </Col>
            <Col className="m-b20" xs={24} sm={12} md={10} lg={10} xl={10}>
              {showNoData[0] != undefined ? (<div className="detailBox">
                <div className="detailTop">
                  <div className="goodsDay">
                    <span className='label'>货期：</span>
                    <span>{stampToLocale(showNoData[0].shipment_date)}</span>
                  </div>
                  <div className="num">
                    <span className='label'>数量：</span>
                    <span>{showNoData[0].qty}</span>
                  </div>
                  <div className="styleNo">
                    <span className='label'>款号：</span>
                    <span>{showNoData[0].style_no}</span>
                  </div>
                </div>
                <div className="detailBottom">
                  <div className="style">
                    <span className='label'>款式：</span>
                    <span>{showNoData[0].cloth_style}</span>
                  </div>
                </div>
              </div>) : ''}

              {/* <ArtLineStep></ArtLineStep> */}

              <div className="stepWrapper">
                {workArr.length ? workArr.map((v, i) => {
                  console.log(' workArr v ：', v, )
                  return (<div className="stepContent" key={v.work_name}>
                    <div className="stepItem">
                      {v.work_name}&nbsp;&nbsp;{v.work_days}(天)
                    </div>
                    {workArr.length - 1 !== i ? <Icon type="arrow-down" /> : ''}
                  </div>)
                }) : ''}
              </div>
            </Col>
            <Col className="artLineForm" xs={24} sm={12} md={10} lg={10} xl={10}>
              <Row gutter={16}>
                {showNoData.length ? showNoData.map((v, i) => (<Col xs={12} sm={12} md={12} lg={8} xl={8} key={i}>
                  <ProduceForm formItemLayout={formItemLayout} init={v} formLayout={'horizontal'} ref={forms => this.forms = forms} config={airlineForm}></ProduceForm>
                </Col>)) : ''}
              </Row>
            </Col>
          </Row>
        </Collapses>
      </section>
    );
  }
}
export default ArtLine;