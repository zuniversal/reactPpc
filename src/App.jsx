import React, { Component } from 'react';
import PureRenderMixin from "react-addons-pure-render-mixin"
import './App.less';
import {hashHistory} from 'react-router'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import {dragPage} from 'config'
import {logout, collapse} from 'actions/login'
import * as scheduleAction from 'actions/scheduleNo.js'
import logo from 'static/img/logo.png'
import Indexs from '@@/Indexs'
import HeaderComponents from '@@@/HeaderComponents/index'
import SideBar from '@@@/SideBar'
import Loading from '@@@@/Loading'
// import ClipBoards from '@@@@/ClipBoards'
import { Layout,  } from 'antd';
const { Content } = Layout;


class App extends Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    const path = this.props.location.pathname
    this.state = {
      path,
      collapsed: false,
      word: <div className="logoTitle">PPC System</div>,
    }
  }
  toggle = () => {
    const {collapsed} = this.state
    const word = collapsed 
    ? <div className="logoTitle">PPC System</div> 
    : <div className="logoWrapper"><img className="logo" src={logo} style={{width: 50,height: 50}} alt="logo"/></div>
    this.setState({
      collapsed: !collapsed,
      word
    });
    this.props.collapse(!collapsed)
  }
  logout = () => {
    this.props.logout()
    hashHistory.push(`/login`)
  }
  
  componentWillUpdate(nextProps) {
    const path = nextProps.location.pathname
    this.setState({path})
  }

  render() {
    const {path, word, collapsed, } = this.state
    const {routes, children, userInfo, filterInfo, customerData, styleNoData, } = this.props
    const {name} = routes[routes.length - 1]
    // const isDragPage = dragPage.some(v => v === this.props.location.pathname)
    const isDragPage = dragPage.some(v => this.props.location.pathname.indexOf((v)) > -1)
    console.log('App组件：', isDragPage, userInfo, name, this.state, this.props, path, path === '/', );
    return (// 
      // <ClipBoards>
        <Layout className="ant-layout-has-sider">
          <SideBar userInfo={userInfo} path={path} name={name} word={word} collapsed={collapsed} theme='dark' ></SideBar>
          <Layout>
            <HeaderComponents collapsed={collapsed} scheduleAction={this.props.scheduleAction} customerData={customerData} styleNoData={styleNoData} filterInfo={filterInfo} dateChange={this.dateChange} isDragPage={isDragPage} toggle={this.toggle} logout={this.logout}></HeaderComponents>
            <Content className={`${isDragPage ? 'dragPage' : ''} contentsWrapper`} ref={con => this.con = con}>
              {path === '/' ? <Indexs></Indexs> : children}
              {/* {children} */}
            </Content>
          </Layout> 
          {/* <Loading loading={loading}></Loading>  */}
        </Layout>
        // </ClipBoards>
      // 
    );
  }
}

const state = state => state
const action = action => {
  return {
    logout: bindActionCreators(logout, action),
    collapse:  bindActionCreators(collapse, action),
    scheduleAction: bindActionCreators(scheduleAction, action),
  }
}
export default connect(state, action)(App);
// export default App;